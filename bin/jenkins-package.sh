#!/bin/sh
git log | head -4  > deploy.txt
git status >> deploy.txt
echo $GIT_COMMIT  > version.txt
mkdir target
tar -c -z --exclude target -f target/djaoe-$BUILD_ID.tar.gz .
