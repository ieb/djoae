#!env python
import sys
import django.utils.simplejson as json
from base64 import b64encode
from hashlib import sha1

def hash(v):
    return b64encode(sha1(":%s" % (str(v))).digest(),'-$').replace("=", "")


def dumpToSQL(path, depth, directory):
    parentHash = hash(path) 
    path = path+"/"  
    for (k,v) in directory.items():
        thisPath = "%s%s" % (path,k)
        thisPathHash = hash(thisPath)
        print "insert into content_contenttag ( hash, parenthash, name, fullpath, titlekey, depth ) values ( '%s', '%s','%s','%s','%s', %s ); " % (thisPathHash.replace('-','\-'), parentHash.replace('-','\-'), k, thisPath, v['title'],depth)
        if "children" in v:
            dumpToSQL("%s%s" % (path,k), depth+1 ,v['children'])

def main():
    directory = json.load(open(sys.argv[1],"r"))
    dumpToSQL("/directory/tags",1,directory) 
    
        
if __name__ == '__main__':
    main()
