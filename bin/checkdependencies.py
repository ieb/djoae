#!/usr/bin/python
import logging
import sys, os
import subprocess
import traceback
error = 0
logging.error("Checking Dependencies ")
THIS_LOCATION =  os.path.dirname(os.path.abspath(__file__))
workspace = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append("%s/app" % workspace)
os.environ['DJANGO_SETTINGS_MODULE'] = 'oae.settings'


try:
    from django.db import models 
    logging.error("DJango Found Ok ")
except:
    logging.error(traceback.format_exc())
    logging.error("Oh Dear, no DJango present you need to install with easy_install django or some other method, see the README")
    error = 1
    
try:
    from south.db import db
    logging.error("South Found Ok ")
except:
    logging.error(traceback.format_exc())
    logging.error("Please install South for data migration with easy_install South or some other method, see the README")
    error = 1

try:
    from topia.termextract import extract 
    logging.error("Topia Found Ok ")
except:
    logging.error("Please install South for data migration with easy_install topia.termextract or some other method, see the README")
    error = 1
    
try:
    from lxml import etree
    logging.error("lxml Found Ok ")
except:
    logging.error("Non fatal error, no lxml present, render optimisation will not be available. Please install South for data migration with easy_install lxml or some other method, see the README")

    
    
JOD_CONVERTER_JAR = os.path.abspath("%s/../app/oae/content/preview/deps/jodconverter-core-3.0-beta-4.jar" % THIS_LOCATION)
CHECK_COMMANDS = (
        (['convert','-version'],0),
        (['java','-version'],0),
        (['java', '-jar', JOD_CONVERTER_JAR,'-v'],255),
        (['pdfinfo','-v'],99),
        (['pdftotext','-v'],99),
        )
for c,ret in CHECK_COMMANDS:
    try:
        i = subprocess.call(c,stdout=None,stderr=None)
        if i == ret or i == 0:
            logging.error("Command %s Ok" % c)
        else:
            logging.error("Command %s Returned Non zero code %s " % (c,i))
            error = 1
    except:
        logging.error("Command %s failed " % c)
        error = 1

if error != 0:
    logging.error("Some missing dependencies, please correct and retry deploy build")
else:
    logging.error("Dependencies checked Ok.")
        
sys.exit(error)