#!/bin/sh
#
# Usage, setupnode servername [password]
# If password is not supplied the database wont be created
servername=$1
password=$2


apt-get install python postgresql libapache2-mod-uwsgi apache2-mpm-worker \
     build-essential python-dev python-setuptools python-lxml python-psycopg2 \
     libpq-dev
easy_install pip
cat << EOR > requirements.txt
Django==1.4
South==0.7.4
oauthlib==0.1.3
psycopg2==2.4.2
pyasn1==0.1.3
requests==0.12.1
rsa==3.0.1
topia.termextract==1.1.0

https://s3.amazonaws.com/DjOAEdeps/python/pyelasticsearch-0.0.5.tar.gz#egg=django-haystack
https://s3.amazonaws.com/DjOAEdeps/python/django-haystack-2.0.0-beta.tar.gz#egg=pyelasticsearch
EOR
pip install -r requirements.txt

a2enmod headers
a2enmod rewrite
a2enmod expires
 

cat << EOR > /etc/apache2/sites-available/$servername
<VirtualHost *:80>
  Servername $servername
  DocumentRoot /var/www/$servername/htdocs

  <Directory /var/www/$servername/htdocs>
    Options None
    AllowOverride None
    Order deny,allow
    allow from all
  </Directory>

  Include /var/www/$servername/apache/oae.conf


</Virtualhost>
EOR

a2dissite default
a2ensite $servername

if [ "a$password" != "a" ]
then
cat << EOR > createdb.sql
CREATE ROLE djoaerole
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE;

CREATE ROLE djoae LOGIN
  PASSWORD '$password'
  SUPERUSER INHERIT CREATEDB CREATEROLE;
GRANT djoaerole TO djoae;

CREATE DATABASE djoae
  WITH OWNER = djoaerole
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;

EOR
sudo -u postgres psql -f createdb.sql
rm createdb.sql
fi

service apache2 reload




