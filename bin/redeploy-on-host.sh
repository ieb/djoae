#!/bin/sh
set -e


PACKAGE_DIR=$HOME/package
WORKING_DIR=$HOME/working
DEPLOY_DIR=$HOME/working/deploy
VIRTUAL_HOST=loadmeup.caret.cam.ac.uk
VIRTUAL_HOST_LOCATION=/var/www/$VIRTUAL_HOST
APP_DATA_LOCATION=/var/www/$VIRTUAL_HOST-app-data

UI_TARBALL=$HOME/oae-ui-1.3.0.tar.gz
SERVER_TARBALL=$PACKAGE_DIR/deploy-staging-$VIRTUAL_HOST.tgz

if [ -d $WORKING_DIR ]
then
  sudo rm -rf $WORKING_DIR
fi
mkdir $WORKING_DIR
mkdir $DEPLOY_DIR
cd $DEPLOY_DIR
tar xzf $SERVER_TARBALL


# fix permissions and ownership to the current user for maintanence
userid=`id -u`
sudo chown -R $userid $DEPLOY_DIR 
find $DEPLOY_DIR -type d -exec chmod 775 {} \;
find $DEPLOY_DIR -type f -exec chmod 664 {} \;

# Sym link in the app data
if [ -d $APP_DATA_LOCATION ]
then
   mkdir -p $APP_DATA_LOCATION
fi 
if [ -d $DEPLOY_DIR/app-data ]
then
   rm -rf $DEPLOY_DIR/app-data
fi
ln -s $APP_DATA_LOCATION $DEPLOY_DIR/app-data


# prepare the UI
if [ -d $DEPLOY_DIR/static-data/ui ]
then
     rm -rf $DEPLOY_DIR/static-data/ui
fi
if [ -d $DEPLOY_DIR/static-data/tmp ]
then
     rm -rf $DEPLOY_DIR/static-data/tmp
fi
mkdir $DEPLOY_DIR/static-data/ui
mkdir $DEPLOY_DIR/static-data/tmp
cd $DEPLOY_DIR/static-data/ui
tar xzf $UI_TARBALL

# Check the code
cd $DEPLOY_DIR/app/oae
python manage.py validate

# All ready to deploy we have to stop just in case there are db cahnges
set +e
sudo /etc/init.d/apache2 stop
set -e
python manage.py syncdb --noinput
python manage.py migrate
python manage.py collectstatic --noinput -v 0
# fix permissions and ownership for production
sudo chown -R root:root $DEPLOY_DIR

# deploy to production
if [ -d /tmp/_www ]
then
    sudo rm -rf /tmp/_www
fi
mkdir /tmp/_www
sudo chown -R www-data:root /tmp/_www 

# snapshot old version of app
if [ -d $VIRTUAL_HOST_LOCATION.old ]
then
   sudo rm -rf $VIRTUAL_HOST_LOCATION.old
fi
sudo mv $VIRTUAL_HOST_LOCATION $VIRTUAL_HOST_LOCATION.old
sudo mv $DEPLOY_DIR $VIRTUAL_HOST_LOCATION
sudo /etc/init.d/apache2 start


sudo chown -R www-data:root $APP_DATA_LOCATION
sudo find $APP_DATA_LOCATION -type d -exec chmod 755 {} \;
sudo find $APP_DATA_LOCATION -type f -exec chmod 644 {} \;

echo Apache Restarted, all done.
sudo rm -rf $WORKING_DIR


