#!env python
import random

random.seed("0xdeadbeef")
def sel(listOfNames):
    n =  listOfNames[random.randint(0,len(listOfNames)-1)]
    return n[0].upper()+n[1:].lower()

def addUserId(firstName, lastName, map):
    while True:
        uid = "%s%s%s" % (firstName[0].lower(),lastName[0].lower(),random.randint(0,10000))
        if uid not in map:
            map[uid] = uid
            return uid

def main():
    firstnames = open('oaetest/testdata/firstnames.txt',"r").read().splitlines()
    lastnames = open('oaetest/testdata/lastnames.txt',"r").read().splitlines()
    print "Got %s firstname and %s lastnames \n" % (len(firstnames), len(lastnames))
    userids = {}
    a = open("accounts.txt","w")
    for i in range(1,100000): #@UnusedVariable
        fn = sel(firstnames)
        ln = sel(lastnames)
        uid = addUserId(fn,ln,userids)
        a.write("%s, %s, %s, %s@example.org\n" % (uid, fn, ln, uid))
    
        
if __name__ == '__main__':
    main()
