#!env python
'''
This is the deployment script for production instances.
It takes a settings file in the directory you run it in and generates two scripts.
part1 creates the deployment image, part2 deploys it to production.
These are normally run on the server where deployment is being performed to eliminate transfering 
the image over a slow link. The script will ruin part1 for you, and if thats all Ok prompt you to
ssh into the target host to run part 2.

args:
python bin/deploy.py ieb@djao.alexandria.edu

will:
   use settings ./djao.alexandria.edu.settings
   deploy to djao.alexandria.edu
   as user ieb
   
python bin/deploy.py ieb@djao.alexandria.edu oae

will: do the same but using the oae django app to perform manage operations (oae is the default)

python bin/deploy.py ieb@djao.alexandria.edu oae git@bitbucket.org:ieb/djoae.git

will: do the same but using the Git repo specified to pull the code from (oae is the default)


If the repo is private, and there is a ssh key at /.ssh/deploy-keys/deploy-key-djao.alexandria.edu
that key will be transfered to ieb@djao.alexandria.edu:.ssh/deploy-key and used to build the deploy image.
as soon as all git operations that need ssh access are complete the key will be deleted from the target host.
This happens almost immediately the part1 script is run to the key will only be present on the target server 
for a minute or less. You may be prompted to enter the passkey for the key.

'''
import os, sys
from django.utils import simplejson as json

from django.template.context import Context
from django.template.base import Template
import subprocess
THIS_LOCATION =  os.path.dirname(os.path.abspath(__file__))
workspace = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append("%s/app" % workspace)
os.environ['DJANGO_SETTINGS_MODULE'] = 'oae.settings'

def renderTemplate(settings, templateName):
    f = open("%s/%s.template" % (THIS_LOCATION, templateName))
    template = Template(f.read())
    f.close()
    d = {}
    d.update(settings)
    d.update({ 'script_name' : "%s-%s" % (settings['deploy_host'], templateName)})
    settingsContext = Context(d)
    if not os.path.exists("temp"):
        os.mkdir("temp")
    out = open("temp/%s-%s" % (settings['deploy_host'], templateName),"wb")
    out.write(template.render(settingsContext))
    out.close()
    return out.name

def main(deploy_target, main_app, git_url):
    deploy_user = deploy_target.split("@",1)[0]
    deploy_host = deploy_target.split("@",1)[1]
    settings = {
        'deploy_host' : deploy_host,
        'deploy_user' : deploy_user,
        'deploy_target' : deploy_target,
        'main_app': main_app,
        'git_url' : git_url
        }
    # custom settings may override any or all of the above
    if os.path.exists("%s.settings" % deploy_host):
        f = open("%s.settings" % deploy_host)
        settings.update(json.load(f))
        settings.update({'customsettings' : True})
        f.close()
    else:
        settings.update({'customsettings' : False})

    part1 = renderTemplate(settings, "deploy-part1.sh")    
    part2 = renderTemplate(settings, "deploy-part2.sh")    
    devnull = open("/dev/null")
    if subprocess.call(["scp", part1,  "%s:%s" % ( settings['deploy_target'], os.path.basename(part1))],stderr=devnull) != 0:
        print >> sys.stderr, "Abort: Unable to upload %s script to %s " % (part1, settings['deploy_target'])
        sys.exit(1)
        
    if subprocess.call(["scp", part2,  "%s:%s" % ( settings['deploy_target'], os.path.basename(part2))],stderr=devnull) != 0:
        print >> sys.stderr, "Abort: Unable to upload %s script to %s " % (part2, settings['deploy_target'])
        sys.exit(1)
    
    home = os.environ.get("HOME")
    if os.path.exists("%s/.ssh/deploy-keys/deploy-key-%s" % (home, settings['deploy_host'])):
        if subprocess.call(["scp", "%s/.ssh/deploy-keys/deploy-key-%s" % (home, settings['deploy_host']),  "%s:.ssh/deploy-key" % settings['deploy_target']],stderr=devnull) != 0:
            print >> sys.stderr, "Abort: Unable to upload deploy key  %s/.ssh/deploy-keys/deploy-key-%s to %s" % (home, settings['deploy_host'], settings['deploy_target'])
            sys.exit(1)
            
    if subprocess.call(["ssh", settings['deploy_target'], "sh", os.path.basename(part1)]) != 0:
            print >> sys.stderr, "Abort: Failed to execute %s:%s " % (settings['deploy_target'], part1)
            sys.exit(1)
    print >> sys.stderr, "Deployment Image ready for moving to production at %s, please execute the following commands which will need sudo access "  % (settings['deploy_target'])
    print >> sys.stderr, "ssh %s" % (settings['deploy_target'])
    print >> sys.stderr, "sh %s" % (os.path.basename(part2))





if __name__ == '__main__':
    if len(sys.argv) == 4:
        deploy_target = sys.argv[1]
        main_app = sys.argv[2]
        git_url = sys.argv[3]
    elif len(sys.argv) == 3:    
        deploy_target = sys.argv[1]
        main_app = sys.argv[2]
        git_url = "git@bitbucket.org:ieb/djoae.git"
    elif len(sys.argv) == 2:
        deploy_target = sys.argv[1]
        main_app = "oae"
        git_url = "git@bitbucket.org:ieb/djoae.git"
    else:
        print >> sys.stderr, "Usage: deploy.py userid@deployhost "
        sys.exit(1)
    main(deploy_target, main_app, git_url)