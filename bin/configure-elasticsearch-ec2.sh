#!/bin/sh
#
# install-elasticsearch.sh clustername
#
$clustername = $1
curl --silent http://source.caret.cam.ac.uk/binary-downloads/elasticsearch-caret-$clustername.tar.gz > /tmp/elasticsearch.tar.gz
tar -x -z --group=root --owner=root -C / -f /tmp/elasticsearch.tar.gz
rm /tmp/elasticsearch.tar.gz
groupadd elasticsearch 
useradd -g elasticsearch -h /usr/share/elasticsearch elasticsearch 
chmod 755 /etc/elasicsearch
chmod 640 /etc/elasticsearch/elasticsearch.yml
chmod 640 /etc/elasticsearch/logging.yml
chmod 640 /etc/default/elasticsearch
chmod 750 /var/log/elasticsearch
chmod 750 /var/lib/elasticsearch
chmod 754 /etc/init.d/elasticsearch
chown root:elasticsearch /etc/elasticsearch/elasticsearch.yml
chown root:elasticsearch /etc/elasticsearch/logging.yml
chown root:elasticsearch /etc/default/elasticsearch
chmod elasticsearch:elasticsearch /var/log/elasticsearch
chmod elasticsearch:elasticsearch /var/lib/elasticsearch
chmod root:root /etc/init.d/elasticsearch
/etc/inid.elasicsearch start
