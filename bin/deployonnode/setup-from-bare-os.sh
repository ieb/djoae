#!/bin/sh
set -e

ehco Not a tested script, cut and paste from the script and  react as you go.
echo Where there is a vi, look for the file in this dir.
echo Just so you dont try and run it... there is an exit after this echo ;)
exit

and some garbage to trigger the -e

# Download things
wget https://s3.amazonaws.com/DjOAEdeps/elasticsearch-caret-0.19.4.tar.gz
wget https://s3.amazonaws.com/DjOAEdeps/oae-ui-1.3.0.tar.gz
wget https://s3.amazonaws.com/DjOAEdeps/preview-deps.tar.gz


# remove perl pip if present
sudo apt-get remove pip
# install dependencies
sudo apt-get install apache2 postgresql default-jdk python-pip python-lxml python-psycopg2 libapache2-mod-wsgi imagemagick xpdf memcached python-memcache


# create database

sudo -u postgres createdb djoa
sudo -u postgres createuser -p djoaeuser

# change DB memory and kernel settings if required
sudo vi /etc/postgresql/8.4/main/postgresql.conf
sudo sysctl -w kernel.shmmax=2147483648
sudo sysctl -w kernel.shmall=524288
sudo /etc/init.d/postgresql restart


#Install elastic search from custom tarball
sudo tar -x -z --group=root --owner=root -C / -f elasticsearch-caret-0.19.4.tar.gz

sudo groupadd elasticsearch
sudo useradd -g elasticsearch -d /usr/share/elasticsearch elasticsearch

sudo mkdir /etc/elasticsearch
sudo chmod 0755 /etc/elasticsearch
sudo mkdir -p /var/log/elasticsearch
sudo chmod 0750 /var/log/elasticsearch
sudo chown elasticsearch:elasticsearch /var/log/elasticsearch
sudo mkdir -p /var/log/elasticsearch
sudo chmod 0750 /var/log/elasticsearch
sudo chown elasticsearch:elasticsearch /var/log/elasticsearch

# Customise config
sudo vi /etc/elasticsearch/elasticsearch.yml
sudo chgrp elasticsearch /etc/elasticsearch/elasticsearch.yml
sudo chmod 640 /etc/elasticsearch/elasticsearch.yml

# Customise config
sudo vi /etc/elasticsearch/logging.yml
sudo chgrp elasticsearch /etc/elasticsearch/logging.yml
sudo chmod 640 /etc/elasticsearch/logging.yml

# Customise config
sudo vi /etc/default/elasticsearch
sudo chgrp elasticsearch /etc/default/elasticsearch
csudo hmod 640 /etc/default/elasticsearch

sudo /etc/init.d/elasticsearch start


#Install pip packages

cat << EOF > requirements.txt
Django==1.4
South==0.7.4
oauthlib==0.1.3
pyasn1==0.1.3
requests==0.12.1
rsa==3.0.1
topia.termextract==1.1.0
https://s3.amazonaws.com/DjOAEdeps/python/django-haystack-2.0.0-beta.tar.gz
https://s3.amazonaws.com/DjOAEdeps/python/pyelasticsearch-0.0.5.tar.gz
https://s3.amazonaws.com/DjOAEdeps/python/johnny-cache-1.4-pre1.tar.gz
EOF
sudo pip install -r requirements.txt 

# Configure Apache 
sudo vi /etc/apache2/sites-available/loadup.caret.cam.ac.uk
sudo a2enmod headers
sudo a2enmod expires
a2dissite default
a2ensite loadup.caret.cam.ac.uk 

# some standard mimetype files dont have json. Needed by the python mimetype lib (not Apache)
echo "application/json     json" >> /etc/mime.types


# Adjust logging
# LogFormat "%h %D %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined_time
# Chang workers to match cores
sudo vi /etc/apache2/apache2.conf

mkdir settings
vi settings/loadup.caret.cam.ac.uk-local_settings.py

# From repo
vi prepare-deploy-package.sh
# From repo
vi deploy-package.sh

# prepare ssh
mkdir ~/.ssh
chmod 700 ~/.ssh
cat << EOF > ~/.ssh/config
Host=bitbucket.org
        user=git
        port=22
        Hostname=bitbucket.org
        IdentityFile=~/.ssh/deploy-key

EOF
chmod 600 ~/.ssh/config
sh prepare-deploy-package.sh
sh deploy-package.sh

