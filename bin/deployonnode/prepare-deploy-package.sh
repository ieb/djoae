#!/bin/sh
set -e

VIRTUAL_HOST=loadmeup.caret.cam.ac.uk
WORKING_DIR=$HOME/deploy-source-$VIRTUAL_HOST
PREVIEW_DEPS_TARBALL=$HOME/preview-deps.tgz
echo "Replace this with an SSH key that can access the repo, and then save. " > $HOME/.ssh/deploy-key
echo "The key will be deleted as soon as the git update has completed " >> $HOME/.ssh/deploy-key
vi $HOME/.ssh/deploy-key
chmod 600 $HOME/.ssh/deploy-key
# prepare deployment --------------------------------------------------------
if [ ! -d $WORKING_DIR ]
then
   git clone git@bitbucket.org:ieb/djoae.git $WORKING_DIR
fi
cd $WORKING_DIR
git checkout .
git checkout deployment
git checkout .
git pull origin deployment
rm $HOME/.ssh/deploy-key

# Unpack the JOD converter Jars first
cd $WORKING_DIR/app/oae/content/preview/deps
tar xvzf $PREVIEW_DEPS_TARBALL
cd $WORKING_DIR

#check that the dependencies are all present and correct. If this fails we should correct the error.
python bin/checkdependencies.py
git log | head -4  > deploy.txt

# the following command will fail if there is anything modified and the script will stop.
echo Checking for no local changes, script will exit if there are local changes.
git status >> deploy.txt
git show --pretty=oneline HEAD | cut -c1-41 | head -1  > version.txt

echo Good, no local changes, continuing to deploy.

PACKAGE_DIR=$HOME/package
if [ -d $PACKAGE_DIR ]
then
   rm -rf $PACKAGE_DIR
fi
mkdir $PACKAGE_DIR
cd $WORKING_DIR
tar czf $PACKAGE_DIR/deploy-staging-$VIRTUAL_HOST.tgz .
echo do: sh deploy-package.sh

