DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'djoae',                      # Or path to database file if using sqlite3.
            'USER': 'XXXXXXX',                      # Not used with sqlite3.
            'PASSWORD': 'XXXXXXXXX',                  # Not used with sqlite3.
            'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
            'OPTIONS': {
                        'autocommit': True,   # If you set this to False, a transaction will be created every time even if the app doesnt use it. Dont set it to False, transactions are managed differently.
            }
        },
    }

CACHES = {
      'default' : {
        'BACKEND': 'johnny.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'JOHNNY_CACHE' : True,
    },
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

TIME_ZONE = "Europe/London"

LANGUAGE_CODE = 'en-GB'
ENABLE_RESPONSE_CACHE = False
BASIC_WWW_AUTHENTICATION = False

DISABLE_QUERYSET_CACHE = False

DEBUG = False


