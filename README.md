# DjOAE

DjOAE is a backend for the Sakai Open Academic Environment (OAE) written in DJango/Python targeted to
run on a PostgreSQL. Unlike Nakamura the OAE Java backend, it uses a completely relational database
to store all data.

# License/Contributing

This codebase is currently closed source. Please read the NOTICE file carefully before using 
or contributing. By using or contributing you are effectively agreeing to the terms in the NOTICE file.

# Why?

In late 2010 I developed SparseMap, the content system behind Nakamura with the intention that
it would support a content store with relational queries where appropriate. After leaving
the Sakai OAE Managed Project in June 2011, the managed project took a different route and eliminated
all relational queries relying on Solr for all queries. I was already concerned that Sparse Maps
reliance on Caching to achieve any performance would be its Achilles heal and that coupled with 
the concern that Solr would never work real time enough for OAE prompted me to write this code base.

This code base is written in DJango/Python because there is substantial evidence that Python is
more than capable of serving the sort of concurrent user base that Sakai OAE was originally planned for.
DJango because it is an extremely mature framework, with all the features required to build this
application, in the smallest code base, with the greatest ease and the lowest barrier to entry
for new programmers. To date, that has proved true as has DJangos reason for existence:

       "For perfectionists with deadlines" 

# Source folders

This code base is structured for deployment and may not follow an absolutely standard DJango layout.

        app - contains the DJango DjOAE application code, test classes and client classes.
        apache - contains Apache HTTPD configuration files
        app-data - is where the application writes user data
        bin - utility scripts executed on the command line only
        htdocs - the wsgi script loaded by Apache HTTPD
        static-data - additional static data including a checkout of the OAE UI code
        CONTRIBUTING - A note on contributing
        LICENSE - The license file
        NOTICE - Information on the LICENSE, please read.
        README.md - This file.
       
# To develop

Ensure that you have Python 2.6 installed. If you are on a OSX, then the default Python is Ok.
Ensure you have PostgreSQL installed and working.
Ensure you have the necessary PostgreSQL Python drivers installed.

Install DJango as per the instructions at https://docs.djangoproject.com/en/dev/topics/install/?from=olddocs The recommended way is 

        sudo pip install Django
        
Having done that, create a database

The default settings for the PostgreSQL database are
        
        Database Name: djoae
        Database User: djoae
        Database Password: djoae
        Host: localhost
        Port 5432
        
If you need to change this you will need to edit app/oae/settings.py
        
Create the database schema

        cd app/oae
        python manage.py syncdb
        
Answer NO to creating the super user.

        python manage.py migrate
        python manage.py createsuperuser
        


get the UI code 

        cd static-data
        git clone git://github.com/sakaiproject/3akai-ux.git ui
        cd ui
        git checkout 1.2.0
        
Start the Server

        cd app/oae
        python manage.py runserver
        open http://localhost:8000


# To deploy


Ensure that you have Python 2.6 installed. If you are on a OSX, then the default Python is Ok.
Ensure you have PostgreSQL installed and working.
Ensure you have the necessary PostgreSQL Python drivers installed.
Ensure a recent Apache HTTPD server setup with mod_wsgi available.

Install DJango as per the instructions at https://docs.djangoproject.com/en/dev/topics/install/?from=olddocs The recommended way is 

        sudo pip install Django
        
Also install any DJango drivers for you chosen database.
        
Having done that, create a database

The default settings for the PostgreSQL database are
        
        Database Name: djoae
        Database User: djoae
        Database Password: djoae
        Host: localhost
        Port 5432
        
Change these settings to ensure your DB is a bit more secure.
        
Create the database schema

        cd app/oae
        python manage.py syncdb
        
Answer a few questions about the default super user.

get the UI code 

        cd static-data
        git clone git://github.com/sakaiproject/3akai-ux.git ui
        cd ui
        git checkout 1.2.0
        
Map the DJango WSGI script into apache in a suitable apache config file.

There is a configuration file maintained at apache/httpd.conf with the assumption that /var/www/djoae.localhost points to the source code.

Hence if you add

        Include /var/www/djoae.localhost/apache/httpd.conf

to you apache HTTPD conf that should configure mod_wsgi and the static files.

Rebuild the static files provided by other applications (eg the admin interface)

        cd app/djoae
        python manage.py collectstatic
        
This will collect all the satic files and place them in static-data/staticroot at the moment we only use static-data/staticroot/admin


For more details on how to deploy DJango under mod_wsgi see the instructions at https://docs.djangoproject.com/en/dev/howto/deployment/wsgi/modwsgi/

# File System Permissions

The app will need to read everything under the deploy directory, it will need to write *.pyc files as it compiles Python under app, and it will need to write application data under app-data. It will also need to write to /tmp

# Additional dependencies

## Preview Processor
To enable the Preview Processor you need to install the following. Without these it will not work.
OS Level dependencies

    Package: ImageMagic 
    Command: convert  
    License: http://www.imagemagick.org/script/license.php  (non GPL)
    Install Linux: apt-get install ImageMagic


    Package: Xpdf 
    Command: pdfinfo, pdftotext  
    License: GPLv2
    Install Linux: apt-get install xpdf

    Package: JOD Converter
    Command: java utility
    Download http://code.google.com/p/jodconverter/
    and add to oae/content/preview/deps
    
    Tika app (optional, used for indexing the text in files not handled by JODConverter )
    Command: java utility
    Download http://tika.apache.org/download.html  
    add tika-app-1.1.jar to oae/content/preview/deps

Python 

    Package: Termextract 
    Location: http://pypi.python.org/pypi/topia.termextract/
    Install: easy_install topia.termextract
    
    
    Package: Python memcached Client   If using memcached for caching (recommended)
    Location: PyPi
    Install: pip install python-memcached
    
    Package: Python Gearman Client  If offloading background processing (recommended)
    Location: pypi
    Install: pip install gearman
    
    

#Data Migration

    Package: South
    Location: http://south.aeracode.org/
    Install: easy_install South
    
    
Data and schema migration is performed using South. If you already have a database are are not 
creating it from scratch then you need to perform the following steps to fake the first migration.

    cd app/oae
    python manage.py migrate permissions 0001 --fake
    python manage.py migrate tag 0001 --fake
    python manage.py migrate user 0001 --fake
    python manage.py migrate activity 0001 --fake
    python manage.py migrate contact 0001 --fake
    python manage.py migrate content 0001 --fake
    python manage.py migrate message 0001 --fake
    
If you have just created a database with syncdb, then you need to migrate the database.

    cd app/oae
    # you just did this step
    python manage.py syncdb 
    # Then migrate
    python manage.py migrate  
 
Once you have run the above commands, every time you update the code, simply run

    python manage.py migrate
    
## Developers workflow.

Whenever you make a change to the schema you must create a migration including a migration of data if required.
If its a simple migration of data then all you need to do is 

    python manage.py schemamigration <appname> --auto name_of_the_change
    
That will create a new migration.

If you recieve a migration simply do 

    python manage.py migrate
    
    
    
Search

ElasticSearch server as per instructions at elasticsearch.org

Haystack
    git://github.com/toastdriven/django-haystack.git using master
    easy_install haystack or python setup.py

    
easy_install requests
    requests-0.12.1-py2.6.egg
    chardet-1.0.1-py2.6.egg
    oauthlib-0.1.3-py2.6.egg
    certifi-0.0.8-py2.6.egg
    rsa-3.0.1-py2.6.egg
    pyasn1-0.1.3-py2.6.egg
    
pyelasticsearch
    git://github.com/toastdriven/pyelasticsearch.git (I took the top of master)
    and after testing did sudo python setup.py install

  
To view elastic search 
    git clone git://github.com/Aconex/elasticsearch-head.git
    cd elasticsearch-head
    open index.html
   

       