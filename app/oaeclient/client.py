
import httplib
import urllib
import django.utils.simplejson as json



class Client(object):
    
    def __init__(self, host, port=80, debug=0):
        self.connection = httplib.HTTPConnection(host, port)
        self.connection.set_debuglevel(debug)
        self.postheaders = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain"}
        self.headers = {"Accept": "text/plain"}
        self.cookies = []
        
    def getheaders(self, base):
        d = dict(base)
        if len(self.cookies) > 0:
            d["cookie"] = "; ".join(self.cookies)
        return d
            
        
    def login(self, username='ieb', password='ieb'):
        self.connection.request("POST", "/system/sling/formlogin", 
                        urllib.urlencode({ 'sakaiauth:un' : username, 'sakaiauth:pw' : password }), 
                        self.getheaders(self.postheaders))
        return self.check(self.checkresponse(),{302:1})
    
    def logout(self):
        self.connection.request("GET", "/logout", None, self.getheaders(self.headers))
        self.check(self.connection.getresponse(), {302:1})
        self.cookies = []
        
    
            
    def checkresponse(self):
        response = self.connection.getresponse()
        ch = response.getheader("Set-Cookie")
        if ch is not None:
            self.cookies.append(ch.split(";")[0])
        return response
    
    def check(self, response, status={200:1}):
        if response.status not in status:
            raise Exception("Failed to get response, %s cause %s %s" % (response.status, response.reason, response.read()))
        return response
    
    def tojson(self, response):
        body = response.read()
        return json.loads(body)
    
    def post(self, url, values):
        self.connection.request("POST", url, 
                        urllib.urlencode(values), 
                        self.getheaders(self.postheaders))
        return self.checkresponse()
    
    def get(self, url):
        self.connection.request("GET", url, 
                        None, 
                        self.getheaders(self.headers))
        return self.checkresponse()

class UserClient(object):
    
    def __init__(self, client):
        self.client = client
        self.client.userClient = self
    
    def createUser(self, username, firstname, lastname, email, password):
        return self.client.post("/system/userManager/user.create.html", {
                    ':name' : username,
                    'firstName' : firstname,
                    'lastName' : lastname,
                    'email' : email,
                    ':sakai:profile-import' : '{"basic":{"elements":{"firstName":{"value":"%s"},"lastName":{"value":"%s"},"email":{"value":"%s"}},"access":"everybody"},"email":"%s"}' % (firstname, lastname, email, email),
                    'pwd' : password,
                    'pwdConfirm' : password
                })
        
    def exists(self,username):
        response = self.client.get("/system/userManager/user.exists.json?userid=%s" % username)
        return response.status == 204
         
    def checkMe(self, meJson, username="anon"):
        if meJson is None or "userid" not in meJson:
            raise Exception("No UserID in me response")
        if meJson['username'] != username:
            raise Exception("No UserID did not match in response, was: %s " % meJson['username'])
        

    def me(self,username=None):
        if username is None:
            meJson = self.client.tojson(self.client.check(self.client.get("/system/me")))
            self.client.userid = meJson['userid']
            return meJson
        else:
            return self.client.tojson(self.client.check(self.client.get("/system/me?u=%s" % username)))
            
    
    
    
class MessageClient(object):
    
    def __init__(self, client):
        self.client = client
        self.client.messageClient = self

    def sendMessage(self,users, subject, body):
        '''
        sakai:type the type of message, optional defaults to smtp
        sakai:sendstate (pending) optional defaults to pending
        sakaimessagebox (outbox) optional defautls to outbox
        sakai:to multi list of usernames required
        sakai:from single list of uiserid its from (ignored by delivery)
        sakai:subject subject required
        sakai:body body required
        sakai:category "message" optional, defaults to message

        @param users:
        @param subject:
        @param body:
        '''
        if self.client.userid is None:
            self.client.userClient.me()
        
        return self.client.check(self.client.post("/~%s/message.create.html" % self.client.userid, {
                    'sakai:to' : ",".join(users),
                    'sakai:from' : self.client.userid,
                    'sakai:subject' : subject,
                    'sakai:body' : body
                }), {201:1})
        
            

