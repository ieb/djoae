from oaeclient.client import Client, UserClient, MessageClient
import time
from unittest import TestCase

class TestLoginLogut(TestCase):

    def test_loginlogut(self):
        client = Client("localhost", 8000)
        userClient = UserClient(client)
        
        client.login()
        userClient.checkMe(userClient.me(),"ieb")
        i = time.time()
        base = "%s" % i
        for j in range(0,10): 
            response = userClient.createUser("ian%s-%s" % (base,j), "Ian%s-%s" % (base,j), "Boston%s-%s" % (base,j), "ieb%s@tfd.co.uk" % j, "password")
            if response.status == 201:
                print "Created user"
            else:
                raise Exception("Didnt create user, %s %s" % (response.status, response.reason))    
        
        client.logout()
        userClient.checkMe(userClient.me())
        sendingUsername = "ian%s-%s" % (base,0)
        client.login(username=sendingUsername, password="password")
        userClient.checkMe(userClient.me(),sendingUsername)
        messageClient = MessageClient(client)
        
        # send messages
        for i in range(0,10):
            c = i%7
            users = []
            for j in range(c,min(c+3,10)):
                users.append("ian%s-%s" % (base,j))
            messageClient.sendMessage(users, "Message To %s " % " ".join(users), "Body of message to %s Read your emails please " % "\nHi,  ".join(users))
    
    

