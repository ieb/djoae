from oaeclient.client import Client, UserClient
import time
from unittest import TestCase

class TestLoginLogut(TestCase):

    def test_loginlogut(self):
        client = Client("localhost", 8000)
        userClient = UserClient(client)
        client.login()
        userClient.checkMe(userClient.me(),"ieb")
        i = time.time()
        response = userClient.createUser("ian%s" % i, "Ian%s" % i, "Boston%s" % i, "ieb@tfd.co.uk", "password")
        if response.status == 201:
            print "Created user"
        else:
            raise Exception("Didnt create user, %s %s" % (response.status, response.reason))    
        client.logout()
        userClient.checkMe(userClient.me())
    
    

