'''
Created on Apr 1, 2012

@author: ieb
'''
from oaeclient.client import Client, UserClient
import random
import logging

random.seed("0xdeadbeef")

class Initialise(object):
    def __init__(self):
        self.client =  Client("localhost", 8000)
        self.userClient = UserClient(self.client)
        self.client.login()
        self.userClient.checkMe(self.userClient.me(),"ieb")



    def _addUserId(self,firstName, lastName, map):
        while True:
            uid = "%s%s%s" % (firstName[0].lower(),lastName[0].lower(),random.randint(0,10000))
            if uid not in map:
                map[uid] = uid
                return uid


    def createDefaultUsers(self,nextras):
        if not self.userClient.exists("ian"):
            response = self.userClient.createUser("ian", "Ian", "Boston", "ieb@example.org", "ianboston")
        if not self.userClient.exists("susie"):
            response = self.userClient.createUser("susie", "Susie", "Boston", "susie@example.org", "susie")
        firstnames = open('testdata/firstnames.txt',"r").read().splitlines()
        lastnames = open('testdata/lastnames.txt',"r").read().splitlines()
        userids = {}
        for i in range(1,nextras): #@UnusedVariable
            fn = random.choice(firstnames)
            ln = random.choice(lastnames)
            uid = self._addUserId(fn,ln,userids)
            if not self.userClient.exists(uid):
                response = self.userClient.createUser(uid, fn, ln, "%s@example.org" % uid , "test")
            if i % 100 == 0:
                logging.error("Created user %s " % i)



    

if __name__ == '__main__':
    init = Initialise()
    init.createDefaultUsers(1000)
