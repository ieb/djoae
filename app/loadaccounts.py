from oaeclient.client import Client, UserClient
import sys

def main():
    file = sys.argv[1]
    start = int(sys.argv[2])
    end = int(sys.argv[3])
    print "Loading from %s %s to %s" % (file, start, end)
    client = Client("localhost", 8000)
    userClient = UserClient(client)
    client.login()
    userClient.checkMe(userClient.me(),"ieb")
    response = userClient.createUser("ian", "Ian", "Boston", "ieb@tfd.co.uk", "password")
    if response.status == 201:
        print "Created user"
    else:
        print "Didnt create user, %s %s" % (response.status, response.reason)
        
    f = open(file, "rb")
    names = f.read().splitlines()
    f.close()
    for i in range(start,end):
        parts = names[i].split(',')
        parts[0] = parts[0].strip()
        parts[1] = parts[1].strip()
        parts[2] = parts[2].strip()
        parts[3] = parts[3].strip()
        response = userClient.createUser(parts[0], parts[1], parts[2], parts[3], "password")
        if response.status != 201:
            print "Didnt create user, %s %s" % (response.status, response.reason)
        if i%100 == 0:
            print '%s' % i

    
    client.logout()
    userClient.checkMe(userClient.me())
    

if __name__ == '__main__':
    main()
