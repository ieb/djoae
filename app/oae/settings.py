# Django settings for oae project.
import os
import sys
import logging
import traceback


ROOT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
)

MANAGERS = ADMINS

unitTest = False
if 'test' in sys.argv:
    unitTest = True
try:
    unitTest = os.environ['DJANGO_UNIT_TEST'] == 'true'
except KeyError:
    pass

if unitTest:
    # only test on sqllite in memory
    DATABASES = {    
        'default': {
            'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': "%s/app-data/data" % ROOT_PATH,                      # Or path to database file if using sqlite3.
            'USER': '',                      # Not used with sqlite3.
            'PASSWORD': '',                  # Not used with sqlite3.
            'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
        },
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'djoae',                      # Or path to database file if using sqlite3.
            'USER': 'djoae',                      # Not used with sqlite3.
            'PASSWORD': 'djoae',                  # Not used with sqlite3.
            'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
            'OPTIONS': {
                        'autocommit': True,   # If you set this to False, a transaction will be created every time even if the app doesnt use it. Dont set it to False, transactions are managed differently.
            }
        },
    }
    
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'oae',
    },
}

CACHES = {}


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ROOT_PATH+'/app-data/uploads/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ROOT_PATH+'/static-data/staticroot'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'


# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    ROOT_PATH+"/static-data/static",
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'r&&ly3l5$*a!=q-&d4smr4&-u&*)5yfmg+zvij$!bu-rsptyx@'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'oae.user.middleware.BasicAuthenticationMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
 # Use xact and not   'django.middleware.transaction.TransactionMiddleware',  
 # Probably dont need this   'django.contrib.messages.middleware.MessageMiddleware',
 # Will need this at come point 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'oae.user.middleware.SessionExtend',
    'oae.common.profile.SQLProfileMiddleware',
)

ROOT_URLCONF = 'oae.urls'

TEMPLATE_DIRS = (
       ROOT_PATH + '/app/templates',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    #'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    'haystack',
    'oae.system',
    'oae.permission',
    'oae.user',
    'oae.message',
    'oae.activity',
    'oae.content',
    'oae.contact',
    'oae.tag',
    'south', # For schema migration, easy_install South to use.

)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'oae.content.backends.ContentMetaAuthorizationBackend',
    'oae.user.backends.GroupAuthorizationBackend',
#    'oae.permission.backends.ObjectPermissionAuthorizationBackend',
)
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


# ReCaptcha Settings, adjust for your local installation if required.
RECAPTCHA_PUBLIC_KEY = "6Lef4bsSAAAAAJOwQE-qwkAOzGG3DizFP7GYYng-"
RECAPTCHA_PRIVATE_KEY = "6Lef4bsSAAAAAId09ufqqs89SwdWpa9t7htW1aRc"
RECAPTCHA_ENDPOINT = "org.sakaiproject.nakamura.captcha.endpoint"
RECAPTCHA_SERVER = "www.google.com"

DOCUMENT_ROOT = os.path.abspath("%s/static-data/ui" % ROOT_PATH)
TEMP_ROOT = "/tmp/_www"

CONTENT_POOL_BASE = os.path.abspath("%s/app-data/contentpool" % ROOT_PATH)

    
#
#
# If this is set, then all static content is emitted with expire headers that stop the browser asking for the content
# again until some time in the future. For development you may want to disable although, frankly, there are other
# ways to force browsers to reload static content (add an ?x=12123 to the url, clear the browser cache(yes even chrome))
#
STATIC_EXPIRE_SECONDS = 3600
#
# HMAC Tokens are used to transfer identity between hosts. They are used in redirects and should never
# be available to the user to bookmark or reload from. This controls how long a token can remain valid.
# 30s is long enough, but there is no harm in upto an hour. The longer it is, the longer someone will have
# to reuse the token to acccess that one piece of content.
#
HMAC_TOKEN_TTL = 3600 # Hmac tokens expire after 1h
#
# This allows requests for user content to be redirected to a content host so that
# the user content can't interact with the application. 
# Typically there are 2 lines for a tenant.
# The line contains  applicationhost, url pattern, secret
# application host matches the HTTP_HOST header sent by the browser
# If url pattern is not None, a redirect is performed encoding the userid into a hmac with the secret
# If the url patter is None, the hmac on the redirect is verified and the content is served with the identity in the Hmac token.
# The mechanism does not support chaining of redirects.
# For large deployments, this setting could be stored in a table.
#CONTENT_HOST = (
#        ('applicationhost', 'http://contenthost%s&%s', 'secret_to_transfer_userid' ),
#        ('contenthost', None, 'secret_to_transfer_userid' ),
#    )
CONTENT_HOST = None

#
# Make sessions expire when a user closes the browser window.
#
SESSION_EXPIRE_AT_BROWSER_CLOSE=True
#
# Protect the session cookie from snooping by client javascript.
#
SESSION_COOKIE_HTTPONLY=True
#
# Make the session cookie expire in 20 minutes (set in seconds)
#
SESSION_COOKIE_AGE=1200
#
# This set of regexes defines URLs that wont extend the session. These ensure the UI will auto logout when 
# the session expires.
#
SESSION_EXTEND_IGNORE_URLS = (
            r'^/var/search/activity/all.json',
            r'/p/.*small.jpg',
            r'^/system/me',
            r'^/dev/',
            r'^/devwidgets/',
        )

#
# If you have any concerns about the privacy of username this should be set to false. 
# Setting it to True allows user information to be retrieved using the username although
# the username is never sent back in any response. This is unlike Nakamura which sends
# the username out all the time.
#
ALLOW_QUERY_BY_USERNAME = True

#
# list of property names that should never be sent out over http
#
FILTER_USER_PROPERTIES = ( "something-really-private", )

#
# This is the default password hasher setup with DJango 1.4, PBKDF2PasswordHasher can be slow the second 
# definition uses SHA1 by default. Its less secure but faster. 
# 
# PBKDF2PasswordHasher takes 200ms to has a password wheresas SHA1PasswordHasher takes 1ms
#
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)



#
# Use this if you know your DB is secure and not going to leak ever.
#
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

# set to true to enable basic auth operations, if using the MP OAE loader
# you must enable this.
BASIC_WWW_AUTHENTICATION = False

#
# Add Query Level Cache if present on system.
# This needs an install of Johnny Cache from https://github.com/jmoiron/johnny-cache
try:
    import johnny.backends
    CACHES['default'] = {
            'BACKEND' : 'johnny.backends.locmem.LocMemCache',
            'LOCATION' : 'unique-snowflake',
            'JOHNNY_CACHE' : True,
           }
        
    JOHNNY_MIDDLEWARE_KEY_PREFIX='djoae'
    JOHNNY_TABLE_BLACKLIST = ("django_session",)
    MIDDLEWARE_CLASSES = (     
        'johnny.middleware.LocalStoreClearMiddleware',
        'johnny.middleware.QueryCacheMiddleware',
        ) + MIDDLEWARE_CLASSES

    DISABLE_QUERYSET_CACHE = False
except ImportError:
    logging.error("Query Level Cache is disabled, please install johnny cache")
    pass

# OS level binaries, these are ubuntu values
CONVERT_IMAGE_BINARY = "/usr/bin/convert"
PDFINFO_BINARY = "/usr/bin/pdfinfo"
PDFTOTEXT_BINARY = "/usr/bin/pdftotext"
JAVA_BINARY = "/usr/bin/java"

# If set to true, all SQL from slow requests will be dumped to the error log
DUMP_FULL_SQL = False


# GEARMAN Setup
# A list of Worker classes for registering tasks.
# Gearman will offload background processing from the request processes.
# To use you must run gearmand see http://gearman.org/ on one or
# more hosts. Those hosts must be listed in GEARMAN_HOSTS and then on the 
# worker VMs you must start worker processes with python manage.py gearman_worker
# If you have sufficient cores you can run standalone on one box, without using gearman.
GEARMAN_WORKERS_CLASSES = (
    "oae.content.preview.preview.PreviewProcessorGearmanWorker",
    )

GEARMAN_HOSTS = None # Disabled

# Localhost only
# GEARMAN_HOSTS = (
#    'localhost:4730',
#    )

# Cluster
#GEARMAN_HOSTS = (
#    'gmserver1.example.org:4730',
#    'gmserver2.example.org:4730',
#    'gmserver3.example.org:4730',
#    )


try:
    from local_settings import *
    logging.error("Loaded Local Settings")
except:
    logging.error(traceback.format_exc())
    logging.error("No Local Settings")
    pass

# Only put things here that you want be controlled by local settings, NO SETTINGS PLEASE

try:
   if ENABLE_RESPONSE_CACHE:
        MIDDLEWARE_CLASSES = (
           'django.middleware.cache.UpdateCacheMiddleware',
                  ) + MIDDLEWARE_CLASSES + (
           'django.middleware.cache.FetchFromCacheMiddleware',
       )
except:
   logging.error(traceback.format_exc())

try:
   if ENABLE_MEMORY_PROFILE:
        import guppy
        from guppy.heapy import Remote
        Remote.on()
        DEBUG = False

        logging.error("Memory Debugging Is On read http://www.toofishes.net/blog/using-guppy-debug-django-memory-leaks/")
        logging.error( "DEBUG forced off, to prevent DEBUG statements looking like a leak.")
except:
   pass

if DEBUG:
    JSON_INDENT = 4
else:
    JSON_INDENT = 0
