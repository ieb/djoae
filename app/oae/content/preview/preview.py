'''
Created on Apr 18, 2012

@author: ieb
'''

import os
import logging


import mimetypes
import re
import django.utils.simplejson as json
from django.conf import settings
from oae.common.util import Hash, Response
import subprocess
from oae.tag.models import Tag
import traceback
import tempfile
from multiprocessing import Semaphore
from django.db import transaction, close_connection
import time
from oae.content.models import ContentMeta
from django.contrib.auth.decorators import permission_required
import codecs
from oae.content.content_manager import ContentManager
from oae.content import content_signals
from oae.system.jobs import GearmanConnection
from multiprocessing.process import Process
import sys
from subprocess import PIPE

TASK_NAME = "preview_processor"

POOLBASE, \
    CONVERT_IMAGE_BINARY, \
    PDFINFO_BINARY, \
    PDFTOTEXT_BINARY, \
    JAVA_BINARY, \
    ROOT_PATH = \
            ( 
            settings.CONTENT_POOL_BASE,
            settings.CONVERT_IMAGE_BINARY, 
            settings.PDFINFO_BINARY, 
            settings.PDFTOTEXT_BINARY, 
            settings.JAVA_BINARY,
            settings.ROOT_PATH
             )
THIS_LOCATION =  os.path.dirname(os.path.abspath(__file__))
JOD_CONVERTER_FORMATS = "%s/document-formats.js" % THIS_LOCATION
JOD_CONVERTER_JAR = "%s/deps/jodconverter-core-3.0-beta-4.jar" % THIS_LOCATION
TIKA_JAR = "%s/deps/tika-app-1.1.jar" % THIS_LOCATION


IGNORE_MIME_TYPES = (
    re.compile(r'application/octet-stream'),
    re.compile(r'application/x-diskcopy'),
    re.compile(r'.*zip$'),
    re.compile(r'.*tar$'),
    re.compile(r'^video/.*$'),
    re.compile(r'^audio/.*$'),
    )
PDF_TYPES = (
    'application/x-pdf',
    'application/pdf',
    )
IMAGE_TYPES = (
 'image/gif', 
 'image/jpeg', 
 'image/png', 
 'image/x-ms-bmp', 
 'image/svg+xml', 
 'image/tiff', 
 'image/x-portable-bitmap', 
 'application/postscript', 
 'image/x-portable-pixmap',
 )


CONVERT_IMAGE_COMMAND = [
            CONVERT_IMAGE_BINARY
            ]

CONVERT_OTHER_TO_PDF_COMMAND = [
            JAVA_BINARY,
            '-jar', JOD_CONVERTER_JAR,
            '-r', JOD_CONVERTER_FORMATS
            ]
EXTRACT_INFO_COMMAND = [
            PDFINFO_BINARY
            ]

EXTRACT_IMAGES_COMMAND = [
            CONVERT_IMAGE_BINARY,
            "+adjoin",
            "-limit", "memory", "256MiB", 
            "-limit", "map", "512MiB",
            "-density", "150",
            "-resize", "1000x",
            "-quality", "85",
                        
            ]
EXTRACT_TEXT_COMMAND = [
            PDFTOTEXT_BINARY,
            "-enc",
            "UTF-8",
            "-nopgbrk"
            ]

TIKA_DETECT = [
            JAVA_BINARY,
            "-jar",
            TIKA_JAR,
            "-d"
            ]
TIKA_SUPPORTS = [
            JAVA_BINARY,
            "-jar",
            TIKA_JAR,
            "--list-parser-details"
            ]
TIKA_EXTRACT = [
            JAVA_BINARY,
            "-jar",
            TIKA_JAR,
            "-t"
            ]

TIKA_INFO = [
            JAVA_BINARY,
            "-jar",
            TIKA_JAR,
            "-m"
            ]

# Non aplha or http    
REJECT_TERM = re.compile(r"\W+|http") 

class PreviewProcessor(object):
    
    def __init__(self):
        self.dependencies = False
        self.tika_supports = None
        self.has_tika = False
        deps = self.hasDependencies(quick=True)
       
        if not deps:
            logging.error("One of more of the dependencies is missing, preview processor will not be available")
    
    def _getFileName(self, metadata):
        try:
            return metadata['sakai:pooled-content-file-name']
        except:
            return "none"

    def _determinMimeType(self, content):
        try:
            if content.mimetype is not None:
                return content.mimetype
            return content._metadata['_mimeType']
        except:
            try:
                filename = content._metadata['sakai:pooled-content-file-name']
                mimetypes.init()
                return mimetypes.guess_type(filename)
            except:
                None
                
    
    def _tikaSupports(self, content):
        if not hasattr(content,"_tikasupports"):
            self._tikasupports = False
            if self.has_tika:
                    if self.tika_supports is None:
                        supportsData = tempfile.NamedTemporaryFile()
                        subprocess.call(TIKA_SUPPORTS,stdout=supportsData)
                        supportsData = open(supportsData.name,"r")
                        self.tika_supports = []
                        for s in supportsData.readline():
                            self.tika_supports.append(s)
                        supportsData.close()
                    detectType = list(TIKA_DETECT)
                    detectType.append(content._baseFileName)
                    detected = tempfile.NamedTemporaryFile()
                    subprocess.call(detectType, stdout=detected)
                    detected = open(detected.name,"r")
                    detectedMime = detected.read().strip()
                    if detectedMime in self.tika_supports:
                        self._tikasupports = True
        return self._tikasupports

    def _extractText(self, content):
        if not hasattr(content,"_text"):
            textExtractFileName = ContentManager.getPoolFileText(content._baseFileName)
            if not os.path.exists(textExtractFileName):
                
                if hasattr(content,"_pdfFileName") and os.path.exists(content._pdfFileName):
                    # use pdftotext if we can, its faster
                    extractText = list(EXTRACT_TEXT_COMMAND)
                    extractText.append(content._pdfFileName)
                    extractText.append(textExtractFileName)
                    subprocess.call(extractText)
                    termsFile = codecs.open(textExtractFileName,mode="r",encoding="utf-8")
                    terms = termsFile.read()
                    termsFile.close()
                elif self._tikaSupports(content):
                    # For some reason, if you give Tika a massive file it will try and process it
                    # before checking it can parse it.
                    extractText = list(TIKA_EXTRACT)
                    extractText.append(content._baseFileName)
                    termsFile = codecs.open(textExtractFileName,mode="w",encoding="utf-8")
                    subprocess.call(extractText,stdout=termsFile)
                    termsFile.close()
                    termsFile = codecs.open(textExtractFileName,mode="r",encoding="utf-8")
                    terms = termsFile.read()
                    termsFile.close()
                if os.path.exists(textExtractFileName):
                    # replace quotes
                    terms = terms.replace(u"\x201c", '"').replace(u"\x201d", '"')
                    # replace apostrophes
                    terms = terms.replace(u"\x2018", "'").replace(u"\x2019", "'")
                    # remove ellipses ()
                    terms = terms.replace(u"\x2026", '')
                    # replace non-breaking spaces with a space char
                    terms = terms.replace(u"\xa0", ' ')
                    termsFile = codecs.open(textExtractFileName,mode="w", encoding="utf-8")
                    termsFile.write(terms)
                    termsFile.close()
                    content._text = terms
                else:
                    return None
            else:
                termsFile = codecs.open(textExtractFileName,mode="r", encoding="utf-8")
                terms = termsFile.read()
                termsFile.close()
                content._text = terms
            return content._text
        return None
    
    def _extractTerms(self, content):
        if not hasattr(content, "_terms"):
            termsFileName = ContentManager.getPoolFileTerms(content._baseFileName)
            if not os.path.exists(termsFileName):
                terms = self._extractText(content)
                if terms is None:
                    return []
                # late import so we can test if the term extractor is present
                from topia.termextract import extract 
                e = extract.TermExtractor()
                tags = e(terms)
                f = open(termsFileName,"w")
                f.write(json.dumps(tags))
                f.close()
            f = open(termsFileName,"r")
            tags = json.loads(f.read())
            f.close()
            selected_terms = {}
            for term, times, score in tags:
                key = term.strip().lower()
                if len(key) == 1 or REJECT_TERM.match(key) or len(key.split(' ')) > 2: 
                    continue
                selected_terms[term] = ( score, times, )
            # sort to the 5 highest scored values and extract ther terms
            content._terms = [ '%s' % t for (t,scoring) in sorted(selected_terms.iteritems(), key=lambda (k,v): v[0]*100+v[1])[:5]] #@UnusedVariable
        return content._terms
        
        
        
        

    def _convertToPdf(self, content):
        content._baseFileName = ContentManager.getPoolFile(content)
        if not hasattr(content,'_pdfFileName'):
            if content._mime_type not in PDF_TYPES and not self._getFileName(content._metadata).endswith(".pdf"):
                content._pdfFileName =  "%s.pdf" % content._baseFileName
                if not os.path.exists(content._pdfFileName):
                    logging.error("Generating PDF file %s " % content._pdfFileName)          
                    if content._mime_type in IMAGE_TYPES:
                        convertToPdf = list(CONVERT_IMAGE_COMMAND)
                    else:
                        convertToPdf = list(CONVERT_OTHER_TO_PDF_COMMAND)
                    convertToPdf.append(content._baseFileName)
                    convertToPdf.append(content._pdfFileName)
                    subprocess.call(convertToPdf)
                else:
                    logging.error("PDF Already Exists PDF file %s " % content._pdfFileName)
            else:
                content._pdfFileName = content._baseFileName 

    def _getInfo(self, content):
        if not hasattr(content, "_info"):
            pdfInfoFile = ContentManager.getPoolFileInfo(content._baseFileName)
            if not os.path.exists(pdfInfoFile):
                if hasattr(content,"_pdfFileName") and os.path.exists(content._pdfFileName):
                    # use pdftoinfo if we can, its faster
                    extractInfo = list(EXTRACT_INFO_COMMAND)
                    extractInfo.append(content._pdfFileName)
                    infoFile = open(pdfInfoFile,"w")
                    subprocess.call(extractInfo,stdout=infoFile)
                    infoFile.close()
                elif self._tikaSupports(content):
                    extractInfo = list(TIKA_INFO)
                    extractInfo.append(content._baseFileName)
                    infoFile = open(pdfInfoFile,"w")
                    subprocess.call(extractInfo,stdout=infoFile)
                    infoFile.close()                
                if os.path.exists(pdfInfoFile):
                    infoFile = open(pdfInfoFile,"r")
                    info = {}
                    
                    for l in infoFile.readlines():
                        parts = l.split(': ',2)
                        info[parts[0].lower().strip()] = parts[1].strip()
                    infoFile.close()
                    if "xmpTPg:NPages" in info:
                        info["pages"] = int(info["xmpTPg:NPages"])
                    if "pages" in info:
                        info["pages"] = int(info["pages"])
                    infoFile.close()
                else:
                    info = { "pages" : 0}
                    
                infoFile = open(pdfInfoFile,"w")
                infoFile.write(json.dumps(info, indent=2))
                infoFile.close()
                content._info = info
            else:
                logging.error("Loading %s " % pdfInfoFile)
                infoFile = open(pdfInfoFile,"r")
                content._info = json.loads(infoFile.read())
                infoFile.close()
        return content._info

    def _extractImages(self, content):
        pdfInfo = self._getInfo(content)
        pdfPageSpec = "%s" % (content._pdfFileName)
        npages = pdfInfo["pages"]
        if npages == 1:
            imageFile = "%s_page1.jpg" % (content._baseFileName)
        else:
            imageFile = "%s_page%%d.jpg" % (content._baseFileName)
        testImageFile = "%s_page%s.jpg" % (content._baseFileName,(npages))
        if not os.path.exists(testImageFile):
            logging.error("Extracting Images from %s " % pdfPageSpec)
            extractImages = list(EXTRACT_IMAGES_COMMAND)
            extractImages.append(pdfPageSpec)
            extractImages.append("-scene")
            extractImages.append("1")
            extractImages.append(imageFile)
            subprocess.call(extractImages)
        else:
            logging.error("Images already Exist for  %s " % pdfPageSpec)

        
        for i in range(1,npages+1):
            imageFile = "%s_page%s.jpg" % (content._baseFileName,i)
            normalImageFile = "%s_page%s.normal.jpg" % (content._baseFileName,i)
            smallImageFile = "%s_page%s.small.jpg" % (content._baseFileName,i)
            if not os.path.exists(imageFile):
                logging.error("File %s is missing " % imageFile)
                continue
            
            if not os.path.exists(normalImageFile):
                normalImage = list(CONVERT_IMAGE_COMMAND)
                normalImage.append(imageFile)
                normalImage.append("-resize")
                normalImage.append("900")
                normalImage.append(normalImageFile)
                subprocess.call(normalImage)
            
            if not os.path.exists(smallImageFile):
                smallImage = list(CONVERT_IMAGE_COMMAND)
                smallImage.append(imageFile)
                smallImage.append("-resize")
                smallImage.append("180x225>")
                smallImage.append(smallImageFile)
                subprocess.call(smallImage)


    def _tagContent(self,content):
        for tag in self._extractTerms(content):
            tag = re.sub(r'/$','',tag)
            if tag.endswith("/"):
                tag = tag[:-1]
            taghash = Hash.hash(tag)
            try:
                tagObject = content.tags.filter(hash=taghash)[0]
            except:
                parent = os.path.dirname(tag)
                name = os.path.basename(tag)
                parenthash = Hash.hash(parent)
                depth = len(re.sub(r'[^/]+', '', tag)) # counts the number of / in the path 
                tagObject, created = Tag.objects.get_or_create( #@UnusedVariable
                                        hash=taghash,
                                        parenthash=parenthash,
                                        name=name,
                                        fullpath=tag,
                                        titlekey=name,
                                        depth=depth
                                        )
                content.tags.add(tagObject)
        t = [ u"%s" % t.fullpath.replace("/tags/","") for t in content.tags.all() ]
        logging.error("Tagged with %s " % t)
        content._metadata['sakai:tags'] = t
        
    def _messageUser(self, content, user):
        pass
        
    def _save(self, content, user):
        self._tagContent(content)
        self._messageUser(content, user)
        content._metadata['sakai:pagecount'] = self._getInfo(content)["pages"]
        content.value = json.dumps(content._metadata)
    
    def _cleanup(self, content):
        pass
    
    def hasDependencies(self, quick=False):
        if self.dependencies:
            return True
        self.dependencies = True
        if not os.path.exists(JOD_CONVERTER_JAR):
            logging.error("JOD Converter Not available, please install as %s with all dependencies " % JOD_CONVERTER_JAR)
            self.dependencies = False
        self.has_tika = True
        if not os.path.exists(TIKA_JAR):
            logging.error("Tika Not available, please install as %s with all dependencies " % TIKA_JAR)
            self.has_tika = False
        if not quick:
            dum = tempfile.TemporaryFile()
            try:
                subprocess.call(PDFINFO_BINARY, stdout=dum, stderr=dum)
            except OSError:
                logging.error(traceback.format_exc())
                logging.error("Xpdf is not installed, missing pdfinfo")
                self.dependencies = False
            try:
                subprocess.call(PDFTOTEXT_BINARY, stdout=dum, stderr=dum)
            except OSError:
                logging.error(traceback.format_exc())
                logging.error("Xpdf is not installed, missing pdftotext")
                self.dependencies = False
            try:
                subprocess.call(CONVERT_IMAGE_BINARY, stdout=dum, stderr=dum)
            except OSError:
                logging.error(traceback.format_exc())
                logging.error("ImageMagix is not installed, missing convert")
                self.dependencies = False
            try:
                subprocess.call(JAVA_BINARY, stdout=dum, stderr=dum)
            except OSError:
                logging.error(traceback.format_exc())
                logging.error("Java is not installed, missing convert")
                self.dependencies = False
            
                dum.close()
        try:
            from topia.termextract import extract
            e = extract.TermExtractor() #@UnusedVariable
        except:
            logging.error(traceback.format_exc())
            logging.error("Python Module Topia Term Extract is missing please install (easy_install topia.termextract)")
            self.dependencies = False
        if quick:
            self.dependencies = False
            return True
        return self.dependencies

    def process(self, content, user=None):
        pid = os.getpid()
        contentid = content.contentid
        if not self.dependencies:
            logging.error("Process:%s %s Done:Dependencies missing " % (pid, contentid))
            return
        if content.bitstream is None:
            logging.error("Process:%s %s Done:No Bitstream " % (pid, contentid))
            return
        # do we want to process this file ?
        content._metadata = json.loads(content.value)
        content._mime_type = self._determinMimeType(content)
        if content._mime_type is None:
            logging.error("Process:%s %s Done:No Mimetype " % (pid, contentid))
            return
        for imt in IGNORE_MIME_TYPES:
            if imt.match(content._mime_type):
                logging.error("Process:%s %s Done:Ignoring Mimetype  %s " % (pid, contentid, content._mime_type))
                return
        
        logging.error("Process:%s %s Converting to PDF " % (pid, contentid))
        self._convertToPdf(content)
        logging.error("Process:%s %s Extracting Images " % (pid, contentid))
        self._extractImages(content)
        self._save(content, user)        
        self._cleanup(content)
        logging.error("Process:%s %s Done " % (pid, contentid))
        return
    
class PreviewProcessorJob():
    '''
    This is a job that takes a list of content IDs and processes them if there is no IDs it gets the most recent one 
    and processes that.
    '''
    
    def __init__(self, dependency_check = True):
        self.previewProcessor = PreviewProcessor()
        if not dependency_check:
            self.previewProcessor.dependencies = True

    
    
    def runJob(self, contentidlist=None):
        '''
        Run a job processing content ids.
        @param contentidlist: list of content ids to process
        '''
        if not self.previewProcessor.hasDependencies():
            return False
        try:
            transaction.enter_transaction_management()
            return self._processContent(contentidlist)
        except (KeyboardInterrupt, SystemExit):
            try:
                transaction.rollback()
            except:
                pass
            raise
        except:
            logging.error("Failed %s " % traceback.format_exc())
            try:
                transaction.rollback()
            except:
                pass
        finally:
            try:
                transaction.leave_transaction_management()
            except:
                pass
            try:
                close_connection()
            except:
                pass

    def _processContent(self, contentidlist):
        '''
        Use the preview process to process content.
        '''
        if not self.previewProcessor.hasDependencies():
            logging.error("Dependencies not satisfied " )
            return False
        try:
            ContentMeta.objects.filter(status="W").exclude(category='S').update(status="D")
        except:
            logging.error("Unable to clear hidden content from processing")
        try:
            if contentidlist is None or len(contentidlist) == 0:
                try:
                    content = ContentMeta.objects.filter(status="W", category='S').order_by('-id')[0]
                    return self._processContentMeta(content)
                except IndexError:
                    return False
            else:
                nfound = 0
                try:
                    idlistasint = [ int(x) for x in contentidlist]
                except:
                    idlistasint = None
                if idlistasint is not None:
                    for content in ContentMeta.objects.filter(status="W",category='S',id__in=idlistasint):
                        nfound = nfound + 1
                        if not self._processContentMeta(content):
                            return False
                else:
                    for content in ContentMeta.objects.filter(status="W",category='S',contentid__in=contentidlist):
                        nfound = nfound + 1
                        if not self._processContentMeta(content):
                            return False
                logging.error("Found %s " % nfound)
                return True
        except:
            logging.error(traceback.format_exc())
            return False


    def _processContentMeta(self, content):
        content.status = "P"
        content.save()
        transaction.commit()
        try:
            if content.category == 'S':
                self.previewProcessor.process(content)
            content.status = "D"
        except:
            logging.error("Processing content failed %s " % traceback.format_exc())
            content.status = "F"
            v = json.loads(content.value)
            v['_traceback'] = traceback.format_exc()
            content.value = json.dumps(v)
        content.save()
        transaction.commit()
        # Index the trigger indexing by signalling that preview processing has been completed.
        logging.error("Notifying Listeners Content is ready")
        content_signals.post_prepare.send(ContentMeta, instance=content)
        return True

        
    
class ProcessingDaemon(object):
    '''
    DONT USE, this has problems since its hard not to share parts of the GIL.
    
    Processes content as an in process daemon. This method needs no additional setup
    but is not suitable for production since this daemon thread will block request threads
    when runing things like OpenOffice
    '''
    routers = []
    sem = None
    child_process = None
    
    @staticmethod
    def start():
        
        if ProcessingDaemon.child_process is not None:
            if ProcessingDaemon.child_process.is_alive():
                return
        if ProcessingDaemon.sem is None:
            ProcessingDaemon.sem = Semaphore()
            daemon = ProcessingDaemon(os.getpid())
            child_process = Process(target=daemon.run)
            child_process.daemon = True
            child_process.start()
            ProcessingDaemon.child_process = child_process
            logging.error("%s Preview Processor Child running as process %s " % (os.getpid(),ProcessingDaemon.child_process))
        
    
        
    @staticmethod
    def end():
        if ProcessingDaemon.child_process is not None:
            if ProcessingDaemon.child_process.is_alive():
                ProcessingDaemon.sem.release()
                ProcessingDaemon.child_process.terminate()
                ProcessingDaemon.sem = None
            
            
    def __init__(self, parentpid):
        self.previewProcessorJob = PreviewProcessorJob()
        self.parentpid = parentpid
        
    def _checkRunning(self):
        '''
        Check that the child should be running and the 
        '''
        try:
            if os.getsid(self.parentpid) >= 0:
                return True
        except:
            pass
    
    def run(self):
        logging.error("%s Child process entering loop " % os.getpid())
        try:
            while self._checkRunning():
                logging.error("Waiting for content")
                ProcessingDaemon.sem.acquire()
                tries = 0
                while self._checkRunning():
                    try:
                        if not self.previewProcessorJob.runJob():
                            if tries > 0: 
                                # deal with the uncommitted transaction by sleeping
                                break;
                            time.sleep(1)
                        tries = tries+1
                    except:
                        logging.error("Failed %s " % traceback.format_exc())
        except:
            logging.error(traceback.format_exc())
        
        
def notify_of_new_content(content_ids=None,**kwargs):
        '''
        Notifiy whatever processing mechanism is active of new content.
        '''
        if content_ids is not None and len(content_ids) > 0:
            GearmanConnection.enqueue(TASK_NAME, content_ids)
        else:
            logging.error("Notified, but no content ids found ")
            raise Exception("Notified, but no content ids found ")


def enableInProcessProcessing():       
    if GearmanConnection.isEnabled():
        content_signals.notify_prepare.connect(notify_of_new_content)
    else:
        commands = [x.rstrip('\n') \
                            for x in subprocess.Popen(['ps','ax','-o','command'], stdout=PIPE).stdout]
        for c in commands:
            if c.endswith("manage.py process_preview forever"):
                logging.error("Preview Processor already present on this box, wont start another")
                return
        args = [sys.executable,"manage.py","process_preview","forever"]
        wd = "%s/app/oae" % ROOT_PATH
        logging.error("Executing Preview Process Command %s in %s/app/oae " % (args, wd))
        try:
            subprocess.Popen(args,cwd=wd,close_fds=True)
        except:
            logging.error("Failed to start preview_processor %s " % traceback.format_exc())
        logging.error("Done")


@permission_required('content.can_admin')
def startDelivery(request):
    ProcessingDaemon.notify()
    return Response.Ok()

@permission_required('content.can_admin')
def stopDelivery(request):
    ProcessingDaemon.end()
    return Response.Ok()

    


_previewProcessorTask = None





class PreviewProcessorGearmanWorker(object):
    '''
    Register preview processor task with the worker
    '''

    @staticmethod
    def dispatch_gearman_job(worker, job):
        '''
        This is a Gearman job dispatcher that dispatches to a singleton PreviewProcessorJob
        @param worker:
        @param job:
        '''
        contentidlist = job.data
        if _previewProcessorTask is None:
            _previewProcessorJob = PreviewProcessorJob() 
        _previewProcessorJob.runJob(contentidlist)
    
    
    @staticmethod
    def register_gearman_task(gearman_worker):
        '''
        Register the preview processor task
        @param gearman_worker:
        '''
        gearman_worker.register_task(TASK_NAME,PreviewProcessorGearmanWorker.dispatch_gearman_job)
        
    @staticmethod
    def deregister_german_task(gearman_worker):
        '''
        Deregister the preview processor task
        @param gearman_worker:
        '''
        gearman_worker.unregister_task(TASK_NAME)
