This folder should contain the following Jar files used by JODConverter. 
You can get them from the JODConverter Google code site. They have
not been included due to licensing.


Apache Commons

    commons-cli-1.1.jar
    commons-io-1.4.jar

JOD Converter
    
    jodconverter-core-3.0-beta-4.jar

JSON Org (read the license and do no evil)
    
    json-20090211.jar

OpenOffice/Libre Office Interface Jars
    
    juh-3.2.1.jar
    jurt-3.2.1.jar
    ridl-3.2.1.jar
    unoil-3.2.1.jar
    
    
Tika
   
   tika-app-1.1.jar
   Download from http://tika.apache.org/download.html