'''
Created on May 7, 2012

@author: ieb
'''
from base64 import urlsafe_b64encode
from django.conf import settings
from django.http import HttpResponse, HttpResponseNotFound, \
    HttpResponseBadRequest, HttpResponseForbidden, HttpResponseRedirect, \
    HttpResponseNotModified
from django.utils.http import http_date
from django.views.static import was_modified_since
from hashlib import sha1
from oae.common.util import Hash, Response, Path
from oae.common.xact import xact
from oae.content.encoder import ContentEncoder
from oae.content.models import ContentMeta, ContentMember, ContentMetaPart, \
    ContentMetaPartVersion, ContentMetaVersion
from oae.content.operations import TagOperation, DeleteOperation, \
    DeleteTagOperation, ContentPartImportOperation, PublishSakaiDocOperation
from oae.content.util import ContentUtil
from oae.permission.models import Principal
from oae.user.models import InternalUserGroup
import datetime
import django.utils.simplejson as json
import logging
import mimetypes
import os

POOLBASE, JSON_INDENT, CONTENT_HOST = (
                settings.CONTENT_POOL_BASE,
                settings.JSON_INDENT,
                settings.CONTENT_HOST)

OPERATIONS = {
    "tag" : TagOperation(),
    "delete" : DeleteOperation(),
    "deletetag" : DeleteTagOperation(),
    "import" : ContentPartImportOperation(),
    "publish-sakaidoc-page" : PublishSakaiDocOperation(),
    }


class ContentManager(object):
    def __init__(self, user):
        self.user = user
        self.ugid = InternalUserGroup.getUgid(user)

    @staticmethod
    def getPoolFile(content):
        if content is None or content.bitstream is None:
            return None
        if hasattr(content, "_baseFileName"):
            return content._baseFileName
        if content.bitstream[:5] == "pool:":
            content._baseFileName = "%s%s" % (POOLBASE, content.bitstream[5:])
            return content._baseFileName
        return None

    @staticmethod
    def getPoolFileInfo(basename):
        return "%s.info.json" % basename

    @staticmethod
    def getPoolFileText(basename):
        return "%s.txt" % (basename)

    @staticmethod
    def getPoolFileTerms(basename):
        return "%s.terms.json" % (basename)

    @staticmethod
    def hashPath(path):
        return Hash.hash(path, "content:")

    def getContent(self, id, contentMeta=None, path=None):
        if contentMeta is None:
            try:
                contentMeta = ContentMeta.objects.get(contentid=id)
                if not self.user.has_perm("content.view", contentMeta):
                    return False, None
            except ContentMeta.DoesNotExist:
                return None, None

        if path is not None:
            ## logging.error("Path is %s filename is %s %s " % (path, contentMeta.filename, path.startswith(contentMeta.filename)))
            if contentMeta.filename is not None and \
                    len(contentMeta.filename) > 0 and \
                    path.startswith(contentMeta.filename):
                return contentMeta, None
            else:
                pathParts = Path.getPathElements(path)
                itemPath = pathParts[0]
                try:
                    if len(pathParts) > 1:
                        targetPath = path[len(itemPath):]
                    else:
                        targetPath = None
                    ## logging.error("Finding %s " % itemPath)
                    return ContentMetaPart.objects.get(contentMeta=contentMeta,
                                                       contentid=itemPath), \
                                                       targetPath
                except ContentMetaPart.DoesNotExist:
                    ##logging.error("Didnt find %s " % itemPath)
                    return contentMeta, path
        return contentMeta, path

    @xact
    def updateFile(self, file, poolId, initial_status='W'):
        try:
            content = ContentMeta.objects.get(contentid=poolId)
            if not self.user.has_perm("content.manage", content):
                return None, None, HttpResponseForbidden()
        except ContentMeta.DoesNotExist:
            return None, None, HttpResponseNotFound()
        contentHash = self._calculateContentHash(file)
        content.bitstream = self._saveFile(file, self._createBitStreamUri(contentHash))
        content.status = initial_status
        metadata = json.loads(content.value)
        content.contenthash = contentHash
        metadata.update({
                '_mimeType' : file.content_type,
                '_length' : file.size,
                '_lastModifiedBy' : self.ugid,
                '_bodyLastModifiedBy' : self.ugid,
                'sakai:needsprocessing' : True,
                'sakai:pooled-content-file-name' : file.name
                })
        content.value = json.dumps(metadata, ensure_ascii=False)
        content.filename = file.name
        content.save()
        return file.name, {
                'poolId' : content.contentid,
                'item' : content
                }, None


    @xact
    def createFile(self, file, initial_status='W', full_path=None, category='S'):
        contentHash = self._calculateContentHash(file)
        if full_path is None:
            contentid = Hash.genCompactUUID()
            content = ContentMeta.objects.create(owner=self.user,
                            lastmodifiedby=self.user,
                            status=initial_status,
                            contentid=contentid,
                            mimetype=file.content_type,
                            category=category)
            full_path = content.contentid
        else:
            contentid = ContentManager.hashPath(full_path)
            try:
                content = ContentMeta.objects.get(contentid=contentid)
                content.category = category
            except ContentMeta.DoesNotExist:
                content = ContentMeta.objects.create(owner=self.user,
                            lastmodifiedby=self.user,
                            status=initial_status,
                            mimetype=file.content_type,
                            contentid=contentid,
                            category=category)
        content.contenthash = contentHash
        metadata = {
                '_mimeType' : file.content_type,
                '_length' : file.size,
                '_path' : full_path,
                '_id' : content.contentid,
                '_lastModifiedBy' : self.ugid,
                '_bodyLastModifiedBy' : self.ugid,
                'sakai:showalways' : True,
                'sakai:pool-content-created-for' : self.ugid,
                'commentCount': 0,
                'sakai:needsprocessing' : True,
                'sakai:pooled-content-file-name' : file.name,
                'sakai:pooled-content-manager' : [ self.ugid ],
                'sakai:pooled-content-editor' : [],
                'sakai:pooled-content-viewer' : [],
                }
        content.value = json.dumps(metadata, ensure_ascii=False)
        content.filename = file.name
        content.view = 'N'
        content.bitstream = self._saveFile(file, self._createBitStreamUri(contentHash))
        content.save()
        ContentMember.objects.get_or_create(content=content,
                                            principal=Principal.objects.get(
                                                        user=self.user),
                                            role="M")
        # Cant create a file and update membership at the mometn, self._updateMembership(content, values)
        return file.name, {
                'poolId' : content.contentid,
                'item' : content
                }

    @xact
    def createMetaFile(self, values, category='S'):
        content = ContentMeta.objects.create(owner=self.user,
                                lastmodifiedby=self.user,
                                mimetype=values.get("mimeType"),
                                status='W',
                                category=category)
        content.contentid = Hash.genCompactUUID()
        name = values.get("sakai:pooled-content-file-name")
        metadata = {
                    '_mimeType' : values.get("mimeType"),
                    '_length' : 0,
                    '_path' : content.contentid,
                    '_id' : content.contentid,
                    '_lastModifiedBy' : self.ugid,
                    '_bodyLastModifiedBy' : self.ugid,
                    'sakai:showalways' : values.get("sakai:showalways") == 'true',
                    'sakai:pool-content-created-for' : self.ugid,
                    'commentCount': 0,
                    'sakai:needsprocessing' : True,
                    'sakai:allowcomments' :
                            values.get('sakai:allowcomments') == 'true',
                    'sakai:description' : values.get('sakai:description'),
                    'sakai:pooled-content-file-name' : name,
                    'sakai:pooled-content-manager' : [ self.ugid ],
                    'sakai:pooled-content-editor' : [],
                    'sakai:pooled-content-viewer' : [],
                    'structure0' : values.get("structure0"),
                    'sakai:schemaversion' : values.get("sakai:schemaversion"),
                    'sakai:copyright' : values.get("sakai:copyright"),
                    }
        content.value = json.dumps(metadata, ensure_ascii=False)
        permissions = values.get('sakai:permissions')
        if permissions == 'public':
            content.view = 'A'
        else:
            content.view = 'N'
        content.save()
        ContentMember.objects.create(content=content,
                        principal=Principal.objects.get(user=self.user),
                        role="M")
        self._updateMembership(content, values)

        '''
        if "structure0" in values:
            refs = self._collectRefs(json.loads(values.get("structure0")))
            for r in refs:
                ContentMetaParts.objects.get_or_create(contentid=r,contentMeta=content,value="{}")
        '''

        return {
                'poolId' : content.contentid,
                'item' : content
                }

    @xact
    def saveVersion(self, id, path):
        try:
            contentMeta = ContentMeta.objects.get(contentid=id)
            target = contentMeta
        except ContentMeta.DoesNotExist:
            return HttpResponseNotFound()
        if not self.user.has_perm("content.manage", contentMeta):
            return HttpResponseForbidden()
        if path is not None:
            if contentMeta.filename is not None and \
                    len(contentMeta.filename) > 0 and \
                    path.startswith(contentMeta.filename):
                pass
            else:
                pathParts = Path.getPathElements(path)
                itemPath = pathParts[0]
                part = self._getContentPart(contentMeta,
                                            itemPath,
                                            create=False)
                if part is None:
                    return HttpResponseNotFound()
                target = part
        version = target.saveVersion()
        return HttpResponse(json.dumps({"version": version.version}))


    def getVersionList(self, id, path):
        '''
    {"path":"kPWccSNjqi/id9667388",
    "items":3,
    "total":3,
    "versions":{
        "1.2":{
             "versionId":"HiomQLMGEeGJzmu6RaTSfQ+",
             "_versionNumber":1339337496507,
             "_created":1332132474398,
             "_id":"HiomQLMGEeGJzmu6RaTSfQ+",
             "page":"<p>sdfsdf<\/p><p>sdfafsdf<\/p><p>sdfsdfsdf<\/p>
             <p>sdfsdfsdf<\/p><p>sdfdsfdsf<\/p><p>sdfsdfsdfsdfsdfsdf<\/p>
             <p>sdfsdfdsf<\/p><p>sdfsdfdsf<\/p>",
             "_lastModifiedBy":"ieb",
             "_readOnly":"Y",
             "_nextVersion":"MKQ4sLMGEeGJzmu6RaTSfQ+",
             "_lastModified":1339337496457,
             "_previousVersion":"sP0W4XF-EeGkO70RRaTSfQ+",
             "_createdBy":"ieb",
             "_path":"kPWccSNjqi/id9667388",
             "_versionHistoryId":"sP0W4HF-EeGkO70RRaTSfQ+"
             },
        "1.1":{
           "versionId":"sP0W4XF-EeGkO70RRaTSfQ+",
           "_versionNumber":1339337465508,
           "_created":1332132474398,
           "_id":"sP0W4XF-EeGkO70RRaTSfQ+",
           "page":"<p>sdfsdf<\/p><p>sdfafsdf<\/p>
             <p>sdfsdfsdf<\/p><p>sdfsdfsdf<\/p>
             <p>sdfdsfdsf<\/p><p>sdfsdfsdf<\/p>
             <p>sdfsdfdsf<\/p><p>sdfsdfdsf<\/p>",
             "_lastModifiedBy":"ieb",
             "_readOnly":"Y",
             "_nextVersion":"HiomQLMGEeGJzmu6RaTSfQ+",
             "_lastModified":1339337465456,
             "_previousVersion":"sM6z4HF-EeGkO70RRaTSfQ+",
             "_createdBy":"ieb",
             "_path":"kPWccSNjqi/id9667388",
             "_versionHistoryId":"sP0W4HF-EeGkO70RRaTSfQ+"
             },
        "1.0":{
          "versionId":"sM6z4HF-EeGkO70RRaTSfQ+",
          "_versionNumber":1332132474702,
          "_created":1332132474398,
          "_id":"sM6z4HF-EeGkO70RRaTSfQ+",
          "page":"",
          "_lastModifiedBy":"ieb",
          "_readOnly":"Y",
          "_nextVersion":"sP0W4XF-EeGkO70RRaTSfQ+",
          "_lastModified":1332132474398,
          "_createdBy":"ieb",
          "_path":"kPWccSNjqi/id9667388",
          "_versionHistoryId":"sP0W4HF-EeGkO70RRaTSfQ+"
          }}}
'''
        try:
            contentMeta = ContentMeta.objects.get(contentid=id)
            target = contentMeta
            model = ContentMetaVersion
        except ContentMeta.DoesNotExist:
            return None, None, HttpResponseNotFound()
        # NB, permissions are not versioned.
        if not self.user.has_perm("content.view", contentMeta):
            return None, None, HttpResponseForbidden()
        if path is not None:
            if contentMeta.filename is not None and \
                    len(contentMeta.filename) > 0 and \
                    path.startswith(contentMeta.filename):
                pass
            else:
                pathParts = Path.getPathElements(path)
                if len(pathParts) == 0:
                    pass
                else:
                    itemPath = pathParts[0]
                    part = self._getContentPart(contentMeta,
                                                itemPath,
                                                create=False)
                    if part is None:
                        return None, None, HttpResponseNotFound()
                    target = part
                    model = ContentMetaPartVersion

        return target.versions.order_by("version"), model, None


    @xact
    def updateContent(self, id, changes, contentMeta=None, path=None):
        if contentMeta is None:
            try:
                contentMeta = ContentMeta.objects.get(contentid=id)
                targetObj = contentMeta
                modelType = ContentMeta
                targetPath = path
                if not self.user.has_perm("content.view", contentMeta):
                    return HttpResponseForbidden()
            except ContentMeta.DoesNotExist:
                return HttpResponseNotFound()

        contentAuthoringResponse = self._doCustomUpdate(contentMeta, changes, path)
        if contentAuthoringResponse is not None:
            return Response.toHttpResponse(contentAuthoringResponse)

        if path is not None:
            ##logging.error("Path is %s " % path)
            if contentMeta.filename is not None and \
                    len(contentMeta.filename) > 0 and \
                    path.startswith(contentMeta.filename):
                targetPath = None
                ##logging.error("Part of the filename %s " % contentMeta.filename)
            else:
                pathParts = Path.getPathElements(path)
                itemPath = pathParts[0]
                ##logging.error("Checking  Item Path %s " % itemPath)
                part = self._getContentPart(contentMeta,
                                itemPath,
                                create=self._isPart(changes,
                                                    path))
                if part is not None:
                    targetObj = part
                    modelType = ContentMetaPart
                    if len(pathParts) > 1:
                        targetPath = path[len(itemPath):]
                    else:
                        targetPath = None



        if not self.user.has_perm("content.manage", contentMeta):
            logging.error("User %s doesnt have update on %s " % (self.ugid, ContentEncoder().default(contentMeta)))
            return HttpResponseForbidden()
        ##logging.error("Target Object is %s  path is %s  targetPath [%s]" % (targetObj, path, targetPath))

        if ':operation' in changes:
            op = changes[':operation']
            if op in OPERATIONS:
                ##logging.error("Trying operation %s " % OPERATIONS[op])
                ret, changes = OPERATIONS[op].doOperation(self.user,
                                                          changes,
                                                          targetObj,
                                                          path)
                if ret is not None:
                    return ret
            else:
                logging.error("Operation %s not present " % op)
                return HttpResponseBadRequest()
        # no operation, just update
        self._updateMembership(contentMeta, changes)
        ##logging.error("Performing update of properties through normal route")
        return Response.toHttpResponse((ContentUtil.toUpdateResponse(
                            ContentUtil.updateProperties(
                                        self.user,
                                        changes,
                                        targetObj,
                                        modelType,
                                        path=targetPath))))
    @xact
    def updateMembership(self, contentMeta, values):
        return self._updateMembership(contentMeta, values)

    @xact
    def changeMembership(self, contentMeta, added, removed):
        return self._changeMembership(contentMeta, added, removed)

    def _isPart(self, changes, path):
        ##logging.error("Changes %s path %s " % (changes, path))
        return False

    def _doCustomUpdate(self, contentMeta, changes, path):
        '''
        This method performs custom updates to the content model that have been expressed as Sling protocol, but
        have special meaning. Once the API is fixed, these can be replaced by something less like mind reading.
        Warning: this is ugly flaky code, but I can't do much else at the moment.
        @param contentMeta:
        @param changes:
        @param path:
        '''

        # Content Authoring emits an edit lock using a content import with the structure
        # {"id4225841":{"editing":{"time":1337108459828,"sakai:modifierid":"id6273091"}}}
        # this code latches on to that. If the UI could add some semantics, then I can 
        # remove this. However, thats never going to happen.
        try:
            v = json.loads(changes[":content"])
            if len(v) == 1:
                for k, n in v.iteritems():
                    if len(n) == 1 and len(n["editing"]) == 2:
                        editTime = datetime.datetime.utcfromtimestamp(
                                            int(n["editing"]["time"]) / 1000)
                        value = n
                        part, _ = ContentMetaPart.objects.get_or_create(
                                        contentMeta=contentMeta,
                                        contentid=k)
                        part.editor = self.user
                        part.editing = editTime
                        try:
                            v = json.loads(part.value)
                        except:
                            v = {}
                        v.update(value)
                        part.value = json.dumps(v)
                        part.save()
                        return True
        except KeyError:
            pass
        return None

    def _getContentPart(self, contentMeta, path, create=False):
        if create:
            obj, _ = ContentMetaPart.objects.get_or_create(
                                        contentMeta=contentMeta, contentid=path)
            ##logging.error("Created %s Path %s " % (created, path))
            return obj
        else:
            try:
                return ContentMetaPart.objects.get(
                                contentMeta=contentMeta, contentid=path)
                ##logging.error("Got Path %s " % (path))
            except ContentMetaPart.DoesNotExist:
                ##logging.error("Path Does not exist %s " % (path))
                return None




    def _collectRefs(self, refs, structure):
        for k, v in structure.items():
            if k == "_ref":
                refs.append(v)
            elif isinstance(v, dict):
                self._collectRefs(refs, v)



    def _updateMembership(self, contentMeta, values):

        manage, changes, principalAdded, principalRemoved = \
            Principal.getMembershipChanges(self.user,
                                    contentMeta,
                                    "content",
                                    values,
                                    ugid=self.ugid)
        if not changes:
            return Response.Ok()
        if not manage:
            return HttpResponseForbidden()
        ##logging.error("Adding %s Removing %s " % (principalAdded, principalRemoved))
        return Response.toHttpResponse(self._changeMembership(
                                                    contentMeta,
                                                    principalAdded,
                                                    principalRemoved))

        pass


    def _changeMembership(self, contentMeta, added, removed):
        ##logging.error("Membership add %s membership remove %s " % ( added, removed))
        changed = False
        for (k, toRemove) in removed.iteritems():
            if len(toRemove) > 0:
                ##logging.error("Removing %s %s " % (k,toRemove))
                ContentMember.objects.filter(content=contentMeta,
                                        role=k,
                                        principal__ugid__in=toRemove).delete()
                changed = True
        processed = set()
        for k in ['M', 'E', 'V']:
            for pid in added[k]:
                if pid not in processed:
                    processed.add(pid)
                    try:
                        try:
                            principal = Principal.viaCache("u:%s" % pid,
                                            lambda: Principal.objects.get(ugid=pid))
                        except Principal.DoesNotExist:
                            principal = Principal.viaCache("g:%s" % pid,
                                            lambda: Principal.objects.get(
                                                            group__name=pid))
                        cm, _ = ContentMember.objects.get_or_create(
                                            content=contentMeta,
                                            principal=principal) #@UnusedVariable
                        if cm.role != k:
                            ##logging.error("Setting %s %s " % (k, pid))
                            cm.role = k
                            cm.save()
                            changed = True
                    except Principal.DoesNotExist:
                        logging.error("Cant find principal %s " % pid)
                        pass
        if changed:
            # Only update if there were changes.    
            globalViewers = [ u'%s' % p['principal__ugid']
                             for p in ContentMember.objects.filter(
                                        content=contentMeta,
                                        principal__ugid__in=[
                                                    'anonymous',
                                                    'everyone']).values(
                                                            "principal__ugid")]
            if 'anonymous' in globalViewers:
                contentMeta.view = 'A'
            elif 'everyone' in globalViewers:
                contentMeta.view = 'E'
            else:
                contentMeta.view = 'N'


            ##logging.error("Json Before %s " % contentMeta.value)
            value = json.loads(contentMeta.value)
            value['sakai:pooled-content-manager'] = contentMeta.getMembers('M', useCache=False)
            value['sakai:pooled-content-editor'] = contentMeta.getMembers('E', useCache=False)
            value['sakai:pooled-content-viewer'] = contentMeta.getMembers('V', useCache=False)
            contentMeta.value = json.dumps(value, ensure_ascii=False)

            logging.error("Content Managers on %s are %s " % (contentMeta.contentid, contentMeta.getMembers('M')))
            logging.error("Content Editors  on %s are %s " % (contentMeta.contentid, contentMeta.getMembers('E')))
            logging.error("Content Viewers  on %s are %s " % (contentMeta.contentid, contentMeta.getMembers('V')))
            # logging.error("FinalX is %s %s " % (contentMeta.view,json.dumps(value,indent=4)))
            contentMeta.save()

            o = ContentMeta.objects.get(id=contentMeta.id)
            logging.error("Post Save        on %s are %s " % (o.contentid, o.getMembers('M')))
            logging.error("Post Save        on %s are %s " % (o.contentid, o.getMembers('E')))
            logging.error("Post Save        on %s are %s " % (o.contentid, o.getMembers('V')))
            ##logging.error("View is %s " % (o.view))
            ##logging.error("Json is %s " % o.value)


        return True

    def streamFileResponse(self, request, contentid, subPath=None):
        '''
        Stream the file body, taking account of hostnames for user content
        @param request: the request
        @param contentid: the contentID
        @param subPath: andy subpath
        '''

        # If CONTENT_HOST is configured, redirect to the content host with a token
        # unless there is no url patten, in which case stream from this host.
        # Use the browsers host header since thats the one that the browser is using
        # for its sandbox. This prevents user content from interacting with the 
        # application.
        if CONTENT_HOST is not None:
            request_host = request.META['HTTP_HOST']
            for (host, url_pattern, secret) in CONTENT_HOST:
                if request_host == host:
                    qs = request.META['QUERY_STRING'] \
                        if 'QUERY_STRING' in request.META else "1=1"
                    url = "%s?%s" % (request.path, qs)
                    if url_pattern is not None:
                        hmac = Hash.encodeUserHmac(self.user, url, secret)
                        targetUrl = url_pattern % (url, "_hmac=%s" % hmac)
                        return HttpResponseRedirect(targetUrl)
                    else:
                        try:
                            requestingUser = Hash.decodeUserHmac(
                                                request.GET['_hmac'], url, secret)
                        except:
                            requestingUser = self.user
        else:
            requestingUser = self.user
        try:
            ## FIXME Add in a readers join here
            contentMeta = ContentMeta.objects.get(contentid=contentid)
            if not requestingUser.has_perm("content.view", contentMeta):
                return HttpResponseForbidden()
        except ContentMeta.DoesNotExist:
            return HttpResponseNotFound()

        jsonValues = json.loads(contentMeta.value)
        # need to adjust for subpaths
        fileName = jsonValues["sakai:pooled-content-file-name"] if "sakai:pooled-content-file-name" in jsonValues else None
        mimeType = contentMeta.mimetype if contentMeta.mimetype is not None else jsonValues["_mimetype"] if "_mimetype" in jsonValues else None

        return self._streamFile(request,
                                contentMeta.bitstream,
                                subPath,
                                fileName,
                                mimeType)

    def _streamFile(self, request, poolUri, subPath, fileName, mimeType):
        if poolUri[:5] == "pool:":
            if subPath is None:
                path = "%s%s" % (POOLBASE, poolUri[5:])
                targetFileName = fileName
            else:
                path = "%s%s_%s" % (POOLBASE, poolUri[5:], subPath)
                targetFileName = path
                if not os.path.exists(path):
                    path = "%s%s" % (POOLBASE, poolUri[5:])
                    targetFileName = fileName
            if not os.path.exists(path):
                return HttpResponseNotFound()
            statobj = os.stat(path)
            mimetype, encoding = mimetypes.guess_type(targetFileName)
            mimetype = mimeType or mimetype or 'application/octet-stream'
            if not was_modified_since(request.META.get('HTTP_IF_MODIFIED_SINCE'),
                                  statobj.st_mtime, statobj.st_size):
                return HttpResponseNotModified(mimetype=mimetype)
            response = HttpResponse(open(path, 'rb').read(), mimetype=mimetype)
            response["Last-Modified"] = http_date(statobj.st_mtime)
            response["Content-Length"] = statobj.st_size
            if encoding:
                response["Content-Encoding"] = encoding
            return response

    def _saveFile(self, file, poolUri):
        '''
        Save the file into the pool, checking for a good match after the hash
        @param file:
        @param poolUri:
        '''
        if poolUri[:5] == "pool:":
            poolFile = "%s%s" % (POOLBASE, poolUri[5:])
            if os.path.exists(poolFile):
                if os.path.getsize(poolFile) == file.size:
                    return poolUri
            poolFileBase = poolFile
            poolUriBase = poolUri
            i = 0
            while(os.path.exists(poolFile)):
                poolFile = "%s!%s" % (poolFileBase, i)
                poolUri = "%s!%s" % (poolUriBase, i)
                i = i + 1
            parent = os.path.dirname(poolFile)
            if not os.path.exists(parent):
                os.makedirs(parent)
            f = open(poolFile, "wb")
            for chunk in file.chunks():
                f.write(chunk)
            f.close()
            return poolUri
        raise Exception("Unable to process BitStream URI %s " % poolUri)


    def _createBitStreamUri(self, hash):
        '''
        Implements a single instance pool
        @param hash:
        '''
        try:
            return ContentMeta.objects.filter(contenthash=hash)[0].bitstream
        except:
            today = datetime.date.today()
            return "pool:/%s/%s/%s/%s/%s/%s" % (
                                today.year,
                                today.month,
                                hash[0:2],
                                hash[2:4],
                                hash[4:6],
                                hash)



    def _calculateContentHash(self, file):
        hasher = sha1()
        for chunk in file.chunks():
            hasher.update(chunk)
        return urlsafe_b64encode(hasher.digest()).replace("=", "")
try:
    from oae.content.preview.preview import enableInProcessProcessing
    enableInProcessProcessing()
except:
    pass
