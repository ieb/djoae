'''
Created on Apr 19, 2012

@author: ieb
'''
from oae.content.preview.preview import PreviewProcessorJob
from django.core.management.base import BaseCommand
import logging
import os
import time


class Command(BaseCommand):
    args = '<content_id content_id content_hash...>'
    help = 'Runs the preview processor on each of the content items in turn, will also trigger a re-index operation on the item'
    
    
    def handle(self, *args, **options):
        logging.error("Process:%s Preview Processor String to process ids %s " % (os.getpid(), args))
        if len(args) == 1 and args[0] == "forever":
            while(True):
                try:
                    pp = PreviewProcessorJob(dependency_check=False)
                    pp.runJob(None)
                except (KeyboardInterrupt, SystemExit):
                    return
                except:
                    pass
                finally:
                    logging.error("Process:%s Done check" % (os.getpid()))
                try:
                    time.sleep(5)
                except (KeyboardInterrupt, SystemExit):
                    return
        else:
            try:
                pp = PreviewProcessorJob(dependency_check=False)
                pp.runJob(list(args))
            finally:
                logging.error("Process:%s Preview Processor Finished on ids %s " % (os.getpid(), args))
            