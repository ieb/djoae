'''
Created on Mar 12, 2012

@author: ieb
'''
from django.conf.urls.defaults import patterns, url
from oae.content.views import createFile


urlpatterns = patterns('', 
    url(r'^createfile.(?P<poolId>.*?)$', createFile, name="reupload file"),
    url(r'^createfile$', createFile, name="upload file"),
)