
from oae.content.models import ContentMeta, ContentComment, ContentMetaPart,\
    ContentMetaPartVersion, ContentMetaVersion
from oae.tag.models import Tag
from oae.user.encoder import ProfileEncoder, NullProfileEncoder
from oae.user.models import Profile, InternalUserGroup
import django.utils.simplejson as json
import time
from oae.common.util import Properties
import logging


    

class ContentEncoder(json.JSONEncoder):
    '''
    Encodes all ContentMeta objects in Json output
    '''
    
    
    @staticmethod
    def depend():
        '''
        Get a list of dependencies for this encoder.
        '''
        s = ProfileEncoder.depend()
        s.update([ContentEncoder])
        return s
    
    @staticmethod
    def dependShortProfile():
        s = NullProfileEncoder.depend()
        s.update([ContentEncoder])
        return s

    def default(self, obj):
        if isinstance(obj, ContentMetaPartVersion):
            return {
                "versionId": obj.version,
                "_lastModifiedBy": InternalUserGroup.getUgid(obj.lastmodifiedby),
                "_lastModified": int(time.mktime(obj.modified.timetuple())*1000),
                "_versionNumber": int(time.mktime(obj.versioned.timetuple())*1000)
                }
        if isinstance(obj, ContentMetaVersion):
            return {
                "versionId": obj.version,
                "_lastModifiedBy": InternalUserGroup.getUgid(obj.lastmodifiedby),
                "_lastModified": int(time.mktime(obj.modified.timetuple())*1000),
                "_versionNumber": int(time.mktime(obj.versioned.timetuple())*1000)       
                }
        if isinstance(obj, ContentMetaPart):
            subpath = None
            if hasattr(obj, "_subpath"):
                subpath = obj._subpath
            ret =  Properties.extract(subpath, json.loads(obj.value))
            ret['_objectType '] = "ContentMetaPart"
            return ret
        if isinstance(obj, ContentMeta):
            subpath = None
            if hasattr(obj, "_subpath"):
                subpath = obj._subpath
            ret =  Properties.extract(subpath, json.loads(obj.value))
            ret['_created'] = int(time.mktime(obj.created.timetuple())*1000)
            ret['_lastModified'] = int(time.mktime(obj.modified.timetuple())*1000)
            if subpath is not None:
                ret['sakai:hasPreview'] = (obj.status == 'D')
                ret['sakai:needsprocessing'] = (obj.status == 'W')
                ret['_processing_status'] = obj.status
            ret['_objectType '] = "ContentMeta"
            if self.object_levels > 0:
                for c in obj.contentmetapart_set.all():
                    ret[c.contentid] = c

            # this one is not good since it makes the result bound to user              
            # ret['sakai:canmanage'] = False
          
            return ret
        if isinstance(obj, Tag):
            return obj.fullpath
        return super(ContentEncoder,self).default(obj)

class CommentEncoder(json.JSONEncoder):
    '''
    Encodes all ContentComment objects in json output
    '''            
    @staticmethod
    def depend():
        '''
        Get a list of dependencies for this encosder.
        '''
        s = ProfileEncoder.depend()
        s.update([CommentEncoder])
        return s
    
    def default(self, obj):
        if isinstance(obj, ContentComment):
            try:
                profile = Profile.objects.get(user=obj.author)
            except Profile.DoesNotExist:
                profile = Profile.createAnonProfile()
            comment = ProfileEncoder().default(profile)
            comment.update({
                    'comment' : obj.comment,
                    '_created' : int(time.mktime(obj.created.timetuple())*1000),
                    'commentId' : "%s/comments/%s" % (obj.content.contentid, obj.id ) 
                    })
            return comment
        if isinstance(obj, Tag):
            return obj.fullpath
        return super(CommentEncoder,self).default(obj)
    
    
