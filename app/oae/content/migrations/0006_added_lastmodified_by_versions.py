# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'ContentMetaPart.modified'
        db.add_column('content_contentmetapart', 'modified',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=datetime.datetime(2012, 6, 12, 0, 0), db_index=True, blank=True),
                      keep_default=False)

        # Adding field 'ContentMetaPart.lastmodifiedby'
        db.add_column('content_contentmetapart', 'lastmodifiedby',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='contentpart_lastmodifiedby', to=orm['auth.User']),
                      keep_default=False)

        # Adding field 'ContentMetaPartVersion.modified'
        db.add_column('content_contentmetapartversion', 'modified',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=datetime.datetime(2012, 6, 12, 0, 0), db_index=True, blank=True),
                      keep_default=False)

        # Adding field 'ContentMetaPartVersion.lastmodifiedby'
        db.add_column('content_contentmetapartversion', 'lastmodifiedby',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='contentpart_v_lastmodifiedby', to=orm['auth.User']),
                      keep_default=False)

    def backwards(self, orm):
        # Deleting field 'ContentMetaPart.modified'
        db.delete_column('content_contentmetapart', 'modified')

        # Deleting field 'ContentMetaPart.lastmodifiedby'
        db.delete_column('content_contentmetapart', 'lastmodifiedby_id')

        # Deleting field 'ContentMetaPartVersion.modified'
        db.delete_column('content_contentmetapartversion', 'modified')

        # Deleting field 'ContentMetaPartVersion.lastmodifiedby'
        db.delete_column('content_contentmetapartversion', 'lastmodifiedby_id')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'content.contentcomment': {
            'Meta': {'object_name': 'ContentComment'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'content': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['content.ContentMeta']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'})
        },
        'content.contentmember': {
            'Meta': {'unique_together': "(('principal', 'content'),)", 'object_name': 'ContentMember'},
            'content': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['content.ContentMeta']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'principal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['permission.Principal']"}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'content.contentmeta': {
            'Meta': {'object_name': 'ContentMeta'},
            'bitstream': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'category': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1', 'db_index': 'True'}),
            'contenthash': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'contentid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastmodifiedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'content_lastmodifiedby'", 'to': "orm['auth.User']"}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'content_owner'", 'to': "orm['auth.User']"}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_index': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'content_tags'", 'symmetrical': 'False', 'to': "orm['tag.Tag']"}),
            'value': ('django.db.models.fields.TextField', [], {}),
            'view': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'content.contentmetapart': {
            'Meta': {'unique_together': "(('contentMeta', 'contentid'),)", 'object_name': 'ContentMetaPart'},
            'contentMeta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['content.ContentMeta']"}),
            'contentid': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'editing': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'db_index': 'True'}),
            'editor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'editing'", 'null': 'True', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastmodifiedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contentpart_lastmodifiedby'", 'to': "orm['auth.User']"}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {'default': "'{}'"})
        },
        'content.contentmetapartversion': {
            'Meta': {'unique_together': "(('contentMeta', 'contentid', 'version'),)", 'object_name': 'ContentMetaPartVersion'},
            'contentMeta': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'partv'", 'to': "orm['content.ContentMeta']"}),
            'contentid': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'currentVersion': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'to': "orm['content.ContentMetaPart']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastmodifiedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contentpart_v_lastmodifiedby'", 'to': "orm['auth.User']"}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {'default': "'{}'"}),
            'version': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        'content.contentmetaversion': {
            'Meta': {'unique_together': "(('contentid', 'version'),)", 'object_name': 'ContentMetaVersion'},
            'bitstream': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'category': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1', 'db_index': 'True'}),
            'contenthash': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'contentid': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'currentVersion': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'versions'", 'to': "orm['content.ContentMeta']"}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastmodifiedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'content_v_lastmodifiedby'", 'to': "orm['auth.User']"}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'content_v_owner'", 'to': "orm['auth.User']"}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_index': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {}),
            'version': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'view': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'permission.principal': {
            'Meta': {'object_name': 'Principal'},
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_index': 'True'}),
            'ugid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'})
        },
        'tag.tag': {
            'Meta': {'object_name': 'Tag'},
            'depth': ('django.db.models.fields.IntegerField', [], {}),
            'fullpath': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2048', 'db_index': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'}),
            'parenthash': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'titlekey': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['content']
