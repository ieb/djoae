# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):
    
    depends_on = (
        ("tag", "0001_initial"),
        ("permission", "0001_initial"),
    )


    def forwards(self, orm):
        # Adding model 'ContentMeta'
        db.create_table('content_contentmeta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contentid', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(related_name='content_owner', to=orm['auth.User'])),
            ('contenthash', self.gf('django.db.models.fields.CharField')(max_length=64, db_index=True)),
            ('lastmodifiedby', self.gf('django.db.models.fields.related.ForeignKey')(related_name='content_lastmodifiedby', to=orm['auth.User'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, db_index=True, blank=True)),
            ('value', self.gf('django.db.models.fields.TextField')()),
            ('bitstream', self.gf('django.db.models.fields.CharField')(max_length=512, null=True)),
            ('filename', self.gf('django.db.models.fields.CharField')(max_length=64, db_index=True)),
            ('view', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=1, db_index=True)),
        ))
        db.send_create_signal('content', ['ContentMeta'])

        # Adding M2M table for field tags on 'ContentMeta'
        db.create_table('content_contentmeta_tags', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('contentmeta', models.ForeignKey(orm['content.contentmeta'], null=False)),
            ('tag', models.ForeignKey(orm['tag.tag'], null=False))
        ))
        db.create_unique('content_contentmeta_tags', ['contentmeta_id', 'tag_id'])

        # Adding model 'ContentMember'
        db.create_table('content_contentmember', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('principal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['permission.Principal'])),
            ('content', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['content.ContentMeta'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('content', ['ContentMember'])

        # Adding unique constraint on 'ContentMember', fields ['principal', 'content']
        db.create_unique('content_contentmember', ['principal_id', 'content_id'])

        # Adding model 'ContentComment'
        db.create_table('content_contentcomment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['content.ContentMeta'])),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, db_index=True, blank=True)),
            ('comment', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('content', ['ContentComment'])

    def backwards(self, orm):
        # Removing unique constraint on 'ContentMember', fields ['principal', 'content']
        db.delete_unique('content_contentmember', ['principal_id', 'content_id'])

        # Deleting model 'ContentMeta'
        db.delete_table('content_contentmeta')

        # Removing M2M table for field tags on 'ContentMeta'
        db.delete_table('content_contentmeta_tags')

        # Deleting model 'ContentMember'
        db.delete_table('content_contentmember')

        # Deleting model 'ContentComment'
        db.delete_table('content_contentcomment')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'content.contentcomment': {
            'Meta': {'object_name': 'ContentComment'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'content': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['content.ContentMeta']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'})
        },
        'content.contentmember': {
            'Meta': {'unique_together': "(('principal', 'content'),)", 'object_name': 'ContentMember'},
            'content': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['content.ContentMeta']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'principal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['permission.Principal']"}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'content.contentmeta': {
            'Meta': {'object_name': 'ContentMeta'},
            'bitstream': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'contenthash': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'contentid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastmodifiedby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'content_lastmodifiedby'", 'to': "orm['auth.User']"}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'content_owner'", 'to': "orm['auth.User']"}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_index': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'content_tags'", 'symmetrical': 'False', 'to': "orm['tag.Tag']"}),
            'value': ('django.db.models.fields.TextField', [], {}),
            'view': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'permission.principal': {
            'Meta': {'object_name': 'Principal'},
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_index': 'True'}),
            'ugid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'})
        },
        'tag.tag': {
            'Meta': {'object_name': 'Tag'},
            'depth': ('django.db.models.fields.IntegerField', [], {}),
            'fullpath': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2048', 'db_index': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'}),
            'parenthash': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'titlekey': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['content']