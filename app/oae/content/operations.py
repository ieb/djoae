'''
Created on Mar 14, 2012

@author: ieb
'''
from django.conf import settings
from oae.common.util import Response, Hash, Request, Properties
from oae.content.models import ContentComment, ContentMeta, ContentMember,\
    ContentMetaPart
from oae.tag.models import Tag
from oae.user.models import InternalUserGroup
import django.utils.simplejson as json
import os
import re
import logging
from oae.common.operations import ImportOperation
import datetime


POOLBASE = ( settings.CONTENT_POOL_BASE )

class DummyOperation(object):
    def doOperation(self,user, changes, contentMeta):
        return None, changes

class TagOperation(object):
    
    def doOperation(self, user, changes, contentMeta, path=None):
        tags = changes.getlist('key')
        for tag in tags:
            tag = re.sub(r'/$','',tag)
            if tag.endswith("/"):
                tag = tag[:-1]
            taghash = Hash.hash(tag)
            try:
                tagObject = contentMeta.tags.filter(hash=taghash)[0]
            except:
                parent = os.path.dirname(tag)
                name = os.path.basename(tag)
                parenthash = Hash.hash(parent)
                depth = len(re.sub(r'[^/]+', '', tag)) # counts the number of / in the path 
                tagObject, created = Tag.objects.get_or_create( #@UnusedVariable
                                        hash=taghash,
                                        parenthash=parenthash,
                                        name=name,
                                        fullpath=tag,
                                        titlekey=name,
                                        depth=depth
                                        )
                contentMeta.tags.add(tagObject)
        v = json.loads(contentMeta.value)
        t = [ u"%s" % t.fullpath.replace("/tags/","") for t in contentMeta.tags.all() ]
        v['sakai:tags'] = t
        contentMeta.value = json.dumps(v)
        contentMeta.save()
        return Response.Ok(), changes
    
class DeleteTagOperation(object):
    def doOperation(self, user, changes, contentMeta, path=None):
        tags = changes.getlist('key')
        for tag in tags:
            taghash = Hash.hash(tag)
            try:
                contentMeta.tags.remove(contentMeta.tags.filter(hash=taghash)[0])
            except:
                pass
        v = json.loads(contentMeta.value)
        t = [ u"%s" % t.fullpath.replace("/tags/","") for t in contentMeta.tags.all() ]
        v['sakai:tags'] = t
        contentMeta.value = json.dumps(v)
        contentMeta.save()
        return Response.Ok(), changes
    
class DeleteOperation(object):
    '''
    Delete the item. 
    For the moment this will delete the item.
    We need to extend the model to have where the item is used 
    and then remove the reference for this user
    same model as the Message/MessageBox
    ie Content/ContentBox
    '''
    
    def doOperation(self, user, changes, contentMeta, path=None):
        hash = contentMeta.contenthash
        ugid = InternalUserGroup.getUgid(user)
        nl = ContentMember.objects.filter(content=contentMeta).exclude(principal__ugid__in=['everyone','anonymous',ugid]).count()
        if nl > 0:
            logging.error("Forced deletion of content in use by others item ID %s " % contentMeta.id)
        poolUri = contentMeta.bitstream
        ContentComment.objects.filter(content=contentMeta).delete()
        ContentMember.objects.filter(content=contentMeta).delete()
        contentMeta.delete()
        if poolUri is not None and poolUri[:5] == "pool:":
            c = ContentMeta.objects.filter(contenthash=hash).count()
            # No references to this file in the pool, so remove it.
            if c == 0:
                poolFile = "%s%s" % ( POOLBASE, poolUri[5:])
                dir = os.path.dirname(poolFile)
                fn = os.path.basename(poolFile)
                notfn = "%s!" % fn
                for f in os.listdir(dir):
                    if f.startswith(fn) and not f.startswith(notfn):
                        os.remove("%s/%s" % (dir,f))

        return Response.Ok(), changes
    
    
class ContentPartImportOperation(object):
    def doOperation(self, user, changes, contentMeta, path=None):
        '''
        Perform an import on parts, taking care not to overwrite existing parts.
        @param user:
        @param changes:
        @param contentMeta:
        '''
        if isinstance(contentMeta,ContentMeta):
            importOperation = ImportOperation()
            
            ##logging.error('Changes are %s ' % changes)
            replacement = json.loads(changes[":content"])
            finalChanges = dict(changes)
            for part in contentMeta.contentmetapart_set.all():
                ##logging.error("Inspecting part %s " % (part.contentid))
                if part.contentid in replacement:
                    ##logging.error("Part Is present in replacement ")
                    update = dict(replacement[part.contentid])
                    for k,v in replacement.iteritems():
                        if k[0] == ':':
                            update[k] = v
                    partValue = json.loads(part.value)
                    partValue = importOperation.doOperation(update, None, partValue, None)
                    part.value = json.dumps(partValue)
                    ##logging.error("Replaced parts values %s %s " % (part.contentid, part.value))
                    part.lastodifiedby = user
                    part.save()
                    del(replacement[part.contentid])
            finalChanges[':content'] = json.dumps(replacement)
            changes = Request.toQueryDict(finalChanges)
        return None, changes # let the normal routes handle the remainder of the update
    
class PublishSakaiDocOperation(object):

    def doOperation(self, user, changes, obj, path=None):
        '''
        Perform a publisH operation moving the source to a destination part.
        This is essentially a move operation. However it must take into account that
        the part might have been created by the UI and so be in the contentmeta space.
        (Bad or non api design).
        
        In addition not parts of the temp space get updates.
        The best I can get for documentation is https://github.com/mrvisser/nakamura/commit/0da5e3bf9e3d3525269d401302be5243185efd04
        
        
        
        @param user:
        @param changes:
        @param obj:
        @param path:
        '''
        
        
        ##logging.error("Performing Publish Operation for %s %s %s %s" % (user, changes, obj, path))
        contentPoolId = None
        if isinstance(obj, ContentMeta):
            contentPoolId = obj.contentid
        elif isinstance(obj,ContentMetaPart): 
            contentPoolId = obj.contentMeta.contentid
        destUrl = changes[':dest']
        targetRE = re.compile("/p/%s/(.*)$" % contentPoolId)
        m = targetRE.match(destUrl)
        if m is not None:
            destPartId = m.group(1)
            m = re.match("(.*?)/(.*)$", destPartId)
            if m is not None:
                destPartId = m.group(1)
                destPath = m.group(2)
            else:
                destPath = ""
            ##logging.error("Dest Path is RE[%s] [%s] [%s] " % (targetRE.pattern,destPartId, destPath))
            sourceMapBase, sourceMapParent, sourceMapName, sourceMap = Properties.extractWithParent(obj.value, path, create=False)       
            if sourceMapName is not None and sourceMap is not None:
                part, _ = ContentMetaPart.objects.get_or_create(contentid=destPartId,contentMeta=obj)
                destMapBase, destMapParent, destMapName, _ = Properties.extractWithParent(part.value, destPath, create=True)
                # THIS IS AN UGLY FIX to prevent the version property from recursively filling up. 
                # See issue 61 for more details
                try:
                    del(sourceMap['version'])
                except KeyError:
                    pass
                try:
                    del(sourceMap['editor'])
                except KeyError:
                    pass


                if destMapParent is None:
                    ##logging.error("Replacing entire map %s with %s " %  (destMapBase, sourceMap))
                    destMapBase = sourceMap
                else:
                    ##logging.error("Replacing element %s in  map %s with %s " %  (destMapName, destMapBase, sourceMap))
                    destMapParent[destMapName] = sourceMap
                part.value = json.dumps(destMapBase)
                part.editing = datetime.datetime.now()
                part.editor = user
                part.lastmodifiedby = user
                ##logging.error("Set value of part [%s] to %s  " % (part.contentid, part.value))
                part.save()
                
                if sourceMapName in sourceMapParent:
                    del(sourceMapParent[sourceMapName])
                obj.value = json.dumps(sourceMapBase)
                obj.save()
            return Response.Ok(200), changes            
        return None, changes

    