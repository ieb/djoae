'''
Created on Mar 12, 2012

@author: ieb
'''
from django.conf.urls.defaults import patterns, url
from oae.content.views import doManagerViewerListContent, doAllListContent,\
    doRelatedContent


urlpatterns = patterns('',
    url(r'^manager-viewer\.json$', doManagerViewerListContent),
    url(r'^me/related-content.json$', doRelatedContent),
    url(r'^all.*', doAllListContent),
    url(r'^auth-all.json', doAllListContent),
)