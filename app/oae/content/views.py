'''
Created on Mar 12, 2012

@author: ieb
'''
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.files.uploadedfile import UploadedFile
from django.db import models
from django.http import HttpResponseNotAllowed, HttpResponse, \
    HttpResponseNotFound, HttpResponseBadRequest, HttpResponseForbidden
from oae.common.util import JSON_CT, Response, Search, MultiEncoder, Properties
from oae.content.encoder import ContentEncoder
from oae.content.models import ContentMeta, ContentMember
from oae.permission.models import ObjectPermission
from oae.user.encoder import ProfileEncoder
from oae.user.models import Profile
import django.utils.simplejson as json
import logging
from oae.content.content_manager import ContentManager
from oae.common.xact import xact
from oae.activity.activity_manager import ActivityManager
from oae.content import content_signals
from oae.tag.models import Tag
from django.views.decorators.cache import never_cache

POOLBASE, JSON_INDENT = ( settings.CONTENT_POOL_BASE, settings.JSON_INDENT)

    
@never_cache  # Cache at a lower level with invalidation.
def doContentStream(request, poolId, subPath=None):
    '''
    Stream content
    @param request:
    @param poolId:
    @param subPath:
    '''
    if request.method not in ["GET"]:
        return doContent(request, poolId, subPath)
    
    return ContentManager(request.user).streamFileResponse(request, poolId, subPath)

@never_cache  # Cache at a lower level with invalidation.
def doContent(request, poolId, path=None):
    '''
    Dump the content item,
    @param request:
    @param poolId:
    @param path:
    '''
    if request.method not in ["GET","POST"]:
        return HttpResponseBadRequest("GET","POST")
    
    # remove all selectors
    poolId = poolId.split('.')[0]
        
    if request.method == "POST":
        
        return Response.toHttpResponse(ContentManager(request.user).updateContent(poolId, request.POST, path=path))
        
    
    else:
        obj, targetPath = ContentManager(request.user).getContent(poolId, path=path)
        if obj == None:
            return HttpResponseNotFound()
        if not obj:
            return HttpResponseForbidden()            
        if request.user.has_perm("content.view",obj):
            # inform the encoder about subpaths into the value object
            obj._subpath = targetPath
            data = Properties.extract(targetPath, json.loads(obj.value))
            if data is None:
                return HttpResponseNotFound()
            return HttpResponse(json.dumps(obj,cls=MultiEncoder, encoder_classes=ContentEncoder.dependShortProfile(),indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)
        return HttpResponseForbidden()
    
    
@login_required
def doSaveContent(request, poolId, path=None):
    if request.method not in ["POST"]:
        return HttpResponseBadRequest("POST")
    return ContentManager(request.user).saveVersion(poolId, path=path)

def doContentVersions(request, poolId, path=None):
    if request.method not in ["GET"]:
        return HttpResponseBadRequest("GET")
    versions, model, response = ContentManager(request.user).getVersionList(poolId, path=path)
    if response is not None:
        return response
    versions = Search(defaultItems=25).dbSearch(request, model, versions)
    if path is None:
        results={
            "path" : "%s/%s" % (poolId, path),            
            }
    else:
        results={
            "path" : "%s" % poolId      
            }
    results['items'] = versions['items']
    results['total'] = versions['total']
    v = {}
    for version in versions['results']:
        v.update( { version.version : version })
    results['versions'] = v;
    return HttpResponse(json.dumps(results,
                                       cls=MultiEncoder, 
                                       encoder_classes=ContentEncoder.dependShortProfile(),
                                       indent=JSON_INDENT,
                                ensure_ascii=False),
                                content_type=JSON_CT)



@login_required
def createFile(request, poolId = None):
    if request.method != "POST":
        return HttpResponseNotAllowed("POST")
    if not request.user.has_perm("content.create"):
        return HttpResponseForbidden("Permission Denied, user must have content.create permission")
    result = {}
    itemids = []
    if request.META['CONTENT_TYPE'] == 'application/x-www-form-urlencoded':
        # not a file upload
        # NOTE: Not all parameters are stored, some are ignored, if the UI 
        # changes this code will need to change to match.
        if poolId is not None:
            return HttpResponseBadRequest("User update to update an existing pool item")
        result['_contentItem'] = ContentManager(request.user).createMetaFile(request.POST)
        itemids.append(result['_contentItem']['poolId'])
    else: 
        contentManager = ContentManager(request.user)
        for name in request.FILES:
            files = request.FILES.getlist(name)
            for file in files:
                if isinstance(file, UploadedFile):
                    if poolId is None:
                        filename, metadata = contentManager.createFile(file)
                    else:
                        filename, metadata, response = contentManager.updateFile(file, poolId)
                        if response is not None:
                            return response
                    result[filename] = metadata
                    itemids.append(metadata['poolId'])
    content_signals.notify_prepare.send(ContentMeta,content_ids=itemids)
    return HttpResponse(json.dumps(result,cls=MultiEncoder, encoder_classes=ContentEncoder.dependShortProfile(),indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)

    
@never_cache  # Cache at a lower level with invalidation.
def doMembers(request, poolId):
    if ( "GET" == request.method):
        return _getMembers(request, poolId)
    if ( "POST" == request.method):
        ret =  _setMembers(request, poolId)
        return ret
    return HttpResponseNotAllowed(("POST","GET",))
    
    
def _getMembers(request, poolId):
    '''
    Get members profiles
    @param request:
    @param poolId:
    '''
    if ( "GET" != request.method):
        return HttpResponseNotAllowed("GET")
    try:
        contentMeta = ContentMeta.objects.get(contentid=poolId)
        if request.user.has_perm("content.view",contentMeta):
            result = {
                'viewers' : list(Profile.objects.filter(models.Q(user__in=ContentMember.objects.filter(content=contentMeta,role='V').values("principal__user"))|
                                                   models.Q(group__in=ContentMember.objects.filter(content=contentMeta,role='V').values("principal__group"))).
                                                   prefetch_related("group__usergroupdata")),
                'editors' : list(Profile.objects.filter(models.Q(user__in=ContentMember.objects.filter(content=contentMeta,role='E').values("principal__user"))|
                                                   models.Q(group__in=ContentMember.objects.filter(content=contentMeta,role='E').values("principal__group"))).
                                                   prefetch_related("group__usergroupdata")),
                'managers' : list(Profile.objects.filter(models.Q(user__in=ContentMember.objects.filter(content=contentMeta,role='M').values("principal__user"))|
                                                   models.Q(group__in=ContentMember.objects.filter(content=contentMeta,role='M').values("principal__group"))).
                                                   prefetch_related("group__usergroupdata")),
                }
            
            return HttpResponse(json.dumps(result,cls=MultiEncoder, encoder_classes=ProfileEncoder.depend(),indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)
        return HttpResponseForbidden()
    except ContentMeta.DoesNotExist:
        return HttpResponseNotFound()
    
def _setMembers(request, poolId):
    '''
    Update membership
    @param request:
    @param poolId:
    '''
    if ( "POST" != request.method):
        return HttpResponseNotAllowed("POST")
    try:
        contentMeta = ContentMeta.objects.get(contentid=poolId)        
        return Response.toHttpResponse(ContentManager(request.user).updateMembership(contentMeta, request.POST))

    except ContentMeta.DoesNotExist:
        return HttpResponseNotFound()


@login_required
@xact
def doModifyAce(request, poolId):
    '''
    We dont do explict ACE's on content items, we use the editor, view and manager fields to express ACE's
    We might have to support ACE's later.
    @param request:
    @param poolId:
    '''
    logging.error("Please implement %s %s Request: %s  PoolID: %s  " % (__file__,"doModifyAce", request, poolId))
    if ( "POST" != request.method):
        return HttpResponseNotAllowed("POST")
    try:
        contentMeta = ContentMeta.objects.get(contentid=poolId)
        if request.user.has_perm("content.manage",contentMeta):
            return Response.Ok()            
        return HttpResponseForbidden()
    except ContentMeta.DoesNotExist:
        return HttpResponseNotFound()


@never_cache  # Cache at a lower level with invalidation.
def doListContent(request):
    '''
    /var/search/pool/manager-viewer.json?sortOn=_lastModified&sortOrder=desc&q=*&page=0&items=18&_charset_=utf-8&_=1332122436873
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    if "userid" in request.GET: # Prevent a double filter on the same criteria
        libraryUgid = request.GET["userid"]
        # userGroupPrincipals = ObjectPermission.getUserPrincipalsFromUgid(libraryUgid)
        # If you want a library to contain everything a user can see, then use the above
        # If the library is to contain only those items the ugid has been explicitly granted view, edit or manage on
        # then the filter needs to select only those
        # select either a group or a user
        q1 = ContentMember.objects.filter(
                        models.Q(principal__ugid=libraryUgid)|
                        models.Q(principal__type='G',principal__group__name=libraryUgid))
        # Not certain this is right, I think we may want to filter on principals as well where the library is not the request users library.
        # ie q2 = ContentMember.objects.filter(principal__in=principals) 
        # then ContentMeta.objects.filter(id__in=q1.values("content_id"),id__in=q2.values("content_id"),category='S')
        principals = ObjectPermission.getUserPrincipals(request.user)
        q2 = ContentMember.objects.filter(principal__in=principals)
        q = ContentMeta.objects.filter(id__in=q1.values("content_id"),category='S').filter(id__in=q2.values("content_id"))
    else:
        principals = ObjectPermission.getUserPrincipals(request.user)
        q1 = ContentMember.objects.filter(principal__in=principals)
        q = ContentMeta.objects.filter(id__in=q1.values("content_id"),category='S')
        pass # assume if its not specified the user is searching for all content they can see
    if 'q' in request.GET and request.GET['q'] !='*':
        logging.error("Warning, this search is a temporary measure which needs to be replaced by full text search at some point")
        q = q.filter(value__icontains=request.GET['q'])
    if 'mimetype' in request.GET and request.GET['mimetype'] != '*':
        q = q.filter(mimetype=request.GET['mimetype'])
    search = Search()
    managedObjects = search.dbSearch(request, ContentMeta,q.order_by(search.getSortOn(request)))

    
    return HttpResponse(json.dumps(managedObjects,cls=MultiEncoder, encoder_classes=ContentEncoder.depend(), object_levels=0, indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)

def doAllListContent(request):
    return doListContent(request)
    
def doManagerViewerListContent(request):
    return doListContent(request)   



def doActivity(request, poolId):
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    try:
        contentMeta = ContentMeta.objects.get(contentid=poolId)
        if request.user.has_perm("content.view",contentMeta):
            return ActivityManager(request.user).addActivity(contentMeta, request.POST)
        return HttpResponseForbidden()
    except ContentMeta.DoesNotExist:
        return HttpResponseNotFound()
    
    

@never_cache  # Cache at a lower level with invalidation.
def doRelatedContent(request):
    '''
    Content that is tagged with a tag I have used in my content, that I can see.
    @param request:
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    # My principals
    principals = ObjectPermission.getUserPrincipals(request.user)
    # tags from my content
    myTags = Tag.objects.filter(content_tags__owner=request.user.id) 
    # The Content I am a member of (ie can read)
    q1 = ContentMember.objects.filter(principal__in=principals)
    q = ContentMeta.objects.filter(
                                   models.Q(id__in=q1.values("content_id"))|
                                     models.Q(view='A'),
                                   tags__in=myTags,
                                   category='S')
    search = Search()
    managedObjects = search.dbSearch(request, ContentMeta,q.order_by(search.getSortOn(request)))
    return HttpResponse(json.dumps(managedObjects,
                                   cls=MultiEncoder,
                                   encoder_classes=ContentEncoder.depend(),
                                   object_levels=0,
                                   indent=JSON_INDENT,
                                   ensure_ascii=False),
                        content_type=JSON_CT)
    
