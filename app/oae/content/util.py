'''
Created on Mar 14, 2012

@author: ieb
'''
from django.conf import settings
from django.http import HttpResponseNotFound, HttpResponse
from oae.common.util import Properties, JSON_CT
from oae.user.models import InternalUserGroup
import django.utils.simplejson as json

POOLBASE, JSON_INDENT, CONTENT_HOST = ( settings.CONTENT_POOL_BASE, settings.JSON_INDENT, settings.CONTENT_HOST )

class ContentUtil(object):
    @staticmethod
    def extractProperties(path, contentMeta):                
        '''
        get the subtree identified by path as a json response
        '''
        if contentMeta is None:
            return HttpResponseNotFound()
        subtree = Properties.extract(path, json.loads(contentMeta.value))
        if subtree is None:
            return HttpResponseNotFound()
        return HttpResponse(json.dumps(subtree,indent=JSON_INDENT),content_type=JSON_CT,ensure_ascii=False)

    @staticmethod
    def updateProperties(user, changes, contentMeta, modelType, path=None):
        '''
          Update the userdata of a model object from a post. The model must have a userdata property
          which will be a json string.
        '''
        value = contentMeta.value
        if value is None:
            value = "{}"
        value = Properties.update(json.loads(value), changes, path)
        value['_lastModifiedBy'] = InternalUserGroup.getUgid(user)
        contentMeta.value = json.dumps(value,ensure_ascii=False)
        contentMeta.lastmodifiedby = user
        
        return 200, contentMeta
    
    @staticmethod
    def toUpdateResponse(resp):
        status, model = resp #@UnusedVariable
        if model is None:
                return None
        model.save()
        return True


    
        

        



