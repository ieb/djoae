'''
Created on Mar 12, 2012

NB: Not change the schema without generating a migration.


@author: ieb
'''
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from oae.permission.models import Principal
from oae.user.models import Profile
from django.db.models.signals import post_save, post_delete
import django.utils.simplejson as json
from oae.tag.models import Tag
from django.db.models.aggregates import Max

    

class AbstractContentMeta(models.Model):
    '''
    Defines the content meta information
    '''
    class Meta(object):
        abstract = True
        permissions = (
                ('create','Can create a file'),
                ('create_tag','Can create a tag'),
                ('manage', 'Manages this contentmeta'),
                ('edit', 'Edits this contentmeta'),
                ('view', 'Views this contentmeta'),
            )
    GLOBAL_VIEW_CHOICES = (
                ('A','Anon users can view'),
                ('E','Everyone can view'),
                ('N','Only those granted view can view')
            )
    PROCESSING_STATUS_CHOICES = (
                ('W','Waiting'),
                ('P','Processing'),
                ('D','Done'),
                ('F','Fail'),
            )
    
    CATEGORY_TYPE_CHOICES = (
                ('S','Standard'),
                ('H','Hidden'),
            )
    
    id = models.AutoField(primary_key=True)
    contenthash = models.CharField("Hash of the content item",
                              db_index=True,
                              max_length=32,
                              help_text="hash of the content bitstream")
    created = models.DateTimeField(
                auto_now_add=True,
                verbose_name="Time the content item was created",
                help_text="Time the content item was created")
    modified = models.DateTimeField(
                auto_now=True,
                db_index=True,
                verbose_name="Time the content item was modified",
                help_text="Time the content item was modifed")
    value = models.TextField(help_text="JSON string containing all the content data")
    bitstream =  models.CharField("Bitstream URI",
                              null=True,
                              max_length=64,
                              help_text="URI to the content") # URI to the content body
    mimetype =  models.CharField("Mimetype",
                              null=True,
                              max_length=64,
                              help_text="Mimetype of the item") # URI to the content body
    filename = models.CharField(max_length=256, db_index=True, verbose_name='Primary file name, used for sorting')
    view = models.CharField(max_length=1, verbose_name="Anon and Everyone view permissions", choices=GLOBAL_VIEW_CHOICES )
    status = models.CharField(max_length=1,db_index=True, verbose_name="Processing Status", choices=PROCESSING_STATUS_CHOICES )
    category = models.CharField(max_length=1,db_index=True, default='S', null=False, verbose_name="Content Category", choices=CATEGORY_TYPE_CHOICES )


    def getMembers(self,role, useCache=True):
        if not hasattr(self,"_members"):
            self._members = {}
        if not useCache or role not in self._members:
            principals = Principal.objects.filter(contentmember__content=self,contentmember__role=role)
            l = []
            for p in principals:
                if p.group is None:
                    l.append(p.ugid)
                else:
                    l.append(p.group.name)
                    # Grr the UI does some manipulations using the ID of the content item 
                    # to build group names and then uses that later. very bad, MUST be fixed.
            self._members[role] = l
        return self._members[role]

class ContentMeta(AbstractContentMeta):
    contentid = models.CharField("Content Id",
                              unique=True,
                              db_index=True,
                              max_length=32,
                              help_text="The Content ID of the object.")
    owner = models.ForeignKey(User,related_name="content_owner")
    lastmodifiedby = models.ForeignKey(User,related_name="content_lastmodifiedby")
    tags = models.ManyToManyField(Tag, related_name="content_tags")
    
    def saveVersion(self):
        newVersion = self.versions.aggregate(Max("version"))["version__max"]
        if newVersion is None:
            newVersion = 1
        else:
            newVersion = newVersion+1
        return ContentMetaVersion.objects.create(
                            contentid=self.contentid,
                            owner=self.owner,
                            contenthash=self.contenthash,
                            lastmodifiedby=self.lastmodifiedby,
                            created=self.created,
                            modified=self.modified,
                            value=self.value,
                            bitstream=self.bitstream,
                            filename=self.filename,
                            view=self.view,
                            mimetype=self.mimetype,
                            status=self.status,
                            category=self.category,
                            currentVersion=self,
                            version=newVersion)
    

class ContentMetaVersion(AbstractContentMeta):

    class Meta:
        unique_together=(("contentid","version"),)
        
    owner = models.ForeignKey(User,related_name="content_v_owner")
    lastmodifiedby = models.ForeignKey(User,related_name="content_v_lastmodifiedby")
    contentid = models.CharField("Content Id",
                              db_index=True,
                              max_length=32,
                              help_text="The Content ID of the object.")
    versioned = models.DateTimeField(
                auto_now=True,
                db_index=True,
                verbose_name="Time the content item was versioned",
                help_text="Time the content item was versioned")

    version = models.IntegerField("Version Name",
                               db_index=True,
                               help_text="A Version")
    currentVersion = models.ForeignKey(ContentMeta, related_name="versions")

class AbstractContentMetaPart(models.Model):
    class Meta:
        abstract = True
        
    id = models.AutoField(primary_key=True)
    modified = models.DateTimeField(
                auto_now=True,
                db_index=True,
                verbose_name="Time the content item was modified",
                help_text="Time the content item was modified")
    contentid = models.CharField("Subpart Content Id",
                              db_index=True,
                              max_length=32,
                              help_text="The Content ID of sub part")
    value = models.TextField(default="{}",help_text="JSON string containing all the content data")

class ContentMetaPart(AbstractContentMetaPart):
    class Meta:
        unique_together=(("contentMeta","contentid"),)

    lastmodifiedby = models.ForeignKey(User,related_name="contentpart_lastmodifiedby")
    editor = models.ForeignKey(
                    User,
                    null=True,
                    related_name="editing",
                    help_text="The User who is performing the editing")
    editing = models.DateTimeField(
                null=True,
                db_index=True,
                help_text="Time this content was edited")
    contentMeta = models.ForeignKey(ContentMeta)
    
    def saveVersion(self):
        newVersion = self.versions.aggregate(Max("version"))["version__max"]
        if newVersion is None:
            newVersion = 1
        else:
            newVersion = newVersion+1

        return ContentMetaPartVersion.objects.create(
                            contentid=self.contentid, 
                            contentMeta=self.contentMeta, 
                            value=self.value,
                            lastmodifiedby=self.lastmodifiedby,
                            modified=self.modified,
                            currentVersion=self,
                            version=newVersion)
    
class ContentMetaPartVersion(AbstractContentMetaPart):
    class Meta:
        unique_together=(("contentMeta","contentid","version"),)

    contentMeta = models.ForeignKey(ContentMeta, related_name="partv")
    lastmodifiedby = models.ForeignKey(User,related_name="contentpart_v_lastmodifiedby")
    version = models.IntegerField("Version Name",
                               db_index=True,
                               help_text="A Version")
    versioned = models.DateTimeField(
                auto_now=True,
                db_index=True,
                verbose_name="Time the content item was versioned",
                help_text="Time the content item was versioned")
    currentVersion = models.ForeignKey(ContentMetaPart, related_name="versions")
    
class ContentMember(models.Model):
    class Meta:
        unique_together=(("principal","content"),)
        
    CONTENT_MEMBER_ROLE_CHOICES=(
                ('M',"Manager"),
                ('E',"Editor"),
                ('V',"Viewer"),
            )
    principal = models.ForeignKey(Principal)
    content = models.ForeignKey(ContentMeta)
    role = models.CharField(max_length=1, choices=CONTENT_MEMBER_ROLE_CHOICES)    

    
    
class ContentComment(models.Model):
    '''
    Holds comments, on the ContentMeta item
    '''
    content = models.ForeignKey(ContentMeta)
    author = models.ForeignKey(User)
    created = models.DateTimeField(
                auto_now_add=True,
                verbose_name="Time the comment was created",
                help_text="Time the comment was created")
    modified = models.DateTimeField(
                auto_now=True,
                db_index=True,
                verbose_name="Time the comment was modified",
                help_text="Time the comment was modified")
    comment = models.TextField(help_text="Comment")


class ContentStats(object):
    @staticmethod
    def syncFromComments(sender, instance=None, created=False, **kwargs):
        if instance is not None:
            if instance.content is not None:
                c = ContentComment.objects.filter(content=instance.content).count()
                v = json.loads(instance.content.value)
                v['commentCount'] = c
                instance.content.value = json.dumps(v,ensure_ascii=False)
                instance.content.save()
                
        
post_save.connect(ContentStats.syncFromComments, sender=ContentComment)
post_delete.connect(ContentStats.syncFromComments, sender=ContentComment)


class ContentMetaStats(object):
    @staticmethod
    def syncFromContentMeta(sender, instance=None, created=False, **kwargs):
        if instance is not None:
            try:
                profile = Profile.objects.get(user__principal=instance.principal)
                if profile.ugid not in ['everyone','anonymous']:                
                    #### userGroupPrincipals = ObjectPermission.getUserPrincipalsFromUgid(instance.principal.ugid)
                    # If the content count is a count of everything the user can see, then we need to expand this to contain all the prinsipals
                    # However, I think that its a count of things that are in the users library, and hence has the exect principal in the list 
                    # Content members, 
                    # It might not really be necessary to count the actual ContentMeta objects although this prevents a Content
                    # Member appearing twice and being double content, or a removed ContentMeta object being counted.
                    profile.content_count = ContentMeta.objects.filter(id__in=ContentMember.objects.filter(principal=instance.principal).values("content_id"),category='S').count()
                    profile.save()
            except Profile.DoesNotExist:
                pass
                
                
        
post_save.connect(ContentMetaStats.syncFromContentMeta, sender=ContentMember)
post_delete.connect(ContentMetaStats.syncFromContentMeta, sender=ContentMember)


class ContentMetaAdmin(admin.ModelAdmin):
    list_display = ('id', 'contentid', 'ownername', 'lastmodifiedbyname', 'created', 'modified')
    search_fields = ('owner__username','lastmodifiedby__username','value')
    
    def ownername(self,obj):
        return obj.owner.username
    
    def lastmodifiedbyname(self,obj):
        return obj.lastmodiofiedby.username



class ContentCommentAdmin(admin.ModelAdmin):
    list_display = ('authorname','created','modified','comment')
    search_fields = (['author__username','comment'])

    def authorname(self,obj):
        return obj.author.username


admin.site.register(ContentMeta,ContentMetaAdmin)
admin.site.register(ContentComment,ContentCommentAdmin)


    
    

