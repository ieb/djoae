'''
Created on Mar 19, 2012

@author: ieb
'''
from oae.content.models import ContentMeta, ContentMember, ContentMetaPart
from oae.permission.models import ObjectPermission
import logging


class ContentMetaAuthorizationBackend(object):
    '''
    This authorization backend uses the ContentMeta object itself to perform authorization.
    '''
    
    supports_object_permissions=True
    supports_anonymous_user=True
    supports_inactive_user=True

    def get_user(self, user_id):
        return None
    
    def authenticate(self,**credentials):
        return None
    
    def get_all_permissions(self, user_obj, obj=None):
        perms = set()
        if isinstance(obj,ContentMetaPart):
            obj = obj.contentMeta
        if isinstance(obj,ContentMeta):
            if user_obj.is_anonymous():
                if obj.view == 'A': # Anon can view
                    return set(['content.view'])
                return set()
            if obj.view == 'E': # Everyone can view
                perms = set(['content.view'])
            principals = ObjectPermission.getUserPrincipals(user_obj)
            try:
                for contentMember in ContentMember.objects.filter(content=obj,principal__in=principals):
                    if contentMember.role == 'M':
                        perms.update(['content.view','content.edit','content.manage'])
                    elif contentMember.role == 'E':
                        perms.update(['content.view','content.edit'])
                    elif contentMember.role == 'V':
                        perms.update(['content.view'])
            except:
                pass
            logging.info("User %s has permissions %s on  %s " % (user_obj.username, perms, obj))
        return perms

    def has_perm(self, user_obj, perm, obj=None):
        return self.has_perms(user_obj, [ perm ], obj)
    
    def has_perms(self, user_obj, perms, obj=None):
        logging.info("Checking Content backend for %s %s %s " % (user_obj,perms,obj))
        if isinstance(obj,ContentMeta) or isinstance(obj,ContentMetaPart):
            if hasattr(user_obj,"_authorized") and  user_obj._authorized:
                return True
            if user_obj.is_staff:
                return True
            allperms = self.get_all_permissions(user_obj, obj)
            return set(perms).issubset(allperms)
        return False
    