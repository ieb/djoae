'''
Created on May 20, 2012

@author: ieb
'''
from haystack import indexes
from oae.content.models import ContentMeta
import datetime
from oae.content.content_manager import ContentManager
import os
from django.template.context import Context
import mimetypes
from django.utils import simplejson as json
from django.db.models import signals
from oae.content import content_signals
import logging
from django.template import loader


class ContentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True)
    modified = indexes.DateTimeField(model_attr='modified')
    created = indexes.DateTimeField(model_attr='created')
    category = indexes.CharField(model_attr='category')
    owner = indexes.CharField(model_attr='owner__principal__ugid')
    contentid = indexes.CharField(model_attr='contentid')
    contenthash = indexes.CharField(model_attr='contenthash')
    #tags = indexes.MultiValueField(model_attr="tags__name")
    mimetype = indexes.CharField()
    filename = indexes.CharField()
    

    def get_model(self):
        return ContentMeta

    def _determinMimeType(self, content):
        try:
            if content.mimetype is not None:
                return content.mimetype
            return content._metadata['_mimeType']
        except:
            try:
                filename = content._metadata['sakai:pooled-content-file-name']
                mimetypes.init()
                return mimetypes.guess_type(filename)
            except:
                None
    
    def prepare(self, obj):
        logging.error("Preparing for %s " % obj)
        data = super(ContentIndex, self).prepare(obj)

        poolFile = ContentManager.getPoolFile(obj)
        if poolFile is not None:
            if os.path.exists(poolFile):
                textContents = ContentManager.getPoolFileText(poolFile)
                extracted_data = ""
                info_data = ""
                if os.path.exists(textContents):
                    f = open(textContents)
                    extracted_data = f.read()
                    f.close()
                info = ContentManager.getPoolFileInfo(poolFile)
                if os.path.exists(info):
                    f = open(info)
                    infoRaw = f.read()
                    info_data = json.loads(infoRaw)
                    f.close()
                t = loader.select_template(('search/indexes/content/contentmeta.txt', ))
                data['text'] = t.render(Context({'object': obj,
                                                 'extracted': extracted_data,
                                                 'info' : info_data }))
        else:
            # Do something about SakaiDocs
            logging.error("Dont currently index Sakai Docs")
            pass
        obj._metadata = json.loads(obj.value)
        data['mimetype'] = self._determinMimeType(obj)
        data['filename'] = obj._metadata['sakai:pooled-content-file-name']
        return data
    

    def index_queryset(self):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(modified__lte=datetime.datetime.now())
    
    
    def _setup_save(self):
        """ Wire the save into the post prepare operation after the preview processor has completed """
        content_signals.post_prepare.connect(self.update_object, sender=self.get_model())

    def _setup_delete(self):
        """ Wire the delete in the Database delete operation """
        signals.post_delete.connect(self.remove_object, sender=self.get_model())

    def _teardown_save(self):
        content_signals.post_prepare.disconnect(self.update_object, sender=self.get_model())

    def _teardown_delete(self):
        signals.post_delete.disconnect(self.remove_object, sender=self.get_model())
