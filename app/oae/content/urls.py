'''
Created on Mar 12, 2012

@author: ieb
'''
from django.conf.urls.defaults import patterns, url
from oae.content.views import doMembers,\
    doModifyAce, doActivity, doContent, doContentStream, doSaveContent,\
    doContentVersions
from oae.content.comment_views import doComments


urlpatterns = patterns('',
    url(r'^(.*?)/(.*?)\.infinity\.json$', doContent, name="do content infinity with path"),
    url(r'^(.*?)\.infinity\.json$', doContent, name="do content infinity"),
    url(r'^(.*?)/(.*?)\.activity\.json$', doActivity, name="do content activity"),
    url(r'^(.*?)\.activity\.json$', doActivity, name="do content activity"),
    url(r'^(?P<poolId>.*?)\.comments/(?P<commentId>.*?)$', doComments),
    url(r'^(?P<poolId>.*?)/comments/(?P<commentId>.*?)$', doComments),
    url(r'^(.*?)\.comments$', doComments),
    url(r'^(.*?)\.members\.html$', doMembers),
    url(r'^(.*?)\.members\.json$', doMembers),
    url(r'^(.*?)\.modifyAcs\.html$', doModifyAce),
    url(r'^(.*?)/(.*?)\.versions\.json$', doContentVersions, name="content versions with path"),
    url(r'^(.*?)/(.*?)\.save\.json$', doSaveContent, name="savecontent with path"),
    url(r'^(.*?)/(.*?)\.json$', doContent, name="docontent with path"),
    url(r'^(.*?)\.versions\.json$', doContentVersions, name="content versions no path"),
    url(r'^(.*?)\.save\.json$', doSaveContent, name="savecontent no path"),
    url(r'^(.*?)\.json$', doContent, name="do content no path"),
    url(r'^(.*?)/(.*?)$', doContentStream),
    url(r'^(.*?)$', doContentStream),
)