'''
Created on Mar 20, 2012

@author: ieb
'''
from django.test.testcases import TestCase
import logging
from oae.common.util import Hash
from oae.common.tests import OAETestClient
from django.utils import simplejson as json
import random
import filecmp

class ContentTest(TestCase):
    
    def setUp(self):
        self.client = OAETestClient(self)
        self.client.createAdmin()
        self.client.loginAdmin()
        self.un1, self.pw1 = self.client.createUser()
        self.un2, self.pw2 = self.client.createUser()
        self.client.logout()

    def testEncoding(self):
        testSet = "1"
        logging.info("Encoding %s into %s " % (testSet, Hash.hash(testSet) ))
        logging.info("Type1 ID         %s " % (Hash.genCompactUUID()))
        
    def _createFile(self, name, size):
        f = open(name,"wb")
        for i in range(0,size): #@UnusedVariable
            f.write(random.choice("ABCDEFGHIJKLMNOPqrstubwxyZ"))
        f.close()
            

    def testFileUplaod(self):
        
        # Just login and upload the file
        fileNames = [ "testfile1.xls", "testfile2.ppt", "testfile3.bin"]
        mimeTypes = ["application/vnd.ms-excel", "application/vnd.ms-powerpoint", "application/octet-stream"]
        size = 1000
        of = []
        for f in fileNames:
            self._createFile("/tmp/%s" % f, size)
            size = size+1000
            of.append(open("/tmp/%s" % f, "rb"))
        postdata = {'datafile': of}
        response = self.client.post('/system/pool/createfile',postdata,status=302)
        logging.info("Response to upload was %s " % response)
        for fo in of:
            fo.close()

        self.client.login(self.un1, self.pw1)
        meJson1 = self.client.checkMe(self.un1)
        userid1 = meJson1['userid']
        self.client.login(self.un2, self.pw2)
        meJson2 = self.client.checkMe(self.un2)
        userid2 = meJson2['userid']
        self.client.login(self.un1, self.pw1)
        meJson1 = self.client.checkMe(self.un1)
        
        of = []
        for f in fileNames:
            of.append(open("/tmp/%s" % f, "rb"))
        
        postdata = {'datafile': of}
        response = self.client.post('/system/pool/createfile',postdata)
        logging.info("Response to upload was %s " % response)
        rjson = json.loads(response.content)
        self.assertEqual(len(rjson),3,"Expected 3 files to have been processed")
        for fo in of:
            fo.close()

        size = 1000
        for i in range(0,len(fileNames)):
            f = fileNames[i]
            self.assertTrue(f in rjson, "%s missing" % f)
            self.assertTrue("poolId" in rjson[f])
            poolId = rjson[f]["poolId"]
            self.assertTrue("item" in rjson[f])
            item = rjson[f]['item']
            self.assertTrue("_length" in item, "%s appears to have no size" % f)
            self.assertEqual(size, item["_length"], "%s appears to be the worong size" % f)
            self.assertTrue("_mimeType" in item)
            self.assertEqual(mimeTypes[i], item["_mimeType"], "%s appears to be the worong mimetype %s " % (f,mimeTypes[i]))
            size = size+1000
            response = self.client.get("/p/%s.json" % poolId)
            logging.info("Response to download meta was %s " % response)
            response = self.client.get("/p/%s" % poolId)
            fo = open("/tmp/test", "wb")
            fo.write(response.content)
            fo.close()
            self.assertTrue(filecmp.cmp("/tmp/test", "/tmp/%s" % f), "Files are different")
            response = self.client.get("/p/%s/%s" % (poolId, f))
            fo = open("/tmp/test", "wb")
            fo.write(response.content)
            fo.close()
            self.assertTrue(filecmp.cmp("/tmp/test", "/tmp/%s" % f), "Files are different")
            

            # do activity as a manager  
            response = self.client.post("/p/%s.activity.json" % poolId, { 
                                    'sakai:activity-appid' : "content", 
                                    'sakai:activity-templateid' : "template1",
                                    'sakai:activityMessage' : "_A_MESSAGE_TO_YOU_RUDY_"  
                                    })

            # make the other user a viewer
            mjson = json.loads(self.client.get("/p/%s.members.json" % poolId).content)
            self.assertEqual(userid1,mjson["managers"][0]['userid'],"Didnt find the current user as a manger of %s " % poolId)
            self.assertEqual(0,len(mjson["editors"]),"Should have been no editors for %s " % poolId)
            self.assertEqual(0,len(mjson["viewers"]),"Should have been no editors for %s " % poolId)
            response = self.client.post("/p/%s.members.html" % poolId, { ":viewer" : [ userid2 ] })
            logging.info("Response to change members meta was %s " % response)
            memberupdate = json.loads(response.content)
            self.assertEqual("Ok",memberupdate['status'])
            mjson = json.loads(self.client.get("/p/%s.members.json" % poolId).content)
            self.assertEqual(1,len(mjson["managers"]),"Should have been 1 manager for %s " % poolId)
            self.assertEqual(userid1,mjson["managers"][0]['userid'],"Didnt find the current user as a manger of %s " % poolId)
            self.assertEqual(0,len(mjson["editors"]),"Should have been no editors for %s " % poolId)
            self.assertEqual(1,len(mjson["viewers"]),"Should have been 1 manager for %s " % poolId)
            self.assertEqual(userid2,mjson["viewers"][0]['userid'],"Didnt find the other user as a viewer of %s " % poolId)
            response = self.client.post("/p/%s.members.html" % poolId, { ":viewer@Delete" : [ userid2 ], ":editor" : [ userid2 ] })
            memberupdate = json.loads(response.content)
            self.assertEqual("Ok",memberupdate['status'])
            mjson = json.loads(self.client.get("/p/%s.members.json" % poolId).content)
            self.assertEqual(1,len(mjson["managers"]),"Should have been 1 manager for %s " % poolId)
            self.assertEqual(userid1,mjson["managers"][0]['userid'],"Didnt find the current user as a manger of %s " % poolId)
            self.assertEqual(1,len(mjson["editors"]),"Should have been no editors for %s " % poolId)
            self.assertEqual(userid2,mjson["editors"][0]['userid'],"Didnt find the other user as a editor of %s " % poolId)
            self.assertEqual(0,len(mjson["viewers"]),"Should have been 1 manager for %s " % poolId)
            
            response = self.client.post("/p/%s.members.html" % poolId, { ":editor@Delete" : [ userid2 ], ":manager" : [ userid2 ] })
            memberupdate = json.loads(response.content)
            self.assertEqual("Ok",memberupdate['status'])
            mjson = json.loads(self.client.get("/p/%s.members.json" % poolId).content)
            self.assertEqual(2,len(mjson["managers"]),"Should have been 2 manager for %s " % poolId)
            self.assertEqual(userid1,mjson["managers"][0]['userid'],"Didnt find the current user as a manger of %s " % poolId)
            self.assertEqual(userid2,mjson["managers"][1]['userid'],"Didnt find the otehr user as a manger of %s " % poolId)
            self.assertEqual(0,len(mjson["editors"]),"Should have been no editors for %s " % poolId)
            self.assertEqual(0,len(mjson["viewers"]),"Should have been 1 manager for %s " % poolId)


            response = self.client.post("/p/%s.members.html" % poolId, { ":manager@Delete" : [ userid1 ], ":viewer" : [ userid1 ]  })
            memberupdate = json.loads(response.content)
            self.assertEqual("Ok",memberupdate['status'])
        
            response = self.client.post("/p/%s.members.html" % poolId, { ":manager" : [ userid1 ] }, status=403)
            
            mjson = json.loads(self.client.get("/p/%s.members.json" % poolId).content)
            self.assertEqual(1,len(mjson["managers"]),"Should have been 1 manager for %s " % poolId)
            self.assertEqual(userid2,mjson["managers"][0]['userid'],"Didnt find the other user as a manger of %s " % poolId)
            self.assertEqual(0,len(mjson["editors"]),"Should have been no editors for %s " % poolId)
            self.assertEqual(1,len(mjson["viewers"]),"Should have been 1 viewer for %s " % poolId)
            self.assertEqual(userid1,mjson["viewers"][0]['userid'],"Didnt find the other user as a viewer of %s " % poolId)
            
            
            # do activity as a viewer
            response = self.client.post("/p/%s.activity.json" % poolId, { 
                                    'sakai:activity-appid' : "content", 
                                    'sakai:activity-templateid' : "template1",
                                    'sakai:activityMessage' : "_A_MESSAGE_TO_YOU_RUDY_"  
                                    })
            
            # do comment as a viewer
            response = self.client.post("/p/%s.comments" % poolId, { 
                                    'comment' : "A Comment about nothin in particular"  
                                    },status=201)
            commentID = json.loads(response.content)['cid']
            response = self.client.post("/p/%s/comments/%s" % (poolId, commentID), { 
                                    'comment' : "A Comment about nothing in particular, udapted"  
                                    })
            
            response = self.client.get("/p/%s.comments" % poolId)
            self.assertEqual(1, len(json.loads(response.content)['comments']))

            # do activity as a viewer
            response = self.client.delete("/p/%s.comments" % poolId, {
                                                'commentId' : commentID 
                                            })
            self.assertEqual(204, response.status_code, "Didnt delete comment")

            response = self.client.get("/p/%s.comments" % poolId)
            self.assertEqual(0, len(json.loads(response.content)['comments']))
            
            # If I am a viewer, I can remove myself
            response = self.client.post("/p/%s.members.html" % poolId, { ":viewer@Delete" : [ userid1 ]  })
            memberupdate = json.loads(response.content)
            self.assertEqual("Ok",memberupdate['status'])

            # do activity as a nobody
            response = self.client.post("/p/%s.activity.json" % poolId, { 
                                    'sakai:activity-appid' : "content", 
                                    'sakai:activity-templateid' : "template1",
                                    'sakai:activityMessage' : "_A_MESSAGE_TO_YOU_RUDY_"  
                                    }, 403)
            
            
class TagTest(TestCase):
    
    
    def setUp(self):
        self.client = OAETestClient(self)
        self.client.createAdmin()
        self.client.loginAdmin()
        self.un1, self.pw1 = self.client.createUser()
        self.un2, self.pw2 = self.client.createUser()
        self.client.logout()


    def testDirectory(self):
        response = self.client.get("/tags/directory.tagged.json")
        logging.error(response.content)