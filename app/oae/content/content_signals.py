'''
Created on May 21, 2012

@author: ieb
'''
from django.dispatch.dispatcher import Signal


post_prepare = Signal(providing_args=["instance"])

notify_prepare = Signal(providing_args=["content_ids"])
