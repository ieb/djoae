'''
Created on Mar 12, 2012

@author: ieb
'''
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotAllowed, HttpResponse, \
    HttpResponseNotFound, HttpResponseForbidden
from oae.common.util import JSON_CT, Search, MultiEncoder
from oae.content.encoder import CommentEncoder
from oae.content.models import ContentMeta, ContentComment
import django.utils.simplejson as json
import logging
import traceback
from oae.common.xact import xact
from django.views.decorators.cache import never_cache

JSON_INDENT = ( settings.JSON_INDENT )

@never_cache  # Cache at a lower level with invalidation.
def doComments(request, poolId, commentId=None):
    if "POST" == request.method:
        return _updateComment(request, poolId, commentId)
    elif "GET" == request.method:
        return _listComments(request, poolId)
    elif "DELETE" == request.method:
        return _deleteComment(request, poolId)
    return HttpResponseNotAllowed("GET","POST","DELETE")
     
@login_required
@xact
def _deleteComment(request, poolId):
    if "DELETE" == request.method:
        if "commentId" in request.REQUEST:
            ContentComment.objects.filter(id=int(request.REQUEST['commentId']),content__contentid=poolId,author=request.user).delete()
            return HttpResponse(status=204)
        return HttpResponseNotFound()
    return HttpResponseNotAllowed("DELETE")
    

@login_required
@xact
def _updateComment(request, poolId, commentId = None):
    if "POST" == request.method:
        try:
            if commentId is None and 'commentId' not in request.POST:
                contentMeta = ContentMeta.objects.get(contentid=poolId)
                if request.user.has_perm("content.view",contentMeta):                
                    comment = ContentComment.objects.create(content=contentMeta,author=request.user,comment=request.POST['comment'])
                    return HttpResponse(json.dumps({'commentId' : "%s/comments/%s" % (poolId, comment.id), "cid": comment.id}, indent=JSON_INDENT,ensure_ascii=False),status=201,content_type=JSON_CT)            
                return HttpResponseForbidden()
            else:
                if commentId is None:
                    commentId = request.POST['commentId']
                try:
                    comment = ContentComment.objects.filter(id=int(commentId),content__contentid=poolId,author=request.user)[0]
                    comment.value = request.POST['comment']
                    comment.save()
                    return HttpResponse(json.dumps({'commentId' : "%s/comments/%s" % (poolId, comment.id), "cid": comment.id},indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)            
                except:
                    logging.error(traceback.format_exc())
                    return HttpResponseForbidden()
        except ContentMeta.DoesNotExist:
            return HttpResponseNotFound()
    return HttpResponseNotAllowed("POST")
 
def _listComments(request, poolId):
    if "GET" != request.method:
        return HttpResponseNotAllowed("POST")
    try:
        contentMeta = ContentMeta.objects.get(contentid=poolId)
    except ContentMeta.DoesNotExist:
        return HttpResponseNotFound()
    if request.user.has_perm("content.view",contentMeta):
        search = Search()                
        comments = search.dbSearch(request, ContentComment, 
                                ContentComment.objects.filter(
                                        content__contentid=poolId
                                        ).order_by(search.getSortOn(request)))
        return HttpResponse(json.dumps({
                    'comments' : comments['results']
                    },cls=MultiEncoder, encoder_classes=CommentEncoder.depend(),indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)
    return HttpResponseForbidden()
    
    