from django.conf import settings
from django.db import models
from django.http import HttpResponseNotAllowed, HttpResponseBadRequest, \
    HttpResponse
from oae.common.util import Search, Request, JSON_CT, MultiEncoder
from oae.user.encoder import ProfileEncoder
from oae.user.models import Profile
import django.utils.simplejson as json
from oae.content.models import ContentMeta
from oae.content.encoder import ContentEncoder
from django.views.decorators.cache import never_cache


JSON_INDENT = (settings.JSON_INDENT)

@never_cache  # Cache at a lower level with invalidation.
def doSearchUserGroups(request):
    '''
    /var/search/usersgroups.infinity.json?page=0&items=15&q=ian&_charset_=utf-8&_=1330927418053 HTTP/1.1" 404 1899
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    
    
    q = Request.get(request.REQUEST,"q")
    if q is None:
        return HttpResponseBadRequest()
    
    resultMap = Search().dbSearch(request, Profile, 
                    Profile.objects.filter(models.Q(search__icontains=q)).order_by('search')
                    .prefetch_related("group__usergroupdata"))
    return HttpResponse(json.dumps(resultMap,
                                   cls=MultiEncoder,
                                   encoder_classes=ProfileEncoder.depend(),
                                   indent=JSON_INDENT,
                                   ensure_ascii=False),
                        content_type=JSON_CT)

@never_cache  # Cache at a lower level with invalidation.
def doSearchUsers(request):
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    try:
        q = Profile.objects.filter(type='U')
        if 'q' in request.GET and request.GET['q'] != '*':
            q = q.filter(search__icontains=request.GET['q'])
        results = Search().dbSearch(request, Profile, q.order_by('search')
                                    .prefetch_related("group__usergroupdata"))
        return HttpResponse(json.dumps(results,
                                       cls=MultiEncoder,
                                       encoder_classes=ProfileEncoder.depend(),
                                       indent=JSON_INDENT,
                                       ensure_ascii=False),
                            content_type=JSON_CT)
    except KeyError:
        return HttpResponseBadRequest()

@never_cache  # Cache at a lower level with invalidation.
def doSearchGroups(request):
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    try:
        q = Profile.objects.filter(type='G')
        if 'category' in request.GET and request.GET['category'] != '*':
            q = q.filter(category=request.GET['category'])
        if 'q' in request.GET and request.GET['q'] != '*':
            q = q.filter(search__icontains=request.GET['q'])
        results = Search().dbSearch(request, Profile, q.order_by('search').prefetch_related("group__usergroupdata"))
        return HttpResponse(json.dumps(results,
                                       cls=MultiEncoder,
                                       encoder_classes=ProfileEncoder.depend(),
                                       indent=JSON_INDENT,
                                       ensure_ascii=False),
                            content_type=JSON_CT)
    except KeyError:
        return HttpResponseBadRequest()
    
@never_cache  # Cache at a lower level with invalidation.
def doSearchGroupMembers(request):
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    try:
        group = request.GET['group']
        q = Profile.objects.filter(models.Q(user__principal__groupmember__group__name=group)|
                                   models.Q(user__principal__groupmember__group__parent__group__name=group)) # members of all groups that have a parent group matching the name
        results = Search().dbSearch(request, Profile, q.order_by('search').prefetch_related("group__usergroupdata"))
        request._dump_sql=True
        return HttpResponse(json.dumps(results,
                                       cls=MultiEncoder,
                                       encoder_classes=ProfileEncoder.depend(),
                                       indent=JSON_INDENT,
                                       ensure_ascii=False),
                            content_type=JSON_CT)
    except KeyError:
        return HttpResponseBadRequest()
    
@never_cache  # Cache at a lower level with invalidation.
def doPublicRandomContent(request):
    '''
    tag=&type=c might be relevant
    @param request:
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    try:
        q = ContentMeta.objects.filter(view='A') # members of all groups that have a parent group matching the name
        results = Search().dbSearch(request, Profile, q.order_by('?'))
        request._dump_sql=True
        return HttpResponse(json.dumps(results,
                                       cls=MultiEncoder,
                                       encoder_classes=ContentEncoder.depend(),
                                       indent=JSON_INDENT,
                                       ensure_ascii=False),
                            content_type=JSON_CT)
    except KeyError:
        return HttpResponseBadRequest()
    
    