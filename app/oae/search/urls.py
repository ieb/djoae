from django.conf.urls.defaults import patterns, url
from oae.search.views import doSearchUsers, doSearchUserGroups, doSearchGroups,\
    doSearchGroupMembers, doPublicRandomContent
from oae.tag.views import doTaggedSearch

urlpatterns = patterns('',
    url(r'^usersgroups.*', doSearchUserGroups),
    url(r'^users.*', doSearchUsers),
    url(r'^groups.*', doSearchGroups),
    url(r'^groupmembers-all.*', doSearchGroupMembers),
    url(r'^bytag\.json$', doTaggedSearch),
    url(r'^public/random-content.json', doPublicRandomContent)
)

    