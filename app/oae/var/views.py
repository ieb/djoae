import logging
import os
import django.utils.simplejson as json
from oae.common.util import Response, Properties, TreeBuilder, FileSystemContent,\
    JSON_CT
from oae.content.models import ContentMeta
import subprocess
from django.core.files.uploadedfile import TemporaryUploadedFile
from mimetypes import MimeTypes
from django.http import HttpResponseNotFound, HttpResponse
import re
from oae.user.models import PrivateUserGroupData, PublicUserGroupData
from django.conf import settings
from oae.content.content_manager import ContentManager
from oae.common.xact import xact
from oae.common.gitrevision import GIT_REVISION, GIT_BRANCH, UI_GIT_REVISION, UI_GIT_BRANCH
from django.db import models
from django.views.decorators.cache import cache_control, never_cache

CONVERT_IMAGE_COMMAND = ( settings.CONVERT_IMAGE_BINARY )

TEMP_ROOT, DOCUMENT_ROOT, \
 JSON_INDENT, ALLOW_QUERY_BY_USERNAME = (
                settings.TEMP_ROOT, 
                settings.DOCUMENT_ROOT, 
                settings.JSON_INDENT,
                settings.ALLOW_QUERY_BY_USERNAME)

@cache_control(public=True)
def doWidgets(request):    
    cachedWidgets = "%s/widgets.json" % TEMP_ROOT
    if "r" in request.GET:
        if os.path.exists(cachedWidgets):
            os.remove(cachedWidgets)
    if os.path.exists(cachedWidgets):
        return FileSystemContent.static(request, document_root=TEMP_ROOT, path="widgets.json" )
    # list the content of uiroot/devwidgets
    # if a folder contains config.json, parse it and add it to a map using the id as the key
    # dump the file to widgets.json
    # invoke static
    wc = {}
    widgets = os.listdir("%s/devwidgets" % DOCUMENT_ROOT)
    for widget in widgets:
        config ="%s/devwidgets/%s/config.json" % (DOCUMENT_ROOT,widget)
        if os.path.exists(config):
            w = open(config, "rb")
            try:
                c = json.load(w)
                w.close()
                if "id" in c:
                    wc[c['id']] = c
                else:
                    logging.error("Widget config at %s has not id, ignored " % config)
            except:
                logging.info("Error loading %s ignoring " % config)
    parent =os.path.dirname(cachedWidgets)
    if not os.path.exists(parent):
        os.makedirs(parent)
    co = open(cachedWidgets, "wb")
    co.write("define(")
    co.write(json.dumps(wc,indent=2))
    co.write(");")
    co.close()
    return FileSystemContent.static(request, document_root=TEMP_ROOT, path="widgets.json" )

@xact
def doCrop(request):
    '''
    Request contains
    'dimensions': [u'256x256']
    'img': [u'/~sVmwoI3iEeGrpgAjMsoUZA/public/profile/tmp1335256090350.jpg']
    'height': [u'499'], 
    u'width': [u'499'], 
    u'_charset_': [u'utf-8'], 
    u'y': [u'0'], 
    u'x': [u'0'], 
    u'save': [u'/~sVmwoI3iEeGrpgAjMsoUZA/public/profile']}>,

    @param request:
    '''
    image = request.POST['img']
    savelocation = request.POST['save']
    dimensions = request.POST['dimensions']
    cropGeom = "%sx%s+%s+%s" % ( request.POST['width'], request.POST['height'], request.POST['x'],request.POST['y'])
    contentid = ContentManager.hashPath(image)
    try:
        content = ContentMeta.objects.get(contentid=contentid)
    except ContentMeta.DoesNotExist:
        return HttpResponseNotFound()
    
    # we cant crop directly into the content store as this is a new file.
    poolFileBase = ContentManager.getPoolFile(content)
    filename = os.path.basename(image)
    targetFileName = "%s_%s" % ( dimensions, filename)
    targetContentItemName = "%s/%s" % ( savelocation, targetFileName)
    mimeTypes = MimeTypes()
    targetUploadFile = TemporaryUploadedFile(targetFileName, content_type=mimeTypes.guess_extension(filename), size=0, charset=None)
    tempConvertFile = targetUploadFile.temporary_file_path()
    subprocess.call([
                     CONVERT_IMAGE_COMMAND,
                     poolFileBase,
                     "-crop",
                     cropGeom,
                     "-resize",
                     dimensions,
                     tempConvertFile
                     ])
    targetUploadFile.size = os.path.getsize(tempConvertFile)
    
    
    
    filename, meta = ContentManager(request.user).createFile(targetUploadFile, initial_status="D", full_path=targetContentItemName, category='H')
    targetUploadFile.close()
    # the above was relatively neat, this next bit is Ugly
    privateProfileRE = re.compile(r"^/~(?P<ugid>.*?)/private/(?P<path>.*?)$")
    publicProfileRE = re.compile(r"^/~(?P<ugid>.*?)/public/(?P<path>.*?)$")
    privMatch = privateProfileRE.match(targetContentItemName)
    pubMatch = publicProfileRE.match(targetContentItemName)
    if privMatch is not None:
        
        ugid = privMatch.group('ugid')
        path = privMatch.group('path')
        modelClass = PrivateUserGroupData
        
    elif pubMatch is not None:
        ugid = pubMatch.group('ugid')
        path = pubMatch.group('path')
        modelClass = PublicUserGroupData
    else:        
        modelClass = None
    
    if modelClass is not None:
        try:
            if ALLOW_QUERY_BY_USERNAME:
                model = modelClass.objects.get(models.Q(ugid=ugid)|
                                               models.Q(name=ugid))
            else:
                model = modelClass.objects.get(models.Q(ugid=ugid)|
                                               models.Q(name=ugid,type='G'))
            v = json.loads(model.value)
            Properties.extract(path, v, create=True).update({ '_iscontent' : True, '_contentid' : meta['poolId']  })
            model.value = json.dumps(v)
            model.save()
        except modelClass.DoesNotExist:
            logging.error("User %s Unable to locate model of type %s for update at for %s after cropping " %
                           (request.user.username, modelClass, ugid))
    else:
        logging.error("No User Object to update for target path %s " % savelocation)

    return Response.Ok()


@cache_control(public=True, max_age=3600)
def doVar(request, path):
    tempVar = "%s/%s" % ( TEMP_ROOT, path )
    if os.path.exists(tempVar):
        return FileSystemContent.static(request, document_root=TEMP_ROOT, path=path)
    tb = TreeBuilder("%s/var" % DOCUMENT_ROOT, TEMP_ROOT)
    return tb.getResponse(request, path)


@cache_control(public=True, max_age=3600)
def doGetVersion(request):
    return HttpResponse(json.dumps({
        "sakai:ux-version": "%s %s" % ( UI_GIT_REVISION, UI_GIT_BRANCH )
            },indent=JSON_INDENT),content_type=JSON_CT)

@cache_control(public=True, max_age=3600)
def doDJOAEVersion(request):
    return HttpResponse(json.dumps({
        "sakai:nakamura-version": "DJOAE %s %s" % ( GIT_REVISION, GIT_BRANCH )
            },indent=JSON_INDENT),content_type=JSON_CT)
    