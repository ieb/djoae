from django.conf.urls.defaults import patterns, url
from oae.var.views import doCrop, doWidgets, doVar, doGetVersion, doDJOAEVersion

urlpatterns = patterns('',
    url(r'^widgets\.json', doWidgets),
    url(r'^image/cropit', doCrop),
    url(r'scm-version.json', doDJOAEVersion),
    url(r'ux-version/ux-version.json', doGetVersion),
    url(r'^(?P<path>.*)$', doVar),
)