from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.conf import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
admin.autodiscover()

DOCUMENT_ROOT = settings.DOCUMENT_ROOT
#from haystack.views import SearchView, search_view_factory, FacetedSearchView
#from haystack.forms import FacetedSearchForm
#from haystack.query import SearchQuerySet

#sqs = SearchQuerySet().facet("email")

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'timetables.views.home', name='home'),
    # url(r'^timetables/', include('timetables.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    #url(r'^$', defaultView , name='home'),
    # Although these are here, they must only be used for batch processes and in development.
    # In production the files must be served direct from apache httpd.
    # If you add any files here, please update apache/oae.config
    #
    url(r'^$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "index.html"},  name = "home"  ),
    url(r'^me$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "me.html"},  name = "me"  ),
    url(r'^index\.html$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "index.html"},  name = "index"  ),
    url(r'^index$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "index.html"},  name = "indexdirect"  ),
    url(r'^search$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "search.html"},  name = "search"  ),
    url(r'^register', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "create_new_account.html"},  name = "register"  ),
    url(r'^categories$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "allcategories.html"},  name = "categories"  ),
    url(r'^category$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "category.html"},  name = "categories"  ),
    url(r'^create$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "createnew.html"},  name = "create"  ),
    url(r'^content', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "content_profile.html"},  name = "create"  ),
    url(r'^favicon.ico$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT, "path" : "favicon.ico"},  name = "favicon.ico"  ),
    url(r'^tests/(?P<path>.*)$', 'oae.system.views.static', {'document_root': "%s/tests" % DOCUMENT_ROOT}),
    url(r'^dev/(?P<path>.*)$', 'oae.system.views.static', {'document_root': "%s/dev" % DOCUMENT_ROOT}),
    url(r'^devwidgets/(?P<path>.*)$', 'oae.system.views.static', {'document_root': "%s/devwidgets" % DOCUMENT_ROOT}),
    
    # Non static urls
#    url(r'^esearch/', include('haystack.urls')),
#    url(r'^fsearch/$', FacetedSearchView(form_class=FacetedSearchForm, searchqueryset=sqs), name='haystack_search'),   
    url(r'^system/pool/', include('oae.content.system_pool_urls') ),
    url(r'^system/userManager/', include('oae.user.usermanager_urls') ),
    url(r'^system/messageManager/', include('oae.message.messagemanager_urls') ),
    url(r'^system/', include('oae.system.urls')),
    url(r'^tags/directory', include('oae.tag.urls')),
    url(r'^var/search/activity/', include('oae.activity.search_urls')),
    url(r'^var/search/pool/', include('oae.content.search_pool_urls')),
    url(r'^var/search/', include('oae.search.urls')),
    url(r'^var/contacts/', include('oae.contact.var_urls')),
    url(r'^var/message/', include('oae.message.var_urls')),
    url(r'^var/', include('oae.var.urls')),
    url(r'^logout', "oae.system.login.doLogout"),
    url(r'^~', include('oae.user.storage_urls')),
    url(r'^p/', include('oae.content.urls')),
    url(r'^test/perms/(.*?)/(.*)$', 'oae.permission.views.createLotsOfContent'),
    url(r'^test/perms2/(.*?)/(.*)$', 'oae.permission.views.createLotsOfContent2'),
    url(r'^X(?P<path>.*)$', 'oae.system.renderer.renderhtml')
    

)

