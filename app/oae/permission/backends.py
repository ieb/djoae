'''
Created on Mar 15, 2012

@author: ieb
'''
from oae.permission.models import ObjectPermission
import logging

## Auth backend configuration for User and Group. 
## First match always wins. Not certain about get all permissions.


class ObjectPermissionAuthorizationBackend(object):
    
    supports_object_permissions=True
    supports_anonymous_user=True
    
    def get_user(self, user_id):
        return None
    
    def authenticate(self,**credentials):
        return None
    
    def get_all_permissions(self, user_obj, obj=None):
        return ObjectPermission.get_all_permissions(user_obj, obj)

    def has_perm(self, user_obj, perm, obj=None):
        return self.has_perms(user_obj, [ perm ], obj)
    
    def has_perms(self, user_obj, perms, obj=None):
        logging.info("Checking Object Permission backend for %s %s %s " % (user_obj,perms,obj))
        if obj is None:            
            return False  # we only auth if there is an object
        if hasattr(user_obj,"_authorized") and  user_obj._authorized:
            return True
        allperms = self.get_all_permissions(user_obj, obj)
        return set(perms).issubset(allperms)


