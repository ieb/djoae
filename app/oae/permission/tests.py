'''
Created on Mar 16, 2012

@author: ieb
'''

from django.contrib.auth.models import User, Group
from django.test import TestCase
from oae.common.util import Hash
from oae.content.models import ContentMeta, ContentMember
from oae.message.models import Message, MessageBox
from oae.permission.models import ObjectType, ObjectPermission, Principal
import logging
import time
from oae.tag.models import Tag

class ObjectPermissionsTest(TestCase):
    def testPermissions(self):
        # create some users, which shhould create some principals
        nusers = 10
        nitems = 10
        for i in range(0,nusers):
            User.objects.create_user("user%s" % i, "use%s@example.comn" % i, "test")

        ObjectType.objects.create(name=str(type(Message())),app_label="message")
        ObjectType.objects.create(name=str(type(ContentMeta())),app_label="content")
        ObjectType.objects.create(name=str(type(Tag())),app_label="content")
        ObjectType.objects.create(name=str(type(MessageBox())),app_label="content")
        ObjectType.objects.create(name=str(type(User())),app_label="user")
        ObjectType.objects.create(name=str(type(Group())),app_label="user")
        for ot in ObjectType.objects.all():
            logging.error("Object types %s %s " % (ot.app_label, ot.name))

        ObjectType.objects.get(name=str(type(Message())))
        ObjectType.objects.get(name=str(type(ContentMeta())))
        ObjectType.objects.get(name=str(type(Tag())))
        ObjectType.objects.get(name=str(type(MessageBox())))
        ObjectType.objects.get(name=str(type(User())))
        ObjectType.objects.get(name=str(type(Group())))
        
        tot = nusers*nitems
        n = 0
        tlast = time.time()
        logging.error("Will create %s conent items " % tot)
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)
            
            ## Create some ContentMeta items
            for j in range(0,nitems):
                contentMeta = ContentMeta.objects.create(owner=user, lastmodifiedby=user, value="{}", mimetype="text/plain")
                contentMeta.contentid = Hash.genCompactUUID()
                card = (i*j)%(nusers*nitems)
                contentMeta.contenthash = Hash.hash("cardinality%s" % card,"user" )
                contentMeta.save()
                ## Give the current user manage rights,
                ## Give the 2 next users manage rights
                ## Give the following 5 view rights
                tomanagePrincipals = [Principal.objects.get(user=user)]
                for k in range(user.id,min(nusers,user.id+3)):
                    try:
                        tomanagePrincipals.append(Principal.objects.get(user__id=k))
                    except Principal.DoesNotExist:
                        logging.error("Cant get Principal %s " % (k))
                        
                toviewPrincipals = []
                for k in range(min(nusers,user.id+3),min(nusers,user.id+8)):
                    try:
                        toviewPrincipals.append(Principal.objects.get(user__id=k))
                    except Principal.DoesNotExist:
                        logging.error("Cant get Principal %s " % (k))
                ObjectPermission.grantPermission(tomanagePrincipals, ( "manage", ), contentMeta)
                ObjectPermission.grantPermission(toviewPrincipals, ( "view", ), contentMeta)
                n = n+1
                if (n % (tot/10)) == 0:
                    rate = (time.time()-tlast)/(tot/10)
                    remain = (tot-n)*rate
                    tlast = time.time()
                    logging.error("Created %s of %s %s s eta at %s s per item  " % (n,tot,remain,rate))
        logging.error("Done All creates ")
        
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)
            # Find the items that this user created
            createdObjects = set(ContentMeta.objects.filter(owner=user))
            
            managedObjects = set(ContentMeta.objects.filter(
                            id__in=ObjectPermission.getGrantedPermissionObjectSet(
                                user, ("manage", ), ContentMeta)))
            viewObjects = set(ContentMeta.objects.filter(
                            id__in=ObjectPermission.getGrantedPermissionObjectSet(
                                user, ("view", "manage",), ContentMeta)))
            
            ## user should be able to view an manage all objects they created
            self.assertEqual(len(createdObjects), nitems, "Didnt find all the objects created")
            self.assertGreater(len(managedObjects), 0, "Didnt find any managed objects")
            self.assertGreater(len(viewObjects), 0, "Didnt find any view objects")
            self.assertTrue(createdObjects.issubset(managedObjects),"Cant manage all created Objects for user %s " % user.username)
            self.assertTrue(createdObjects.issubset(viewObjects),"Cant view all created Objects for user %s " % user.username)
            self.assertGreaterEqual(len(managedObjects), len(createdObjects), "Cant manage more objects than user %s owns " % user.username)
            self.assertGreaterEqual(len(viewObjects), len(createdObjects), "Cant view more objects than user %s owns " % user.username)
            
        s = time.time()
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)
            createdObjects = set(ContentMeta.objects.filter(owner=user))
        logging.error(" Time per createdObjects List %s " % (((time.time()-s)/nitems)))
        
        s = time.time()
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)            
            managedObjects = set(ContentMeta.objects.filter(
                            id__in=ObjectPermission.getGrantedPermissionObjectSet(
                                user, ("manage", ), ContentMeta)))
        logging.error(" Time per managedObjects List %s " % (((time.time()-s)/nitems)))
        
        s = time.time()       
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)            
            viewObjects = set(ContentMeta.objects.filter(
                            id__in=ObjectPermission.getGrantedPermissionObjectSet(
                                user, ("view", "manage",), ContentMeta)))
        logging.error(" Time per viewObjects List %s " % (((time.time()-s)/nitems)))
   
   
    def testManagersAndViewers(self):
        # create some users, which shhould create some principals
        nusers = 10
        nitems = 10
        for i in range(0,nusers):
            User.objects.create_user("user%s" % i, "use%s@example.comn" % i, "test")

        
        tot = nusers*nitems
        n = 0
        tlast = time.time()
        logging.error("Will create %s conent items " % tot)
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)
            
            ## Create some ContentMeta items
            for j in range(0,nitems):
                contentMeta = ContentMeta.objects.create(owner=user, lastmodifiedby=user, value="{}", mimetype="text/test")
                contentMeta.contentid = Hash.genCompactUUID()
                card = (i*j)%(nusers*nitems)
                contentMeta.contenthash = Hash.hash("cardinality%s" % card,"user" )
                ## Give the current user manage rights,
                ## Give the 2 next users manage rights
                ## Give the following 5 view rights
                tomanagePrincipals = [Principal.objects.get(user=user)]
                for k in range(user.id,min(nusers,user.id+3)):
                    try:
                        tomanagePrincipals.append(Principal.objects.get(user__id=k))
                    except Principal.DoesNotExist:
                        logging.error("Cant get Principal %s " % (k))
                        
                toviewPrincipals = list(tomanagePrincipals)
                for k in range(min(nusers,user.id+3),min(nusers,user.id+8)):
                    try:
                        toviewPrincipals.append(Principal.objects.get(user__id=k))
                    except Principal.DoesNotExist:
                        logging.error("Cant get Principal %s " % (k))
                
                for viewer in toviewPrincipals:
                    cm, created = ContentMember.objects.get_or_create(content=contentMeta,principal=viewer) #@UnusedVariable
                    if cm.role != 'V':
                        cm.role = 'V'
                        cm.save()
                for manager in tomanagePrincipals:
                    cm, created = ContentMember.objects.get_or_create(content=contentMeta,principal=manager) #@UnusedVariable
                    if cm.role != 'M':
                        cm.role = 'M'
                        cm.save()
                contentMeta.save()

                n = n+1
                if (n % (tot/10)) == 0:
                    rate = (time.time()-tlast)/(tot/10)
                    remain = (tot-n)*rate
                    tlast = time.time()
                    logging.error("Created %s of %s %s s eta at %s s per item  " % (n,tot,remain,rate))
        logging.error("Done All creates ")
        
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)
            # Find the items that this user created
            createdObjects = set(ContentMeta.objects.filter(owner=user))
            
            managedObjects = set(ContentMeta.objects.filter(contentmember__principal__in=ObjectPermission.getUserPrincipals(user),contentmember__role='M'))
            viewObjects = set(ContentMeta.objects.filter(contentmember__principal__in=ObjectPermission.getUserPrincipals(user),contentmember__role__in=['M','E','V']))
            
            ## user should be able to view an manage all objects they created
            self.assertEqual(len(createdObjects), nitems, "Didnt find all the objects created")
            self.assertGreater(len(managedObjects), 0, "Didnt find any managed objects")
            self.assertGreater(len(viewObjects), 0, "Didnt find any view objects")
            self.assertTrue(createdObjects.issubset(managedObjects),"Cant manage all created Objects for user %s " % user.username)
            self.assertTrue(createdObjects.issubset(viewObjects),"Cant view all created Objects for user %s " % user.username)
            self.assertGreaterEqual(len(managedObjects), len(createdObjects), "Cant manage more objects than user %s owns " % user.username)
            self.assertGreaterEqual(len(viewObjects), len(createdObjects), "Cant view more objects than user %s owns " % user.username)
            
        s = time.time()
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)
            createdObjects = set(ContentMeta.objects.filter(owner=user))
        logging.error(" Time per createdObjects List %s " % (((time.time()-s)/nitems)))
        
        s = time.time()
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)            
            managedObjects = set(ContentMeta.objects.filter(contentmember__principal__in=ObjectPermission.getUserPrincipals(user),contentmember__role='M'))
        logging.error(" Time per managedObjects List %s " % (((time.time()-s)/nitems)))
        
        s = time.time()       
        for i in range(0,nusers):
            user = User.objects.get(username="user%s" % i)            
            viewObjects = set(ContentMeta.objects.filter(contentmember__principal__in=ObjectPermission.getUserPrincipals(user),contentmember__role__in=['M','E','V']))
        logging.error(" Time per viewObjects List %s " % (((time.time()-s)/nitems)))
            