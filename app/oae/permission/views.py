'''
Created on Mar 16, 2012

@author: ieb
'''
from oae.permission.models import ObjectType, Principal, ObjectPermission
from oae.message.models import Message, MessageBox
from oae.content.models import ContentMeta, ContentMember
from django.contrib.auth.models import Group, User
import logging
import time
from oae.common.util import Hash
from django.http import HttpResponseServerError, HttpResponse
from django.db import connection, transaction
from oae.tag.models import Tag


@transaction.commit_manually()
def createLotsOfContent(self, susers, sitems):
    '''
ERROR:root:Called with [10] [10] 
ERROR:root:Object types message <class 'oae.message.models.Message'> 
ERROR:root:Object types message <class 'oae.message.models.MessageBox'> 
ERROR:root:Object types content <class 'oae.content.models.ContentMeta'> 
ERROR:root:Object types content <class 'oae.content.models.ContentTag'> 
ERROR:root:Object types user <class 'django.contrib.auth.models.User'> 
ERROR:root:Object types user <class 'django.contrib.auth.models.Group'> 
ERROR:root:Will create 100 conent items 
ERROR:root:Created 10 of 100 2.64026784897 s eta at 0.029336309433 s per item  
ERROR:root:Created 20 of 100 2.17043113708 s eta at 0.0271303892136 s per item  
ERROR:root:Created 30 of 100 1.93601059914 s eta at 0.0276572942734 s per item  
ERROR:root:Created 40 of 100 1.52024459839 s eta at 0.0253374099731 s per item  
ERROR:root:Created 50 of 100 1.25282526016 s eta at 0.0250565052032 s per item  
ERROR:root:Created 60 of 100 1.22514438629 s eta at 0.0306286096573 s per item  
ERROR:root:Created 70 of 100 0.768929958344 s eta at 0.0256309986115 s per item  
ERROR:root:Created 80 of 100 0.448679924011 s eta at 0.0224339962006 s per item  
ERROR:root:Created 90 of 100 0.21254992485 s eta at 0.021254992485 s per item  
ERROR:root:Created 100 of 100 0.0 s eta at 0.0204848051071 s per item  
ERROR:root:Done All creates 
ERROR:root: Time per createdObjects List 0.00425579547882 
ERROR:root: Time per managedObjects List 0.0441151857376 
ERROR:root: Time per viewObjects List 0.0400793075562 



SELECT "content_contentmeta"."id", 
"content_contentmeta"."contentid", 
"content_contentmeta"."owner_id", 
"content_contentmeta"."contenthash", 
"content_contentmeta"."lastmodifiedby_id", 
"content_contentmeta"."created", 
"content_contentmeta"."modified",
 "content_contentmeta"."value", 
 "content_contentmeta"."bitstream" 
 FROM "content_contentmeta" 
 WHERE "content_contentmeta"."id" 
    IN (SELECT U0."target" 
        FROM "permission_objectpermission" U0 
           INNER JOIN "permission_objecttype" U2 ON (U0."objectType_id" = U2."id") 
           INNER JOIN "auth_permission" U3 ON (U0."permission_id" = U3."id") 
        WHERE (U0."principal_id" 
           IN (SELECT U0."id" 
               FROM "permission_principal" U0 
                  LEFT OUTER JOIN "auth_group" U2 ON (U0."group_id" = U2."id") 
                  LEFT OUTER JOIN "auth_user_groups" U3 ON (U2."id" = U3."group_id") 
                  WHERE (U0."user_id" = 2181  OR U3."user_id" = 2181 )) 
              AND U2."name" = '<class ''oae.content.models.ContentMeta''>'  
              AND U3."codename" IN ('view', 'manage'))) 
  ORDER BY "content_contentmeta"."modified" DESC LIMIT 25
  
  
  "Limit  (cost=7836.19..7836.25 rows=25 width=603)"
"  ->  Sort  (cost=7836.19..7876.15 rows=15983 width=603)"
"        Sort Key: content_contentmeta.modified"
"        ->  Hash Semi Join  (cost=5153.09..7385.16 rows=15983 width=603)"
"              Hash Cond: (content_contentmeta.id = u0.target)"
"              ->  Seq Scan on content_contentmeta  (cost=0.00..1750.87 rows=65687 width=603)"
"              ->  Hash  (cost=3873.54..3873.54 rows=102364 width=4)"
"                    ->  Hash Join  (cost=197.69..38    73.54 rows=102364 width=4)"
"                          Hash Cond: (u0.permission_id = u3.id)"
"                          ->  Hash Join  (cost=195.68..2464.02 rows=102364 width=8)"
"                                Hash Cond: (u0."objectType_id" = u2.id)"
"                                ->  Nested Loop  (cost=186.05..1046.89 rows=102364 width=12)"
"                                      ->  HashAggregate  (cost=186.05..186.32 rows=27 width=4)"
"                                            ->  Hash Left Join  (cost=161.63..185.98 rows=27 width=4)"
"                                                  Hash Cond: (u0.group_id = u2.id)"
"                                                  Filter: ((u0.user_id = 2181) OR (u3.user_id = 2181))"
"                                                  ->  Seq Scan on permission_principal u0  (cost=0.00..16.71 rows=871 width=12)"
"                                                  ->  Hash  (cost=137.38..137.38 rows=1940 width=8)"
"                                                        ->  Merge Right Join  (cost=30.08..137.38 rows=1940 width=8)"
"                                                              Merge Cond: (u3.group_id = u2.id)"
"                                                              ->  Index Scan using auth_user_groups_group_id on auth_user_groups u3  (cost=0.00..73.35 rows=1940 width=8)"
"                                                              ->  Sort  (cost=30.08..31.03 rows=380 width=4)"
"                                                                    Sort Key: u2.id"
"                                                                    ->  Seq Scan on auth_group u2  (cost=0.00..13.80 rows=380 width=4)"
"                                      ->  Index Scan using permission_objectpermission_principal_id on permission_objectpermission u0  (cost=0.00..26.71 rows=413 width=16)"
"                                            Index Cond: (u0.principal_id = u0.id)"
"                                ->  Hash  (cost=9.61..9.61 rows=2 width=4)"
"                                      ->  Bitmap Heap Scan on permission_objecttype u2  (cost=4.27..9.61 rows=2 width=4)"
"                                            Recheck Cond: ((name)::text = '<class ''oae.content.models.ContentMeta''>'::text)"
"                                            ->  Bitmap Index Scan on permission_objecttype_name_like  (cost=0.00..4.27 rows=2 width=0)"
"                                                  Index Cond: ((name)::text = '<class ''oae.content.models.ContentMeta''>'::text)"
"                          ->  Hash  (cost=1.99..1.99 rows=2 width=4)"
"                                ->  Seq Scan on auth_permission u3  (cost=0.00..1.99 rows=2 width=4)"
"                                      Filter: ((codename)::text = ANY ('{view,manage}'::text[]))"


This apporach will work better where the user has access to a small number of content items, since the filter from a large
number of nodes will happen earlier on in the process, reducing the number of rows earlier and hence the cost 
of query.

If the user in question can view a large number of rows the final cost potentially becomes much larger.
The problematic step is 

"              ->  Seq Scan on content_contentmeta  (cost=0.00..1750.87 rows=65687 width=603)"

with a range of cost from 0 to 1750
  
    @param susers:
    @param sitems:
    '''
    logging.error("Called with [%s] [%s] " % (susers, sitems))
    nusers = int(susers)
    nitems = int(sitems)
    # create some users, which shhould create some principals
    
    userbase = "u%s" % time.time()
    for i in range(0,nusers):
        User.objects.create_user("%s%s" % (userbase,i,), "%s%s@example.comn" % (userbase,i), "test")
        
    
    ObjectType.add("message", Message)
    ObjectType.add("message", MessageBox)
    ObjectType.add("content", ContentMeta)
    ObjectType.add("tag", Tag)
    ObjectType.add("user", User)
    ObjectType.add("user", Group)
    for ot in ObjectType.objects.all():
        logging.error("Object types %s %s " % (ot.app_label, ot.name))

    ObjectType.objects.get(name=str(type(Message())))
    ObjectType.objects.get(name=str(type(ContentMeta())))
    ObjectType.objects.get(name=str(type(Tag())))
    ObjectType.objects.get(name=str(type(MessageBox())))
    ObjectType.objects.get(name=str(type(User())))
    ObjectType.objects.get(name=str(type(Group())))
    
    tot = nusers*nitems
    n = 0
    tlast = time.time()
    logging.error("Will create %s conent items " % tot)
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        
        ## Create some ContentMeta items
        for j in range(0,nitems):
            contentMeta = ContentMeta.objects.create(owner=user, lastmodifiedby=user, value="{}")
            contentMeta.contentid = Hash.genCompactUUID()
            card = (i*j)%(nusers*nitems)
            contentMeta.contenthash = Hash.hash("cardinality%s" % card,"user" )
            contentMeta.save()
            ## Give the current user manage rights,
            ## Give the 2 next users manage rights
            ## Give the following 5 view rights
            tomanagePrincipals = []
            for k in range(user.id,user.id+3):
                try:
                    tomanagePrincipals.append(Principal.objects.get(user__id=k))
                except Principal.DoesNotExist:
                    pass
                    
            toviewPrincipals = []
            for k in range(user.id+3,user.id+8):
                try:
                    toviewPrincipals.append(Principal.objects.get(user__id=k))
                except Principal.DoesNotExist:
                    pass
            p = ObjectPermission.grantPermission(tomanagePrincipals, ( "manage", ), contentMeta)
            if "manage" not in p or p["manage"] < len(tomanagePrincipals):
                logging.error(p)
                return HttpResponseServerError("Failed to grant enough principals %s " % p)
            p = ObjectPermission.grantPermission(toviewPrincipals, ( "view", ), contentMeta)
            if "view" not in p or p["view"] < len(tomanagePrincipals):
                logging.error(p)
                return HttpResponseServerError("Failed to grant enough principals %s " % p)
            
            transaction.commit()

            n = n+1
            if (n % (tot/10)) == 0:
                rate = (time.time()-tlast)/(tot/10)
                remain = (tot-n)*rate
                tlast = time.time()
                logging.error("Created %s of %s %s s eta at %s s per item  " % (n,tot,remain,rate))
    logging.error("Done All creates ")
    
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        # Find the items that this user created
        createdObjects = set(ContentMeta.objects.filter(owner=user))
        
        managedObjects = set(ContentMeta.objects.filter(
                        id__in=ObjectPermission.getGrantedPermissionObjectSet(
                            user, ("manage", ), ContentMeta)))
        viewObjects = set(ContentMeta.objects.filter(
                        id__in=ObjectPermission.getGrantedPermissionObjectSet(
                            user, ("view", "manage",), ContentMeta)))
        
        ## user should be able to view an manage all objects they created
        if len(createdObjects) != nitems:
            return HttpResponseServerError("Didnt find all the objects created")
        if len(managedObjects) == 0:
            return HttpResponseServerError("Didnt find any managed objects")
        if len(viewObjects) == 0:
            return HttpResponseServerError("Didnt find any view objects")
        if not createdObjects.issubset(managedObjects):
            return HttpResponseServerError("Cant manage all created Objects for user %s " % user.username)
        if not createdObjects.issubset(viewObjects):
            return HttpResponseServerError("Cant view all created Objects for user %s " % user.username)
        if len(managedObjects) < len(createdObjects):
            return HttpResponseServerError("Cant manage more objects than user %s owns " % user.username)
        if len(viewObjects) < len(createdObjects):
            return HttpResponseServerError("Cant view more objects than user %s owns " % user.username)
        
    s = time.time()
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        createdObjects = set(ContentMeta.objects.filter(owner=user).order_by("-modified")[:25])
    logging.error(" Time per createdObjects List %s " % (((time.time()-s)/nitems)))
    
    s = time.time()
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        managedObjects = set(ContentMeta.objects.filter(
                        id__in=ObjectPermission.getGrantedPermissionObjectSet(
                            user, ("manage", ), ContentMeta)).order_by("-modified")[:25])
    logging.error(" Time per managedObjects List %s " % (((time.time()-s)/nitems)))
    
    s = time.time()       
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        viewObjects = set(ContentMeta.objects.filter(
                        id__in=ObjectPermission.getGrantedPermissionObjectSet(
                            user, ("view", "manage",), ContentMeta)).order_by("-modified")[:25])
    logging.error(" Time per viewObjects List %s " % (((time.time()-s)/nitems)))
    transaction.commit()
    
    return HttpResponse(connection.queries,content_type="text/plain") #@UndefinedVariable


#@transaction.commit_manually()
def createLotsOfContent2(self, susers, sitems):
    '''
    ERROR:root:Called with [100] [100] 
    ERROR:root:Will create 10000 conent items 
    ERROR:root:Created 1000 of 10000 293.503120422 s eta at 0.0326114578247 s per item  
    ERROR:root:Created 2000 of 10000 270.269359589 s eta at 0.0337836699486 s per item  
    ERROR:root:Created 3000 of 10000 228.676286936 s eta at 0.0326680409908 s per item  
    ERROR:root:Created 4000 of 10000 195.372353554 s eta at 0.0325620589256 s per item  
    ERROR:root:Created 5000 of 10000 157.356309891 s eta at 0.0314712619781 s per item  
    ERROR:root:Created 6000 of 10000 130.787648201 s eta at 0.0326969120502 s per item  
    ERROR:root:Created 7000 of 10000 98.0025844574 s eta at 0.0326675281525 s per item  
    ERROR:root:Created 8000 of 10000 70.7561941147 s eta at 0.0353780970573 s per item  
    ERROR:root:Created 9000 of 10000 40.1955940723 s eta at 0.0401955940723 s per item  
    ERROR:root:Created 10000 of 10000 0.0 s eta at 0.0337763290405 s per item  
    ERROR:root:Done All creates 
    ERROR:root: Time per createdObjects List 0.00701091051102 
    ERROR:root: Time per managedObjects List 0.0498654413223 
    ERROR:root: Time per viewObjects List 0.0799915599823 


SELECT "content_contentmeta"."id", 
"content_contentmeta"."contentid", 
"content_contentmeta"."owner_id", 
"content_contentmeta"."contenthash", 
"content_contentmeta"."lastmodifiedby_id", 
"content_contentmeta"."created", 
"content_contentmeta"."modified",
 "content_contentmeta"."value", 
 "content_contentmeta"."bitstream" 
 FROM "content_contentmeta" 
    INNER JOIN "content_contentmeta_viewers" 
    ON ("content_contentmeta"."id" = "content_contentmeta_viewers"."contentmeta_id") 
    WHERE "content_contentmeta_viewers"."principal_id" 
        IN (SELECT U0."id" 
        FROM "permission_principal" U0 
        LEFT OUTER JOIN "auth_group" U2 ON (U0."group_id" = U2."id") 
        LEFT OUTER JOIN "auth_user_groups" U3 ON (U2."id" = U3."group_id") 
        WHERE (U0."user_id" = 2170  OR U3."user_id" = 2170 )) 
        ORDER BY "content_contentmeta"."modified" DESC LIMIT 25
        
"Limit  (cost=161.63..181.35 rows=25 width=603)"
"  ->  Nested Loop Semi Join  (cost=161.63..80730.58 rows=102134 width=603)"
"        Join Filter: (content_contentmeta_viewers.principal_id = u0.id)"
"        ->  Nested Loop  (cost=0.00..34581.18 rows=102134 width=607)"
"              ->  Index Scan Backward using content_contentmeta_modified on content_contentmeta  (cost=0.00..3583.42 rows=67248 width=603)"
"              ->  Index Scan using content_contentmeta_viewers_contentmeta_id on content_contentmeta_viewers  (cost=0.00..0.40 rows=5 width=8)"
"                    Index Cond: (content_contentmeta_viewers.contentmeta_id = content_contentmeta.id)"
"        ->  Materialize  (cost=161.63..189.17 rows=30 width=4)"
"              ->  Hash Left Join  (cost=161.63..189.02 rows=30 width=4)"
"                    Hash Cond: (u0.group_id = u2.id)"
"                    Filter: ((u0.user_id = 2170) OR (u3.user_id = 2170))"
"                    ->  Seq Scan on permission_principal u0  (cost=0.00..18.80 rows=980 width=12)"
"                    ->  Hash  (cost=137.38..137.38 rows=1940 width=8)"
"                          ->  Merge Right Join  (cost=30.08..137.38 rows=1940 width=8)"
"                                Merge Cond: (u3.group_id = u2.id)"
"                                ->  Index Scan using auth_user_groups_group_id on auth_user_groups u3  (cost=0.00..73.35 rows=1940 width=8)"
"                                ->  Sort  (cost=30.08..31.03 rows=380 width=4)"
"                                      Sort Key: u2.id"
"                                      ->  Seq Scan on auth_group u2  (cost=0.00..13.80 rows=380 width=4)"

    @param susers:
    @param sitems:
    '''
    logging.error("Called with [%s] [%s] " % (susers, sitems))
    nusers = int(susers)
    nitems = int(sitems)
    # create some users, which shhould create some principals
    userbase = "u%s" % time.time()
    for i in range(0,nusers):
        User.objects.create_user("%s%s" % (userbase,i,), "%s%s@example.comn" % (userbase,i), "test")
    
    
    tot = nusers*nitems
    n = 0
    tlast = time.time()
    logging.error("Will create %s conent items " % tot)
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        
        ## Create some ContentMeta items
        for j in range(0,nitems):
            ## Give the current user manage rights,
            ## Give the 2 next users manage rights
            ## Give the following 5 view rights
        
            tomanagePrincipals = []
            for k in range(user.id,user.id+3):
                try:
                    tomanagePrincipals.append(Principal.objects.get(user__id=k))
                except Principal.DoesNotExist:
                    pass
                    
            toviewPrincipals = list(tomanagePrincipals)
            for k in range(user.id+3,user.id+8):
                try:
                    toviewPrincipals.append(Principal.objects.get(user__id=k))
                except Principal.DoesNotExist:
                    pass
            card = (i*j)%(nusers*nitems)
            contenthash = Hash.hash("cardinality%s" % card,"user" )
            contentMeta = ContentMeta.objects.create(
                            owner=user, 
                            lastmodifiedby=user,
                            contenthash=contenthash,
                            mimetype="text/plain",
                            value="{}")
            contentMeta.contentid = Hash.genCompactUUID()
            
            
            for viewer in toviewPrincipals:
                try:
                    cm, created = ContentMember.objects.get_or_create(content=contentMeta,principal=viewer) #@UnusedVariable
                    if cm.role != 'V':
                        cm.role = 'V'
                        cm.save()
                except:
                    pass
            for manager in tomanagePrincipals:
                try:
                    cm, created = ContentMember.objects.get_or_create(content=contentMeta,principal=manager) #@UnusedVariable
                    if cm.role != 'M':
                        cm.role = 'M'
                        cm.save()
                except:
                    pass

            contentMeta.save()
            # transaction.commit()
            n = n+1
            if (n % (tot/10)) == 0:
                rate = (time.time()-tlast)/(tot/10)
                remain = (tot-n)*rate
                tlast = time.time()
                logging.error("Created %s of %s %s s eta at %s s per item  " % (n,tot,remain,rate))
    logging.error("Done All creates ")
    
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        # Find the items that this user created
        createdObjects = set(ContentMeta.objects.filter(owner=user))
        
        managedObjects = set(ContentMeta.objects.filter(contentmembers__principal__in=ObjectPermission.getUserPrincipals(user),contentmembers__role='M'))
        viewObjects = set(ContentMeta.objects.filter(contentmembers__principal__in=ObjectPermission.getUserPrincipals(user),contentmembers__role__in=['M','E','V']))
        
        ## user should be able to view an manage all objects they created
        if len(createdObjects) != nitems:
            return HttpResponseServerError("Didnt find all the objects created")
        if len(viewObjects) == 0:
            return HttpResponseServerError("Didnt find any view objects")
        if len(managedObjects) == 0:
            return HttpResponseServerError("Didnt find any managed objects")
        if not createdObjects.issubset(managedObjects):
            return HttpResponseServerError("Cant manage all created Objects for user %s " % user.username)
        if not createdObjects.issubset(viewObjects):
            return HttpResponseServerError("Cant view all created Objects for user %s " % user.username)
        if len(managedObjects) < len(createdObjects):
            return HttpResponseServerError("Cant manage more objects than user %s owns " % user.username)
        if len(viewObjects) < len(createdObjects):
            return HttpResponseServerError("Cant view more objects than user %s owns " % user.username)
        
    s = time.time()
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        
        createdObjects = set(ContentMeta.objects.filter(owner=user).order_by("-modified")[:25])
    logging.error(" Time per createdObjects List %s " % (((time.time()-s)/nitems)))
    
    s = time.time()
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        managedObjects = set(ContentMeta.objects.filter(contentmembers__principal__in=ObjectPermission.getUserPrincipals(user),contentmembers__role='M').order_by("-modified")[:25])
    logging.error(" Time per managedObjects List %s " % (((time.time()-s)/nitems)))
    
    s = time.time()       
    for i in range(0,nusers):
        user = User.objects.get(username="%s%s" % (userbase,i))
        viewObjects = set(ContentMeta.objects.filter(contentmembers__principal__in=ObjectPermission.getUserPrincipals(user),contentmembers__role__in=['M','E','V']).order_by("-modified")[:25])
    logging.error(" Time per viewObjects List %s " % (((time.time()-s)/nitems)))
    #  transaction.commit()
    return HttpResponse(connection.queries,content_type="text/plain") #@UndefinedVariable
