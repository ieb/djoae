# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    depends_on = (
    )

    def forwards(self, orm):
        # Adding model 'Principal'
        db.create_table('permission_principal', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True)),
            ('group', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.Group'], unique=True, null=True)),
            ('ugid', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1, db_index=True)),
        ))
        db.send_create_signal('permission', ['Principal'])

        # Adding model 'ObjectType'
        db.create_table('permission_objecttype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80, db_index=True)),
            ('app_label', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('permission', ['ObjectType'])

        # Adding unique constraint on 'ObjectType', fields ['name', 'app_label']
        db.create_unique('permission_objecttype', ['name', 'app_label'])

        # Adding model 'ObjectPermission'
        db.create_table('permission_objectpermission', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('objectType', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['permission.ObjectType'])),
            ('target', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('principal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['permission.Principal'])),
            ('permission', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Permission'])),
        ))
        db.send_create_signal('permission', ['ObjectPermission'])

    def backwards(self, orm):
        # Removing unique constraint on 'ObjectType', fields ['name', 'app_label']
        db.delete_unique('permission_objecttype', ['name', 'app_label'])

        # Deleting model 'Principal'
        db.delete_table('permission_principal')

        # Deleting model 'ObjectType'
        db.delete_table('permission_objecttype')

        # Deleting model 'ObjectPermission'
        db.delete_table('permission_objectpermission')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'permission.objectpermission': {
            'Meta': {'object_name': 'ObjectPermission'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'objectType': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['permission.ObjectType']"}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.Permission']"}),
            'principal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['permission.Principal']"}),
            'target': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        'permission.objecttype': {
            'Meta': {'unique_together': "(('name', 'app_label'),)", 'object_name': 'ObjectType'},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'db_index': 'True'})
        },
        'permission.principal': {
            'Meta': {'object_name': 'Principal'},
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_index': 'True'}),
            'ugid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'})
        }
    }

    complete_apps = ['permission']