'''
Created on Mar 15, 2012

NB: Not change the schema without generating a migration.

@author: ieb
'''
from django.contrib.auth.models import Permission, User, Group, AnonymousUser
from django.db import models
from django.db.models.signals import post_save, pre_delete
import logging
from django.contrib import admin
import traceback
from django.utils.datastructures import MultiValueDictKeyError
from oae.common.util import Request


PRINCIPAL_CACHE = {} # Fix me, make this a proper cache
    
class Principal(models.Model):
    TYPE_CHOICES = (
            ("G","Group"),
            ("U", "User"))
    '''
    Definition of principals with an indication of where the principal came from
    '''
    # at the moment these are 1:1 relationships. however, we could change them to FK which would allow 
    # the principal table to contain all the principals a user had. The problem with that would become
    # that it would be expensive to re-build when anything changed.
    user = models.OneToOneField(User, null=True,
                             verbose_name="User that is this principal",
                             help_text="The User that is this principal, only if type is 'U' ")
    group = models.OneToOneField(Group, null=True,
                             verbose_name="Group that is this principal",
                             help_text="The Group that is this principal, only if type is 'G' ")
    ugid = models.CharField("User Group Id",
                              unique=True,
                              db_index=True,
                              max_length=64,
                              help_text="The Internal User ID, matches the field of the same name on the Internal User object.")
    type=models.CharField(db_index=True,
                              max_length=1,
                              help_text="The type of object this principal relates to",
                              choices=TYPE_CHOICES)
    @staticmethod
    def syncCreateUser(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            try:
                from oae.user.models import InternalUserGroup
                ugid = InternalUserGroup.createId(instance)
                InternalUserGroup.connectId(instance, Principal.objects.create(user=instance,ugid=ugid,type='U'))
            except:
                logging.error("Failed to create Principal for user")
                pass 
    @staticmethod
    def syncDeleteUser(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            try:
                k = "u:%s" % instance.principal.ugid
                if k in PRINCIPAL_CACHE:
                    del(PRINCIPAL_CACHE[k])
                Principal.objects.get(user=instance).delete()
            except:
                logging.error("Failed to delete Principal for group")
                pass 
            
    @staticmethod
    def syncCreateGroup(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            try:
                from oae.user.models import InternalUserGroup
                ugid = InternalUserGroup.createId(instance)
                InternalUserGroup.connectId(instance, Principal.objects.create(group=instance,ugid=ugid,type='G'))
            except:
                logging.error("Failed to create Principal for group")
                pass 
            
    @staticmethod
    def syncDeleteGroup(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            try:
                k = "g:%s" % instance.name
                if k in PRINCIPAL_CACHE:
                    del(PRINCIPAL_CACHE[k])
                Principal.objects.get(group=instance).delete()
            except:
                logging.error("Failed to delete Principal for group")
                pass 
    def __str__(self):
        return unicode(self)
            
    def __unicode__(self):
        if self.type == 'U':
            try:
                return "U-%s" % self.user.username
            except:
                return "U-ugid-%s" % self.ugid
        else:
            try:
                return "G-%s" % self.user.username
            except:
                return "G-ugid-%s" % self.ugid
            
            


    @staticmethod
    def getMembershipChanges(user, object, type, changeData, ugid=None):
        changeData = Request.toQueryDict(changeData)
        if ugid is None:
            ugid = user.principal.ugid
        if user.has_perm("%s.manage" % type, object):
            principalAdded, principalRemoved, changes = Principal._getMembershipChanges(ugid, changeData,'M')
            changes = True
            manage = True
        elif user.has_perm("%s.edit"  % type, object):
            principalAdded, principalRemoved, changes = Principal._getMembershipChanges(ugid, changeData,'E')
            manage = False
        elif user.has_perm("%s.view"  % type, object):
            principalAdded, principalRemoved, changes = Principal._getMembershipChanges(ugid, changeData,'V')        
            manage = False
        else:
            logging.error("No changes, can't even view")
            return False, False, None, None
    
        if not changes:
            logging.error("No Changes, forbidden to update, as not a manager and no permitted membership changes")
            return False, False, None, None
        return manage,changes, principalAdded, principalRemoved

        
    @staticmethod
    def _getMembershipChanges(ugid, changes, role):
        changes = Request.toQueryDict(changes)
        principalRemoved = {
                'M' : [],
                'E' : [],
                'V' : []
                }
        principalAdded = {
                'M' : [],
                'E' : [],
                'V' : []
                }
        keys = {
            'M' : ':manager',
            'E' : ':editor',
            'V' : ':viewer'
            }
        didsomething = False
        if role == 'M':
            for k in ('M','E','V'):
                try:
                    for pid in changes.getlist('%s@Delete' % keys[k]):
                        principalRemoved[k].append(pid)
                except MultiValueDictKeyError:
                    pass
            for k in ('M','E','V'):
                try:
                    for pid in changes.getlist(keys[k]):
                        for k2 in ('M','E','V'):
                            if pid in principalRemoved[k2]:
                                principalRemoved[k2].remove(pid)
                        principalAdded[k].append(pid)
                except MultiValueDictKeyError:
                    pass
            
            didsomething = True
        elif role == 'E':
            for k in ('E','V'):
                try:
                    for pid in changes.getlist('%s@Delete' % keys[k]):
                        if pid == ugid:
                            principalRemoved[k].append(pid)
                except MultiValueDictKeyError:
                    pass
            for k in ('E','V'):
                try:
                    for pid in changes.getlist(keys[k]):
                        if pid == ugid:
                            for k2 in ('E','V'):
                                if pid in principalRemoved[k2]:
                                    principalRemoved[k2].remove(pid)
                            didsomething = True
                            principalAdded[k].append(pid)
                except MultiValueDictKeyError:
                    pass
        elif role == 'V':
            k = 'V'
            try:
                for pid in changes.getlist('%s@Delete' % keys[k]):
                    if pid == ugid:
                        principalRemoved[k].append(pid)
                        didsomething = True
            except MultiValueDictKeyError:
                pass
            try:
                for pid in changes.getlist(keys[k]):
                    if pid == ugid:
                        if pid in principalRemoved[k]:
                            principalRemoved[k].remove(pid)
                        didsomething = True
                        principalAdded[k].append(pid)
            except MultiValueDictKeyError:
                pass

        return principalAdded, principalRemoved, didsomething


    @staticmethod
    def getPrincipalsForUi(obj):
        principals = []
        principalsForUi = []
        for p in ObjectPermission.getUserPrincipals(obj).values("ugid","type","group__name"):
            if p['ugid'] not in ("everyone","anonymous"):
                principals.append(p['ugid'])
                if p['type'] == 'G':
                    principalsForUi.append(p["group__name"])
                else:
                    principalsForUi.append(p["ugid"])
        return principals, principalsForUi

    @staticmethod
    def viaCache(k, getter):
        if k not in PRINCIPAL_CACHE:
            PRINCIPAL_CACHE[k] = getter()
        return PRINCIPAL_CACHE[k]
        
    
post_save.connect(Principal.syncCreateUser, sender=User)
post_save.connect(Principal.syncCreateGroup, sender=Group)
pre_delete.connect(Principal.syncDeleteUser, sender=User)
pre_delete.connect(Principal.syncDeleteGroup, sender=Group)

class ObjectType(models.Model):
    class Meta:
        unique_together=(
            ("name","app_label"),
        )
    '''
    An holder for object type to ensure that object types. 
    
    '''
    name = models.CharField('object type',
                              db_index=True,
                              max_length=80,
                              help_text="The Internal User ID, matches the field of the same name on the Group object.")
    app_label = models.CharField('object type',
                              max_length=20,
                              help_text="The Internal User ID, matches the field of the same name on the Group object.")
    
    
    @staticmethod
    def add(label, modelClass):
        try:
            ObjectType.objects.get(name=str(type(modelClass())),app_label=label)
        except ObjectType.DoesNotExist:
            ObjectType.objects.create(name=str(type(modelClass())),app_label=label)

class ObjectPermission(models.Model):
    '''
    An object that joins target Object, principal and permission with a sepcifiyer
    of ObjectType typpe(object)
    '''
    objectType = models.ForeignKey(ObjectType)
    target = models.IntegerField("the ID of the model type", db_index=True)
    principal = models.ForeignKey(Principal)
    permission = models.ForeignKey(Permission)

    @staticmethod
    def get_all_permissions(user_obj, obj=None):
        '''
        Gets all permissions as a set for a user on an object in the model.permission form
        @param user_obj: the user
        @param obj: the object
        '''
        if obj is None:
            return set()
        if user_obj.is_anonymous():
            return set()
        if hasattr(obj,"id"):
            return set([u"%s.%s" % (p.content_type.app_label, p.codename) 
                        for p in Permission.objects.filter(objectpermission__objectType__name=str(type(obj)),
                                                           objectpermission__target=obj.id,
                                                           objectpermission__principal__in=ObjectPermission.getUserPrincipals(user_obj))])
        return set()

    
    @staticmethod
    def getGrantedPermissionObjectSet(user_obj,perms,objectClass):
        '''
        This generates a query set contain the pk is that the user has the permissions
        on for the specified object type. It should be used in a filter with 
        id__in=ObjectPermission.getGrantedPermissionObjectSet(user,['view_contentmeta'],type(ContentMeta()))
        Please note, the permission is the raw permission.codename and not app + permission.codename
        @param user_obj:
        @param perms:
        @param objectType:
        '''
        return ObjectPermission.objects.filter(
                            objectType__name=str(type(objectClass())),
                            principal__in=ObjectPermission.getUserPrincipals(user_obj),
                            permission__codename__in=perms).values('target')

    @staticmethod
    def getUserPrincipals(user_obj):
        '''
        Gets a QuerySet representing the users principals. intended to be used in a in statement.
        @param user_obj:
        '''
        if isinstance(user_obj,User):
            if user_obj.is_anonymous():
                return Principal.objects.filter(models.Q(ugid="anonymous"))
            else:
                return Principal.objects.filter(
                            models.Q(user=user_obj)|  # The users principals
                            models.Q(group__user=user_obj)| # Groups the user is a member of
                            models.Q(group__child__group__user=user_obj) | # Subgoups the user is a member of, hopefully this is not too much complexity
                                    # this principal<child of <group< member of <user
                                    # therefore this user has the principal of the parent group
                                    # This needs to be verified
                            models.Q(ugid="anonymous")| # Anon
                            models.Q(ugid="everyone")) # Everyone
        if isinstance(user_obj,Group):
            return Principal.objects.filter(
                            models.Q(group=user_obj)| # This group
                            models.Q(group__child__group=user_obj)) # Groups this group is a child of
        if isinstance(user_obj,AnonymousUser):
            return Principal.objects.filter(models.Q(ugid="anonymous"))

        return Principal.objects.filter(ugid=user_obj)

    @staticmethod
    def getUserPrincipalsFromUgid(ugid):
        '''
        Gets a QuerySet representing the users principals. intended to be used in a in statement.
        @param user_obj:
        '''
        try:
            p = Principal.objects.get(ugid=ugid)
            if p.type == 'G':
                return ObjectPermission.getUserPrincipals(p.group)
            else:
                return ObjectPermission.getUserPrincipals(p.user)
        except Principal.DoesNotExist:
            return Principal.objects.filter(ugid=ugid)
        

    @staticmethod
    def can(user_obj, perms, obj):
        '''
        Tests if perms are all present for the user and object
        Avoid using this, use request.user.has_perms in preference
        @param user_obj: the user
        @param perms: the permissions, must be something that can be converted into aset
        @param obj: the object to be tested
        '''
        try:
            return set(perms).issubset(set(ObjectPermission.objects.filter(
                        objectType__name=str(type(obj)),
                        permission__codename__in=perms,
                        principal__in=ObjectPermission.getUserPrincipals(user_obj),
                        target=obj.id).values("codename")))
        except:
            return False
        
    @staticmethod
    def filter(user_obj, perms, obj):
        '''
        If the user has perms to obj, then this will return obj. If not None
        Avoid using this, use request.user.has_perms in preference
        @param user_obj: the user
        @param perms: the pemissions required
        @param obj: the object being tested
        '''
        if ObjectPermission.can(user_obj, perms, obj):
            return obj
        return None

    @staticmethod
    def grantPermission(principals, perms, obj):
        if isinstance(principals,str):
            raise Exception("Principals must be a list not a string")
        try:
            objType = ObjectType.objects.get(name=str(type(obj)))
        except ObjectType.DoesNotExist:
            logging.error("Object type %s Does not exist " % type(obj))
            return False
        permObjs = []
        for p in perms:
            try:
                permObjs.append(Permission.objects.get(codename=p,content_type__app_label=objType.app_label))
            except Permission.DoesNotExist:
                logging.error("Permission %s.%s Does not exist " % (objType.app_label,p))
        added = {}
        for permObj in permObjs:
            added[permObj.codename] = []
            for prin in principals:
                try:
                    ObjectPermission.objects.create(
                                objectType=objType,
                                target=obj.id,
                                principal=prin,
                                permission=permObj)
                    added[permObj.codename].append(prin)
                except:
                    logging.error("Failed to add permission %s " % traceback.format_exc())
        return added
    
    @staticmethod
    def removePermission(principals, perms, obj):
        if isinstance(principals,str):
            raise Exception("Principals must be a list not a string")
        try:
            objType = ObjectType.objects.get(name=str(type(obj)))
        except ObjectType.DoesNotExist:
            logging.error("Object type %s Does not exist " % str(type(obj)))
            return False
        permObjs = []
        for p in perms:
            try:
                permObjs.append(Permission.objects.get(codename=p,content_type__app_label=objType.app_label))
            except Permission.DoesNotExist:
                logging.error("Permission %s Does not exist " % p)
        remove = {}
        for permObj in permObjs:
            remove[permObj.codename] = []
            for prin in principals:
                try:
                    ObjectPermission.objects.get(
                                objectType=objType,
                                target=obj.id,
                                principal=prin,
                                permission=permObj).delete()
                    remove[permObj.codename].append(prin)
                except:
                    logging.error("Failed to remove permission %s " % traceback.format_exc())
        return remove
    


class PrincipalAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'group', 'type')

class ObjectPermissionAdmin(admin.ModelAdmin):
    list_display = ('id', 'objectType', 'target', 'permission', 'principal')

class ObjectTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    
    
admin.site.register(Principal, PrincipalAdmin)
admin.site.register(ObjectPermission, ObjectPermissionAdmin)
admin.site.register(ObjectType, ObjectTypeAdmin)


