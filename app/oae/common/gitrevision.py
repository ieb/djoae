'''
Created on May 9, 2012

@author: ieb
'''
import os

ROOT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))


def _getGitRevision(path):
    if os.path.exists("%s/.git/HEAD" % path):
        f = open("%s/.git/HEAD" % path)
        ref = f.read().split(' ')[1].strip()
        f.close()
        branch = os.path.basename(ref)
        if os.path.exists("%s/.git/%s" % (path, ref)):
            f = open("%s/.git/%s" % (path, ref))
            revision = f.read().strip()
            f.close()
            return revision, branch
    if os.path.exists("%s/version.txt" % path):
        f = open("%s/version.txt" % path)
        revision = f.read()
        f.close()
        branch = "deployed"        
        return revision, branch 
    return "unknown-revision", "unknown-branch"
        
    

GIT_REVISION, GIT_BRANCH = _getGitRevision(ROOT_PATH)
UI_GIT_REVISION, UI_GIT_BRANCH = _getGitRevision("%s/static-data/ui" % ROOT_PATH)


def git_revivion_contextprocessor(request):
    return {
            'GIT_REVISION' : GIT_REVISION,
            'GIT_BRANCH' : GIT_BRANCH,
            'UI_GIT_REVISION' : UI_GIT_REVISION,
            'UI_GIT_BRANCH' : UI_GIT_BRANCH
            }