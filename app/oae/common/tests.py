import os
import sys

THIS_LOCATION = os.path.dirname(os.path.abspath(__file__))
workspace = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append("%s/app" % workspace)
os.environ['DJANGO_UNIT_TEST'] = 'true'
os.environ['DJANGO_SETTINGS_MODULE'] = 'oae.settings'


from django.contrib.auth.models import User
from django.template.base import TemplateDoesNotExist
from django.test.client import Client
import django.utils.simplejson as json
import time
from django.test.testcases import TestCase
from oae.common.operations import ImportOperation

class OAETestClient(Client):
    
    def __init__(self,test,**kv):
        super(OAETestClient,self).__init__(**kv)
        self.test = test
    
    def checkMe(self, username):
        response = self.get("/system/me")
        meJson = json.loads(response.content)
        if username is None:
            username = "anonymous"
        self.test.assertTrue("username" in meJson,"Username missing from response %s " % json.dumps(meJson,indent=2,ensure_ascii=False))
        self.test.assertEquals(username, meJson['username'],"Username incorrect in me response %s " % meJson['username'])
        self.test.assertTrue("userid" in meJson,"Userid missing from response %s " % json.dumps(meJson,indent=2,ensure_ascii=False))
        return meJson

    def createAdmin(self):
        User.objects.create_superuser("admin", "admin@example.org", "admin").save()
        
    def loginAdmin(self):
        self.logout()
        super(OAETestClient,self).login(username="admin",password="admin")
        super(OAETestClient,self).get("/system/login")
        self.checkMe("admin")
        
    def login(self, username=None, password=None):
        self.logout()
        super(OAETestClient,self).login(username=username,password=password)
        super(OAETestClient,self).get("/system/login")
        self.checkMe(username)
        
        

    def createUser(self):
        n = time.time()
        username = "testCreateUser_%s" % n
        firstname = "firstname_%s" % n
        lastname = "lastname_%s" % n
        email = "%s@example.org" % username
        password = "test"
        self.post("/system/userManager/user.create.html",
                    {
                    ':name' : username,
                    'firstName' : firstname,
                    'lastName' : lastname,
                    'email' : email,
                    ':sakai:profile-import' : '{"basic":{"elements":{"firstName":{"value":"%s"},"lastName":{"value":"%s"},"email":{"value":"%s"}},"access":"everybody"},"email":"%s"}' % (firstname, lastname, email, email),
                    'pwd' : password,
                    'pwdConfirm' : password
                }, 201)
        return username, password
    
    def post(self, url, params, status=200):
        try:
            response = super(OAETestClient,self).post(url, params)
            self.test.assertEqual(status,response.status_code,"Failed perform post to %s status: %s, reason: %s" % (url, response.status_code, response.content))
            return response
        except TemplateDoesNotExist:
            if status != 404:
                self.test.fail("URL %s does not exist" % url)
    
    def get(self, url, status=200):
        try:
            response = super(OAETestClient,self).get(url)
            self.test.assertEqual(status,response.status_code, "Failed to get %s, status %s, reason %s" % (url, response.status_code, response.content))
            return response
        except TemplateDoesNotExist:
            if status != 404:
                self.test.fail("URL %s does not exist" % url)
    
    
class ImportOperationTest(TestCase):
    def test1(self):
        imp = ImportOperation()
        m = imp._import({"old": True}, {"xyz": True}, True, True, True)
        self.assertTrue("old" not in m)
        self.assertTrue("xvz" not in m)
        
    def test2(self):        
        imp = ImportOperation()
        m = imp._import({"leave": True, "replace": True, "replaceProp" : 3}, 
                        {"replace": {"a" : 1, "b": 2}, "replaceProp" : 4}, 
                        False, True, True)
        self.assertTrue("leave" in m)
        self.assertTrue(m["leave"])
        self.assertTrue("replace"  in m)
        self.assertTrue("a"  in m['replace'])
        self.assertTrue("b"  in m['replace'])
        self.assertTrue(m['replace']['a'] == 1)
        self.assertTrue(m['replace']['b'] == 2)
        self.assertTrue("replaceProp"  in m)
        self.assertTrue(m['replaceProp'] == 4)
        
    def test3(self):        
        imp = ImportOperation()
        m = imp._import({"leave": True, "replace": True, "replaceProp" : 3}, 
                        {"replace": {"a" : 1, "b": 2}, "replaceProp" : 4}, 
                        False, False, True)
        self.assertTrue("leave" in m)
        self.assertTrue(m["leave"])
        self.assertTrue("replace"  in m)
        self.assertTrue(m['replace'] == True)
        self.assertTrue("replaceProp"  in m)
        self.assertTrue(m['replaceProp'] == 4)
        
    def test4(self):        
        imp = ImportOperation()
        m = imp._import({"leave": True, "replace": True, "replaceProp" : 3}, 
                        {"replace": {"a" : 1, "b": 2}, "replaceProp" : 4}, 
                        False, False, False)
        self.assertTrue("leave" in m)
        self.assertTrue(m["leave"])
        self.assertTrue("replace"  in m)
        self.assertTrue(m['replace'] == True)
        self.assertTrue("replaceProp"  in m)
        self.assertTrue(m['replaceProp'] == 3)
