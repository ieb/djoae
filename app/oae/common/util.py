from base64 import urlsafe_b64encode, b64encode
from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse, HttpResponseNotFound, Http404,\
    HttpResponseServerError, HttpResponseForbidden, QueryDict
from hashlib import sha1
import hmac
import logging
import django.utils.simplejson as json
import urllib
import urllib2
from django.contrib.auth.models import User
from uuid import uuid1
from oae.common.operations import ImportOperation, IgnoreOperation
import os
import traceback
from django.views.static import serve
from time import strftime, time, gmtime
from django.core.urlresolvers import resolve, Resolver404
from django.utils.translation.trans_real import parse_accept_lang_header
import locale


RECAPTCHA_PRIVATE_KEY, RECAPTCHA_SERVER, HMAC_TOKEN_TTL, STATIC_EXPIRE_SECONDS = ( 
            settings.RECAPTCHA_PRIVATE_KEY, settings.RECAPTCHA_SERVER, settings.HMAC_TOKEN_TTL,
            settings.STATIC_EXPIRE_SECONDS
            )

JSON_CT="application/json; charset=utf8"

class Timezone(object):
    
    @staticmethod
    def getTimeZoneOffset(timezoneName):
        tzOffset = 11 #timezone(timezoneName).utcoffset(datetime.datetime.now(), is_dst=True).seconds/3600
        if tzOffset > 12:
            tzOffset = tzOffset-12
        return tzOffset


    @staticmethod
    def getTimeZoneName(timezoneName=None, country=None):            
        if timezoneName is None:
            try:
                if country is None:
                    timezoneName = "Australia/Sydney" #country_timezones('AU')
                else:
                    timezoneName = "Australia/Sydney" #country_timezones(country)
            except:
                timezoneName = "UTC"
        else:
            try:
                timezoneName = "Australia/Sydney" #timezone(timeZoneName).zone
            except:
                timezoneName = "UTC"
        return timezoneName

class InvalidHmacException(Exception):
    pass
    
class Hash(object):
    
    @staticmethod
    def hash(val, prefix=""):
        # NB the UI may use _ to denote a seperator on Ids
        valAsString = "%s:%s" % (prefix,unicode(val))
        return b64encode(sha1(valAsString.encode('utf-8')).digest(),'-$').replace("=", "")
    
    @staticmethod
    def genCompactUUID():
        return b64encode(uuid1().bytes,'-$').replace("=", "")
    
    @staticmethod
    def encodeUserHmac(user,url,secret):
        '''
        Creates a hmac token containing the userid, time and secret
        @param user:
        @param url:
        @param secret:
        '''
        msg = "%s:%s:%s" % ( user.id, time(), url)
        return "%s:%s" % (urlsafe_b64encode(hmac.HMAC(secret,msg,sha1).digest()), msg) 
        
    @staticmethod
    def decodeUserHmac(hmactoken,url,secret):
        '''
        Verifies a hmactoken, returning the User object or raising a InvalidHmacException
        if the hmactoken has expired or is invalid
        @param hmactoken:
        @param url:
        @param secret:
        '''
        (hmac, userid, t, url ) = hmactoken.split(":",4)
        if time() - time > HMAC_TOKEN_TTL:
            raise InvalidHmacException("Expired")
        msg = "%s:%s:%s" % ( userid, t, url)
        if hmac != urlsafe_b64encode(hmac.HMAC(secret,msg,sha1).digest()):
            raise InvalidHmacException("Hmacs don't match ")
        return User.objects.get(id=userid)
    
        
class Path(object):

    '''
    Get the path elements as a []
    If the last element contains a . the path element is the string upto the .
    '''
    @staticmethod
    def getPathElements(path):
        if path == '':
            return []
        if path[-1] == '/':
            path = path[:-1] 
        pathElements = path.split("/")
        lastElement = pathElements.pop()
        pathElements.append(lastElement.split('.',1)[0])
        return pathElements

    
OPERATIONS = {
    "tag" : IgnoreOperation(), # Not possible to tag deep down inside an object
    "deletetag" : IgnoreOperation(), # Not possible to tag deep down inside an object
    "import" : ImportOperation(),
    }

class Properties(object):
    
    
    @staticmethod
    def _convertValue(value, hint):
        if hint == "Boolean":
            return       (str(value).upper()[0] == "T")
        if hint == "Long":
            return long(value)
        if hint == "Integer":
            return int(value)
        return value
    
    @staticmethod
    def _addProperty(m, name, hint, value):
        if hint is not None:
            value = Properties.convertValue(value,hint)
        if m[name] is None:
            m[name] = [ value ]
        elif isinstance(m[name],list):
            m[name].append(value)
        else:
            m[name] = [ m[name], value]
            
        
    @staticmethod
    def _replaceProperty(m, name, hint, values):
        if isinstance(values,str) or isinstance(values,unicode):
            if hint is not None:
                values = Properties._convertValue(values,hint)
            if name.endswith('[]'):
                values = [ values ]
            m[name] = values
        elif len(values) == 1:
            value = values[0]
            if hint is not None:
                value = Properties._convertValue(value,hint)
            if name.endswith('[]'):
                value = [ value ]
            m[name] = value
        else:
            if hint is not None:
                toconvert = values
                values = []
                for v in toconvert:
                    values.append(Properties._convertValue(v,hint))
            m[name] = values
            
                
                
        
    '''
    Update the properties creating sub nodes as we go.
    '''
    @staticmethod
    def update(m, update, path=None):
        '''
        Update a child in a nested dict
        @param m: the dict to update, if None it will be created
        @param update: the values to update
        @param path: the path to the element to replace, it will be created if it does not exist
        @return:  the dict at the basem, if m is not None, then me, otherwise the newly created map.
        '''
        if m is None:
            m = {}
        parent = None
        basem = m
        childName = None
        if path is not None:
            pathElements = Path.getPathElements(path)
            for p in pathElements:
                
                if not isinstance(m,dict):
                    logging.error("Path %s contains a path element %s that is not a container %s " % (path,p,type(m)))
                    raise Exception("Path %s contains a path element %s that is not a container %s " % (path,p,type(m)))
                if p not in m:
                    m[p] = {}
                if m[p] is None:
                    m[p] = {}
                parent = m
                childName = p
                m = m[p]
        
        _updateParamsToIgnore = {
                ':removeTree' : 1, 
                ':replaceProperties' : 1, 
                ':replace' : 1, 
                ':content' : 1, 
                ':contentType' : 1, 
                '_charset_' : 1,  
                }
        update = Request.toQueryDict(update)
        
        for name, values in update.iterlists():
            if name in _updateParamsToIgnore:
                pass
            elif not name.startswith(':'):
                a = name.split('@',2)
                pname = a[0]
                hint = None
                if len(a) > 1:
                    hint = a[1]            
                if hint == 'Delete' and pname in m:
                    del m[pname]
                else:
                    Properties._replaceProperty(m,pname,hint,values)
            elif name == ":operation":
                if isinstance(values,list):
                    for op in values:
                        if op in OPERATIONS:
                            basem = OPERATIONS[op].doOperation(update, parent, basem, childName)
                        else:
                            logging.error("Operartion %s not supported, please implement " % op )
                else:
                    if values in OPERATIONS:
                        basem = OPERATIONS[values].doOperation(update, parent, basem, childName)
                    else:
                        logging.error("Operartion %s not supported, please implement " % op )
                    
            else:
                logging.info("Ignoring property %s %s ",name, values)
        return basem

    '''
    get the subtree identified by path as a json response
    '''
    @staticmethod
    def extract(path, m, create=False):
        if not isinstance(m, dict):
            raise TypeError("Expecting a dict to extract from not a %s " % type(m))
        if path is None:
            return m
        pathElements = Path.getPathElements(path)
        for p in pathElements:
            if p in m:
                if m[p] is None:
                    if create:
                        m[p] = {}
                    else:
                        return None
            else:
                if create:
                    m[p] = {}
                else:
                    return None
            m = m[p]
        return m
    
    @staticmethod
    def mergeTree(target, merge):
        '''
        Merge merge into target, not overwriting any elements.
        @param target:
        @param merge:
        '''
        if merge is None:
            return
        for k in merge.keys():
            if k in target:
                if isinstance(target[k], dict):
                    Properties.mergeTree(target[k], merge[k])
                elif isinstance(target[k], list):
                    target[k].append(merge[k])
                else:
                    target[k] = [
                        target[k],
                        merge[k]]
            else:
                target[k] = merge[k]
    
    @staticmethod
    def filter(m, exclude=None, include=None):
        for k in m.keys():
            if include is not None and k not in include:
                del(m[k])
            elif exclude is not None and k in exclude:
                del(m[k])
        return m
    
    @staticmethod
    def extractWithParent(source, path, create=False):
        '''
        Warning: This function is not that easy to use.
        Extracts the map at the path within the source.
        If 
        @param source a json string or a dict
        @param path the path into the source
        @param create if true the path will be created and attached, if false
            and the path does not exist it will not be created and the nearest
            ancestor will be returned in the 2nd element with the name of the 
            non missing element in the 3rd element, and None will be return as 
            the 4th element.
        @return: the rootmap, 
            the parent of the target map or if create is false and the node does
            not exist the nearest ancestor 
            the key of the target map within the parent or if create is false 
            and the node does not exist, the name of the first missing node in the path
            the targetmap or if create is false and the node does not exist, None.
        '''
        if source is not None:
            if isinstance(source, dict):
                sourceMapBase = source
            else:
                sourceMapBase = json.loads(source)
        else:
            sourceMapBase = {}
        if path is None or path == "" or path == "/":
            return sourceMapBase, None, path, sourceMapBase
            
        sourceMap = sourceMapBase
        sourceMapName = None
        pathParts = path.split("/")
        for pathPart in pathParts:
            sourceMapParent = sourceMap
            sourceMapName = pathPart
            if pathPart in sourceMap:
                sourceMap = sourceMap[pathPart]
            elif create:
                sourceMap[pathPart] = {}
                sourceMap = sourceMap[pathPart]
            else:
                return sourceMapBase, sourceMapParent, sourceMapName, None
        return sourceMapBase, sourceMapParent, sourceMapName, sourceMap

    
class Request:
    
    @staticmethod
    def get(queryDict,name,default=None):
        if name in queryDict:
            return queryDict[name]
        return default
    

    @staticmethod
    def encode(pv):
        if isinstance(pv, unicode):
            return pv.encode('utf-8')
        return pv    
        
    @staticmethod
    def processCaptcha(request):
        if ":create-auth" not in request.POST:
            logging.error("No captcha type specified, can't validate, set :create-auth= reCAPTCHA.net")
            return False, "No captcha type specified, can't validate, set :create-auth= reCAPTCHA.net"
        if request.POST[':create-auth'] != "reCAPTCHA.net":
            logging.error("Incompatible captcha type specified, can't validate, set :create-auth= reCAPTCHA.net ")            
            return False, "Incompatible captcha type specified, can't validate, set :create-auth= reCAPTCHA.net"
        if ":recaptcha-challenge" not in request.POST or len(request.POST[':recaptcha-challenge']) <= 0:
            logging.error("No challenge provided, can't validate, set :recaptcha-challenge")            
            return False, "No challenge provided, can't validate, set :recaptcha-challenge"
        if ":recaptcha-response" not in request.POST or len(request.POST[':recaptcha-response']) <= 0:
            logging.error("No response provided, can't validate, set :recaptcha-response")            
            return False, "No response provided, can't validate, set :recaptcha-response"
        
        validationParameters = {}
        validationParameters['challenge'] = Request.encode(request.POST[':recaptcha-challenge'])
        validationParameters['response'] = Request.encode(request.POST[':recaptcha-response'])
        validationParameters['privatekey'] = Request.encode(RECAPTCHA_PRIVATE_KEY)
        validationParameters['remoteip'] = Request.encode(request.META['REMOTE_ADDR'])
        
        validationRequest = urllib2.Request(
                    url= "http://%s/recaptcha/api/verify" % RECAPTCHA_SERVER,
                    data = urllib.urlencode(validationParameters),
                    headers = {
                        "Content-type": "application/x-www-form-urlencoded",
                        "User-agent": "Sakai Nakamura Python 0.1"
                    })
        validationResponse = urllib2.urlopen(validationRequest)
        validationValues = validationResponse.read().splitlines();
        validationResponse.close();
        return_code = validationValues[0] #@UnusedVariable
    
        if (validationValues[0] == "true"):
            return True, ""
        else:
            return False, validationValues[1]
        
    @staticmethod
    def _redispatch(url, request):
        try:
            view, args, kwargs = resolve(url)
            args = (request,) + args
            try:
                r =  view(*args, **kwargs)
                if r is None:
                    logging.error("ServerError with [%s] [%s]  " % ( args, kwargs))
                return r
            except Http404:
                logging.error("Redispatch Got 404 %s "  % (kwargs['path']))
                return HttpResponseNotFound()
            except:
                logging.error(traceback.format_exc())
                logging.error("Failed to Dispatch with [%s] [%s]  " % ( args, kwargs))
                return HttpResponseServerError(traceback.format_exc())
        except Resolver404:
            logging.error("Failed to Resolve [%s]" % ( url))
            return HttpResponseNotFound("Did not resolve %s " % url)
        
    @staticmethod
    def getLocale(request):
        if hasattr(request, "_locale"):
            return request._locale;
        if "locale" in request.REQUEST:
            requestLocale = request.REQUEST['locale']
        elif 'HTTP_ACCEPT_LANGUAGE' in request.META:
            requestLocales = parse_accept_lang_header(request.META.get('HTTP_ACCEPT_LANGUAGE', ''))
            requestLocale = locale.getdefaultlocale()
            for accept_lang, unused in requestLocales:
                if accept_lang != "*":
                    requestLocale = locale.normalize(accept_lang).replace("-", "_")
                    break;
        else:
            requestLocale = "en_US"
        
        request._locale = requestLocale
        return requestLocale

    @staticmethod
    def getTimeZone(request):        
        if 'timezone' in request.REQUEST:
            timezone = request.REQUEST['timezone']
        else:
            language, country = Request.getLocale(request).split("_") #@UnusedVariable
            timezone = Timezone.getTimeZoneName(country=country)
        return timezone
    
    @staticmethod
    def toQueryDict(o):
        if isinstance(o,QueryDict):
            return o
        q = QueryDict(False, mutable=True)
        for k,v in o.iteritems():
            if isinstance(v,list):
                q.setlist(k, v)
            else:
                q.setlist(k,[v])
        return q
    
class Response(object):
    
    @staticmethod
    def Ok(status=200, body='{"status":"Ok"}'):
        return HttpResponse(body, status=status, content_type=JSON_CT )
    
    @staticmethod
    def toHttpResponse(o):
        if o is None:
            return HttpResponseNotFound()
        if not o:
            return HttpResponseForbidden()
        if o == True:
            return Response.Ok()
        if isinstance(o, int):
            if o == 204:
                return HttpResponse(204)
            if o >= 200 and o < 300:
                return Response.Ok(status=o)
            return HttpResponse(status=o)
        return Response.Ok()
    
    
class Search(object):
    
    CONVERT_SORT = {
    '_lastModified' : 'modified',
    '_created' : 'created',
    'filename' : 'filename',
    }
    
    def __init__(self,defaultPage=0, defaultItems=10, defaultOrder='modified'):
        self.defaultPage = defaultPage
        self.defaultItems = defaultItems
        self.defaultOrder = defaultOrder

    def dbSearch(self, request, model, query): 
        '''
        Perform a paginated search and output the results
        '''
        try:
            page = int(Request.get(request.GET,"page",str(self.defaultPage)))
            items = int(Request.get(request.GET,"items",str(self.defaultItems)))
            pag = Paginator(query, items)
            items = list(pag.page(page+1).object_list)
            return { 'total' : pag.count, 'items' : len(items), 'results' : items }
        except PageNotAnInteger:
            items = list(pag.page(1).object_list)
            logging.error("Page 1 not an integer ")
            return { 'total' : pag.count, 'items' : len(items), 'results' : items }
        except EmptyPage:
            items = list(pag.page(pag.num_pages).object_list)
            return { 'total' : pag.count, 'items' : 0, 'results' : [] }
        except model.DoesNotExist:
            logging.error("Page Null Page " )
            return {'total': 0, 'items' : 0, 'results' : []}
    
    
    def getSortOn(self, request):
        requestedOrder = None
        try:
            requestedOrder = request.GET['sortOn']
            sortOn = Search.CONVERT_SORT[requestedOrder]
        except:
            sortOn = self.defaultOrder
        try:
            if request.GET['sortOrder'] == 'desc':
                sortOn = "-%s" % sortOn
        except:
            pass
        return sortOn


class MultiEncoder(json.JSONEncoder):
    '''
    Allows many encoders to be chained together
    '''

    
    def __init__(self, encoder_classes=None, object_levels=100, **kw):
        super(MultiEncoder,self).__init__(**kw)
        self.encoders=[]
        if encoder_classes is not None:
            for e in encoder_classes:
                enc = e(**kw)
                enc.object_levels = object_levels
                self.encoders.append(enc)
        else:
            raise Exception("Please be certain to set encoder_classes = to a list of encoder classes, was clalled with  %s " % (kw))

    def default(self, obj):
        for e in self.encoders:
            try:
                return e.default(obj)
            except TypeError:
                pass
        return super(MultiEncoder,self).default(obj)



class TreeBuilder(object):
    '''
    Builds json response files and serves them from caches on disk.
    The json response files conform to the sling import protocol where .json files are used to set properties at a node.
    '''
    
    def __init__(self, source, cache):
        self.source = source
        self.cache = cache
        
    def getResponse(self, request, path):
        cachedFile = "%s/%s" % (self.cache, path)
        if os.path.exists(cachedFile):
            return FileSystemContent.static(request, document_root=self.cache, path=path)
        file = os.path.basename(path)
        fileparts = file.split('.')
        filestub = fileparts[0]
        depth = 1
        try:
            extension = fileparts[-1:][0]
            selectors = fileparts[1:-1]
            for i in selectors:
                if depth == 'infinity':
                    depth = -1
                    break
                try:
                    depth = int(i)
                except:
                    pass
        except:
            return HttpResponseNotFound()
        if extension != 'json':
            return FileSystemContent.static(request, document_root=self.source, path=path)

        sourcePath = "%s/%s" % ( os.path.dirname(path), filestub )
        absSourcePath = "%s/%s" % ( self.source, sourcePath)
        if not os.path.exists(absSourcePath):
            return HttpResponseNotFound()
        logging.error("Building json tree for %s at depth %s " % (sourcePath, depth))
        m = self._buildJsonTree(sourcePath, depth=depth)
        parentCachedFile = os.path.dirname(cachedFile)
        if not os.path.exists(parentCachedFile):
            os.makedirs(parentCachedFile)
        fout = open(cachedFile,"w")
        json.dump(m, fout, indent=4)
        fout.close()
        logging.error("Wrote %s" % (cachedFile))
        return FileSystemContent.static(request, document_root=self.cache, path=path)
        
        
    def _buildJsonTree(self, sourcePath, depth=-1):
        if depth == 0:
            return None
        m = {}
        jsonFile = "%s/%s.json" % (self.source, sourcePath)
        if os.path.exists(jsonFile):
            jf = open(jsonFile,"r")
            try:
                logging.error("loading %s " % jsonFile)            
                m = json.load(jf)
            except:
                logging.error(traceback.format_exc())
                logging.error("Failed to load %s " % jsonFile)
            jf.close()
        subFolders = os.listdir("%s/%s" % ( self.source, sourcePath))
        for f in subFolders:
            absFile = "%s/%s/%s" % (self.source, sourcePath, f)
            if os.path.isdir(absFile):
                v = self._buildJsonTree("%s/%s" % (sourcePath, f), depth = depth - 1)
                if v is not None:
                    m[f] = v
            elif absFile.endswith(".json") and not os.path.isdir(absFile[:-5]):
                jf = open(absFile,"r")
                try:
                    logging.error("loading leaf %s %s for %s " % (absFile, absFile[:-5], f[:-5]))            
                    m[f[:-5]] = json.load(jf)
                except:
                    logging.error(traceback.format_exc())
                    logging.error("Failed to load leaf %s " % absFile)
            elif os.path.isdir(absFile[:-5]):
                pass
            else:
                logging.error("Had file %s with no dir at %s %s " % (absFile, absFile[:-5],os.path.isdir(absFile[:-5]) ))
                m[f] = {
                        '_length' : os.path.getsize(absFile)
                    }
            
        return m
    
class FileSystemContent(object):
    
    
    @staticmethod
    def static(request, path, document_root=settings.DOCUMENT_ROOT, show_indexes=False):
        """
        Serve static content and adjust Exipres headers.
        """
        response = serve(request, path, document_root=document_root, show_indexes=show_indexes)
        if STATIC_EXPIRE_SECONDS is not None:
            del response['Last-Modified']
            del response['Cache-Control']
            del response['ETag']    
            del response['Vary']    
            expire = strftime("%a, %d %b %Y %H:%M:%S GMT", gmtime(time()+STATIC_EXPIRE_SECONDS))
            response['Expires'] = expire
            response['Cache-Control'] = 'cache'
            if hasattr(request,"session"):
                request.session.accessed = False  # Stops the session middleware from varying by cookie
        return response



