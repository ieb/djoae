'''
Created on Mar 29, 2012

@author: ieb
'''
from django.utils import simplejson as json


class IgnoreOperation(object):
    def doOperation(self,update, parent, basem, childName):
        return basem
    
    
class ImportOperation(object):
    def _import(self, m,replacement, removeTree, replace, replaceProperties):
        '''
        Replace a section of the dict
        @param m: the dict to be replaced
        @param replacement: the replacement dict
        @param removeTree: if true then the whole tree is replaced
        @param replace: if true then subtrees will be replaced
        @param replaceProperties: if true then properties will be replaced.
        '''
        if removeTree or not isinstance(m,dict) :
            return replacement
        for name, value in replacement.items():
            if isinstance(value,dict):
                if name in m:
                    if replace:
                        m[name] = self._import(m[name],value, removeTree, replace, replaceProperties)
                else:
                    m[name] = value
            else:
                if name in m:
                    if replaceProperties:
                        m[name] = value
                else:
                    m[name] = value
        return m

    def doOperation(self, update, parent, basem, childName):
        '''
        Update
        @param update the data to update:
        @param parent the parent map, if None, this is an update to the basem:
        @param basem the base map:
        @param childName the child name in the parent if parent is None, this can be None:
        '''
        removeTree =  ":removeTree" in update and update[':removeTree'] == "true"
        removeProperties =  ":replaceProperties" in update and update[':replaceProperties'] == "true"
        replace =  ":replace" in update and update[':replace'] == "true"
        replacement = json.loads(update[":content"])
        if parent is None:
            return self._import(basem,replacement,removeTree, replace, removeProperties)
        else:
            parent[childName] = self._import(parent[childName],replacement,removeTree, replace, removeProperties)
            return basem
        
        