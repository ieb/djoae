'''
Created on May 21, 2012

@author: ieb
'''
from django.dispatch.dispatcher import Signal

deliver_message = Signal(providing_args=["instance"])
message_delivered = Signal(providing_args=["instance"])