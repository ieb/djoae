'''
Created on Mar 20, 2012

@author: ieb
'''
from django.conf.urls.defaults import patterns, url
from oae.message.views import personalMessageCount, personalMessageCreate,\
    personalMessageUpdate
urlpatterns = patterns('',
    url(r'^count\.json$', personalMessageCount),
    url(r'^create\.html', personalMessageCreate),
    url(r'^(?P<messageid>.*?)/', personalMessageUpdate),
    url(r'^(?P<messageid>.*?)\.', personalMessageUpdate),
    url(r'^(?P<messageid>.*)', personalMessageUpdate),
)
