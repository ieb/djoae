'''
Created on Mar 20, 2012

@author: ieb
'''
from django.test import TestCase
from oae.common.tests import OAETestClient
import logging

class MessageSendingTest(TestCase):

    def setUp(self):
        self.client = OAETestClient(self)
        self.client.createAdmin()
        self.client.loginAdmin()
        self.username1, self.password1 = self.client.createUser()
        self.username2, self.password2 = self.client.createUser()
        self.username3, self.password3 = self.client.createUser()
        self.client.logout()

    def testSendMessage(self):
        self.client.login(username=self.username1, password=self.password1)
        meJson = self.client.checkMe(self.username1)
        storageUrlPrefix = meJson['user']['userStoragePrefix']
        self.client.post("/%s%s" % ( storageUrlPrefix, "message.create.html"), 
                            {
                             "sakai:to" : [ u"internal:%s" % u for u in [self.username2, self.username3]],
                             "sakai:from" : self.username1,
                             "sakai:subject" : "Test Message",
                             "sakai:body" : "Message Body, Message Body"
                             }, 
                            201)
        logging.error(self.client.get("/var/message/boxcategory-all.json"))
        
        
