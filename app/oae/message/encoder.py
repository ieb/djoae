
import django.utils.simplejson as json
from oae.message.models import MessageBox
from oae.user.models import ProfileLoader, InternalUserGroup
from oae.user.encoder import ProfileEncoder
import time

    

class MessageBoxEncoder(json.JSONEncoder):
    
    
    @staticmethod
    def depend():
        s = ProfileEncoder.depend()
        s.update([MessageBoxEncoder])
        return s
    
        
    def _getEnvelopeValue(self, envelope,key):
        try:
            return envelope[key]
        except KeyError:
            return ""
        
    def _getUGIds(self, envelope, key):
        l = self._getEnvelopeValue(envelope, key)
        if type(l) == type(list()):
            l = ",".join(l)
        return l.replace("internal:", "").split(",")
        
    def default(self, obj):
        if isinstance(obj, MessageBox):
            envelope = json.loads(obj.envelope)
            if not hasattr(self,'profileLoader'):
                self.profileLoader = ProfileLoader()

            toIds = self._getUGIds(envelope,'to')
            self.profileLoader.add(toIds)
            fromIds = self._getUGIds(envelope,'from')
            self.profileLoader.add(fromIds)
            self.profileLoader.load()                
            userTo = self.profileLoader.filter(toIds)
            userFrom = self.profileLoader.filter(fromIds)
            ugid = InternalUserGroup.getUgid(obj.user)
            return {
                "envelope" : str(obj.envelope),
                "sakai:category" : obj.message.category,
                "_created": time.mktime(obj.delivered.timetuple())*1000,
                "_path" : "/~%s/message/%s" % ( ugid, obj.message.messageid),
                "sakai:type": obj.message.type,
                "sakai:to": self._getEnvelopeValue(envelope, 'to'),
                "sakai:messagestore": "/~%s/message/" % ugid,
                "sakai:sendstate": obj.getSendStatus(),
                "sakai:body": obj.message.body,
                "sakai:subject": obj.message.subject,
                "sakai:read": obj.getReadState(),
                "sakai:messagebox": obj.box,
                "sakai:id": obj.message.messageid,
                "sakai:from": userFrom,
                "id": obj.message.messageid,
                "userTo" : userTo,
                "userFrom" : userFrom
                }
        return super(MessageBoxEncoder,self).default(obj)
