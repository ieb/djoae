from django.conf import settings
from django.db.models.aggregates import Count
from django.http import HttpResponseNotAllowed, HttpResponse, \
    HttpResponseForbidden, HttpResponseBadRequest
from models import Message
from oae.common.util import Request, Response, JSON_CT, Search,\
    MultiEncoder
from oae.message.encoder import MessageBoxEncoder
from oae.message.models import MessageBox
from oae.user.models import InternalUserGroup
from oae.user.util import UserUtil
import logging
import django.utils.simplejson as json
from oae.message.message_manager import MessageManager
from django.views.decorators.cache import never_cache

JSON_INDENT = ( settings.JSON_INDENT)

    
@never_cache  # Cache at a lower level with invalidation.
def doBoxCategoryAll(request):
    '''
    {"items":18,"results":[{"sakai:category":"invitation","_charset_":"utf-8","sakai:created":"2012-02-01T04:21:57-05:00","_lastModifiedBy":"ieb","sakai:type":"internal","sakai:to":"internal:ieb","_createdBy":"admin","_copiedFrom":"/~leward/message/outbox/9da1b2a7c607c6392446630ffd4773f30a8b4bf8","_path":"/~ieb/message/inbox/9da1b2a7c607c6392446630ffd4773f30a8b4bf8","_copiedFromId":"MGEv4Ey2EeGcVY8gRaTSfQ+","sakai:messagestore":"/~ieb/message/","sling:resourceType":"sakai/message","_created":1328088117868,"sakai:sendstate":"notified","_id":"MHbawEy2EeGcVY8gRaTSfQ+","sakai:body":"Lynn Ward has invited you to become a contact: \n\nI would like to invite you to become a member of my network on Sakai.\r\n\r\n- Lynn","_copiedDeep":true,"_lastModified":1328088214679,"sakai:subject":"Lynn Ward has invited you to become a connection","sling:resourceSuperType":"sparse/Content","sakai:read":"true","sakai:messagebox":"trash","sakai:id":"9da1b2a7c607c6392446630ffd4773f30a8b4bf8","sakai:from":"leward","id":"9da1b2a7c607c6392446630ffd4773f30a8b4bf8","userTo":[{"hash":"ieb","basic":{"access":"everybody","elements":{"lastName":{"value":"Boston"},"email":{"value":"ieb@tfd.co.uk"},"firstName":{"value":"Ian"}}},"rep:userId":"ieb","userid":"ieb","counts":{"contactsCount":3,"membershipsCount":0,"contentCount":1,"countLastUpdate":1331337564824},"sakai:excludeSearch":false,"homePath":"/~ieb","user":"ieb","sakai:status":"offline","sakai:location":"none"}],"userFrom":[{"hash":"leward","basic":{"access":"everybody","elements":{"picture":{"value":"{\"name\":\"256x256_tmp1327853041583.jpg\",\"_name\":\"tmp1327853041583.jpg\",\"_charset_\":\"utf-8\",\"selectedx1\":0,\"selectedy1\":27,\"selectedx2\":260,\"selectedy2\":287}"},"sakai:tags":{"value":["directory/mathematicalandcomputersciences/computerscience","directory/mathematicalandcomputersciences/informationsystems"]},"lastName":{"value":"Ward"},"email":{"value":"leward@iupui.edu"},"firstName":{"value":"Lynn"}}},"rep:userId":"leward","userid":"leward","counts":{"contactsCount":35,"membershipsCount":10,"contentCount":98,"countLastUpdate":1331337564244},"sakai:excludeSearch":false,"homePath":"/~leward","user":"leward","sakai:status":"offline","sakai:location":"none","sakai:state":"ACCEPTED","sakai:types":[]}]},{"sakai:category":"invitation","_charset_":"utf-8","sakai:created":"2012-01-31T18:42:34-05:00","_lastModifiedBy":"ieb","sakai:type":"internal","sakai:to":"internal:ieb","_createdBy":"admin","_copiedFrom":"/~johnfking2/message/outbox/07ba40cd8f7f1377c11fbd15be8918f6c43cb6d5","_path":"/~ieb/message/inbox/07ba40cd8f7f1377c11fbd15be8918f6c43cb6d5","_copiedFromId":"QA5DcExlEeGcVY8gRaTSfQ+","sakai:messagestore":"/~ieb/message/","sling:resourceType":"sakai/message","_created":1328053354959,"sakai:sendstate":"notified","_id":"QCfl8ExlEeGcVY8gRaTSfQ+","sakai:body":"John King has invited you to become a contact: \n\nI would like to invite you to become a member of my network on Sakai.\r\n\r\n- John","_copiedDeep":true,"_lastModified":1328088216525,"sakai:subject":"John King has invited you to become a connection","sling:resourceSuperType":"sparse/Content","sakai:read":"true","sakai:messagebox":"trash","sakai:id":"07ba40cd8f7f1377c11fbd15be8918f6c43cb6d5","sakai:from":"johnfking2","id":"07ba40cd8f7f1377c11fbd15be8918f6c43cb6d5","userTo":[{"hash":"ieb","basic":{"access":"everybody","elements":{"lastName":{"value":"Boston"},"email":{"value":"ieb@tfd.co.uk"},"firstName":{"value":"Ian"}}},"rep:userId":"ieb","userid":"ieb","counts":{"contactsCount":3,"membershipsCount":0,"contentCount":1,"countLastUpdate":1331337564824},"sakai:excludeSearch":false,"homePath":"/~ieb","user":"ieb","sakai:status":"offline","sakai:location":"none"}],"userFrom":[{"hash":"johnfking2","basic":{"access":"everybody","elements":{"lastName":{"value":"King"},"email":{"value":"johnk@media.berkeley.edu"},"firstName":{"value":"John"}}},"rep:userId":"johnfking2","userid":"johnfking2","counts":{"contactsCount":23,"membershipsCount":1,"contentCount":4,"countLastUpdate":1331337564876},"sakai:excludeSearch":false,"homePath":"/~johnfking2","user":"johnfking2","sakai:status":"offline","sakai:location":"none","sakai:state":"ACCEPTED","sakai:types":["__MSG__COLLEAGUE__"]}]}],"total":2,"unread":0}
    @param request:
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    
    fieldMapping = {
        "_created" : "delivered"
    }
    
    box = Request.get(request.GET,"box")
    sortOn = fieldMapping[Request.get(request.GET,"sortOn","_created")]
    sortOrder = Request.get(request.GET,"sortOn","desc")
    category = Request.get(request.GET,"category","message")
    
    if sortOrder == "desc":
        sortOn = "-" + sortOn
    

    search = Search()
    if category == '*':
        
        resultsMap = search.dbSearch(request, Message,
                    MessageBox.objects.select_related().filter(user=request.user, box=box).
                               order_by(sortOn,'message__subject'))
    else:
        resultsMap = search.dbSearch(request, Message,
                    MessageBox.objects.select_related().filter(user=request.user, box=box, category=category).
                               order_by(sortOn,'message__subject'))
    return HttpResponse(json.dumps(resultsMap, cls=MultiEncoder, encoder_classes=MessageBoxEncoder.depend(), indent=JSON_INDENT,ensure_ascii=False), JSON_CT)


    
@never_cache  # Cache at a lower level with invalidation.
def personalMessageCreate(request,ugid):
    '''
    Post operation only,
    Request Parmaters.
    sakai:type the type of message, optional defaults to smtp
    sakai:sendstate (pending) ignored defaults to pending
    sakaimessagebox (outbox) optional defautls to outbox
    sakai:to multi list of usernames required
    sakai:from single list of uiserid its from (ignored by delivery)
    sakai:subject subject required
    sakai:body body required
    sakai:category "message" optional, defaults to message
    
    from is overwritten on the envelope on delivery to match ugid otherwise the service would be insecure.
    there are no restrictions on who you can send to.
    
    @param request:
    @param ugid:
    '''
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    if not UserUtil.checkUserMatches(request.user, ugid):
        return HttpResponseForbidden()
    type = Request.get(request.POST,"sakai:type","smtp")
    status = Request.get(request.POST,"sakai:sendstate","pending") #@UnusedVariable
    box = Request.get(request.POST,"sakai:messagebox","outbox")
    if box == "pending":
        return Response.Ok(status=200)
    to = Request.get(request.POST,"sakai:to").split(",")
    mfrom = Request.get(request.POST,"sakai:from")
    subject = Request.get(request.POST,"sakai:subject")
    body = Request.get(request.POST,"sakai:body")
    category = Request.get(request.POST,"sakai:category","message")
    
    headers = {}  # need to fill this in
    return Response.toHttpResponse(MessageManager(request.user).sendMessage(to=to, subject=subject, body=body, mfrom=mfrom, category=category, headers=headers, sendingUgid=ugid, targetBox=box ))


@never_cache  # Cache at a lower level with invalidation.
def personalMessageUpdate(request, ugid, messageid):
    if "POST" == request.method:
        if UserUtil.checkUserMatches(request.user, ugid):
            
            return Response.toHttpResponse(MessageManager(request.user).updateMessage(messageid,request.POST,ugid=ugid))
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseNotAllowed("POST")
    
    
@never_cache  # Cache at a lower level with invalidation.
def personalMessageCount(request, ugid):
    '''
    [09/Mar/2012 00:22:32] "GET /~CyimLXWB3_zs8Md7pUgugZgue-Y/message.count.json?filters=sakai:messagebox,sakai:read&values=inbox,false&groupedby=sakai:category&_charset_=utf-8&_=1331274151984 HTTP/1.1" 200 2
    {"count":[{"group":"message","count":1}]}
    {"filter": {"box": "inbox", "readstate": "unread", "useid": "CyimLXWB3_zs8Md7pUgugZgue-Y"}, "count": [{"category": "message", "messages_count": 6}], "groupby": ["category"]}
      Gets the personal message count
    '''
    # TODO, provide proper implementation
    if "GET" == request.method:
        if UserUtil.checkUserMatches(request.user, ugid):
            countUser = InternalUserGroup.objects.get(ugid=ugid)
            filters = Request.get(request.GET,'filters')
            values = Request.get(request.GET, "values")
            if filters is None or values is None:
                logging.error("No filter or value Request")
                return HttpResponseBadRequest()
            if filters == 'sakai:messagebox,sakai:read':
                box, readstate = values.split(',')
                if readstate == "true":
                    readstate = "R"
                else:
                    readstate = "U"
                groupedby = Request.get(request.GET, "groupedby")
                if groupedby is not None:
                    groups = MessageBox.convertToFields(groupedby.split(","))
                    if len(groups) > 1:
                        logging.error("More than 1 groupby specified ")
                        return HttpResponseBadRequest()
                        
                    messageCounts = list(MessageBox.objects.filter(user=countUser,box=box,readstate=readstate)
                                         .values(*groups)
                                         .annotate(count=Count("user")).order_by())
                    
                    for m in messageCounts:
                        for g in groups:
                            if g in m:
                                if g == "message__category":
                                    m["group"] = m[g]
                                    del m[g]

                    return HttpResponse(json.dumps({ 'filter' : {
                                    'box' : box,
                                    'userid' : ugid,
                                    'readstate' : readstate,
                                    },
                                    'groupby' : groups,
                                    'count' : messageCounts }, indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)
                else:
                    messageCounts = list(Message.objects.filter(user=countUser,box=box,readstate=readstate)
                                         .annotate(count=Count("user")).order_by())

                    return HttpResponse(json.dumps({ 'filter' : {
                                    'box' : box,
                                    'userid' : ugid,
                                    'readstate' : readstate,
                                    },
                                    'count' : messageCounts }, indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)
                    
            else:
                logging.error("Unrecognized filter Request ")
                return HttpResponseBadRequest()
                
        else:
            return HttpResponseForbidden()
    return HttpResponseNotAllowed(["GET"])


