'''
Created on Mar 26, 2012

@author: ieb
'''
from oae.message.models import MessageBox
from oae.common.util import Response


class DeleteOperation(object):
    '''
    Delete the message box item. Dont delete the Message itself, admins can do that later if required
    '''
    
    def doOperation(self, user, changes, ugid, messageid):
        MessageBox.objects.filter(user=user,message__messageid=messageid).delete()
        return Response.Ok()