'''
NB: Not change the schema without generating a migration.

'''

from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete
from oae.user.models import Profile



class Message(models.Model):
    '''
    Defines messages. Messages are associated with a user, they have a body, headers,
    a message box and status. The sent field is when they were sent. The fields are present in
    the model to enable rapid retrival.
    '''
    
    id = models.AutoField(
                primary_key=True,
                help_text="Internal ID, not used by the application directly")
    messageid = models.CharField(
                verbose_name="Message ID", 
                max_length=32)
    sendinguser = models.ForeignKey(User,related_name="oaemessage")
    category = models.CharField(
                verbose_name="Message Category",
                max_length=20,
                help_text="The category of message")
    subject = models.CharField(
                verbose_name="Subject of the Message",
                max_length=1024,
                help_text="The message box the message is in")
    sent = models.DateTimeField(
                auto_now_add=True,
                db_index=True,
                verbose_name="Time the message was sent",
                help_text="When the message was sent")
    type = models.CharField(
                verbose_name="Message Type",
                max_length=20,help_text="The message Type")
    body = models.TextField(
                verbose_name="Message Body",
                help_text="The body of the message" )
    

class MessageBox(models.Model):
    READSTATE_CHOICES=(
            ('U','Unread'),
            ('R', 'Read'),
            )
    STATUS_CHOICES=(
            ('W','Pending'),
            ('P','Processing'),
            ('F','Failed'),
            ('D','Delivered'),
            )
    TO_MODEL = {
        "sakai:category" : 'category',
        "_created": 'delivered',
        "sakai:sendstate": 'status',
        "sakai:read": 'readstate',
        "sakai:messagebox": 'box',
#        "sakai:id": 'message__messageid',
#        "sakai:type": 'message__type',
#        "sakai:body": 'message__body',
#        "sakai:subject": 'message__subject',
    }

    id = models.AutoField(
                primary_key=True,
                help_text="Internal ID, not used by the application directly")
    message = models.ForeignKey(Message)
    user = models.ForeignKey(User)
    box = models.CharField(
                verbose_name="Message Box",
                max_length=20,
                db_index=True,
                help_text="The message box the message is in")
    status = models.CharField(
                verbose_name="Status of the message",
                max_length=1,
                db_index=True,
                help_text="Status of the message", 
                choices=STATUS_CHOICES)
    readstate = models.CharField(
                verbose_name="Read Status of the message",
                max_length=1,
                db_index=True,
                help_text="Read Status of the message", 
                choices=READSTATE_CHOICES)
    category = models.CharField(
                verbose_name="Message Category",
                max_length=20,
                help_text="The category of message")
    envelope = models.TextField(
                verbose_name="Message Envelope",
                help_text="The envelope" )
    headers = models.TextField(
                verbose_name="Message headers",
                help_text="Headers for the messages")
    delivered = models.DateTimeField(
                auto_now_add=True,
                verbose_name="Time the message was delivered to this message box",
                help_text="When the message was sent")
    
    
    def getReadState(self):
        return self.readstate == 'R'
    
    def getSendStatus(self):
        for r in self.STATUS_CHOICES:
            if self.status == r[0]:
                return r[1]
        return 'Pending'

    @staticmethod
    def convertToFields(uiFieldNames):
        for i in range(0,len(uiFieldNames)):
            if uiFieldNames[i] in MessageBox.TO_MODEL:
                uiFieldNames[i] = MessageBox.TO_MODEL[uiFieldNames[i]]
        return uiFieldNames
    
class MessageBoxStats(object):
    @staticmethod
    def syncFromMessageBox(sender, instance=None, created=False, **kwargs):
        if instance is not None:
                profile = Profile.objects.get(user=instance.user)
                profile.messages_unread = MessageBox.objects.filter(user=instance.user,box='inbox',readstate='U').count()
                profile.save()
                
        
post_save.connect(MessageBoxStats.syncFromMessageBox, sender=MessageBox)
post_delete.connect(MessageBoxStats.syncFromMessageBox, sender=MessageBox)

    
    


class MessageAdmin(admin.ModelAdmin):
    list_display = ('messageid', 'sendingusername', 'subject', 'sent')
    list_filter = ('category', 'type')
    search_fields = ('user__username', 'subject', 'envelope', 'headers')

    def sendingusername(self,obj):
        return obj.sendinguser.username

class MessageBoxAdmin(admin.ModelAdmin):
    list_display = ('username', 'subject', 'box', 'status', 'delivered')
    list_filter = ('box','status','readstate')
    search_fields = ('user__username', 'message__subject', 'envelope')

    def username(self,obj):
        return obj.user.username
    
    def subject(self,obj):
        return obj.message.subject

admin.site.register(Message, MessageAdmin)
admin.site.register(MessageBox, MessageBoxAdmin)
