'''
Created on Mar 7, 2012

@author: ieb
'''
from django.contrib.auth.decorators import permission_required
from django.db import transaction, models
from multiprocessing.synchronize import Semaphore
from oae.common.util import Response
from oae.message.models import MessageBox
from oae.user.models import InternalUserGroup
from threading import Thread
import django.utils.simplejson as json
import logging
import time
import traceback
from oae.message import message_signals
from django.contrib.auth.models import User
from django.views.decorators.cache import never_cache




class Delivery(object):
    '''
    Delivers messages
    '''
    running = False
    routers = []
    sem = None
    
    @staticmethod
    def notify(**kwargs):
        '''
        Notifiy the delivery thread there is a message to send.
        '''
        
        if Delivery.sem is None:
            Delivery.sem = Semaphore()
            # Start the deivery thread if its not running.
            deliveryProcessor = Delivery()
            deliveryThread = Thread(target=deliveryProcessor.run, name="MessageDelieryProcessor")
            deliveryThread.setDaemon(True)
            deliveryThread.start()
        Delivery.sem.release()
        
    @staticmethod
    def end():
        '''
        End the delivery thread.
        '''
        Delivery.running = False
        Delivery.sem.release()
        Delivery.sem = None
            
    def __init__(self):
        Delivery.running = True
        self.daemon = True
    
    def run(self):
        try:
            while Delivery.running:
                logging.error("Waiting for message")
                Delivery.sem.acquire()
                tries = 0
                while Delivery.running:
                    try:
                        transaction.enter_transaction_management()
                        if not self.sendMessage():
                            if tries > 0: 
                                # deal with the uncommitted transaction by sleeping
                                break;
                            time.sleep(1)
                        tries = tries+1
#                        for q in connection.queries:
#                            logging.error("Query %s " % q)
                        transaction.commit()
                    except:
                        try:
                            transaction.rollback()
                        except:
                            pass
                    finally:
                        try:
                            transaction.leave_transaction_management()
                        except:
                            pass
        except:
            logging.error(traceback.format_exc())
        
    def sendMessage(self):
        '''
        Check for message
        check sender
        devilver by dropping the message into the target users inbox
        '''
        try:
            # this really should be select for update, but thats not supported in this version of Django
            messageBox = MessageBox.objects.filter(status="W", box="outbox").order_by('-id')[0]
            # eat any pending semaphores that arrived when were performing that query
            while Delivery.sem.acquire(False):
                pass
            messageBox.status = "P"
            messageBox.save()
        except:
            return False
        try:
            envelope = json.loads(messageBox.envelope)
        except:
            logging.error("Could not open envelope %s " % messageBox.envelope)
            envelope = {}
        try:
            if 'to' in envelope:
                profile = messageBox.user.profile            
                for target in envelope['to']:
                    # perform internal delivery
                    if target.startswith('internal:'):
                        username = target.replace("internal:","")
                        try:
                            user = User.objects.get(models.Q(username=username) | models.Q(profile__ugid=username))
                            headers = json.loads(messageBox.headers)
                            headers["From"] = '"%s %s" <%s>' % (profile.getFirstName(), profile.getLastName(), profile.ugid)
                            deliveryMessage = MessageBox(
                                                        message = messageBox.message,
                                                        category = messageBox.category,
                                                        user = user,
                                                        box="inbox",
                                                        readstate="U",
                                                        envelope = json.dumps({
                                                                'from' : profile.name,
                                                                'to' : user.username
                                                                },ensure_ascii=False),
                                                        headers = json.dumps(headers,ensure_ascii=False),
                                                        status = 'D'
                                                        )
                            deliveryMessage.save()
                            ## should send email, but not just yet
                        except InternalUserGroup.DoesNotExist: 
                            pass
                    # could do email delivery here, but wont just yet
                messageBox.box = "outbox"
                messageBox.status = "D"
                logging.error("sent %s " % messageBox.id)
            else:
                logging.error("Message send failed, No recipients ")
                envelope['error'] = "No recipients specified"
                messageBox.envelope = json.dumps(envelope,ensure_ascii=False)
                messageBox.box = "outbox"
                messageBox.status = "F"
        except:
            logging.error("Message send failed %s " % traceback.format_exc())
            envelope['error'] = "Sending User Does Not exist"
            envelope['traceback'] = traceback.format_exc()
            messageBox.envelope = json.dumps(envelope,ensure_ascii=False)
            messageBox.box = "outbox"
            messageBox.status = "F"
        messageBox.save()
        message_signals.message_delivered.send(MessageBox, instance=messageBox)
        return True
    

def enableInProcessDelivery():     
    message_signals.deliver_message.connect(Delivery.notify, sender=MessageBox)


@never_cache  # Cache at a lower level with invalidation.
@permission_required('message.can_admin')
def startDelivery(request):
    Delivery.notify()
    return Response.Ok()

@never_cache  # Cache at a lower level with invalidation.
@permission_required('message.can_admin')
def stopDelivery(request):
    Delivery.end()
    return Response.Ok()

    

