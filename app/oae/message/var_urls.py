from django.conf.urls.defaults import patterns, url

from views import doBoxCategoryAll

urlpatterns = patterns('',
    url(r'^boxcategory-all\.json$', doBoxCategoryAll , name='boxcategory'),
)