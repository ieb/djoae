from django.conf.urls.defaults import patterns, url
from oae.message.delivery import startDelivery, stopDelivery


urlpatterns = patterns('',
    url(r'^startDelivery$', startDelivery , name='startDelivery'),
    url(r'^stopDelivery$', stopDelivery , name='stopDelivery'),
)