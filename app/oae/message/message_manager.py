'''
Created on May 8, 2012

@author: ieb
'''
from models import Message
from oae.common.util import Hash
from oae.message.models import MessageBox
from oae.user.models import InternalUserGroup
import logging
import django.utils.simplejson as json
from oae.message.operations import DeleteOperation
from oae.common.xact import xact
from oae.message import message_signals
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User

ALLOW_QUERY_BY_USERNAME = (settings.ALLOW_QUERY_BY_USERNAME)


OPERATIONS = {
    "delete" : DeleteOperation(),
    }


class MessageManager(object):
    
    def __init__(self, user):
        self.user = user
        self.ugid = InternalUserGroup.getUgid(user)
        
    @xact
    def sendMessage(self,  to, subject,  body, mfrom=None, category="message",  headers={}, sendingUgid=None, targetBox="outbox", parameters=None ):  
        if sendingUgid is None:
            sendingUgid = self.ugid
        if mfrom is None:
            mfrom = self.ugid
        envelope = {
                    'to' : to,
                    'from' : mfrom
                }
        
        if ALLOW_QUERY_BY_USERNAME:
            sendingUser = InternalUserGroup.objects.get(models.Q(ugid=sendingUgid)|
                                                        models.Q(name=sendingUgid))
        else:
            sendingUser = InternalUserGroup.objects.get(ugid=sendingUgid)
        message = Message(sendinguser=sendingUser.user,
                          category=category,
                          subject=subject,
                          type=type,
                          body=body)
        # fix me: see if this can be made more efficient.
        message.save()
        message.messageid = Hash.genCompactUUID()
        message.save()
        messageBox = MessageBox(
                        category=category,
                        message=message,
                        user=sendingUser.user,
                        box=targetBox,
                        readstate='U',
                        status='W', #Waiting
                        envelope=json.dumps(envelope,ensure_ascii=False),
                        headers=json.dumps(headers,ensure_ascii=False)
                        )
        messageBox.save()
        message_signals.deliver_message.send(MessageBox,instance=messageBox)
        return True
    
    @xact
    def updateMessage(self, messageid, update ,ugid):
        if ":operation" in update:
            if update[':operation'] in OPERATIONS:
                ret = OPERATIONS[update[':operation']].doOperation(self.user, update,ugid,messageid)
                if ret is not None:
                    return ret
            else:
                logging.error("Operation not supported %s " % update)
                return False
            
        if "sakai:messagebox" in update:
            toMailBox = update['sakai:messagebox']
            logging.error("Moving %s to %s " % (messageid, toMailBox))
            if ALLOW_QUERY_BY_USERNAME:
                messages = MessageBox.objects.filter(
                                        user=User.objects.get(models.Q(profile__ugid=ugid)|
                                                              models.Q(username=ugid)),
                                        message=Message.objects.get(messageid=messageid))
            else:
                messages = MessageBox.objects.filter(
                                        user=User.objects.get(profile__ugid=ugid),
                                        message=Message.objects.get(messageid=messageid))
            moved = False
            for messageBox in messages:
                if moved:
                    messageBox.delete()
                else:
                    if messageBox.box != toMailBox:
                        messageBox.box = toMailBox
                        messageBox.save()
                    moved = True
            if moved:
                return True
            logging.error("Message not found %s " % messageid)
            return None
        elif "sakai:read" in update:
            # Mark the users message box entries relating to the message as read.
            # set the parameters in the request.
            readstate = 'R' if 'true' == update['sakai:read'] else 'U'
            if ALLOW_QUERY_BY_USERNAME:
                messages = MessageBox.objects.filter(
                                        user=User.objects.get(models.Q(profile__ugid=ugid)|
                                                              models.Q(username=ugid)),
                                        message=Message.objects.get(messageid=messageid))
            else:
                messages = MessageBox.objects.filter(
                                        user=User.objects.get(profile__ugid=ugid),
                                        message=Message.objects.get(messageid=messageid))
            marked = False
            for messageBox in messages:
                messageBox.readstate = readstate;
                messageBox.save()
                marked = True
            if marked:
                return True
            return None                
        logging.error("Did not handle %s " % update)
        # Fix me, add other message update methods as required.
        return False
    
from oae.message.delivery import enableInProcessDelivery
enableInProcessDelivery()
