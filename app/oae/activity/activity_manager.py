'''
Created on May 7, 2012

@author: ieb
'''
from django.http import HttpResponseNotAllowed, HttpResponseBadRequest,\
    HttpResponse
from oae.activity.models import ActivityApp, ActivityTemplate, ActivityMessage,\
    Activity, ActivityFeed
from django.utils.datastructures import MultiValueDictKeyError
from oae.common.util import Response, Search, MultiEncoder, JSON_CT, Request
import logging
import traceback
from oae.common.xact import xact
from django.utils import simplejson as json
from django.conf import settings
from oae.user.models import InternalUserGroup
from oae.activity.encoder import ActivityEncoder

JSON_INDENT = (settings.JSON_INDENT)

class ActivityManager(object):
    
    def __init__(self, user):
        self.user = user
        
    @xact
    def addActivity(self,sourceObject, changes):
        # TODO: deal with activity routing to other feeds
        if "POST" != self.request.method:
            return HttpResponseNotAllowed("POST")
        changed = Request.toQueryDict(changes)
        try:
            try:
                activityApp = ActivityApp.objects.get(name=changed['sakai:activity-appid'])
            except ActivityApp.DoesNotExist:
                activityApp = ActivityApp.objects.create(name=changes['sakai:activity-appid'])
            try:
                activityTemplate = ActivityTemplate.objects.get(name=changes['sakai:activity-templateid'])
            except ActivityTemplate.DoesNotExist:
                activityTemplate = ActivityTemplate.objects.create(name=changes['sakai:activity-templateid'])
            try:
                activityMessage = ActivityMessage.objects.get(name=changes['sakai:activityMessage'])
            except ActivityMessage.DoesNotExist:
                activityMessage = ActivityMessage.objects.create(name=changes['sakai:activityMessage'])
            activity = Activity.objects.create(source=self.sourceObject.id,
                                    source_type=type(self.sourceObject),
                                    app=activityApp,
                                    template=activityTemplate,
                                    message=activityMessage,
                                    user=self.user)
            ActivityFeed.objects.create(
                            ugid=InternalUserGroup.getUgid(self.user),
                            activity=activity)
            return Response.Ok()
        except MultiValueDictKeyError:
            logging.error(traceback.format_exc())
            return HttpResponseBadRequest()
    
    def getActivities(self, request):
        '''
        Get the activities for the user, if the user is anon get the activities for the public feed
        @param request:
        '''
        if "GET" != request.method:
            return HttpResponseNotAllowed("GET")
        q = Activity.objects.filter(activityfeed__ugid=InternalUserGroup.getUgid(self.user))
        search = Search()
        results = search.dbSearch(request, Activity, q.order_by(search.getSortOn(request)))
        
        return HttpResponse(json.dumps(results,
                                       cls=MultiEncoder,
                                       encoder_classes=ActivityEncoder.depend(),
                                       indent=JSON_INDENT,
                                       ensure_ascii=False),
                                       content_type=JSON_CT)
