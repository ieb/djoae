'''
Created on Mar 19, 2012

NB: Not change the schema without generating a migration.

@author: ieb
'''
from django.db import models
from django.contrib.auth.models import User


class ActivityApp(models.Model):
    name = models.CharField(max_length=255, verbose_name="The text app id")

class ActivityMessage(models.Model):
    name = models.CharField(max_length=255, verbose_name="The message ID")

class ActivityTemplate(models.Model):
    name = models.CharField(max_length=255, verbose_name="The template name")

class Activity(models.Model):
    source = models.IntegerField(help_text="The ID of the activity source (DB PK ID)")
    app = models.ForeignKey(ActivityApp,
                            help_text="The app for the activity")
    message = models.ForeignKey(ActivityMessage,
                                 help_text="The template for the activity")
    template = models.ForeignKey(ActivityTemplate,
                                 help_text="The template for the activity")
    user = models.ForeignKey(User,
                             help_text="The User creating the activity")
    source_type = models.CharField(
                        max_length=255,
                        help_text="The Type of the source object")

class ActivityFeed(models.Model):
    class Meta:
        unique_together = ("activity", "ugid")
    activity = models.ForeignKey(Activity)
    ugid = models.CharField("User Group Id",
                              db_index=True,
                              max_length=64,
                              help_text="Feed owner.")
