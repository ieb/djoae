'''
Created on Mar 19, 2012

@author: ieb
'''
from oae.activity.activity_manager import ActivityManager
from django.views.decorators.cache import never_cache


@never_cache  # Cache at a lower level with invalidation.
def doActivityAll(request):
    activityManager = ActivityManager(request.user)
    return activityManager.getActivities(request)