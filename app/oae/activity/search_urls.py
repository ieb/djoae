'''
Created on Jun 15, 2012

@author: ieb
'''
from django.conf.urls.defaults import patterns, url
from oae.activity.views import doActivityAll

'''
/var/search/activity  as the base
'''
urlpatterns = patterns('',
    url(r'^all.json', doActivityAll),
)