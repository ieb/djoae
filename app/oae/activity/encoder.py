'''
Created on Jun 15, 2012

@author: ieb
'''
from django.utils import simplejson as json
from oae.user.encoder import ProfileEncoder
from oae.activity.models import Activity
import time


class ActivityEncoder(json.JSONEncoder):
    '''
    
            "target": "johnfking2",
            "profile": {
                "hash": "johnfking2",
                "basic": {
                    "access": "everybody",
                    "elements": {
                        "lastName": {
                            "value": "King"
                        },
                        "email": {
                            "value": "johnk@media.berkeley.edu"
                        },
                        "firstName": {
                            "value": "John"
                        }
                    }
                },
                "rep:userId": "johnfking2",
                "userid": "johnfking2",
                "counts": {
                    "contactsCount": 23,
                    "membershipsCount": 1,
                    "contentCount": 4,
                    "countLastUpdate": 1332227608358
                },
                "sakai:excludeSearch": false,
                "homePath": "/~johnfking2"
            },

    '''
    STATE_TO_UI = {
        "A" : "ACCEPTED",
        "P" : "PENDING",
        "G" : "IGNORED",
        "I" : "INVITED",
        "B" : "BLOCKED",
        "F" : "FOLLOWING"}
    
    @staticmethod
    def depend():
        s = ProfileEncoder.depend()
        s.update([ActivityEncoder])
        return s
    
            
    def default(self, obj):
        '''
            {
            "_lastModifiedBy": "admin",
            "sakai:schemaversion": "2",
            "sakai:permissions": "public",
            "_mimeType": "x-sakai/document",
            "commentCount": 0,
            "_createdBy": "admin",
            "_path": "lvCDAsmaa",
            "excludeSearch": false,
            "sakai:description": "",
            "sling:resourceType": "sakai/pooled-content",
            "sakai:pooled-content-viewer": [
                "anonymous",
                "aaa-contributor",
                "everyone",
                "aaa-evaluator"
            ],
            "_created": 1339723804537,
            "sakai:pooled-content-manager": [
                "aaa-researchassistant",
                "aaa-researcher",
                "aaa-leadresearcher"
            ],
            "sakai:pool-content-created-for": "guillermo",
            "_id": "oiumkLaJEeGZHHUjrf_xsA+",
            "sakai:pooled-content-file-name": "References",
            "structure0": "{\"references\":{\"_title\":\"References\",\"_order\":0,\"_ref\":\"id79293106\",\"main\":{\"_title\":\"References\",\"_order\":0,\"_ref\":\"id79293106\"}}}",
            "_lastModified": 1339766608265,
            "sakai:excludeSearch": "false",
            "sakai:pooled-content-editor": [],
            "sakai:copyright": "creativecommons",
            "sakai:activity-source": "lvCDAsmaa",
            "sakai:activity-type": "pooled content",
            "sakai:activity-actor": "guillermo",
            "sakai:activity-appid": "Content",
            "sakai:activity-templateid": "default",
            "sakai:activityMessage": "UPDATED_CONTENT",
            "who": {
                "hash": "guillermo",
                "basic": {
                    "access": "everybody",
                    "elements": {
                        "lastName": {
                            "value": "Rodriguez"
                        },
                        "email": {
                            "value": "guillermolrodriguez@hotmail.com"
                        },
                        "firstName": {
                            "value": "Guillermo"
                        }
                    }
                },
                "rep:userId": "guillermo",
                "userid": "guillermo",
                "counts": {
                    "contentCount": 0,
                    "countLastUpdate": 1339723806508,
                    "contactsCount": 0,
                    "membershipsCount": 1
                },
                "sakai:excludeSearch": false,
                "homePath": "/~guillermo"
            }
        },
        @param obj:
        '''
        if isinstance(obj, Activity):
            return {
                "sakai:activity-source": obj.source,
                "sakai:activity-type": obj.app.name,
                "sakai:activity-actor": obj.user.profile.ugid,
                "sakai:activity-appid": obj.app.name,
                "sakai:activity-templateid": obj.template.name,
                "sakai:activityMessage": obj.message.name,
                "who": obj.user.profile
                }
        return super(ActivityEncoder,self).default(obj)

