'''
Created on Mar 12, 2012

@author: ieb
'''
from django.conf.urls.defaults import patterns, url
from oae.tag.views import doRefused, doTaggedList


urlpatterns = patterns('',
    url(r'^(.*?)\.tagged\.infinity\.json$', doRefused), 
    url(r'^(.*?)\.tagged\.-1\.json$', doRefused),
    url(r'^(.*?)\.tagged\.json$', doTaggedList),
)