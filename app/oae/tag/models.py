'''
Created on Mar 29, 2012

NB: Not change the schema without generating a migration.


@author: ieb
'''
from django.db import models
from django.contrib import admin

class Tag(models.Model):
    '''
    Defines a tag used on Content
    '''
    hash = models.CharField(max_length=32, unique=True, db_index=True, help_text="Hash of the path")
    parenthash  = models.CharField(max_length=32, db_index=True, help_text="Hash of the parent path")
    name  = models.CharField(max_length=128, db_index=True, help_text="Hash of the parent path")
    fullpath = models.CharField("Content Id",
                              unique=True,
                              db_index=True,
                              max_length=2048,
                              help_text="The Name of the tag")
    titlekey = models.CharField("i18n Title Key", max_length=256)
    depth = models.IntegerField()


class TagAdmin(admin.ModelAdmin):
    list_display = (('fullpath','name','titlekey','depth','hash','parenthash',))
    search_fields = (['name'])
    filter_fields = (['depth'])


admin.site.register(Tag,TagAdmin)
