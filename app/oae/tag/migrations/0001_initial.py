# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tag'
        db.create_table('tag_tag', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hash', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32, db_index=True)),
            ('parenthash', self.gf('django.db.models.fields.CharField')(max_length=32, db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128, db_index=True)),
            ('fullpath', self.gf('django.db.models.fields.CharField')(unique=True, max_length=2048, db_index=True)),
            ('titlekey', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('depth', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('tag', ['Tag'])

    def backwards(self, orm):
        # Deleting model 'Tag'
        db.delete_table('tag_tag')

    models = {
        'tag.tag': {
            'Meta': {'object_name': 'Tag'},
            'depth': ('django.db.models.fields.IntegerField', [], {}),
            'fullpath': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2048', 'db_index': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'}),
            'parenthash': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'titlekey': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['tag']