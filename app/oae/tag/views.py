'''
Created on Mar 26, 2012

@author: ieb
'''
from django.conf import settings
from django.http import HttpResponseBadRequest, HttpResponse
from oae.common.util import Hash, Search, JSON_CT, MultiEncoder
from oae.content.encoder import ContentEncoder
from oae.content.models import ContentMember, ContentMeta
from oae.permission.models import ObjectPermission
import django.utils.simplejson as json

import re
from oae.tag.models import Tag
from oae.user.models import Profile
from oae.user.encoder import ProfileEncoder
from django.views.decorators.cache import never_cache

JSON_INDENT=(settings.JSON_INDENT)

def doRefused(request, tagpath):
    return HttpResponseBadRequest()


@never_cache   # Must cache at a lower level in a way we can invalidate   
def doTaggedSearch(request):
    '''
    return a tree of tags, with 1 content item per location, randomised
    format is
    
    
    @param request:
    @param tagpath:
    '''
    try:
        type = request.GET["type"]
        tag = request.GET['tag']
        tagpath = request.GET['tag']
        if tagpath.startswith('directory/'):
                tagpath = tagpath[10:]
        if tagpath is None:
            tagpath = ""
    
        tagpath="/tags/directory/%s" % tagpath
        tagpath = re.sub(r'/$','',tagpath)
    
        hash = Hash.hash(tagpath)
        search = Search()
        if type == "c":
            
            principals = ObjectPermission.getUserPrincipals(request.user)
            q1 = ContentMember.objects.filter(principal__in=principals)
        
            results = search.dbSearch(request, ContentMeta, ContentMeta.objects.filter(id__in=q1.values("content_id"),tags__hash=hash).order_by("-modified"))
            return HttpResponse(json.dumps(results,cls=MultiEncoder, encoder_classes=ContentEncoder.depend(),indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)
        elif type == "u":
            results = search.dbSearch(request, Profile, Profile.objects.filter(tags__hash=hash,type='U').order_by("modified")
                                      .prefetch_related("group__usergroupdata"))
            return HttpResponse(json.dumps(results,cls=MultiEncoder, encoder_classes=ProfileEncoder.depend(),indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)
        elif type == "g":
            results = search.dbSearch(request, Profile, Profile.objects.filter(tags__hash=hash,type='G').order_by("modified")
                                      .prefetch_related("group__usergroupdata"))
            return HttpResponse(json.dumps(results,cls=MultiEncoder, encoder_classes=ProfileEncoder.depend(),indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)
        else:
            return HttpResponseBadRequest()
    except KeyError:
        return HttpResponseBadRequest()

@never_cache   # Must cache at a lower level in a way we can invalidate   
def doTaggedList(request, tagpath=None):
    '''
    return a tree of tags, with 1 content item per location, randomised
    format is
    
    
    @param request:
    @param tagpath:
    '''
    if 'tag' in request.GET:
        tagpath = request.GET['tag']
        if tagpath.startswith('directory/'):
            tagpath = tagpath[10:]
    if tagpath is None:
        tagpath = ""

    tagpath="/tags/directory%s" % tagpath
    tagpath = re.sub(r'/$','',tagpath)
    depth = len(re.sub(r'[^/]+', '', tagpath)) # counts the number of / in the path 

    hash = Hash.hash(tagpath)
    
    principals = ObjectPermission.getUserPrincipals(request.user)
    q1 = ContentMember.objects.filter(principal__in=principals)

    qtags = Search(defaultItems=25).dbSearch(request, Tag, Tag.objects.filter(fullpath__startswith=tagpath,depth__gte=depth).order_by("depth"))
    results={}
    for qtag in qtags['results']:
        elements = qtag.fullpath[len(tagpath):].split('/')
        r = results
        for e in elements:
            if e not in r:
                r[e] = {}
            r = r[e]
        contentItem = _getContentItem(qtag,q1)
        r.update({
            "title" : qtag.titlekey,
            "_path" : qtag.fullpath,
            "content" : contentItem
            })
    return HttpResponse(json.dumps(results,cls=MultiEncoder, encoder_classes=ContentEncoder.depend(),indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)
        
        
def _getContentItem(tag, principals):
    '''
    Returns the most recently modified item that this user can read tagged with this tag
    @param tag:
    @param principals:
    '''
    try:
        return tag.contentMeta.filter(id__in=principals.values("content_id")).order_by("-modified")[0]
    except:
        return None
        
        
        