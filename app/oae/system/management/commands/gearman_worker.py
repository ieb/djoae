'''
Created on Jul 8, 2012

@author: ieb
'''
from django.core.management.base import BaseCommand
from oae.system.gearman import GearmanConnection
from django.utils.importlib import import_module
import logging
from django.conf import settings

GEARMAN_WORKERS_CLASSES = settings.GEARMAN_WORKERS_CLASSES

class Command(BaseCommand):
    args = '<number_of_workers>'
    help = 'Runs a gearman worker'
    
    
    def handle(self, *args, **options):
        worker = GearmanConnection.getWorker()
        worker.set_client_id()
        for task_class in GEARMAN_WORKERS_CLASSES:
            try:
                task_module = self._get_module(task_class)
                import_module(task_module)
                globals()[task_class].register_gearman_task(worker)
            except:
                logging.error("Failed to register %s " % task_class)
        logging.error("Waiting for Jobs")
        try:
            worker.work()
        finally:
            for task_class in GEARMAN_WORKERS_CLASSES:
                try:
                    task_module = self._get_module(task_class)
                    import_module(task_module)
                    globals()[task_class].unregister_gearman_task(worker)
                except:
                    logging.error("Failed to register %s " % task_class)
