from django.http import HttpResponse, HttpResponseRedirect,\
    HttpResponseForbidden, HttpResponseServerError, HttpResponseNotAllowed
import logging
from django.core.urlresolvers import reverse
from oae.common.util import Request
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
import traceback
from django.views.decorators.cache import never_cache

#-------------------------------------------------------------------------------
# Security Methods and views
#-------------------------------------------------------------------------------

def _getLoginResponse(request):
    '''
      Generate a response based on the request post login.
    '''
    if "u" in request.REQUEST:
        return HttpResponseRedirect(request.REQUEST["u"])
    indexUrl = reverse("index")
    return HttpResponseRedirect(indexUrl)


@never_cache
def doLogout(request):
    '''
       Perform logout
    '''
    logout(request)
    if "u" in request.REQUEST:
        return HttpResponseRedirect(request.REQUEST['u'])
    indexUrl = reverse("index")
    return HttpResponseRedirect(indexUrl)

@never_cache
@login_required
def doSystemLogin(request):
    '''
       Perform a system login, trusting the request, this simply sets up OAE
       The user having already logged in by some other Django method
    '''
    return _getLoginResponse(request)

@never_cache
def doSakaiPasswordLogin(request):
    '''
      Do login using the special OAE protocol from the UI form.
      
       **Request Parameters**
       
       ``sakaiauth:un`` Username
       
       ``sakaiauth:pw`` password
       
       **Response**
       
       ``302`` - Logs the user in and Redirects to the destingation page 
       
       ``403`` - Login failed
       
       ``401`` - Login invalid
      
      
    '''
    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    try:
        username = Request.get(request.POST, 'sakaiauth:un', '')
        password = Request.get(request.POST, 'sakaiauth:pw', '')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return _getLoginResponse(request)
            else:
                return HttpResponseForbidden("Account is disabled")
        else:
            return HttpResponse("Invalid Login", status=401)
    except:
        logging.error(traceback.format_exc())
        return HttpResponseServerError()
        
@never_cache
def doPasswordLogin(request):
    '''
       Perform a password login using the Dajango protocol
       
       **Request Parameters**
       
       ``username`` Username
       
       ``password`` password
       
       **Response**
       
       ``302`` - Logs the user in and Redirects to the destingation page 
       
       ``403`` - Login failed
       
       ``401`` - Login invalid

    '''
    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return _getLoginResponse(request)
        else:
            return HttpResponseForbidden("Account is disabled")
    else:
        return HttpResponse("Invalid Login", status=401)

