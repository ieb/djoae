from django.http import HttpResponse, \
    HttpRequest, HttpResponseBadRequest, QueryDict
import logging
import django.utils.simplejson as json
from time import strftime, time, gmtime, mktime
from oae.common.util import JSON_CT, Request
import urllib
from django.conf import settings
from oae.common.xact import xact

JSON_INDENT=(settings.JSON_INDENT)


    

def _getHeaderMap(response):
    '''
    Convert headers to a map, but don't allow duplicates.
    '''
    headers = {}
    for name,value in response:
        headers[name] = value
    return headers      

def doBatch(request):
    '''
      Perform batch requests for GET and POST operations, by locating the view 
      and calling the view directly with a new request header.
    '''
    if "requests" not in request.REQUEST:
        return HttpResponseBadRequest()
    requestJson = json.loads(request.REQUEST["requests"])
    # Dispatch requests
    notGet = False
    for requestOperation in requestJson:
        if "method" in requestOperation and  requestOperation['method'] != "GET":
            notGet = True
            break;
    if notGet:
        return doModifyingBatch(request, requestJson)
    else:
        return doNonModifyingBatch(request, requestJson)

@xact
def doModifyingBatch(request, requestJson):
    return doNonModifyingBatch(request, requestJson)


def doNonModifyingBatch(request, requestJson):
    # jquery adds a _ to the end of urls when not caching.
    cache = "_" not in request.REQUEST
    if not cache:
        logging.info("UI Marked request as non cacheable ")
    results = []
    request_meta = dict(request.META)
    # Strip out any cache control headers in the containing request
    # To avoid confusing the response cache middleware
    if 'HTTP_ACCEPT_ENCODING' in request_meta:
        del(request_meta['HTTP_ACCEPT_ENCODING'])
    if 'HTTP_IF_MODIFIED_SINCE' in request_meta:
        del(request_meta['HTTP_IF_MODIFIED_SINCE'])
    if 'HTTP_IF_NONE_MATCH' in request_meta:
        del(request_meta['HTTP_IF_NONE_MATCH'])
    if 'HTTP_ETAG' in request_meta:
        del(request_meta['HTTP_ETAG'])
    for requestOperation in requestJson:
        url =  requestOperation['url']
        if "method" in requestOperation:
            method = requestOperation['method']
        else:
            method = "GET"
        if 'cache' in requestOperation and not requestOperation['cache']:
            cache = False
            
        # FIXME: transfer security from the current request to batch request
        batchRequest = HttpRequest()
        batchRequest.method = method
        if method == "POST" and 'parameters' in requestOperation:
            params = QueryDict( urllib.urlencode(requestOperation["parameters"], doseq=True))
            batchRequest.POST = params
            batchRequest.GET = params
            batchRequest.REQUEST = params
        else:
            qsparts = url.split('?',2)
            if len(qsparts) == 2:
                params = QueryDict(qsparts[1])
            else:
                params = QueryDict("")
            url = qsparts[0]
            batchRequest.REQUEST = params
            batchRequest.GET = params
        batchRequest.COOKIES = request.COOKIES
        batchRequest.META = request_meta
        batchRequest.user = request.user
        response = Request._redispatch(url, batchRequest)
        result = { 'url': url, 'method' : method}
        if response is None:
            result['status'] = 500
            result['success'] = False
        else:                
            if response.status_code == 304:
                logging.error("Got 304, Meta is %s " % batchRequest.META)
            result['status'] = response.status_code
            if response.status_code >= 200 and response.status_code < 300:
                result['success'] = True
                result['headers'] = _getHeaderMap(response.items())
                # this should force no coding translation
                result['body'] = unicode(response.content,"utf-8","replace")
            else:
                result['success'] = False
                result['headers'] = _getHeaderMap(response.items())
        results.append(result)
    lastModified = 0
    expires = 0
    # check to see if we can set the expires headers on this response
    if cache:
        for result in results:
            if result['method'] != "GET":
                #logging.info("Request %s was not a GET, don't mix GETs and posts, no caching " % result['url'])
                cache = False
                break
            if 'cache-control' in result['headers']:
                if result['headers']['cache-control'] != 'cache':
                    #logging.info("response to %s was not cacheable, no caching %s " % (result['url'],result['headers']['cache-control']))
                    cache = False
                    break
            if 'last-modified' in result['headers']:
                try:
                    lastModified = max(mktime(strftime("%a, %d %b %Y %H:%M:%S +0000",result['headers']['last-modified'])),lastModified)
                except ValueError:
                    #logging.info("In response to %s Unable to parse last Modified header %s" % (result['url'],result['headers']['last-modified']))
                    cache = False
                    break
            if 'expires' in result['headers']:
                try:
                    expires = min(mktime(strftime("%a, %d %b %Y %H:%M:%S +0000",result['headers']['expires'])),expires)
                except ValueError:
                    #logging.info("In response to %s Unable to parse Expires header %s"  % (result['url'],result['headers']['expires']))
                    cache = False
                    break
                
    # build response
    response = HttpResponse(json.dumps({'results' : results},indent=JSON_INDENT,ensure_ascii=False), status=200, content_type=JSON_CT)
    
    # Modify headers to enable caching upstream
    if cache:
        if lastModified > 0:
            response['Last-Modified'] = strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime(lastModified))
            #logging.info("Set last modified to %s  " % (response['last-modified']))
        else:
            del response['last-modified']
        if expires > 0:
            response['Expires'] = strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime(expires))
            #logging.info("Set Exipres to %s  " % (response['expires']))
        else:
            response['Expires'] = strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime(time()+600))
            #logging.info("Set Exipres to 10min from now  %s   " % (response['expires']))
        response['Cache-Control'] = 'cache'
        response['X-Cache-Control'] = 'cache'
        del response['ETag']
        request.session.accessed = False
    else:
        response['Cache-Control'] = 'no-cache'
    return response

