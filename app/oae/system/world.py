'''
Created on May 2, 2012



@author: ieb
'''
import re
from oae.user.models import UserGroupData, InternalUserGroup
from django.db import models
from django.http import HttpResponseBadRequest, HttpResponse
from oae.common.util import Response, Hash, JSON_CT
from django.conf import settings
import os
from django.utils import simplejson as json
from oae.user.operations import TagProfileOperartion
from oae.content.content_manager import ContentManager
from oae.user.user_group_manager import UserGroupManager
from oae.message.message_manager import MessageManager
import logging
import traceback
from oae.common.xact import xact
from django.utils.simplejson.decoder import JSONDecoder

UNSAFESAFE_GROUP_RE = re.compile(r"^[^[a-z,A-Z,0-9,-]]$")
FIRSTNAME_RE = re.compile(r"\$\{creatorName\}")
ROLE_RE = re.compile(r"\$\{role\}")
LINK_RE = re.compile(r"\$\{role\}")
GROUPNAME_RE = re.compile(r"\$\{groupName\}")
CREATOR_RE = re.compile(r"\$\{link\}")


DOCUMENT_ROOT, JSON_INDENT = (settings.DOCUMENT_ROOT, settings.JSON_INDENT)


class World(object):
    '''
    This class is reverse engineered from the OAE code, however unlike the OAE code it uses services to 
    perform its actions rather than re-dispatching HTTP requests. The same services are used
    by the end points but its much simpler to use them rather than batching everything into requests.
    I hope the UI doesn't rely on seeing the results from a batch request.
    '''
    
    def __init__(self, user):
        self.contentManager = ContentManager(user)
        self.userManager = UserGroupManager(user)
        self.messageManager = MessageManager(user)
        self.user = user
        self.ugid = InternalUserGroup.getUgid(user)
        
        pass
    
    def _validate(self, parameters):
        try:
            groupId = parameters['id']
            if UNSAFESAFE_GROUP_RE.match(groupId):
                return Response.Ok(body='{ "error" : "Invalid group id", "created" : False }')
            
            if UserGroupData.objects.filter(models.Q(ugid=groupId)|models.Q(name=groupId)).exists():
                return Response.Ok(body='{ "error" : "Group already exists", "created" : False }')
            template = "%s/%s.json" % (DOCUMENT_ROOT,  parameters['worldTemplate'])
            if not os.path.exists(template):
                return Response.Ok(body='{ "error" : "Template does not exist", "created" : False }')
            return None    
        except:
            logging.error(parameters)
            logging.error(traceback.format_exc())
            return HttpResponseBadRequest()
        
    def _getWorldTemplate(self,data):
        template = "%s/%s.json" % (DOCUMENT_ROOT,  data['worldTemplate'])
        if not os.path.exists(template):
            logging.error("Not template %s " % template)
            return None
        try:
            f = open(template)
            templateJson = json.load(f)
            f.close()
        except Exception as e:
            logging.error("Template file %s is invalid, see traceback that follows" % template)
            raise e
        templateJson['worldTemplateCategory'] = os.path.basename(os.path.dirname(template))
        return templateJson

    def _createWorldGroup(self, data, groupId, template):
        parameters = {':name' : groupId,
                        'sakai:group-title' : data['title'],
                        'sakai:group-description' : data['description'],
                        'sakai:group-id' : groupId,
                        'sakai:category' : template['worldTemplateCategory'],
                        'sakai:templateid' : template['id'],
                        'sakai:joinRole' : template['joinRole'],
                        'sakai:creatorRole' : template['creatorRole']
                        } 
        if 'customStyle' in template:
            parameters['sakai:customStyle'] = template['customStyle']
        try:
            
            parameters['sakai:roles'] = json.dumps(template['roles'])
        except:
            pass
        return self.userManager.createGroup(groupId, parameters)
        
    def _tagGroup(self, data, group):
        if 'tags' in data:
            tagOperation = TagProfileOperartion()
            tagOperation.tagProfile(group.profile, ["/tags/%s" % t for t in data['tags'] ])

    def _createSubGroups(self, data, parentGroup, template):
        logging.info("=============================== Creating Sub Groups ========================================")
        # THIS IS SO WRONG, but its the way the UI does it.
        subgroups = []
        if 'roles' in template:
            groupId = parentGroup.name
            visibility = ""
            joinability = ""
            if "visibility" in data:
                visibility = data["visibility"]
            if "joinability" in data:
                joinability =  data["joinability"]
            mainGroupUpdate = {
                        "sakai:group-visible" : visibility,
                        "sakai:group-joinable" : joinability,
                        ":viewer" : [],
                        ":viewer@Delete" : [],
                        ":member" : [],
                   }
            subgroups = {}
            roles = {}
            for r in template['roles']:
                logging.info("Loading Role %s " % r)
                roles[r['id']] = r

            if "members-only" == visibility:
                mainGroupUpdate.update({
                        ":viewer" : [groupId],
                        ":viewer@Delete" : ["everyone","anonymous"]})
            elif "logged-in-only" == visibility:
                mainGroupUpdate.update({
                        ":viewer" : ["everyone", groupId ],
                        ":viewer@Delete" : ["anonymous"]})
            else:
                mainGroupUpdate.update({
                        ":viewer" : ["everyone","anonymous", groupId ]})

            # First create the roles
            for roleId, role in roles.iteritems():
                subgroupId = "%s-%s" % (groupId, roleId)
                subgroups[roleId] = {
                        ":name" : subgroupId,
                        "sakai:parent-group-id" : groupId,
                        "sakai:parent-group-title" : data['title'],
                        "sakai:role-title" : role['title'],
                        "sakai:role-title-plural"  : role['titlePlural'],
                        "sakai:group-id" : subgroupId,
                        "sakai:excludeSearch" : True,
                        "sakai:pseudoGroup" : True,
                        "sakai:pseudoGroup@TypeHint" : "Boolean",
                        "sakai:group-visible" : visibility,
                        "sakai:group-joinable" : joinability,
                    }
                if "members-only" == visibility:
                    subgroups[roleId].update({
                            ":viewer" : [subgroupId, groupId],
                            ":viewer@Delete" : ["everyone","anonymous"]})
                elif "logged-in-only" == visibility:
                    subgroups[roleId].update({
                            ":viewer" : ["everyone", subgroupId, groupId ],
                            ":viewer@Delete" : ["anonymous"]})
                else:
                    subgroups[roleId].update({
                            ":viewer" : ["everyone","anonymous", subgroupId, groupId ]})
                subgroups[roleId].update({":member" : [subgroupId] })
            # then add management to the roles
            dependency = {}
            for roleId, role in roles.iteritems():
                dependency[roleId] = []
                subgroupId = "%s-%s" % (groupId, roleId)
                if "isManagerRole" in role and role['isManagerRole']:
                    for manages in role["manages"]:
                        self._append( subgroups[manages], ":manager", subgroupId)
                        self._append(dependency, manages, roleId)
                    
                    self._append(subgroups[roleId], ":manager", subgroupId)
                    self._append(mainGroupUpdate, ":manager", subgroupId)
                else:
                    self._append(mainGroupUpdate, ":member", subgroupId)
                self._append(mainGroupUpdate, ":viewer", subgroupId)
            if "usersToAdd" in data:
                for user in data['usersToAdd']:
                    if user['role'] in subgroups:
                        logging.info("User is %s  %s" % ( user, subgroups[user['role']]))
                        self._append(subgroups[user['role']], ":member", user['userid'])
                        self._append(subgroups[user['role']], ":viewer", user['userid'])
                    else:
                        raise Exception("Template invalid role group %s has not been defined in %s " % (user['role'], subgroups))
            
            # Create the groups via the manager.
            order = []
            added = True
            # create an update order adding items that a satisfied
            # to the order until none are added or until
            # all are added.
            while len(order) < len(dependency) and added:
                added = False
                for g,deps in dependency.iteritems():
                    if g not in order:
                        satisfied = True
                        for dep in deps:
                            if dep not in order:
                                satisfied = False
                        if satisfied:
                            order.append(g)
                            added = True
            createdSubGroups = {}
            if not added:
                logging.info("Groups are cyclic %s  %s " % ( order, dependency.keys()))
                # groups are cyclic have to create all first.                
                for k,subgroup in subgroups.iteritems():
                    logging.info("Creating Subgroup as %s " % subgroup)
                    createdSubGroups[k] = self.userManager.createGroup(subgroup[':name'])
                    
                # Update the groups, we can't do this in 1 step due to circular dependencies in the group structure.
                for k,subgroup in subgroups.iteritems():
                    logging.info("Updating Subgroup as %s " % subgroup)
                    group = createdSubGroups[k]
                    self.userManager.updateGroup(subgroup[':name'], subgroup, group=group)
            else:
                # groups are a DAG, create  from the base up.
                logging.info("Groups are DAG %s  %s " % ( order, dependency.keys()))
                for k in order:
                    logging.info("Creating Subgroup as %s " % subgroups[k])
                    createdSubGroups[k] = self.userManager.createGroup(subgroups[k][':name'], subgroups[k])
                
                
            logging.info("Updating %s with %s " % ( groupId, mainGroupUpdate))
            self.userManager.updateGroup(groupId, mainGroupUpdate, group=parentGroup)
            logging.info("Done Updating %s")
        logging.info("=============================== Done Creating Sub Groups ========================================")
        return createdSubGroups
            
    def _append(self, o, key, v):
        logging.info("Setting %s  =============  %s  ===============  %s " % ( o, key, v))
        if key in o:
            o[key].append(v)
        else:
            o[key] = [v]

    def _createDocs(self, data, group, template):
        contentIds = []
        if "structure" in template:
            templateStr = json.dumps(template)
            widgetId = "id%s" % Hash.genCompactUUID()
            refidRE = re.compile(r"\$\{refid\}")
            groupidRE = re.compile(r"\$\{groupid\}")
            groupId = group.name
            processedTemplate = json.loads(groupidRE.sub(group.name,refidRE.sub(widgetId,templateStr)))
            visibility = ""
            if "visibility" in data:
                visibility = data['visibility']
            logging.info("Structure is %s " % processedTemplate['structure'])
            for _,definition in processedTemplate['structure'].iteritems():
                docRef = definition["_docref"]
                content = processedTemplate["docs"][docRef]
                permission = "private"
                if visibility == "public" and "anonymous" in definition['_view']:
                    permission = "public"
                elif (visibility == "public" or visibility == "logged-in-only") and "everyone" in definition['_view']:
                    permission = "everyone"
                
                contentImport = dict(content)
                del(contentImport['structure0'])
                
                doc = {
                       "sakai:pooled-content-file-name": definition["_title"],
                       "sakai:description": "",
                       "sakai:permissions": permission,
                       "sakai:copyright" : "creativecommons",
                       "structure0": json.dumps(content["structure0"]),
                       "mimeType": "x-sakai/document",
                        ':operation' : 'import',
                        ":contentType" :"json",
                        ":replace" : "true",  # dictated by the UI, what ever hapend to types ?
                        ":replaceProperties" : "true",
                        ":content" : json.dumps(contentImport)
                }
                if "excludeSearch" in content: 
                    doc.update({ "sakai:excludeSearch" : content['excludeSearch']})
                if "schemaVersion" in data:
                    doc.update({"sakai:schemaversion": data['schemaVersion']})
                    
                
                
                # Set membership
                
                if "everyone" == permission:
                    self._append(doc,":viewer","everyone")
                    self._append(doc,":viewer@Delete","anonymous")
                elif "public" == permission:
                    self._append(doc,":viewer","everyone")
                    self._append(doc,":viewer","anonymous")
                elif "private" == permission:
                    self._append(doc,":viewer@Delete","everyone")
                    self._append(doc,":viewer@Delete","anonymous")
                elif "group" == permission:
                    self._append(doc,":viewer",groupId)
                    self._append(doc,":viewer@Delete","everyone")
                    self._append(doc,":viewer@Delete","anonymous")

                self._append(doc,":manager@Delete",self.ugid)
                for p in definition['_view']:
                    if p.startswith("-"):
                        self._append(doc,":viewer",groupId + p)
                for p in definition['_edit']:
                    if p.startswith("-"):
                        self._append(doc,":manager",groupId + p)

                logging.info("Creating Content Meta File from %s " % doc)
                # may need to set the category for 'S' for some files, but Library and Participants should not be 
                # shown in the lists
                crec = self.contentManager.createMetaFile(doc, category='H')
                logging.info("Done Creating Content Record is %s " % crec)
                contentDoc = crec['item']
                contentIds.append(crec["poolId"])
                
                for k,v in contentImport.iteritems():
                    if k.startswith(widgetId):
                        logging.info("Adding SubPart %s as %s " % (k, json.dumps(v)))
                        self.contentManager.updateContent(crec["poolId"], 
                                {
                                 ':operation' : 'import',
                                 ':contentType' : 'json',
                                 ':replace' : True,
                                 ':replaceProperties' : True,
                                 ':content' : json.dumps(v)
                                }, None, k)
                    else:
                        logging.info("Not a SubPart %s " % k)

                        

                definition['_view'] = json.dumps(definition['_view'])
                definition['_edit'] = json.dumps(definition['_edit'])
                definition['_pid'] = crec["poolId"]
                
                # setting the ACLs might be missing ?
            
            structure = {
                         'structure0' : json.dumps(processedTemplate["structure"])
                         }
            changes = {
                ':operation' : 'import',
                ':contentType' : 'json',
                ':replace' : True,
                ':replaceProperties' : True,
                ':content' : json.dumps(structure)
            }
            self.userManager.updateGroup(self.ugid, changes, group, "docstructure")
        return contentIds
    
    
    def _notifyUsers(self, data, group, template):
        if "message" in data:
            message = data['message']
            if self._variesByUser(message['body']) or self._variesByUser(message["subject"]):
                for recipient in message['toSend']:
                    if "userid" not in recipient or recipient['userid'] is None:
                        logging.info("Not sending world creationg message to None user specified by %s " % recipient)
                        continue
                    logging.info("Sending message to %s " % recipient)
                    subject = self._replaceTokens(message["subject"], message, recipient)
                    body = self._replaceTokens(message['body'], message, recipient)
                    to = "internal:%s" % recipient["userid"]
                    if "messageMode" not in message or message['messageMode'] == "internal" or message['messageMode'] == "both":
                        self.messageManager.sendMessage(to=to,
                                            subject=subject,
                                            body=body, 
                                            mfrom=self.ugid, 
                                            headers={}, 
                                            targetBox="outbox")
                    elif message['messageMode'] == "external" or message['messageMode'] == "both":
                        self.messageManager.sendMessage(to=to,
                                            subject=subject,
                                            body=body, 
                                            mfrom=self.ugid, 
                                            headers={},
                                            targetBox="outbox",
                                            parameters={
                                               "sakai:type": "smtp",
                                               "sakai:templatePath": "/var/templates/email/group_invitation",
                                               "sender" : message["creatorName"],
                                               "system" : message["system"],
                                               "name" : message["groupName"],
                                               "body" : body,
                                               "link" : message['link'] })
                else:
                    # The body and subject are static, so send in 1 message.
                    subject = self._replaceTokens(message["subject"], message, {"firstName": "--", "role": "--" })
                    body = self._replaceTokens(message['body'], message, {"firstName": "--", "role": "--" })
                    to = ["internal:%s" % t["userid"] for t in message['toSend'] ]
                    logging.info("Message is %s " % message)
                    if "messageMode" not in message or  message['messageMode'] == "internal" or message['messageMode'] == "both":
                        self.messageManager.sendMessage(to=to,
                                            subject=subject,
                                            body=body, 
                                            mfrom=self.ugid, 
                                            headers={}, 
                                            targetBox="outbox")
                    elif message['messageMode'] == "external" or message['messageMode'] == "both":
                        self.messageManager.sendMessage(to=to,
                                            subject=subject,
                                            body=body, 
                                            mfrom=self.ugid, 
                                            headers={},
                                            targetBox="outbox",
                                            parameters={
                                               "sakai:type": "smtp",
                                               "sakai:templatePath": "/var/templates/email/group_invitation",
                                               "sender" : message["creatorName"],
                                               "system" : message["system"],
                                               "name" : message["groupName"],
                                               "body" : body,
                                               "link" : message['link'] })
    
    
    def _variesByUser(self, template):
        return template != FIRSTNAME_RE.sub("YY",ROLE_RE.sub("XX",template))
    
    def _replaceTokens(self, template, message, recipient):
        return FIRSTNAME_RE.sub(recipient['firstName'],
                    ROLE_RE.sub(recipient["role"], 
                        LINK_RE.sub(message['link'],
                            GROUPNAME_RE.sub(message["groupName"],
                                CREATOR_RE.sub(message['creatorName'], template)))))
                    
    def _removeCreatorAsManager(self, group, subgroups):
        for _, subgroup in subgroups.iteritems():
            self.userManager.changeGroupMembershipRoles(subgroup, subgroup.usergroupdata, 
                                                   {'M':[],'E':[],'V':[]}, 
                                                   {'M':[self.ugid],'E':[],'V':[]})
        self.userManager.changeGroupMembershipRoles(group, group.usergroupdata, 
                                               {'M':[],'E':[],'V':[]}, 
                                               {'M':[self.ugid],'E':[],'V':[]})

    @xact
    def create(self, parameters):
        if "data" not in parameters:
            logging.info(parameters)
            return HttpResponseBadRequest()
        
        data = json.loads(parameters['data'])
        v = self._validate(data)
        if v is not None:
            return v
        
        
        template = self._getWorldTemplate(data)
        if template is None:
            return HttpResponseBadRequest()
   
        groupId = data['id']
        group = self._createWorldGroup(data, groupId, template)
        self._tagGroup(data, group)
        subgroups = self._createSubGroups(data, group, template)
        docids = self._createDocs(data, group, template)
        self._notifyUsers(data, group, template)
        self._removeCreatorAsManager(group, subgroups)
        return HttpResponse(json.dumps({
                             "results" : [
                                          {"pooledContentIDs": docids},
                                          {"created": True},
                                          {"url":"/fake-url-to-keep-the-ui-happy","success":True,"status":200}
                                        ]
                            }, indent=JSON_INDENT), content_type=JSON_CT)
        
