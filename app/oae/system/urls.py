from batch import doBatch
from captcha import doCaptcha
from django.conf.urls.defaults import patterns, url
from login import doSystemLogin, doSakaiPasswordLogin, doLogout
from me import doMe
from oae.system.views import doDebugView, doWorldCreate

urlpatterns = patterns('',
    url(r'^me', doMe , name='me'),
    url(r'^batch', doBatch , name='batch'),
    url(r'^captcha', doCaptcha , name='captcha'),
    url(r'^sling/formlogin$', doSakaiPasswordLogin, name='login'),
    url(r'^login$', doSystemLogin , name='systemLogin'),
    url(r'^logout', doLogout, name='logout'),
    url(r'^world/create', doWorldCreate, name='worldcreate'),
    url(r'^debug(?P<path>.*?)$', doDebugView, name='debug_view'),
)