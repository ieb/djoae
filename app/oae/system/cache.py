'''
Created on Jun 21, 2012

@author: ieb
'''
from johnny.cache import get_backend, signals, QueryCacheBackend
import logging
import time
from django.conf import settings


HIT = 0
MISS = 1
CACHE_STATS = None

tcount = {}

JOHNNY_TABLE_BLACKLIST = (settings.JOHNNY_TABLE_BLACKLIST)

class CacheStats(object):
    
    def __init__(self):
        self.hits = 0
        self.misses = 0
        self.nextlog = time.time()
        self.fulllog = time.time()
        self.tcount = {}
        signals.qc_hit.connect(self._cacheHit)
        signals.qc_miss.connect(self._cacheMiss)
    
    def _logCache(self):
        now = time.time()
        if now > self.nextlog:
            logging.error("Cache Hits %s Misses %s " % (self.hits, self.misses))
            self.nextlog = now+30
        if now > self.fulllog:
            for t,h in self.tcount.iteritems():
                logging.error("Table %s Hits %s Misses %s" % (t,h[HIT],h[MISS]))
            self.fulllog = now+120
            
            
    
    def _inc(self, tables, hom):
        for t in tables:
            if t in self.tcount:
                self.tcount[t][hom] = self.tcount[t][hom]+1
            else: 
                self.tcount[t] = [0,0]
                self.tcount[t][hom] = self.tcount[t][hom]+1

                    
        
    
    def _cacheHit(self, tables=None, query=None, size=None, key=None, **kwargs):
        if key is not None:
            self.hits = self.hits + 1
            self._inc(tables,HIT)
            self._logCache()
        
    def _cacheMiss(self, tables=None, query=None, key=None, **kwargs):
        if key is not None:
            self.misses = self.misses + 1
            self._inc(tables,MISS)
            self._logCache()


