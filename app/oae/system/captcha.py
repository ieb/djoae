from django.http import HttpResponse
from oae.settings import RECAPTCHA_PUBLIC_KEY
from oae.common.util import JSON_CT
from django.views.decorators.cache import cache_control


#-------------------------------------------------------------------------------
# System me
#-------------------------------------------------------------------------------

@cache_control(public=True,max_age=3600)   
def doCaptcha(request):
    '''
      Produce the system/captcha response containing the public key.
    '''
    return HttpResponse('{ "public-key" : "%s" }' % RECAPTCHA_PUBLIC_KEY, content_type = JSON_CT  )

