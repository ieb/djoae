'''
Created on Jul 8, 2012

@author: ieb
'''

from django.conf import settings
from django.utils import simplejson as json
from gearman.connection_manager import DataEncoder
from gearman.client import GearmanClient
from gearman.worker import GearmanWorker




GEARMAN_HOSTS = None
try:
    GEARMAN_HOSTS = settings.GEARMAN_HOSTS
except:
    pass

_gearmanClient = None

_gearmanWorker = None

class JsonDataEncoder(DataEncoder):
    @classmethod
    def encode(cls, encodable_object):
        return json.dumps(encodable_object)

    @classmethod
    def decode(cls, decodable_string):
        return json.loads(decodable_string)

class JsonClient(GearmanClient):
    data_encoder = JsonDataEncoder
    
class JsonWorker(GearmanWorker):
    data_encoder = JsonDataEncoder
    
    
    
class GearmanConnection(object):
    
    
    @staticmethod
    def isEnabled():
        return GEARMAN_HOSTS is not None
    
    @staticmethod
    def _getClient():
        if _gearmanClient is None:
            _gearmanClient = JsonClient(GEARMAN_HOSTS)
        return _gearmanClient
        
    @staticmethod
    def enqueue(taskname,payload):
        if GEARMAN_HOSTS is not None:
            client = GearmanConnection._getClient()
            client.submit_job(taskname, payload,  background=True, wait_until_complete=False)
            
    @staticmethod
    def getWorker():
        '''
        Return a new Json based worker
        '''
        if GEARMAN_HOSTS is not None:
            return JsonWorker(GEARMAN_HOSTS)
        return None
        
            