'''
Created on Apr 26, 2012

@author: ieb
'''
from django.conf import settings
from django.utils import simplejson as json
import re
from django.http import HttpResponseNotAllowed, HttpResponseBadRequest,\
    HttpResponseNotFound, HttpResponse
import os
import logging
from io import StringIO
import traceback
from oae.common.util import Hash
from django.utils.http import http_date

DOCUMENT_ROOT, TEMP_ROOT = (settings.DOCUMENT_ROOT, settings.TEMP_ROOT)


I18N_RE = re.compile(r'__MSG__(?P<key>.*?)__')
class WidgetConfig(dict):
    
    def __init__(self, widgetName):
        self.widgetName = widgetName
        f = open("%s/devwidgets/%s/config.json" % (DOCUMENT_ROOT, widgetName))
        self.update(json.load(f))
        f.close()

class HTMLPage(object):
    def __init__(self, file, lang='default'):
        # FIXME: Move these out of here to fix the deployment issues.
        from lxml import etree
        from lxml import html

        self.cacheFile = "%s/%s" % (TEMP_ROOT, Hash.hash(file, "%s_%s" % ("htmlcache", lang) ))
        self.useCache = False
        if True and os.path.exists(self.cacheFile) and os.path.getmtime(self.cacheFile) > os.path.getmtime(file):
            self.useCache = True
        else:
            self.lang = lang
            self.widgetCount = 0
            self.ignoreWidgets = []
            self.links = {}
            self.scripts = {}
            self.widgetConfig = {}
            self.parser = etree.HTMLParser(strip_cdata=False)
            self.document = etree.parse(StringIO(self._localise(file)), self.parser)
            html.xhtml_to_html(self.document)
        
    def expand(self):
        if self.useCache:
            return
        i = 1
        while(self._resolveWidgets()):
            logging.error("Completed Pass %s " % i)
            i = i + 1
            if i > 10:
                break
            
        head = self._tag("head")
        body = self._tag("body")
        title = self._tag("title")
        if title is None:
            etree.SubElement(head, "title").append(etree.Comment("title"))
        elif title.text == "":
            title.append(etree.Comment("title"))
        
        inlineCSS = False
        inlineJS = False
        if inlineCSS:
            for path in self.links.keys():
                p = "%s%s" % ( DOCUMENT_ROOT, path)
                if os.path.exists(p):
                    f = open(p)
                    comment = etree.Comment("\n%s\n //" % (f.read()))
                    f.close()
                    style = etree.Element("style")
                    style.set('type',"text/css")
                    style.append(comment)
                    head.append(style)
        else:
            for style in self.links.values():
                head.append(style)

        
        require = []
        preamble = 'require.config({  \n\
                baseUrl:"/dev/lib/",  \n\
                paths: { \n\
                "jquery-plugins": "jquery/plugins",\n\
                "jquery": "jquery/jquery-1.7.0",\n\
                "jquery-ui": "jquery/jquery-ui-1.8.16.custom",\n\
                "underscore": "misc/underscore",\n\
                "config": "../configuration"\n\
              },\n\
              priority: ["jquery", "underscore"] });\n'
        if inlineJS:
            for path in self.scripts.keys():
                p = "%s%s" % ( DOCUMENT_ROOT, path)
                if os.path.exists(p):
                    f = open(p)
                    
                    comment = etree.Comment("\n%s%s\n //" % (preamble, f.read()))
                    f.close()
                    script = etree.Element("script")
                    script.append(comment)
                    body.append(script)
                    script.getprevious().tail = '\n'
                else:
                    require.append('"%s"' % ( path ))
        else:
            for path in self.scripts.keys():
                require.append('"%s"' % ( path ))
            
        
        if len(require) > 0: 
            script = etree.Element("script")    
            script.text = "%srequire([%s]);" % (preamble,",".join(require))        
            body.append(script)
            script.getprevious().tail = '\n'
        
    def getResponse(self):
        if not self.useCache:
            if not os.path.exists(os.path.dirname(self.cacheFile)):
                os.mkdir(os.path.dirname(self.cacheFile))
            f = open(self.cacheFile,"wb")
            s = html.tostring(self.document,pretty_print=True,doctype='<!DOCTYPE HTML>',encoding='UTF-8')
            f.write(s)
            f.close()
        
        f = open(self.cacheFile,"rb")
        statobj = os.stat(self.cacheFile)
        response = HttpResponse(f.read(),content_type="text/html")
        f.close()
        response["Last-Modified"] = http_date(statobj.st_mtime)
        response["Content-Length"] = statobj.st_size
        return response
    
    def _tag(self, tag):
        t = self.document.find(tag)
        if t is None:
            head = self.document.find("xhtml:%s" % tag, {'xhtml' : 'http://www.w3.org/1999/xhtml'})
        return t
        
    def _resolveWidgets(self):
        nresolved = 0
            
        for div in list(self.document.iter()):
            id = div.get('id')
            classes = div.get('class')
            if id is not None and classes is not None and (classes == 'widget_inline' or 'widget_inline' in classes.split(' ')):
                widgetName, widgetId = self._parseWidgetId(id)
                if widgetName is not None:
                    if widgetName not in self.ignoreWidgets:
                        if self._loadWidget(widgetName, widgetId, div):
                            nresolved = nresolved + 1
                        else:
                            self.ignoreWidgets.append(widgetId)
        #logging.error("Resolved a widget ? %s " % nresolved)
        return nresolved > 0
    
    def _parseWidgetId(self,widgetId):
        parts = widgetId.split('_', 2)
        if parts[0] != 'widget':
            return None, None
        widgetName = parts[1]
        if len(parts) == 3:
            widgetId = parts[2]
        else:
            widgetId = "%s_container_%s" % (widgetName,self.widgetCount)
            self.widgetCount = self.widgetCount + 1
        return widgetName, widgetId
        
    
    def _loadBundle(self, path, bundle=None):
        f = open(path)
        if bundle is None:
            bundle = {}
        key = None
        line = 0
        for l in f.readlines():
            line = line + 1
            l = l.rstrip('\r').rstrip('\n')
            if l.startswith('#'):
                continue
            if key is None:
                v = l.split('=',1)
                if len(v) != 2:
                    if len(l.strip()) > 0:
                        logging.error("Ignoring bundle line %s:%s %s %s parsed" % (path, line, v, l))
                    continue
                key = v[0].strip()
                value = v[1].lstrip()
            else:
                value = value+l
                
            if value.endswith('/'):
                value = value[:len(value)-1]
            else:
                bundle[key] = value
                key = None
        return bundle
    
    def _localise(self, file, wc={}):
        bundleFiles = []
        bundleFiles.append('%s/dev/bundle/default.properties' % (DOCUMENT_ROOT))        
        bundleFiles.append('%s/bundle/default.properties' % (os.path.dirname(file)))
        bundleFiles.append('%s/dev/bundle/%s.properties' % (DOCUMENT_ROOT, self.lang))        
        if 'i18n' in wc and 'default' in  wc['i18n'] and 'bundle' in wc['i18n'][self.lang]:
            bundleFiles.append('%s%s' % ( DOCUMENT_ROOT, wc['i18n']['default']['bundle']))
        bundleFiles.append('%s/bundle/%s.properties' % (os.path.dirname(file),self.lang))
        if 'i18n' in wc and self.lang in wc['i18n'] and 'bundle' in wc['i18n'][self.lang]:
            bundleFiles.append('%s%s' % ( DOCUMENT_ROOT, wc['i18n'][self.lang]['bundle']))
        bundle = None
        for f in bundleFiles:
            if os.path.exists(f):
                try:
                    bundle = self._loadBundle(f, bundle)
                except:
                    logging.error(traceback.format_exc())
                    logging.error("Failed to load bundle file %s " % f)
                    pass
            else:
                logging.error("Bundle file %s does not exist " % f)

        if bundle is None:
            logging.error("No bundle found for %s " % file)
            f = open(file)
            localisedContent = f.read()
            f.close()
        else:
            def rep(m):
                k = m.group('key')
                if k in bundle:
                    return bundle[k]
                logging.error("Missing Key %s " % k)
                return ""
            f = open(file)
            localisedContent = re.sub(I18N_RE, rep, f.read())
            f.close()            
        return localisedContent
            
    
    def _loadWidget(self, widgetName, widgetId, div):
        try:
            wc = self.widgetConfig[widgetName]
        except:
            wc = WidgetConfig(widgetName)
            self.widgetConfig[widgetName]  = wc
        if wc is None:
            logging.error("Widget Not Found %s " % widgetName)
            return False
        if 'iframe' in wc and wc['iframe']:
            # Not doign Iframes at the moment.
            logging.error("Widget Is an Iframe %s " % widgetName)
            return False
        if not hasattr(wc,'_widgettree'):
            wc._widgettree = etree.parse(StringIO(self._localise('%s%s' % (DOCUMENT_ROOT, wc['url']),wc)), self.parser)
        
        self._addTags(wc._widgettree.getroot(), div)
        div.set("id",widgetId)
        return True
    
    def _addTags(self, tree, div):
        for e in tree:
            if e.tag =="head":
                self._addTags(e, div)
            elif e.tag == 'body':
                self._addTags(e, div)
            elif e.tag == 'script':
                self._addScript(e, div)
            elif e.tag == 'link':
                self._addLink(e, div)
            else:
                div.append(e)
                
        return True

    def _addScript(self, script, div):
        src = script.get('src')
        if src is not None:
            if src not in self.scripts:
                self.scripts[src] = script
        else:
            div.append(script)

    def _addLink(self, link, div):
        href = link.get('href')
        if href is not None:
            if href not in self.links:
                self.links[href] = link
        else:
            div.append(link)

def renderhtml(request, path):
    '''
    Renders HTML pre-processing as much as it is able before delivering to the client
    @param request:
    @param path:
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    htmlFilePath = "%s/%s" % (DOCUMENT_ROOT, path)
    if not os.path.exists(htmlFilePath):
        return HttpResponseNotFound()
    htmlFilePath = os.path.abspath(htmlFilePath)
    if  not htmlFilePath.startswith(DOCUMENT_ROOT):
        return HttpResponseBadRequest()
    
    htmlPage = HTMLPage(htmlFilePath)
    htmlPage.expand()
    return htmlPage.getResponse()
    
    
    
