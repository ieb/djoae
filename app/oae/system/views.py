from django.conf import settings
from oae.common.util import FileSystemContent, JSON_CT
from django.core.urlresolvers import resolve
from django.http import HttpResponse, HttpResponseBadRequest,\
    HttpResponseNotAllowed
from django.utils import simplejson as json
from oae.system.world import World
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache

JSON_INDENT, DEBUG = (  settings.JSON_INDENT, settings.DEBUG )

#-------------------------------------------------------------------------------
# Static file handler, with modification of Expires header for 10 min caching
#-------------------------------------------------------------------------------

def static(request, path, document_root=settings.DOCUMENT_ROOT, show_indexes=False):
    return FileSystemContent.static(request, path, document_root, show_indexes)

@never_cache # No point in polluting the cache with debug responses.    
def doDebugView(request, path):
    if not DEBUG:
        return HttpResponseBadRequest("Debug must be enabled on the server to debug urls")
    resolverMatch = resolve(path)
    if resolverMatch is None:
        return HttpResponse("No Url Found")
    return HttpResponse(json.dumps({
                    'path': path,
                    'func_name' : resolverMatch.func.__name__,
                    'args' : resolverMatch.args,
                    'kwargs' : resolverMatch.kwargs,
                    'url_name' : resolverMatch.url_name,
                    'app_name' : resolverMatch.app_name,
                    'namespace' : resolverMatch.namespace,               
                    'namespaces' : resolverMatch.namespaces,               
                    },indent=JSON_INDENT), content_type=JSON_CT)
    
    
@login_required
def doWorldCreate(request):
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    request._dump_sql = False
    return World(request.user).create(request.POST)
