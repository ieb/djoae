from django.conf import settings
from django.http import HttpResponse
from oae.common.util import JSON_CT, MultiEncoder
from oae.user.encoder import ProfileEncoder
from oae.user.models import ANON_USER_PROFILE, Profile
import django.utils.simplejson as json
from oae.permission.models import Principal, ObjectPermission
from django.views.decorators.cache import never_cache



JSON_INDENT=(settings.JSON_INDENT)

#-------------------------------------------------------------------------------
# System me
#-------------------------------------------------------------------------------

@never_cache   # Must cache at a lower level in a way we can invalidate   
def doMe(request):
    '''
      Produce the system/me response
    '''    
    profile = None
    if request.user.is_authenticated():
        try:
            profile = request.user.profile
        except Profile.DoesNotExist:
            pass
    if profile is None:
        profile = ANON_USER_PROFILE
    if profile.isAnon():
        me = {
            'userid': "anonymous",
            'username' : "anonymous",
            'user' : {
                 'anon' : True,
                 'subjects' : [],
                 'superUser' : False
                 },
            'profile' : profile,
            'messages' : {
                'unread' : 0
                },
            'contacts' : {},
            'groups' : []
            }
    else:
        ugid = profile.ugid
        userProperties = None
        try:
            userProperties = request.user.userdata.value
        except:
            pass
        if userProperties is None:
            userProperties = {}
        # FIXME, add counts and group membership
        _, principalsForUi = Principal.getPrincipalsForUi(request.user)
        userProperties['principals'] = ";".join(principalsForUi) 
        me = {
            'userid': ugid,
            'username' : profile.name,
            'user' : {
                 'anon' : False,
                 'subjects' : principalsForUi,
                 'superUser' : request.user.is_staff,
                 'userid': ugid,
                 'userStoragePrefix': "~%s/" % ugid,
                 'userProfilePath': "/~%s/public/authprofile" % ugid,
                 'locale': profile.getLocale(),
                 'properties' : userProperties
                 },
            'profile' : profile,
            'messages' : {
                'unread' : profile.messages_unread
                },
            'contacts' : {
                "accepted": profile.contact_accepted+profile.contact_following,
                "pending": profile.contact_recieved,
                "invited": profile.contact_sent,
                },
            # FixeME: this should be a 1 shot load possibly with a simplified profile encoding. The grou__usergroupdata does make this expensive.
            # Its there because the profile encoder needs to access data in that object. It would be better of the data was in the profile itself to avoid the extra
            # operation.
            'groups' : list(Profile.objects.filter(
                                            ugid__in=ObjectPermission.getUserPrincipals(request.user).values("ugid"),
                                            type="G").
                                            exclude(ugid="everyone").
                                            prefetch_related("group__usergroupdata"))
            }
        
    return HttpResponse(json.dumps(me, cls=MultiEncoder, encoder_classes=ProfileEncoder.depend(), indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)


