
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.core.validators import email_re
from django.db import models
from django.http import HttpResponse, HttpResponseForbidden, \
    HttpResponseBadRequest, HttpResponseNotFound, HttpResponseNotAllowed
from oae.common.util import Response, Request, MultiEncoder, JSON_CT
from oae.permission.models import Principal
from oae.user.user_group_manager import UserGroupManager
from oae.user.encoder import UserManagerEncoder, ProfileEncoder
from oae.user.models import Profile
from util import UserUtil
import django.utils.simplejson as json
import logging
import traceback
from django.views.decorators.cache import never_cache

JSON_INDENT, ALLOW_QUERY_BY_USERNAME = (settings.JSON_INDENT, settings.ALLOW_QUERY_BY_USERNAME)


def userManagerUpdate(request, ugid):
    '''
    Update user properties.
    '''
    ugid = str(ugid)
    if "POST" != request.method:
        return HttpResponseNotAllowed(["POST"])
    u = UserGroupManager(request.user).updateUser(request, ugid, request.POST)
    if u is None:
        return HttpResponseNotFound()
    if not u:
        return HttpResponseForbidden()
    return Response.Ok()
    

def groupManagerUpdate(request, ugid):
    '''
    Update user properties.
    '''
    ugid = str(ugid)
    if "POST" != request.method:
        return HttpResponseNotAllowed(["POST"])
    u = UserGroupManager(request.user).updateGroup(ugid, request.POST)
    if u is None:
        return HttpResponseNotFound()
    if not u:
        return HttpResponseForbidden()
    return Response.Ok()

    

@never_cache   # Must cache at a lower level in a way we can invalidate   
def userExists(request):
    '''
      Respond with a 204 if the user does exist, and a 404 if not. ugid in the request
      is the username, not the ugid.
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    # note, userid not the right name for this parameter, however its what the UI uses, so cant correct it
    if "userid" not in request.GET:
        return HttpResponseBadRequest("userid parameter is required")
    if UserGroupManager(request.user).userExists(request.GET["userid"]):
        return HttpResponse("",status=204)
    return HttpResponseNotFound()
    
@never_cache   # Must cache at a lower level in a way we can invalidate   
def groupExists(request):
    '''
      Respond with a 204 if the user does exist, and a 404 if not. ugid in the request
      is the username, not the ugid.
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    # note, userid not the right name for this parameter, however its what the UI uses, so cant correct it
    if "groupid" not in request.GET:
        return HttpResponseBadRequest("groupid parameter is required")
    if UserGroupManager(request.user).groupExists(request.GET["groupid"]):
        return HttpResponse("",status=204)
    return HttpResponseNotFound()
    
    
def userCreate(request):
    '''
    Creates a User. If the user is Admin, then the Captcha is not checked. 
    If the user is not an admin, then the Captcha must be valid
    
    **Path Parameters**
    
    None
    
    **Request Paramerter**
    
    ``:name`` the username
    
    ``email`` the users email
    
    ``pwd`` and ``pwdConfirm`` the password
    
    ``firstName`` the users first name
    
    ``lastName`` the users last name
    
    ``:sakai:profile-import`` Profile to be imported
    
    **Responses**
    ``401`` - Captcha failed
    
    ``400`` - bad request with reason
    
    ``500`` - something went wrong
    
    ``201`` - User created with a json response
    
    Only the ``200`` response has a json response.
    
    '''
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    
    if not request.user.is_staff:
        authorized, message = Request.processCaptcha(request)
        if not authorized:
            logging.error("Captcha Failed: %s " % message)
            return HttpResponse(message, status=401)
        logging.debug("Captcha validated ")
    else:
        logging.debug("User is admin, captcha not needed")

    # check the password is correct
    # create the user in the Django DB using the Django API
    # create the user in the OAE DB
    # set everything that should be set
    # send a redirect (I guess)
    username = Request.get(request.POST,":name")
    if username is None or len(username) < 3:
        return HttpResponseBadRequest("Username is too short, must be 3 characters or more")
    email = Request.get(request.POST,"email")
    if not email_re.match(str(email)):
        return HttpResponseBadRequest("Email is not valid %s" % email)
    pwd = Request.get(request.POST,"pwd")
    pwdConfirm = Request.get(request.POST,"pwdConfirm")
    if pwd is None or pwd != pwdConfirm:
        return HttpResponseBadRequest("Passwords do not match")
    
    user = UserGroupManager(request.user).createUser(username, email, pwd, Request.getLocale(request), Request.getTimeZone(request), request.POST)
    if isinstance(user, HttpResponse):
        return user
    if user is not None:
        return Response.Ok(status=201)
    return HttpResponseBadRequest()

@login_required
def groupCreate(request):
    '''
    
    'sakai:role-title': [u'MEMBER'], 
    u'sakai:group-title': [u''], 
    u'sakai:group-id': [u'c-Gw1rUnuxEeG$gAAjMsoUZA-members'], 
    u'sakai:excludeSearch': [u'True'], 
    u'sakai:roles': [u''], 
    u'sakai:parent-group-id': [u'c-Gw1rUnuxEeG$gAAjMsoUZA'], 
    u'sakai:pseudoGroup': [u'True'], 
    u'sakai:role-title-plural': [u'MEMBERS'], 
    u'sakai:pseudoGroup@TypeHint': [u'Boolean'], 
    u'sakai:parent-group-title': [u'group11'], 
    u'sakai:category': [u'collection'], 
    u':name': [u'c-Gw1rUnuxEeG$gAAjMsoUZA-members'], 
    u'_charset_': [u'utf-8']}>,

    
    '''
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    
    if UserGroupManager(request.user).createGroup(request.user, request.POST[':name'], request.POST):
        return Response.Ok(status=201)
    return HttpResponseBadRequest() 
    
     
def changePassword(request, ugid):
    '''
      Change the password.
      The current use can change the password if they supply an old password
      staff can change the password
      If the user is externally authentication it will not be changed.
      
      **Path Parameters, see the URL mapping**
      
      ugid - the internal user ID (not the username) who's password should be changed.       
      
      **Request Paramters**
      
      ``oldPwd`` - only if not an admin, must match the existing password
      
      ``newPwd`` - New password
      
      ``newPwdConfirm``  - Must match newPwd
      
      **Responses**
      
      ``400`` - Invalid request with reason
      ``404`` - User does not exist
      ``403`` - passwords dont match
      ``200`` - password changed ok with json response.
      
      Only the ``200`` response is valid Json
    '''     
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    
    if not UserUtil.checkUserMatches(request.user, ugid):
        return HttpResponseForbidden()

    oldPwd = Request.get(request.POST,"oldPwd")
    newPwd = Request.get(request.POST,"newPwd")
    newPwdConfirm = Request.get(request.POST,"newPwdConfirm")

    if newPwd is None or newPwd != newPwdConfirm:
        return HttpResponseBadRequest("New Passwords do not match")
    
    u = UserGroupManager(request.user).changePassword(ugid, oldPwd, newPwd)
    if u is None:
        return HttpResponseNotFound("User does not exist")
    if not u:
        return HttpResponseBadRequest()
    return Response.Ok(status=200)



@never_cache   # Must cache at a lower level in a way we can invalidate   
def doUser(request, ugid):
    if request.method != "GET":
        return HttpResponseNotAllowed("GET")
    try:
        if ALLOW_QUERY_BY_USERNAME:
            user = User.objects.get(models.Q(usergroupdata__ugid=ugid)|models.Q(username=ugid))
        else:
            user = User.objects.get(usergroupdata__ugid=ugid)
        if not request.user.has_perm("user.view",user):
            logging.error("Permission denied accessing user %s " % ugid)
            return HttpResponseNotFound()
        return HttpResponse(json.dumps(user,cls=MultiEncoder, encoder_classes=UserManagerEncoder.depend(), indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)
    except User.DoesNotExist:
        return doGroup(request, ugid)

@never_cache   # Must cache at a lower level in a way we can invalidate   
def doGroup(request, ugid):
    if request.method != "GET":
        return HttpResponseNotAllowed("GET")
    try:
        group = Group.objects.get(models.Q(usergroupdata__ugid=ugid)|models.Q(name=ugid))
        if not request.user.has_perm("group.view",group):
            logging.error("Permission denied accessing group %s " % ugid)
            return HttpResponseNotFound()
        return HttpResponse(json.dumps(group,cls=MultiEncoder, encoder_classes=UserManagerEncoder.depend(), indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)
    except Group.DoesNotExist:
        logging.error("does not exist group %s " % ugid)
        return HttpResponseNotFound()
    
@never_cache   # Must cache at a lower level in a way we can invalidate   
def doGroupMembers(request, uid):
    if request.method != "GET":
        return HttpResponseNotAllowed("GET")
    try:
        logging.error("Finding Group members for %s " % (uid))
        group = Group.objects.get(models.Q(name=uid)|models.Q(usergroupdata__ugid=uid))
        if not request.user.has_perm("group.view",group):
            logging.error("Permission denied accessing group %s " % uid)
            return HttpResponseNotFound()
        principals = Principal.objects.filter(groupmember__group=group)
        profiles = list(Profile.objects.filter(models.Q(user__principal__in=principals)|models.Q(group__principal__in=principals))
                        .prefetch_related("group__usergroupdata"))
        return HttpResponse(json.dumps(profiles,cls=MultiEncoder, encoder_classes=ProfileEncoder.depend(), indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)
    except:
        logging.error(traceback.format_exc())
        logging.error(request)
        return Response.Ok()
    


