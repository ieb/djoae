
from django.http import HttpResponseNotFound, HttpResponseNotAllowed
from oae.common.util import FileSystemContent
from django.conf import settings
from django.db import models
from django.contrib.auth.decorators import login_required
from oae.user.profile_manager import ProfileManager
from oae.user.user_group_store_manager import UserGroupStoreManager
from oae.user.models import Profile
from django.views.decorators.cache import never_cache
from django.views.decorators.vary import vary_on_cookie


JSON_INDENT, DOCUMENT_ROOT = (  settings.JSON_INDENT, settings.DOCUMENT_ROOT )



@never_cache   # Must cache at a lower level in a way we can invalidate   
def doPublicProfile(request, ugid, path=None):
    if request.method not in ["GET", "POST"]:
        return HttpResponseNotAllowed(["GET", "POST"])
    manager = ProfileManager(request.user)
    if request.method == "GET":
        return manager.getProfile(request, ugid, path)
    else:
        return manager.updateProfile(request, ugid, path)

@login_required
@never_cache   # Must cache at a lower level in a way we can invalidate   
def doPrivateUserStore(request, ugid, path=None):
    if request.method not in ["GET", "POST"]:
        return HttpResponseNotAllowed(["GET", "POST"])
    manager = UserGroupStoreManager(request.user)
    if request.method == "GET":
        return manager.getFromPrivateUserStore(request, ugid, path)
    else:
        return manager.updatePrivateUserStore(request, ugid, path)

@never_cache   # Must cache at a lower level in a way we can invalidate   
def doPublicUserStore(request, ugid, path=None):
    if request.method not in ["GET", "POST"]:
        return HttpResponseNotAllowed(["GET", "POST"])
    manager = UserGroupStoreManager(request.user)
    if request.method == "GET":
        return manager.getFromPublicUserStore(request, ugid, path)
    else:
        return manager.updatePublicUserStore(request, ugid, path)

@never_cache   # Must cache at a lower level in a way we can invalidate   
def doUserGroupStore(request, ugid, path=None):
    if request.method not in ["GET", "POST"]:
        return HttpResponseNotAllowed(["GET", "POST"])
    manager = UserGroupStoreManager(request.user)
    if request.method == "GET":
        return manager.getFromUserGroupStore(request, ugid, path)
    else:
        return manager.updateUserGroupStore(request, ugid, path)
    

@vary_on_cookie
def doUserGroupPage(request, ugid):
    if request.method != "GET":
        return HttpResponseNotAllowed("GET")
    try:
        model = Profile.objects.get(models.Q(ugid=ugid)|models.Q(name=ugid,type='G'))
    except Profile.DoesNotExist:
        return HttpResponseNotFound()
    if model.type == 'G':
        return FileSystemContent.static(request, "/dev/group.html", DOCUMENT_ROOT)
    else:
        return FileSystemContent.static(request, "/dev/user.html", DOCUMENT_ROOT)
