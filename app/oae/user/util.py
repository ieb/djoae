from django.http import HttpResponse,\
    HttpResponseNotFound
from oae.common.util import Properties, JSON_CT
import django.utils.simplejson as json
from oae.user.models import InternalUserGroup
from django.conf import settings


JSON_INDENT, ALLOW_QUERY_BY_USERNAME=(settings.JSON_INDENT, settings.ALLOW_QUERY_BY_USERNAME)

class UserUtil(object):
    '''
    get the subtree identified by path as a json response
    '''
    @staticmethod
    def extractProperties(path, userData, mergeTree=None):                
        if userData is None:
            if mergeTree is None:
                return HttpResponseNotFound()
            return HttpResponse(json.dumps(mergeTree,indent=JSON_INDENT,
                                           ensure_ascii=False),
                                           content_type=JSON_CT)
        subtree = Properties.extract(path, json.loads(userData.value))
        if subtree is None:
            return HttpResponseNotFound()
        Properties.mergeTree(subtree, mergeTree)
        return HttpResponse(json.dumps(subtree,indent=JSON_INDENT,ensure_ascii=False),content_type=JSON_CT)
    
    '''
      Update the userdata of a model object from a post. The model must have a userdata property
      which will be a json string.
    '''
    @staticmethod
    def updateProperties(user, values, ugid, userDataModel, path=None, modelType=None):
        status=200
        if userDataModel is None:
            if modelType is not None:
                status=201
                userDataModel = modelType(ugid=ugid, value="{}")
            else:
                #logging.error("User mode is none and not found ")
                return 404, None
        userd = userDataModel.value
        if userd is None:
            userd = "{}"
        userd = Properties.update(json.loads(userd), values, path)
        userd['_lastModifiedBy'] = InternalUserGroup.getUgid(user)
        userDataModel.value = json.dumps(userd,ensure_ascii=False)
        return status, userDataModel
    
    
    
    @staticmethod
    def toUpdateResponse(resp):
        status, model = resp #@UnusedVariable
        if model is None:
                return None
        model.save()
        return True
    
    '''
      True if the ugid matches the user or is an admin
    '''
    @staticmethod
    def checkUserMatches(user, ugid):
        try:
            return user.is_staff or \
                ugid == InternalUserGroup.getUgid(user) or \
                ( ALLOW_QUERY_BY_USERNAME and ugid == user.username )
        except:
            return False


class GroupUtil(object):

    @staticmethod
    def updateProperties(request, values, group, modelType, path=None,):
        status=200
        try:
            groupDataModel = modelType.objects.get(name=group.name)
        except modelType.DoesNotExist:
            groupDataModel = modelType.objects.create(name=group.name)
            status=201
        
        groupdata = groupDataModel.value
        if groupdata is None:
            groupdata = "{}"

        groupdata = Properties.update(json.loads(groupdata), values, path)
        groupdata['_lastModifiedBy'] = InternalUserGroup.getUgid(request)
        groupDataModel.value = json.dumps(groupdata,ensure_ascii=False)
        return status, groupDataModel
    

    @staticmethod
    def toUpdateResponse(resp):
        return UserUtil.toUpdateResponse(resp)