'''
Created on Apr 1, 2012

@author: ieb
'''
from django.contrib.auth.models import Group, User, AnonymousUser
from oae.permission.models import ObjectPermission
from oae.user.models import GroupMember
import logging

class GroupAuthorizationBackend(object):
    '''
    This authorization backend uses the Group object itself to perform authorization.
    '''
    
    supports_object_permissions=True
    supports_anonymous_user=True
    supports_inactive_user=True

    def get_user(self, user_id):
        return None
    
    def authenticate(self,**credentials):
        return None
    
    def get_all_permissions(self, user_obj, obj=None):
        perms = set()
        if isinstance(obj,Group):
            if user_obj.is_anonymous():
                logging.error("Anon Users are granted view on %s " % obj)
                # FIXME: How are we going to do anon ?
                # if obj.view == 'A': # Anon can view
                return set(['group.view'])
            #if obj.view == 'E': # Everyone can view
            #    perms = set(['group.view'])
            
            #
            # There may be some milage in loading and caching the list of principals once per request since
            # user_obj is often request.user.
            # It might also be worth caching the permissions set against the groupmember.
            # Only do this if queries show up a lot.
            # 
            principals = ObjectPermission.getUserPrincipals(user_obj)
            try:
                for groupMember in GroupMember.objects.filter(group=obj,principal__in=principals):
                    if groupMember.role == 'M':
                        perms.update(['group.view','group.edit','group.manage'])
                    elif groupMember.role == 'E':
                        perms.update(['group.view','group.edit'])
                    elif groupMember.role == 'V':
                        perms.update(['group.view'])
            except:
                pass
            logging.error("User %s has permissions %s on  %s " % (user_obj.username, perms, obj))
        elif isinstance(obj,User):
            if user_obj.is_anonymous():
                logging.error("Anon Users are granted view on %s " % obj)
                return set(['user.view'])
            if user_obj.is_staff or user_obj == obj:
                return set(['user.view','user.edit','user.manage'])
            logging.error("User %s has permissions %s on  %s " % (user_obj.username, perms, obj))
        elif isinstance(obj,AnonymousUser):
            logging.error("Anon Users are granted view on %s " % obj)
            return set(['user.view'])
        return perms

    def has_perm(self, user_obj, perm, obj=None):
        return self.has_perms(user_obj, [ perm ], obj)
    
    def has_perms(self, user_obj, perms, obj=None):
        logging.info("Checking Group/User backend for %s %s %s " % (user_obj,perms,obj))
        if isinstance(obj,Group) or isinstance(obj,User):
            if hasattr(user_obj,"_authorized") and  user_obj._authorized:
                return True
            if user_obj.is_staff:
                return True
            allperms = self.get_all_permissions(user_obj, obj)
            return set(perms).issubset(allperms)
        return False
    