'''
Created on May 18, 2012

@author: ieb
'''
from haystack import indexes
from django.contrib.auth.models import User
import datetime


class UserIndex(object): # disable indexing of users indexes.RealTimeSearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    email = indexes.CharField(model_attr='email')
    date_joined = indexes.DateTimeField(model_attr='date_joined')

    def get_model(self):
        return User

    def index_queryset(self):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(date_joined__lte=datetime.datetime.now())