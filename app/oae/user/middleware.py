'''
Created on Mar 26, 2012

@author: ieb
'''
import re
from django.conf import settings
import time
import logging
from django.http import HttpResponse
from django.contrib.auth import authenticate, logout, login

SESSION_EXTEND_IGNORE_URLS = (settings.SESSION_EXTEND_IGNORE_URLS)
COMPILED_IGNORE_PATTERNS = [ re.compile(pattern, re.UNICODE) for pattern in SESSION_EXTEND_IGNORE_URLS ]
SESSION_REFRESH_INTERVAL = (settings.SESSION_COOKIE_AGE/5)
CONTENT_HOSTS = [ host if redirect is None else None for (host, redirect, secret) in settings.CONTENT_HOST  ] if settings.CONTENT_HOST is not None else []


class SessionExtend(object):
    '''
    Middleware that will extend the session, should be placed after the session middleware,
    will only update the local session date and wont work for persistent sessions.
    '''
    
    def process_request(self,request):
        if not request.user.is_anonymous():
            path = request.path
            host = None
            if 'HTTP_HOST' in request.META:
                host = request.META['HTTP_HOST']
            for pattern in COMPILED_IGNORE_PATTERNS: # ignore some patterns
                if pattern.search(path):
                    return
            if host is not None:
                for h in CONTENT_HOSTS: # do nothing for content hosts
                    if h is not None and h == host:
                        return
            next_refresh = request.session.get('next-refresh')
            if next_refresh is None or next_refresh < time.time():
                next_refresh = time.time()+SESSION_REFRESH_INTERVAL
                request.session['next-refresh'] = next_refresh
                request.session.save()
                logging.info("Extending session %s till  %s " % (request.session._session_key,next_refresh))
        


def basic_challenge(realm = None):
    if realm is None:
        realm = getattr(settings, 'WWW_AUTHENTICATION_REALM', ('Restricted Access'))
    # TODO: Make a nice template for a 401 message?
    response =  HttpResponse(('Authorization Required'), mimetype="text/plain")
    response['WWW-Authenticate'] = 'Basic realm="%s"' % (realm)
    response.status_code = 401
    return response

def basic_authenticate(authentication):
    # Taken from paste.auth
    (authmeth, auth) = authentication.split(' ',1)
    if 'basic' != authmeth.lower():
        return None
    auth = auth.strip().decode('base64')
    username, password = auth.split(':',1)
    return authenticate(username = username, password = password)

SESSION_KEY = '_auth_user_id'
BACKEND_SESSION_KEY = '_auth_user_backend'

def login(request, user):
    """
    Since the request is stateless, login should not generate a session.
    """
    if user is None:
        user = request.user
    # TODO: It would be nice to support different login methods, like signed cookies.
    if SESSION_KEY in request.session:
        if request.session[SESSION_KEY] != user.id:
            # To avoid reusing another user's session, create a new, empty
            # session if the existing session corresponds to a different
            # authenticated user.
            request.session.flush()
    request.session[SESSION_KEY] = user.id
    request.session[BACKEND_SESSION_KEY] = user.backend
    if hasattr(request, 'user'):
        request.user = user

class BasicAuthenticationMiddleware:
    '''
    Performs basic auth login for the request but does not create a session.
    '''
    def process_request(self, request):
        if not getattr(settings, 'BASIC_WWW_AUTHENTICATION', False):
            return None
        if not request.META.has_key('HTTP_AUTHORIZATION'):
            # No login, so logout. Basic Auth does not create a session.
            return None
        user =  basic_authenticate(request.META['HTTP_AUTHORIZATION'])
        if user is None:
            return basic_challenge()
        else:
            login(request, user)
            request._stateless_request = True
    
    def process_response(self, request, response):
        if hasattr(request, "_stateless_request") and request._stateless_request:
            request.session.modified = False # dont see a cookie this is stateless.
        return response
