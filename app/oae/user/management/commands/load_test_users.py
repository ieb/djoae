'''
Created on Jun 18, 2012

@author: ieb
'''

from django.core.management.base import BaseCommand
import logging
from django.contrib.auth.models import User
from oae.user.user_group_manager import UserGroupManager
import re

import sys
import os
workspace = os.path.dirname(
              os.path.dirname(
                os.path.dirname(
                  os.path.dirname(
                    os.path.dirname(
                      os.path.abspath(__file__))))))
logging.error("Adding %s to path" % workspace)
sys.path.append("%s" % workspace)

from oaeclient.client import Client, UserClient


class Command(BaseCommand):
    args = '<file> <start> <end> <use http if http>'
    help = 'Loads a set of test users and associated data into the database, does this direct'
    

    
    def handle(self, *args, **options):
        file = args[0]
        f = open(file, "rb")
        names = f.read().splitlines()
        f.close()
        start = 0
        end = len(names)
        if len(args) > 1:
            start = int(args[1])
        if len(args) > 2:
            end = int(args[2])
        endpoint = None
        if len(args) > 3:
            endpoint = args[3]

        logging.error( "Loading from %s %s to %s" % (file, start, end))
        if endpoint is None:
            self.adminUser = User(username="ieb")
            self.userManager = UserGroupManager(self.adminUser)
            
            for i in range(start,end):
                parts = names[i].split(',')
                parts[0] = parts[0].strip()
                parts[1] = parts[1].strip()
                parts[2] = parts[2].strip()
                parts[3] = parts[3].strip()
                # FIXME: note, this is very slow. Needs checking to see where the source of the problem is.
                # Slow even with very few users in the DB. Lots of Python CPU seen. DB and ElasticSearch showing 
                # no load.
                user = self._createUser(parts[0], "password", parts[3],  parts[1], parts[2], "en_GB", "Australia/Sydney")
                if i%100 == 0:
                    logging.error('User %s' % i)
        else:
            m = re.match("http://(?P<hostname>[^:/]*):*(?P<port>[^/]*)",endpoint)
            if m is not None:
                epinfo = m.groupdict()
                logging.error("Connecting to http://%s:%s " % (epinfo['hostname'], int(epinfo['port'])))
                client = Client(epinfo['hostname'], int(epinfo['port']))
                userClient = UserClient(client)
                client.login()
                userClient.checkMe(userClient.me(),"ieb")
                response = userClient.createUser("ian", "Ian", "Boston", "ieb@tfd.co.uk", "password")
                if response.status == 201:
                    logging.error("Created user")
                else:
                    logging.error("Didnt create user, %s %s" % (response.status, response.reason))        
                for i in range(start,end):
                    parts = names[i].split(',')
                    parts[0] = parts[0].strip()
                    parts[1] = parts[1].strip()
                    parts[2] = parts[2].strip()
                    parts[3] = parts[3].strip()
                    response = userClient.createUser(parts[0], parts[1], parts[2], parts[3], "password")
                    if response.status != 201:
                        print "Didn't create user, %s %s" % (response.status, response.reason)
                    if i%100 == 0:
                        print '%s' % i


    
    
    
    def _createUser(self, username, password, email, firstname, lastname, locale, timezone):
            return self.userManager.createUser(username, email, password, locale, timezone, {
                    ':name' : username,
                    'firstName' : firstname,
                    'lastName' : lastname,
                    'email' : email,
                    ':sakai:profile-import' : 
                        '{"basic":{"elements":{"firstName":{"value":"%s"}, \
                        "lastName":{"value":"%s"},"email":{"value":"%s"}}, \
                        "access":"everybody"},"email":"%s"}' % 
                            (firstname, lastname, email, email),
                })


