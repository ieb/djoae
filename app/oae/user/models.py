'''

NB: Not change the schema without generating a migration.


'''

from django.contrib import admin
from django.contrib.auth.models import User, Group, AnonymousUser
from django.db import models
from django.db.models.signals import pre_save, post_save
from oae.common.util import Timezone, Hash
import logging
import django.utils.simplejson as json
import traceback
from django.http import HttpRequest
from oae.tag.models import Tag

USER_GROUP_CHOICES=(          
        ('U','User'),
        ('G','Group'),
        )

# Create your models here.
class PrivateUserGroupData(models.Model):
    """
    Stores private User/Group data, one record per user keyed by the ugid,
    the value contains json
    """
    id = models.AutoField(primary_key=True, help_text="Internal ID, not used by the application directly")
    user = models.OneToOneField(User,null=True)    
    group = models.OneToOneField(Group,null=True)    
    value = models.TextField(help_text="JSON string containing all the private data")
    ugid = models.CharField("User/Group Id",
                          unique=True,
                          db_index=True,
                          max_length=64,
                          help_text="The Internal User/Group ID, matches the field of the same name on the Internal User object.")
    name = models.CharField("Name of User or Group",
                            unique=True,
                            db_index=True,
                            max_length=64)
    type = models.CharField(max_length=1,choices=USER_GROUP_CHOICES)
 
    @staticmethod
    def syncFromUser(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            if not hasattr(instance,'_usermanager_created'):
                try:
                    ugid = InternalUserGroup.createId(instance)
                    InternalUserGroup.connectId(instance, PrivateUserGroupData.objects.create(user=instance,
                                        name = instance.username,
                                        ugid = ugid,
                                        value="{}",
                                        type='U'))
                except:
                    logging.error(traceback.format_exc())
                    logging.error("Failed to create PrivateUserGroupData Object")
                    pass

    @staticmethod
    def syncFromGroup(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            try:
                ugid = InternalUserGroup.createId(instance)
                InternalUserGroup.connectId(instance, PrivateUserGroupData.objects.create(group=instance,
                                    name = instance.name,
                                    ugid = ugid,
                                    value="{}",
                                    type='G'))
            except:
                logging.error(traceback.format_exc())
                logging.error("Failed to create PrivateUserGroupData Object")
                pass

post_save.connect(PrivateUserGroupData.syncFromUser, sender=User)
post_save.connect(PrivateUserGroupData.syncFromGroup, sender=Group)
    
    
class PublicUserGroupData(models.Model):
    """
    Stores Public User data, one record per user keyed by the ugid,
    the value contains json
    """
    id = models.AutoField(primary_key=True, help_text="Internal ID, not used by the application directly")
    user = models.OneToOneField(User,null=True)
    group = models.OneToOneField(Group,null=True)
    type = models.CharField(max_length=1,choices=USER_GROUP_CHOICES)
    value = models.TextField(help_text="JSON string containing all the public data")
    ugid = models.CharField("User/Group Id",
                          unique=True,
                          db_index=True,
                          max_length=64,
                          help_text="The Internal User/Group ID, matches the field of the same name on the Internal User object.")
    name = models.CharField("Name of User or Group",
                            unique=True,
                            db_index=True,
                            max_length=64)
    
    @staticmethod
    def syncFromUser(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            if not hasattr(instance,'_usermanager_created'):
                try:
                    ugid = InternalUserGroup.createId(instance)
                    InternalUserGroup.connectId(instance, PublicUserGroupData.objects.create(user=instance,
                                        name = instance.username,
                                        ugid = ugid,
                                        value="{}",
                                        type='U'))
                except:
                    logging.error(traceback.format_exc())
                    logging.error("Failed to create PublicUserGroupData Object")
                    pass

    @staticmethod
    def syncFromGroup(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            try:
                ugid = InternalUserGroup.createId(instance)
                logging.error("Creating PublicUserGroupData %s %s " % (instance.name, ugid))
                InternalUserGroup.connectId(instance, PublicUserGroupData.objects.create(group=instance,
                                    name = instance.name,
                                    ugid = ugid,
                                    value="{}",
                                    type='G'))
            except:
                logging.error(traceback.format_exc())
                logging.error("Failed to create PublicUserGroupData Object")
                pass
            

post_save.connect(PublicUserGroupData.syncFromUser, sender=User)
post_save.connect(PublicUserGroupData.syncFromGroup, sender=Group)


class UserGroupData(models.Model):
    """
    Stores User/Group Data associated with the account, one record per user keyed by the ugid,
    the value contains json
    """
    id = models.AutoField(primary_key=True, help_text="Internal ID, not used by the application directly")
    user = models.OneToOneField(User,null=True)
    group = models.OneToOneField(Group,null=True)
    type = models.CharField(max_length=1,choices=USER_GROUP_CHOICES)
    value = models.TextField(help_text="JSON string containing all the user data")
    ugid = models.CharField("User/Group Id",
                          unique=True,
                          db_index=True,
                          max_length=64,
                          help_text="The Internal User ID, matches the field of the same name on the Internal User object.")
    name = models.CharField("Name of User or Group",
                            unique=True,
                            db_index=True,
                            max_length=64)

    def getMembers(self,role, useCache=True):
        if not hasattr(self,"_members"):
            self._members = {}
        if not useCache or role not in self._members:
            from oae.permission.models import Principal # avoiding circular and keeping the query efficient.
            principals = Principal.objects.filter(groupmember__group=self.group,groupmember__role=role)
            l = []
            for p in principals:
                if p.group is None:
                    l.append(p.ugid)
                else:
                    l.append(p.group.name) 
                    # Grr the UI does some manipulations using the ID of the content item 
                    # to build group names and then uses that later. very bad, MUST be fixed.
            self._members[role] = l
        return self._members[role]


    
    
    @staticmethod
    def syncFromUser(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            if not hasattr(instance,'_usermanager_created'):
                try:
                    ugid = InternalUserGroup.createId(instance)
                    InternalUserGroup.connectId(instance, UserGroupData.objects.create(user=instance,
                                        name = instance.username,
                                        ugid = ugid,
                                        value="{}",
                                        type='U'))
                except:
                    logging.error(traceback.format_exc())
                    logging.error("Failed to create UserGroupData Object")
                    pass

    @staticmethod
    def syncFromGroup(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            try:
                ugid = InternalUserGroup.createId(instance)
                InternalUserGroup.connectId(instance, UserGroupData.objects.create(group=instance,
                                    name = instance.name,
                                    ugid = ugid,
                                    value="{}",
                                    type='G'))
            except:
                logging.error(traceback.format_exc())
                logging.error("Failed to create UserGroupData Object")
                pass

post_save.connect(UserGroupData.syncFromUser, sender=User)
post_save.connect(UserGroupData.syncFromGroup, sender=Group)



class Profile(models.Model):
    """
    Stores Profile data, one record per user keyed by the ugid,
    the value contains json
    """
    class Meta:
        '''
        Permissions relating to profile, Not used at present add to usermanager_views 
        '''
        permissions = (
                ("update_profile", "can update a profile object"),
                ("manage_profile", "can manage a profile object"),
                ("view_profile", "can view this profile object"),
                ("manage_any_profile", "can update any profile")
            )
    id = models.AutoField(primary_key=True, help_text="Internal ID, not used by the application directly")
    ugid = models.CharField("User or Group Id",
                              unique=True,
                              null=True,
                              db_index=True,
                              max_length=64,
                              help_text="The Internal User ID, matches the field of the same name on the Internal User object.")

    user = models.OneToOneField(User, null=True)
    group = models.OneToOneField(Group, null=True)
    type = models.CharField(max_length=1,choices=USER_GROUP_CHOICES)
    value = models.TextField(help_text="JSON string containing all the profile data")
    search = models.CharField("Search Field",
                              db_index=True,
                              max_length=128,
                              help_text="Search field")
    name = models.CharField("Name of User or Group",
                            unique=True,
                            null=True,
                            db_index=True,
                            max_length=64)
    contact_sent = models.IntegerField(default=0)
    contact_recieved = models.IntegerField(default=0)
    contact_accepted = models.IntegerField(default=0)
    contact_blocked = models.IntegerField(default=0)
    contact_rejected = models.IntegerField(default=0)
    contact_ignored = models.IntegerField(default=0)
    contact_following = models.IntegerField(default=0)
    messages_unread = models.IntegerField(default=0)
    content_count = models.IntegerField(default=0)
    created = models.DateTimeField(
                auto_now_add=True,
                verbose_name="Time the content item was created",
                help_text="Time the content item was created")
    modified = models.DateTimeField(
                auto_now=True,
                db_index=True,
                verbose_name="Time the content item was modified",
                help_text="Time the content item was modifed")
    tags = models.ManyToManyField(Tag, related_name="profile_tags")
    category = models.CharField("Category of user or group",
                            null=True,
                            db_index=True,
                            max_length=32)

    anon = False        
    parsedProfile = None
    
    def getFirstName(self):
        try:
            return self.getProfile()["basic"]["elements"]["firstName"]["value"]
        except KeyError:
            return None

    def getLastName(self):
        try:
            return self.getProfile()["basic"]["elements"]["lastName"]["value"]
        except KeyError:
            return None

    def getEmail(self):
        try:
            return self.getProfile()["basic"]["elements"]["email"]["value"]
        except KeyError:
            return None

    def getProfile(self):
        if self.parsedProfile is None:
            self.parsedProfile = json.loads(self.value)
            self.parsedProfile['userid'] = self.ugid
            self.parsedProfile['rep:groupId'] = self.ugid
        return self.parsedProfile

    def getLocale(self):
        userDataMap = None
        try:
            # FIXME: It would be nice to avoid this load
            userDataMap = json.loads(UserGroupData.objects.get(ugid=self.ugid).value)
        except:
            userDataMap = {}
        if "locale" in userDataMap:
            language, country = userDataMap['locale'].replace("-", "_").split("_")
        else:
            language = "en"
            country = "GB"
        if "timezone" in userDataMap:
            timezoneName = userDataMap['timezone']
        else:
            timezoneName = Timezone.getTimeZoneName(country=country)
        
        return {
                'language' : language,
                'country' : country,
                'timezone' : {
                              'name' : timezoneName,
                              'GMT' : Timezone.getTimeZoneOffset(timezoneName)
                              }
                }

    @staticmethod
    def _getEveryoneGroup():
        everyone = Profile(ugid="everyone", type='G')
        return everyone

    @staticmethod
    def _getAnonUser():
        anon = Profile(ugid="anonymous", type='U')
        anon.anon = True
        anon.value = json.dumps(Profile.createAnonProfile(),ensure_ascii=False)
        return anon

    def isAnon(self):
        return self.anon


    @staticmethod
    def createAnonProfile():
        return {
                     'basic':{
                              'access':'everybody',
                              'elements':{
                                          'lastName':{ 'value':'User'},
                                          'email':{'value':"anonymous@internet.org"},
                                          'firstName':{'value': "anonymous"}
                                          }
                              },
                     'rep:userId': "anonymous",
                     'userid': "anonymous"
                     }
            
    @staticmethod
    def updateSearch(sender, instance=None, created=False, **kwargs):
        if instance is not None and not hasattr(instance,"_skipupdate"):
            m = json.loads(instance.value)
            values = []
            try:
                values.append(m['basic']['elements']['firstName']['value'])
            except:
                pass
            try:
                values.append(m['basic']['elements']['lastName']['value'])
            except:
                pass
            try:
                values.append(m['basic']['elements']['email']['value'])
            except:
                pass
            if len(values) > 0:
                instance.search = " ".join(values)
                
    @staticmethod
    def syncFromUser(sender, instance=None, created=False, **kwargs):
        if instance is not None:
            if not hasattr(instance,'_usermanager_created'):
                try:
                    profile = Profile.objects.get(user=instance)
                    newsearch  =" ".join([instance.first_name, instance.last_name, instance.email])
                    if newsearch != profile.search:
                        logging.error("Updating User profile [%s] [%s] " % (profile.search, newsearch))
                        profile.search = newsearch
                        profile._skipupdate = True
                        profile.save()
                except Profile.DoesNotExist:
                    ugid = InternalUserGroup.createId(instance)
                    InternalUserGroup.connectId(instance, Profile.objects.create(user=instance,
                                name=instance.username,
                                ugid=ugid,
                                value=json.dumps({
                                    'rep:userId' : ugid,
                                    'userid' : ugid              
                                }),
                                type='U',
                                search=" ".join([instance.first_name, 
                                                 instance.last_name, instance.email])))

    @staticmethod
    def syncFromGroup(sender, instance=None, created=False, **kwargs):
        if instance is not None:
            try:
                profile = Profile.objects.get(group=instance)
                newsearch = " ".join([instance.name])
                if newsearch != profile.search:
                    logging.error("Updating Group profile [%s] [%s] " % (profile.search, newsearch))
                    profile.search = newsearch
                    profile._skipupdate = True
                    profile.save()
            except Profile.DoesNotExist:
                ugid = InternalUserGroup.createId(instance)
                InternalUserGroup.connectId(instance, Profile.objects.create(group=instance,
                            name=instance.name,
                            ugid=ugid,
                            value=json.dumps({
                                'rep:groupId' : instance.name,
                                'groupid' : instance.name              
                            }),
                            type='G',
                            search=" ".join([instance.name])))
            
pre_save.connect(Profile.updateSearch, sender=Profile)
post_save.connect(Profile.syncFromUser, sender=User)
post_save.connect(Profile.syncFromGroup, sender=Group)

ANON_USER_PROFILE = Profile._getAnonUser()
EVERYONE_GROUP_PROFILE = Profile._getEveryoneGroup()



class ProfileLoader(object):
    '''
    Loads profiles from a list of Ids in a single operation
    '''
    
    def __init__(self):
        self.profiles = {}
    
    def add(self, ids):
        '''
        Add some more ids to be loaded, can be usernames of userids
        @param ids:
        '''
        for i in ids:
            if i not in self.profiles:
                self.profiles[i] = None
    
    def load(self):
        '''
        Load the profiles, that are not already loaded
        '''
        l = []
        for k,v in self.profiles.items():
            if v is None:
                l.append(k)
        if len(l) > 0:
            try:
                plist = list(Profile.objects.filter(models.Q(name__in=l,type='G') | models.Q(ugid__in=l) ))
                for p in plist:
                    self.profiles[p.name] = p
                    self.profiles[p.ugid] = p
            except Profile.DoesNotExist:
                pass
            # Mark any not found as unknown
            for k,v in self.profiles.items():
                if v is None:
                    self.profiles[k] = {"userid" : "unknown" }
        
    def filter(self, ids):
        '''
        Return the ids from the loaded set
        @param ids:
        '''
        p = {}
        for id in ids:
            try:
                if self.profiles[id] is None:
                    p[id] = {"userid" : "unknown" }
                else:
                    p[id] = self.profiles[id]
            except KeyError:
                pass
        return p.values()

UGID_CACHE = {} # fixme replace with a real cache. No invalidation is required since
# the contents are immutable.

class InternalUserGroup(models.Model):
    class Meta:
        '''
        Cover Public and Private User Data and InternalUserGroup 
        '''
        permissions = (
                ("create_group", "can create a user"),
                ("update_group", "can update this user"),
                ("manage_group", "can manage this user"),
                ("view_group", "can view this user"),
                ("manage_any_group", "can update any user"),
                ("create_user", "can create a user"),
                ("update_user", "can update this user"),
                ("manage_user", "can manage this user"),
                ("view_user", "can view this user"),
                ("manage_any_user", "can update any user")
            )
        
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User,null=True)
    group = models.OneToOneField(Group,null=True)
    type = models.CharField(max_length=1,choices=USER_GROUP_CHOICES)
    name = models.CharField("name of the user or the group",
                            unique=True,
                            db_index=True,
                            max_length=64)
    ugid = models.CharField("User Group Id",
                              unique=True,
                              db_index=True,
                              max_length=64,
                              help_text="The Internal User ID, matches the field of the same name on the Internal User object.")
    anon = False
        
    def isAnon(self):
        return self.anon
    
    def getProfile(self):
        try:
            return Profile.objects.get(ugid=self.ugid)
        except:
            return Profile.createAnonProfile()
    
        
        
    
            
    
    
    @staticmethod
    def createId(instance):
        '''
        Create a new ugid for the instance, but only if the instance has not in this operation
        had a ugid created or retrieved. If the instance has the attribute ugid that is used.
        If the instance has an attribute _ugid, that is used, if not, then a new UUID is generated
        and saved in _ugid. This allows this method to be called more than once on an object
        without too much fear of creating additional uuids.
        @param instance:
        '''
        if hasattr(instance, "ugid") and instance.ugid is not None:
            return instance.ugid
        if not hasattr(instance, "_ugid"):
            instance._ugid = Hash.genCompactUUID()
        return instance._ugid
    
    @staticmethod
    def getUgid(instance):
        '''
        Get a UGID, from attribute, ugid or _ugid, failing that from cache.
        Failing that by querying IngernalUserGroup.
        @param instance:
        '''
        if hasattr(instance,"ugid"):
            return instance.ugid
        if hasattr(instance,"_ugid"):
            return instance._ugid
        if isinstance(instance,AnonymousUser):
            return "anonymous"
        if isinstance(instance, User):
            k = "u%s" % instance.id
            if k in UGID_CACHE:
                return UGID_CACHE[k]
            try:
                # This might not be that cleaver since every time the lastlogin date of
                # the user object is updated this query will invalidate in the generational
                # cache. FIXME: modify how last modified is stored to move it out of the User
                # object and make the User object almost immutable.
                instance._ugid = InternalUserGroup.objects.get(user=instance).ugid
                UGID_CACHE[k] = instance._ugid
            except InternalUserGroup.DoesNotExist:
                instance._ugid=instance.username
                UGID_CACHE[k] = instance._ugid
            return instance._ugid
        if isinstance(instance, Group):
            k = "g%s" % instance.id
            if k in UGID_CACHE:
                return UGID_CACHE[k]
            instance._ugid = InternalUserGroup.objects.get(group=instance).ugid
            UGID_CACHE[k] = instance._ugid
            return instance._ugid
        if isinstance(instance, HttpRequest):
            k = "u%s" % instance.user.id
            if k in UGID_CACHE:
                return UGID_CACHE[k]
            instance._ugid = InternalUserGroup.objects.get(user=instance.user).ugid
            UGID_CACHE[k] = instance._ugid
            return instance._ugid
        logging.error("Instance is Not recognized %s " % (instance))
        raise Exception("Unable to get ugid for %s " % instance)

    @staticmethod
    def connectId(source, instance):
        if hasattr(source,"ugid"):
            if not hasattr(instance, "_ugid"):
                instance._ugid = source.ugid
                return instance
            raise Exception("%s already has ugid, cant connect " % instance)
        if hasattr(source,"_ugid"):
            if not hasattr(instance, "_ugid"):
                instance.ugid = source._ugid
                return instance
            raise Exception("%s already has ugid, cant connect " % instance)
        raise Exception("%s has no ugid, cant connect " % source)
    
    @staticmethod
    def syncFromUser(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            if not hasattr(instance,'_usermanager_created'):
                try:
                    ugid = InternalUserGroup.createId(instance)
                    InternalUserGroup.connectId(instance,InternalUserGroup.objects.create(
                                user=instance,
                                name=instance.username,
                                type='U',
                                ugid=ugid))
                except:
                    logging.error(traceback.format_exc())
                    logging.error("Failed to Create Internal User Object")
                    pass 

    @staticmethod
    def syncFromGroup(sender, instance=None, created=False, **kwargs):
        if instance is not None and created:
            try:
                ugid = InternalUserGroup.createId(instance)
                InternalUserGroup.connectId(instance, InternalUserGroup.objects.create(
                            group=instance,
                            name=instance.name,
                            type='G',
                            ugid=ugid))
            except:
                logging.error(traceback.format_exc())
                logging.error("Failed to Create Internal User Object")
                pass 
            
post_save.connect(InternalUserGroup.syncFromUser, sender=User)
post_save.connect(InternalUserGroup.syncFromGroup, sender=Group)
    


class GroupGroups(models.Model):
    group = models.ForeignKey(Group,related_name="child")  # group.child points to this and group.child.parent points back
    child = models.ForeignKey(Group,related_name="parent") # group.parent points to this group.parent.group is the parent 

from oae.permission.models import Principal
class GroupMember(models.Model):
    '''
    Tihs is the role that the user or group has within a group. Membership is part of the DJango model
    '''
    class Meta:
        unique_together=(("principal","group"),)
        
    GROUP_MEMBER_ROLE_CHOICES=(
                ('M',"Manager"),
                ('E',"Editor"),
                ('V',"Viewer"),
            )
    
    principal = models.ForeignKey(Principal)
    group = models.ForeignKey(Group)
    role = models.CharField(max_length=1, choices=GROUP_MEMBER_ROLE_CHOICES)
        
    
## Admin Interface configuration

class InternalUserGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'ugid', 'userrecordid')
    search_fields = ('name', 'value')
    
    
    def userrecordid(self,obj):
        return obj.user.id
    
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = ('ugid', 'name', 'userrecordid')
    search_fields = ('user__username', 'value')

    def username(self,obj):
        if obj.user is not None:
            return obj.user.username
        return obj.group.name

    def userrecordid(self,obj):
        if obj.user is not None:
            return obj.user.id
        return obj.group.id




admin.site.register(InternalUserGroup, InternalUserGroupAdmin)
admin.site.register(Profile, ExtraDataAdmin)
admin.site.register(UserGroupData, ExtraDataAdmin)
admin.site.register(PublicUserGroupData, ExtraDataAdmin)
admin.site.register(PrivateUserGroupData, ExtraDataAdmin)
    
