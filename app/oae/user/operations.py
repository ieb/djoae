'''
Created on Mar 29, 2012

@author: ieb
'''
from oae.common.util import Hash, Response
import os
import re
from oae.tag.models import Tag
import django.utils.simplejson as json

class IgnoreOperation(object):
    def doOperation(self, user, changes, model):
        return  None
    

class TagProfileOperartion(object):
    
    def doOperation(self, user, changes, model):
        profile = model.user.profile
        tags = changes.getlist('key')
        return self.tagProfile(profile, tags)
        
    def tagProfile(self, profile, tags):
        for tag in tags:
            taghash = Hash.hash(tag)
            
            try:
                tagObject = profile.tags.filter(hash=taghash)[0]
            except:
                parent = os.path.dirname(tag)
                name = os.path.basename(tag)
                parenthash = Hash.hash(parent)
                depth = len(re.sub(r'[^/]+', '', tag)) # counts the number of / in the path 
                tagObject, created = Tag.objects.get_or_create( #@UnusedVariable
                                        hash=taghash,
                                        parenthash=parenthash,
                                        name=name,
                                        fullpath=tag,
                                        titlekey=name,
                                        depth=depth
                                        )
                profile.tags.add(tagObject)
        v = json.loads(profile.value)
        t = [ u"%s" % t.fullpath.replace("/tags/","") for t in profile.tags.all() ]
        v['sakai:tags'] = t
        profile.value = json.dumps(v)
        profile.save()
        return Response.Ok()
        
        
class DeleteTagProfileOperation(object):
    def doOperation(self, user, changes, model):
        profile = model.user.profile
        tags = changes.getlist('key')
        for tag in tags:
            taghash = Hash.hash(tag)
            try:
                profile.tags.remove(profile.tags.filter(hash=taghash)[0])
            except:
                pass
        v = json.loads(profile.value)
        t = [ u"%s" % t.fullpath.replace("/tags/","") for t in profile.tags.all() ]
        v['sakai:tags'] = t
        profile.value = json.dumps(v)
        profile.save()
        return Response.Ok()
