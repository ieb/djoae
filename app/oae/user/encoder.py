from models import Profile
import django.utils.simplejson as json
from django.contrib.auth.models import User, Group
from oae.common.util import Properties
from django.conf import settings
from oae.permission.models import Principal
from oae.user.models import GroupMember
import time
import logging

FILTER_USER_PROPERTIES = (settings.FILTER_USER_PROPERTIES)

class NullProfileEncoder(json.JSONEncoder):
    @staticmethod
    def depend():
        return set([NullProfileEncoder])

    def default(self, obj):
        if isinstance(obj, Profile):
            return None
        return super(NullProfileEncoder,self).default(obj)

class ProfileEncoder(json.JSONEncoder):
    @staticmethod
    def depend():
        return set([ProfileEncoder])
    
    def default(self, obj):
        if isinstance(obj, Profile):
            if obj.type == 'U':
                return self._encodeUser(obj)
            elif obj.type == 'G':
                return self._encodeGroup(obj)
        return super(ProfileEncoder,self).default(obj)
    

    def _encodeUser(self, obj):
        e = json.loads(obj.value)
        if not obj.isAnon():
            if 'sakai:tags' not in e:
                t = [ u"%s" % t.fullpath.replace("/tags/","") for t in obj.tags.all() ]
                e['sakai:tags'] = t
                obj.value = json.dumps(e)
                obj.save()
            else:
                t =  e['sakai:tags']
            if 'basic' not in e:
                e['basic'] = {}
            if  'elements' not in e['basic']:
                e['basic']['elements'] = {}
            if 'sakai:tags' not in e['basic']['elements']:
                e['basic']['elements']['sakai:tags'] = {}
            e['basic']['elements']["sakai:tags"]["value"] = t
        e['rep:userId'] = obj.ugid # this value makes lookup of username work
        e['userid'] = obj.ugid
        e['hash'] = obj.ugid
        e["counts"] = {
            "contactsCount": obj.contact_accepted+obj.contact_following,
            "membershipsCount": 0,
            "contentCount": obj.content_count
            }
    # these may be necessary for other feeds.
    #            
    #            e['userid'] = obj.username
    #           e['user'] = obj.username
    #            e["counts"] = {
    #                "contactsCount": 0,
    #                "membershipsCount": 0,
    #                "contentCount": 0,
    #                "countLastUpdate": 1330936930702
    #                }
    #            e["sakai:excludeSearch"] = False
    #            e["homePath"] = "/~%s" % (obj.userid)
    #            e["sakai:status"] = "offline"
    #            e["sakai:location"] = "none"
        return Properties.filter(e, exclude=FILTER_USER_PROPERTIES)
    

    
    def _encodeGroup(self,obj):
        e = json.loads(obj.value)
        # To avoid 100s of queries you must use .prefetch_related("group__usergroupdata") on the query set
        groupdata = json.loads(obj.group.usergroupdata.value)
        for k,v in groupdata.iteritems():
            e[k] = v
        if 'sakai:tags' not in e:
            t = [ u"%s" % t.fullpath.replace("/tags/","") for t in obj.tags.all() ]
            e['sakai:tags'] = t
            obj.value = json.dumps(e)
            obj.save()
        else:
            t =  e['sakai:tags']
        if 'basic' not in e:
            e['basic'] = {}
        if  'elements' not in e['basic']:
            e['basic']['elements'] = {}
        if 'sakai:tags' not in e['basic']['elements']:
            e['basic']['elements']['sakai:tags'] = {}
        e['basic']['elements']["sakai:tags"]["value"] = t
        e['rep:groupId'] = obj.name # this value makes lookup of username work
        e['sakai:group-id'] = obj.name # sigh... this makes worlds work
        e['groupid'] = obj.name # this is used in most other places
        e['hash'] = obj.ugid
        e['sakai:category'] = obj.category
        e['homePath'] = '/~%s' % obj.name
        e['lastModified'] = int(time.mktime(obj.modified.timetuple())*1000)
        e["counts"] = {
            "contactsCount": obj.contact_accepted+obj.contact_following,
            "membershipsCount": 0,
            "contentCount": obj.content_count
            }
        return e
    
    


class UserManagerEncoder(json.JSONEncoder):
    @staticmethod
    def depend():
        return set([UserManagerEncoder])
    

    def default(self, obj):
        if isinstance(obj, User):
            profile = obj.profile
            userdata = obj.usergroupdata
            e = json.loads(userdata.value)
            principals, principalsForUi = Principal.getPrincipalsForUi(obj) 
            e.update({
                "userid" : userdata.ugid,
                "lastName": profile.getLastName(),
                "firstName": profile.getFirstName(),
                "name": obj.username,
                "contentCount": profile.content_count,
                "contactsCount": profile.contact_following+profile.contact_accepted,
                "principals" : ";".join(principalsForUi),
                "type": "u",
                "isAutoTagging": "false",  #todo may be in value
                "sendTagMsg": "false", #todo may be in value
                "membershipsCount": GroupMember.objects.filter(principal__ugid__in=principals).count(),  
                })
            
            
            return Properties.filter(e, exclude=FILTER_USER_PROPERTIES)
            '''
                m = {
                     "lastName": "Garnet",
                     "lastModified": 1333420950597,
                     "locale": "en_US",
                     "isAutoTagging": "false",
                     "sendTagMsg": "false",
                     "contentCount": 31,
                     "contactsCount": 0,
                     "principals": "c-k4I1GsWGaa-managers;c-k4IcKiQfmg-managers;c-k4IQ7MBfaa-managers;c-k4IyAagZOu-managers;c-k4IuwL33ie-managers;c-k4NQ14GIaa-managers;c-k4NiKGgjaa-managers;c-k4NEAtU3mg-managers;c-k4NCfyG5Eb-managers;c-k4NCkEFcie-managers;c-k4NIcXPIaa-managers;yay-friday--member;dave-test-group-member;4601-2-student;the-best-little-math-course-you-ever-did-take-student;researching-groups-in-groups-contributor;c-k4X35OGpaa-managers;c-k4Xm9mA1A-managers;c-k4Xgg6Tjaa-managers;c-k4XIxdU5aa-managers;c-k4XWaY5xaa-managers;c-k43uqTegym-managers;c-k431iq2cmg-managers;c-k43SVofiec-managers;c-k48GchoOaa-managers;c-k48gH268ym-managers;c-k48mECUosP-managers;c-k48kWSgaa-managers;c-k48wsqMIie-managers;c-k48QI7WGie-managers;c-k487ovwigJ-managers;c-k4cfqSIRuk-managers;c-k4cKdAabaa-managers;c-k4cwKJ9daa-managers",
                     "type": "u",
                     "countLastUpdate": 1333416965859,
                     "timezone": "America/New_York",
                     "createdBy": "admin",
                     "created": 1333415930307,
                     "email": "alf@garnet.net",
                     "name": "alf",
                     "lastModifiedBy": "alf",
                     "firstName": "Alf",
                     "membershipsCount": 5  
                     }
            '''
        elif isinstance(obj, Group):
            profile = obj.profile
            userdata = obj.usergroupdata
            principals, principalsForUi = Principal.getPrincipalsForUi(obj) 
            properties = json.loads(userdata.value)
            if "rep:group-managers" not in properties:
                properties['rep:group-managers'] = userdata.getMembers("M")
            if "rep:group-editors" not in properties:
                properties['rep:group-editors'] = userdata.getMembers("E")
            if "rep:group-viewers" not in properties:
                properties['rep:group-viewers'] = userdata.getMembers("V")
            members = []
            members.extend(properties['rep:group-managers'])
            members.extend(properties['rep:group-editors'])
            members.extend(properties['rep:group-viewers'])
            properties.update({
                "contentCount": profile.content_count,
                "principals" : ";".join(principalsForUi),
                "name": obj.name,
                "type": "g",
                "membersCount": len(members),
                "members": ";".join(members),
                "sakai:group-id": obj.name, # UI impl prevents us from using group ID here. Have to use the name as an ID.
                      })
            e = {
                 'properties' : properties,
                 'members' : members,
                }
            
            return e
            '''            
                {
    "properties": {
        "sakai:role-title-plural": "MANAGERS",
        "contentCount": 2,
        "principals": "c-k4cwKJ9daa",
        "type": "g",
        "rep:group-managers": [
            "c-k4cwKJ9daa-managers"
        ],
        "created": 1333416625645,
        "name": "c-k4cwKJ9daa-managers",
        "sakai:role-title": "MANAGER",
        "rep:group-viewers": [
            "anonymous",
            "alf",
            "everyone"
        ],
        "firstName": "unknown",
        "members": "alf",
        "lastName": "unknown",
        "sakai:category": "collection",
        "lastModified": 1333416630625,
        "membersCount": 1,
        "sakai:group-joinable": "yes",
        "countLastUpdate": 1333416646684,
        "sakai:group-id": "c-k4cwKJ9daa-managers",
        "sakai:parent-group-id": "c-k4cwKJ9daa",
        "createdBy": "admin",
        "email": "unknown",
        "sakai:pseudoGroup": true,
        "lastModifiedBy": "alf",
        "sakai:parent-group-title": "wer09d9awfihdsaf",
        "sakai:excludeSearch": "true",
        "sakai:group-visible": "public"
    },
    "profile": "a:c-k4cwKJ9daa-managers/public/authprofile",
    "members": [
        "alf"
    ]
}
            '''
        return super(ProfileEncoder,self).default(obj)
