'''
Created on May 7, 2012

@author: ieb
'''
from django.contrib.auth.models import User, Group, Permission, UserManager
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.http import HttpResponseBadRequest, HttpResponseServerError
from django.utils import simplejson as json
from oae.common.util import Request, Properties
from oae.permission.models import Principal
from oae.user.models import GroupMember, InternalUserGroup, UserGroupData, \
    GroupGroups, PublicUserGroupData, PrivateUserGroupData, Profile
from oae.user.util import UserUtil
import logging
from oae.common.xact import xact
from django.conf import settings
from oae.common.profile import profile
from django.utils import timezone

ALLOW_QUERY_BY_USERNAME = (settings.ALLOW_QUERY_BY_USERNAME)


class UserGroupManager(object):

    def __init__(self, user):
        self.user = user
        self.ugid = InternalUserGroup.getUgid(user)

    def userExists(self, userid):
        return User.objects.filter(username=userid).exists()

    def groupExists(self, groupid):
        return Group.objects.filter(name=groupid).exists()


    @xact
    def createUser(self, username, email, pwd, locale, user_tz, values):

        try:
            User.objects.get(username=username)
            return HttpResponseBadRequest("User already exists")
        except User.DoesNotExist:
            pass
        
        profileImport = Request.get(values, ":sakai:profile-import", "{}")
        try:
            json.loads(profileImport)
        except:
            return HttpResponseBadRequest("Invalid Json found in :sakai:profile-import was %s " % profileImport)

        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = UserManager.normalize_email(email)
        newUser = User(username=username, email=email,
                          is_staff=False, is_active=True, is_superuser=False,
                          last_login=now, date_joined=now)

        newUser.set_password(pwd)
        # create the UGID as soon as possible so that when the principal binds to this
        # on save the same UGID is used.
        ugid = InternalUserGroup.createId(newUser)
        
        
        
        newUser.first_name = Request.get(values, "firstName", "")
        newUser.last_name = Request.get(values, "lastName", "")
        newUser._usermanager_created = True
        
        # Give the user the standard permissions they need to work.
        # Where permissions are used, users or groups must be given the permissions

        # Unfortunately this save is required to generate an ID for later use.
        newUser.save()
        

        PublicUserGroupData.objects.create(user=newUser,
                                    name = newUser.username,
                                    ugid = ugid,
                                    value="{}",
                                    type='U')
        PrivateUserGroupData.objects.create(user=newUser,
                                    name = newUser.username,
                                    ugid = ugid,
                                    value="{}",
                                    type='G')
        UserGroupData.objects.create(user=newUser,
                                    name = newUser.username,
                                    ugid = ugid,
                                    value= json.dumps({
                                                  "locale": locale,
                                                  "timezone" : user_tz},
                                                  ensure_ascii=False),
                                    type='U')
        Profile.objects.create(user=newUser,
                            name=newUser.username,
                            ugid=ugid,
                            value=profileImport,
                            type='U',
                            search=" ".join([newUser.first_name, 
                                             newUser.last_name, newUser.email]))

        contentCT = ContentType.objects.get(app_label="content",
                                            model="contentmeta")
        newUser.user_permissions.add(Permission.objects.get(codename="create",
                                                            content_type=contentCT))
        newUser.user_permissions.add(Permission.objects.get(codename="create_tag",
                                                            content_type=contentCT))

        # this save should only save the above 2 statements, and not the core object.
        newUser.save()

        return newUser


    @xact
    def changePassword(self, ugid, oldPwd, newPwd):
        try:
            if ALLOW_QUERY_BY_USERNAME:
                userObject = User.objects.get(models.Q(internalusergroup__ugid=ugid)|
                                              models.Q(username=ugid))
            else:
                userObject = User.objects.get(internalusergroup__ugid=ugid)
                
        except InternalUserGroup.DoesNotExist:
            return None
        except User.DoesNotExist:
            return None

        if not self.user.is_staff:
            if oldPwd is None:
                return False
            if not userObject.check_password(oldPwd):
                return False
        userObject.set_password(newPwd)
        userObject.save()
        return True


    @xact
    def createGroup(self, groupName, values={}):
        thisGroup = Group.objects.create(name=groupName)
        if "sakai:category" in values:
            thisGroup.profile.category = values['sakai:category']
            thisGroup.profile.save()
        userGroupData = thisGroup.usergroupdata
        ugDataJson = json.loads(userGroupData.value)
        userGroupData.value = json.dumps(Properties.update(ugDataJson,
                                                           values),
                                        ensure_ascii=False)
        userGroupData.save()
        # make certain the user can change this group
        GroupMember.objects.create(principal=self.user.principal,
                                   group=thisGroup,
                                   role='M')
        self.user.groups.add(thisGroup)
        self._updateGroupMembership(thisGroup, userGroupData, values)
        return thisGroup


    @xact
    def updateUser(self, ugid, changes, userToUpdate=None, path=None):
        if UserUtil.checkUserMatches(self.user, ugid):
            if userToUpdate is None:
                try:
                    if ALLOW_QUERY_BY_USERNAME:
                        model = UserGroupData.objects.get(
                                        models.Q(ugid=ugid)|
                                        models.Q(name=ugid),type='U')
                    else:
                        model = UserGroupData.objects.get(ugid=ugid,
                                                          type='U')
                except UserGroupData.DoesNotExist:
                    model = None
            else:
                model = userToUpdate
            return UserUtil.toUpdateResponse(
                            UserUtil.updateProperties(self.user,
                                                      changes,
                                                      ugid,
                                                      model,
                                                      path))
        else:
            return False

    @xact
    def updateGroup(self, ugid, changeData, group=None, path=None):
        if group is None:
            #logging.error("Group Update to %s " % ugid)
            try:
                # groups may be addressed by name or ID unfortunately the UI generates the group names, so
                # its not possible to restrict to by ID
                group = Group.objects.get(
                                models.Q(usergroupdata__ugid=ugid) |
                                models.Q(name=ugid))
                userGroupData = group.usergroupdata
            except Group.DoesNotExist:
                logging.error("Group not found")
                return None
        else:
            userGroupData = group.usergroupdata

        manage, changes = self._updateGroupMembership(group,
                                                      userGroupData,
                                                      changeData)
        if not manage and not changes:
            return False

        if manage:
            if "sakai:category" in changeData:
                group.profile.category = changeData['sakai:category']
                group.profile.save()
            #logging.error("Uppdating %s %s " % (ugid, changeData))
            return UserUtil.toUpdateResponse(UserUtil.updateProperties(self.user,
                                                                       changeData,
                                                                       ugid,
                                                                       userGroupData,
                                                                       path))
        if changes:
            userGroupData.save()
        return True

    @xact
    def changeGroupMembershipRoles(self, group, userGroupData, added, removed):
        return self._changeGroupMembershipRoles(group, userGroupData, added, removed)


    def _updateGroupMembership(self, group, userGroupData, changeData):
        changeData = Request.toQueryDict(changeData)
        manage, changes, principalAdded, principalRemoved = \
            Principal.getMembershipChanges(self.user,
                                           group,
                                           "group",
                                           changeData,
                                           ugid=self.ugid)

        # We need to update members of the the grop based on the role changes.
        # the order is remove then add.
        membershipRemoved = set()
        membershipAdded = set()

        if ':member@Delete' in changeData:
            membershipRemoved.update(changeData.getlist(':member@Delete'))
        for v in principalRemoved.values():
            for m in v:
                membershipRemoved.add(m)
        if ':member' in changeData:
            for m in changeData.getlist(':member'):
                if m in membershipRemoved:
                    membershipRemoved.remove(m)
                membershipAdded.add(m)
        for v in principalAdded.values():
            for m in v:
                if m in membershipRemoved:
                    membershipRemoved.remove(m)
                membershipAdded.add(m)

        logging.error("Membership changes adding:%s removing:%s " %
                      (membershipAdded, membershipRemoved))
        # Remove the principals associated with the group

        # FIXME: There are lots of ways we could bind the User, Group, GroupGroups to ugid.
        # At the moment we are going through usergroupdata because we need to update that later.
        # , but it might be better to go via
        # Principals. It all depends on which ones we care least about when they are invalidated.
        # the other solution is to stop the last loging update kicking entries out of cache.
        # As it hits almost everything.
        # This whole structure is driven by using the user and group tables supplied 
        # By django.
        # Remove all group members in membership Removed
        GroupGroups.objects.filter(child__usergroupdata__in=UserGroupData.objects.filter(
                        models.Q(ugid__in=membershipRemoved) |
                        models.Q(group__name__in=membershipRemoved),
                        type='G')).delete()
        for user in User.objects.filter(usergroupdata__ugid__in=membershipRemoved):
            #logging.error("Removing User %s " % user)
            group.user_set.remove(user)
        # Add group members in
        for child in Group.objects.filter(
                        models.Q(usergroupdata__ugid__in=membershipAdded) |
                        models.Q(name__in=membershipAdded)):
            #logging.error("Adding Group %s " % child)
            GroupGroups.objects.get_or_create(group=group, child=child)
        # Add the user members in
        for user in User.objects.filter(usergroupdata__ugid__in=membershipAdded):
            #logging.error("Adding User %s " % user)
            group.user_set.add(user)




        #logging.error("Updating Group Membership as part of group update ==============================================================")
        self._changeGroupMembershipRoles(group, userGroupData,
                                         principalAdded, principalRemoved)
        #logging.error("Done Updating Group Membership as part of group update ==============================================================")
        return manage, changes

    def _changeGroupMembershipRoles(self, group, userGroupData, added, removed):
        changes = len(added) + len(removed) > 0
        if changes:
            #logging.error("Making Changes to membership of Group %s  %s  added:%s removed:%s" % (group.name, userGroupData.ugid, added, removed))
            for (k, remove) in removed.iteritems():
                if len(remove) > 0:
                    GroupMember.objects.filter(group=group, role=k,
                                               principal__ugid__in=remove).delete()
            processed = set()
            for k in ['M', 'E', 'V']:
                for pid in added[k]:
                    if pid not in processed and pid is not None:
                        processed.add(pid)
                        try:
                            try:
                                principal = Principal.viaCache("u:%s" %
                                                               pid,
                                                               lambda: Principal.objects.get(ugid=pid))
                            except Principal.DoesNotExist:
                                principal = Principal.viaCache("g:%s" %
                                                               pid,
                                                               lambda: Principal.objects.get(group__name=pid))
                            cm, _ = GroupMember.objects.get_or_create(
                                                                            group=group,
                                                                            principal=principal) #@UnusedVariable
                            if cm.role != k:
                                cm.role = k
                                cm.save()
                        except Principal.DoesNotExist:
                            logging.error("Cant find principal %s " % pid)
                            pass
            value = json.loads(userGroupData.value)

            globalViewers = [ u'%s' % p['principal__ugid']
                             for p in GroupMember.objects.filter(group=group,
                                                principal__ugid__in=['anonymous', 'everyone']
                                                ).values("principal__ugid")]
            if 'anonymous' in globalViewers:
                value['view'] = 'A'
            elif 'everyone' in globalViewers:
                value['view'] = 'E'
            else:
                value['view'] = 'N'

            # NAKBUG: Why can't they use consistent naming ?
            value['rep:group-managers'] = userGroupData.getMembers("M", useCache=False)
            value['rep:group-editors'] = userGroupData.getMembers("E", useCache=False)
            value['rep:group-viewers'] = userGroupData.getMembers("V", useCache=False)

            userGroupData.value = json.dumps(value, ensure_ascii=False)

            logging.error("Group Managers on %s are %s " % (group.name, userGroupData.getMembers("M")))
            logging.error("Group Editors on %s are %s " % (group.name, userGroupData.getMembers("E")))
            logging.error("Group Viewers on %s are %s " % (group.name, userGroupData.getMembers("V")))
        else:
            logging.error("No Changes Changes to make to Group %s  %s" % (group.name, userGroupData.ugid))
        # logging.error("FinalX is %s %s " % (contentMeta.view,json.dumps(value,indent=4)))


