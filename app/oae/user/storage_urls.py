from django.conf.urls.defaults import patterns, url, include

from oae.user.storage_views import doUserGroupPage, doPublicProfile,\
    doPrivateUserStore, doUserGroupStore, doPublicUserStore
from oae.user.usermanager_views import doUser

urlpatterns = patterns('',
    url(r'^(?P<ugid>.*?)/public/authprofile\.infinity\.json$', doPublicProfile),
    url(r'^(?P<ugid>.*?)/public/authprofile\.profile\.json$', doPublicProfile),
    url(r'^(?P<ugid>.*?)/public/authprofile/(?P<path>.*?)\.profile\.json$', doPublicProfile),
    url(r'^(?P<ugid>.*?)/public/authprofile$', doPublicProfile),
    url(r'^(?P<ugid>.*?)/public/authprofile/(?P<path>.*?)$', doPublicProfile),
    url(r'^(?P<ugid>.*?)/private/(?P<path>.*?)$', doPrivateUserStore),
    url(r'^(?P<ugid>.*?)/public/(?P<path>.*?)$', doPublicUserStore),
    url(r'^(?P<ugid>.*?)/message\.', include('oae.message.storage_urls')),
    url(r'^(?P<ugid>.*?)/contacts\.', include('oae.contact.storage_urls')),
    url(r'^(?P<ugid>.*?)/message/', include('oae.message.storage_urls')),
    url(r'^(?P<ugid>.*?)/contacts/', include('oae.contact.storage_urls')),
    # This following URL may cause problems with the apache config
    url(r'^(?P<ugid>[^/]*)/(?P<path>.*?).json', doUserGroupStore, name="get user group store"),
    url(r'^(?P<ugid>[^/]*)/(?P<path>.*?)$', doUserGroupStore, name="post to subpath"),
    url(r'^(?P<ugid>[^/]*).json', doUser),
    url(r'^(?P<ugid>[^/]*)$', doUserGroupPage, name = "usergrouppage"),
)