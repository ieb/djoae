'''
Created on Jun 13, 2012

@author: ieb
'''
from django.http import HttpResponseForbidden, HttpResponseNotFound, \
    HttpResponseBadRequest, HttpResponse
from models import PrivateUserGroupData, PublicUserGroupData
from oae.user.models import UserGroupData, InternalUserGroup
from util import UserUtil
import django.utils.simplejson as json
import logging
from oae.user.operations import TagProfileOperartion, IgnoreOperation, \
    DeleteTagProfileOperation
from oae.common.util import JSON_CT, Properties, MultiEncoder, Response
from django.core.files.uploadedfile import UploadedFile
from django.conf import settings
from oae.content.encoder import ContentEncoder
from oae.content.content_manager import ContentManager
from oae.common.xact import xact
from django.db import models
from django.contrib.auth.decorators import login_required

JSON_INDENT, DOCUMENT_ROOT, ALLOW_QUERY_BY_USERNAME = (settings.JSON_INDENT, settings.DOCUMENT_ROOT, settings.ALLOW_QUERY_BY_USERNAME)


OPERATIONS = {
    "tag" : TagProfileOperartion(), # Not possible to tag deep down inside an object
    "deletetag" : DeleteTagProfileOperation(),
    "import" : IgnoreOperation(),
    }

class UserGroupStoreManager(object):



    def __init__(self, user):
        self.user = user
        self.ugid = InternalUserGroup.getUgid(user)



    @login_required
    def getFromPrivateUserStore(self, request, ugid, path):
        return self._getFromStore(request, ugid, path, PrivateUserGroupData,
                                  False, False)

    def getFromPublicUserStore(self, request, ugid, path):
        return self._getFromStore(request, ugid, path, PublicUserGroupData,
                                  True, True)

    def getFromUserGroupStore(self, request, ugid, path):
        return self._getFromStore(request, ugid, path, UserGroupData,
                                  True, True)

    @login_required
    def updatePrivateUserStore(self, request, ugid, path):
        return self._updateStore(request, ugid, path, PrivateUserGroupData,
                                 False)

    def updatePublicUserStore(self, request, ugid, path):
        return self._updateStore(request, ugid, path, PublicUserGroupData,
                                 True)

    def updateUserGroupStore(self, request, ugid, path):
        return self._updateStore(request, ugid, path, UserGroupData,
                                 True)


    def _getFromStore(self, request, ugid, path, modelType, allowPublicRead,
                      allowUserAccess):

        '''
          Get or Update the users private store,
          Only the owner or admins may perform any operation on private stores.
        '''

        try:
            if ALLOW_QUERY_BY_USERNAME:
                model = modelType.objects.get(
                                    models.Q(ugid=ugid) |
                                    models.Q(name=ugid))
            else:
                model = modelType.objects.get(
                                    models.Q(ugid=ugid) |
                                    models.Q(name=ugid, type='G'))
        except modelType.DoesNotExist:
            return HttpResponseNotFound()
        if not (allowPublicRead or
                UserUtil.checkUserMatches(request.user, ugid) or
                (model.type == 'G' and
                 request.user.has_perm("group.view", model.group)) or
                (allowUserAccess and model.type == 'U' and
                 request.user.has_perm("user.view", model.user))):
            return HttpResponseForbidden()

        t = Properties.extract(path, json.loads(model.value))
        if t is not None and "_iscontent" in t and t['_iscontent']:
            try:
                request.user._authorized = True
                return ContentManager(request.user
                            ).streamFileResponse(
                                request, contentid=t['_contentid'])
            finally:
                request.user._authorized = False
        return UserUtil.extractProperties(path, model)


    @xact
    def _updateStore(self, request, ugid, path, modelType, allowUserAccess):
        '''
        Updates one of the user stores
        @param request:
        @param ugid:
        @param path:
        @param modelType:
        '''
        try:
            if ALLOW_QUERY_BY_USERNAME:
                model = modelType.objects.get(
                                    models.Q(ugid=ugid) |
                                    models.Q(name=ugid))
            else:
                model = modelType.objects.get(
                                    models.Q(ugid=ugid) |
                                    models.Q(name=ugid, type='G'))
        except modelType.DoesNotExist:
            return HttpResponseNotFound()
        if not (UserUtil.checkUserMatches(request.user, ugid) or
                (model.type == 'G' and
                 request.user.has_perm("group.edit", model.group)) or
                (allowUserAccess and model.type == 'U' and
                 request.user.has_perm("user.edit", model.user))):
            return HttpResponseForbidden()
        if request.META['CONTENT_TYPE'] == 'application/x-www-form-urlencoded':
            try:
                if ALLOW_QUERY_BY_USERNAME:
                    model = modelType.objects.get(
                                        models.Q(ugid=ugid) |
                                        models.Q(name=ugid))
                else:
                    model = modelType.objects.get(
                                        models.Q(ugid=ugid) |
                                        models.Q(name=ugid, type='G'))
            except modelType.DoesNotExist:
                model = None


            if ":operation" in request.POST:
                if request.POST[":operation"] in OPERATIONS:
                    ret = OPERATIONS[request.POST[":operation"]].doOperation(
                                         request.user,
                                         request.POST,
                                         model)
                    if ret is not None:
                        logging.error("Returning after operation %s " %
                                      request.POST[":operation"])
                        return ret
                else:
                    logging.error("Please implement operation %s for storage_views " %
                                   request.POST[":operation"])
                    return HttpResponseBadRequest()

            return Response.toHttpResponse(
                            UserUtil.toUpdateResponse(
                                UserUtil.updateProperties(
                                        request.user,
                                        request.POST,
                                        ugid,
                                        model,
                                        path,
                                        modelType)))
        else:
            try:
                if ALLOW_QUERY_BY_USERNAME:
                    model = modelType.objects.get(
                                            models.Q(ugid=ugid) |
                                            models.Q(name=ugid))
                else:
                    model = modelType.objects.get(
                                            models.Q(ugid=ugid) |
                                            models.Q(name=ugid, type='G'))
            except modelType.DoesNotExist:
                return HttpResponseNotFound()
            # the post is a file. We need to store that in content using up:ugid/path/filename as the full_path to the content, up meaning private profile content
            result = {}
            m = json.loads(model.value)
            for name in request.FILES:
                files = request.FILES.getlist(name)
                for file in files:
                    if isinstance(file, UploadedFile):
                        _, meta = ContentManager(request.user
                                ).createFile(
                                    file,
                                    initial_status='D',
                                    full_path="%s/%s" % (request.META['PATH_INFO'],
                                    name),
                                       category='H')
                        result[name] = meta
                        Properties.extract("%s/%s" %
                                (path, name),
                                m,
                                True
                                ).update({
                                    '_iscontent' : True,
                                    '_contentid' : meta['poolId']  })
            model.value = json.dumps(m)
            model.save()
            return HttpResponse(json.dumps(result,
                                           cls=MultiEncoder,
                                           encoder_classes=ContentEncoder.depend(),
                                           indent=JSON_INDENT,
                                           ensure_ascii=False),
                                content_type=JSON_CT)
