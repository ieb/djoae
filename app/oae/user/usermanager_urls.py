from django.conf.urls.defaults import patterns, url
from usermanager_views import userManagerUpdate, userExists, \
    userCreate, changePassword
from oae.user.usermanager_views import groupManagerUpdate, groupExists,\
    groupCreate, doUser, doGroup, doGroupMembers

urlpatterns = patterns('',
    url(r'^group/(.*?)\.members\.json$', doGroupMembers),                       
    url(r'^user/(.*?)\.update\.html$', userManagerUpdate),
    url(r'^user/(.*?)\.update\.json$', userManagerUpdate),
    url(r'^group/(.*?)\.update\.html$', groupManagerUpdate),
    url(r'^group/(.*?)\.update\.json$', groupManagerUpdate),
    url(r'user.exists\..*$', userExists),
    url(r'group.exists\..*$', groupExists),
    url(r'user.create\..*$', userCreate),
    url(r'group.create\..*$', groupCreate),
    url(r'^user/(.*?)\.changePassword\.html$', changePassword),
    url(r'^user/(.*?)\.json$', doUser),
    url(r'^group/(.*?)\.json$', doGroup),
)