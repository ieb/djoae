# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    depends_on = (
        ("tag", "0001_initial"),
        ("permission", "0001_initial"),
    )

    def forwards(self, orm):
        # Adding model 'PrivateUserGroupData'
        db.create_table('user_privateusergroupdata', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True)),
            ('group', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.Group'], unique=True, null=True)),
            ('value', self.gf('django.db.models.fields.TextField')()),
            ('ugid', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('user', ['PrivateUserGroupData'])

        # Adding model 'PublicUserGroupData'
        db.create_table('user_publicusergroupdata', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True)),
            ('group', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.Group'], unique=True, null=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('value', self.gf('django.db.models.fields.TextField')()),
            ('ugid', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
        ))
        db.send_create_signal('user', ['PublicUserGroupData'])

        # Adding model 'UserGroupData'
        db.create_table('user_usergroupdata', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True)),
            ('group', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.Group'], unique=True, null=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('value', self.gf('django.db.models.fields.TextField')()),
            ('ugid', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
        ))
        db.send_create_signal('user', ['UserGroupData'])

        # Adding model 'Profile'
        db.create_table('user_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ugid', self.gf('django.db.models.fields.CharField')(max_length=64, unique=True, null=True, db_index=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True)),
            ('group', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.Group'], unique=True, null=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('value', self.gf('django.db.models.fields.TextField')()),
            ('search', self.gf('django.db.models.fields.CharField')(max_length=128, db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64, unique=True, null=True, db_index=True)),
            ('contact_sent', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('contact_recieved', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('contact_accepted', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('contact_blocked', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('contact_rejected', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('contact_ignored', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('contact_following', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('messages_unread', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('content_count', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, db_index=True, blank=True)),
        ))
        db.send_create_signal('user', ['Profile'])

        # Adding M2M table for field tags on 'Profile'
        db.create_table('user_profile_tags', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('profile', models.ForeignKey(orm['user.profile'], null=False)),
            ('tag', models.ForeignKey(orm['tag.tag'], null=False))
        ))
        db.create_unique('user_profile_tags', ['profile_id', 'tag_id'])

        # Adding model 'InternalUserGroup'
        db.create_table('user_internalusergroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True)),
            ('group', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.Group'], unique=True, null=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
            ('ugid', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64, db_index=True)),
        ))
        db.send_create_signal('user', ['InternalUserGroup'])

        # Adding model 'GroupGroups'
        db.create_table('user_groupgroups', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(related_name='child', to=orm['auth.Group'])),
            ('child', self.gf('django.db.models.fields.related.OneToOneField')(related_name='parent', unique=True, to=orm['auth.Group'])),
        ))
        db.send_create_signal('user', ['GroupGroups'])

        # Adding model 'GroupMember'
        db.create_table('user_groupmember', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('principal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['permission.Principal'])),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('user', ['GroupMember'])

        # Adding unique constraint on 'GroupMember', fields ['principal', 'group']
        db.create_unique('user_groupmember', ['principal_id', 'group_id'])

    def backwards(self, orm):
        # Removing unique constraint on 'GroupMember', fields ['principal', 'group']
        db.delete_unique('user_groupmember', ['principal_id', 'group_id'])

        # Deleting model 'PrivateUserGroupData'
        db.delete_table('user_privateusergroupdata')

        # Deleting model 'PublicUserGroupData'
        db.delete_table('user_publicusergroupdata')

        # Deleting model 'UserGroupData'
        db.delete_table('user_usergroupdata')

        # Deleting model 'Profile'
        db.delete_table('user_profile')

        # Removing M2M table for field tags on 'Profile'
        db.delete_table('user_profile_tags')

        # Deleting model 'InternalUserGroup'
        db.delete_table('user_internalusergroup')

        # Deleting model 'GroupGroups'
        db.delete_table('user_groupgroups')

        # Deleting model 'GroupMember'
        db.delete_table('user_groupmember')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'permission.principal': {
            'Meta': {'object_name': 'Principal'},
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_index': 'True'}),
            'ugid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'})
        },
        'tag.tag': {
            'Meta': {'object_name': 'Tag'},
            'depth': ('django.db.models.fields.IntegerField', [], {}),
            'fullpath': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2048', 'db_index': 'True'}),
            'hash': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'}),
            'parenthash': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'titlekey': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'user.groupgroups': {
            'Meta': {'object_name': 'GroupGroups'},
            'child': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'parent'", 'unique': 'True', 'to': "orm['auth.Group']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'child'", 'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'user.groupmember': {
            'Meta': {'unique_together': "(('principal', 'group'),)", 'object_name': 'GroupMember'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'principal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['permission.Principal']"}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'user.internalusergroup': {
            'Meta': {'object_name': 'InternalUserGroup'},
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'ugid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'})
        },
        'user.privateusergroupdata': {
            'Meta': {'object_name': 'PrivateUserGroupData'},
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'ugid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        'user.profile': {
            'Meta': {'object_name': 'Profile'},
            'contact_accepted': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'contact_blocked': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'contact_following': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'contact_ignored': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'contact_recieved': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'contact_rejected': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'contact_sent': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'content_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'messages_unread': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'search': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_index': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'profile_tags'", 'symmetrical': 'False', 'to': "orm['tag.Tag']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'ugid': ('django.db.models.fields.CharField', [], {'max_length': '64', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        'user.publicusergroupdata': {
            'Meta': {'object_name': 'PublicUserGroupData'},
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'ugid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        'user.usergroupdata': {
            'Meta': {'object_name': 'UserGroupData'},
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.Group']", 'unique': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'ugid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True'}),
            'value': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['user']