'''
Created on Jun 13, 2012

@author: ieb
'''
from django.http import HttpResponseForbidden, \
    HttpResponseBadRequest
from oae.user.models import Profile, InternalUserGroup
from util import UserUtil
import django.utils.simplejson as json
import logging
from oae.user.operations import TagProfileOperartion, IgnoreOperation, \
    DeleteTagProfileOperation
from oae.common.util import Response
from oae.common.xact import xact
from django.db import models
from django.conf import settings

ALLOW_QUERY_BY_USERNAME = (settings.ALLOW_QUERY_BY_USERNAME)


OPERATIONS = {
    "tag" : TagProfileOperartion(), # Not possible to tag deep down inside an object
    "deletetag" : DeleteTagProfileOperation(),
    "import" : IgnoreOperation(),
    }

class ProfileManager(object):

    def __init__(self, user):
        self.user = user
        self.ugid = InternalUserGroup.getUgid(user)

    def getProfile(self, request, ugid, path=None):
        try:
            if ALLOW_QUERY_BY_USERNAME:
                model = Profile.objects.get(
                                models.Q(ugid=ugid) |
                                models.Q(name=ugid))
            else:
                model = Profile.objects.get(
                                models.Q(ugid=ugid) |
                                models.Q(name=ugid, type='G'))
            logging.error("Profile found for %s " % ugid)
        except Profile.DoesNotExist:
            logging.error("No Object found for %s " % ugid)
            model = None
        v = None

        # NOTE: This is a PUBLIC profile, so no acls are applied

        ## This is ugly but necesary. Unfortunately the UI overwrites the sakai:tags entries, so we have
        # to put them back in.
        if model is not None and (path is None or path.startswith('basic')):
            v = json.loads(model.value)
            if 'sakai:tags' not in v:
                t = [ u"%s" % t.fullpath.replace("/tags/", "")
                     for t in model.tags.all() ]
                v['sakai:tags'] = t
                model.value = json.dumps(v)
                model.save()
            else:
                t = v['sakai:tags']
            if 'basic' not in v:
                v['basic'] = {}
            if 'elements' not in v['basic']:
                v['basic']['elements'] = {}
            if 'sakai:tags' not in  v['basic']['elements']:
                v['basic']['elements']["sakai:tags"] = {}
            v['basic']['elements']["sakai:tags"]["value"] = t
            v['userid'] = ugid
            return UserUtil.extractProperties(path, None, v)
        return UserUtil.extractProperties(path, model)

    @xact
    def updateProfile(self, request, ugid, path):
        try:
            if ALLOW_QUERY_BY_USERNAME:
                model = Profile.objects.get(
                                models.Q(ugid=ugid) |
                                models.Q(name=ugid))
            else:
                model = Profile.objects.get(
                                models.Q(ugid=ugid) |
                                models.Q(name=ugid, type='G'))
        except Profile.DoesNotExist:
            model = None
        if not (UserUtil.checkUserMatches(request.user, ugid) or
            (model is not None and model.type == 'G' and request.user.has_perm("group.edit", model.group)) or
            (model is not None and model.type == 'U' and request.user.has_perm("user.edit", model.user))):
            logging.error("User %s does not have permission to update %s " % (request.user, ugid))
            return HttpResponseForbidden()
        if ":operation" in request.POST:
            if request.POST[":operation"] in OPERATIONS:
                ret = OPERATIONS[request.POST[":operation"]].doOperation(
                                                                request.user,
                                                                request.POST,
                                                                model)
                if ret is not None:
                    return ret;
            else:
                logging.error("Please implement operation %s for storage_views " %
                              request.POST[":operation"])
                return HttpResponseBadRequest()
        return Response.toHttpResponse(
                            UserUtil.toUpdateResponse(
                                    UserUtil.updateProperties(
                                                    request,
                                                    request.POST,
                                                    ugid,
                                                    model,
                                                    path,
                                                    Profile)))
