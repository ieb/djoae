"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from oae.common.tests import OAETestClient



class TestPopulateWithUsers(TestCase):
        
    
    def setUp(self):
        self.client = OAETestClient(self)
        self.client.createAdmin()
        self.client.loginAdmin()

    def test_CreateUsers(self):
        self.client.loginAdmin()
        username, password = self.client.createUser()
        self.client.logout()
        self.client.login(username=username, password=password)
        self.client.checkMe( username)
        self.client.logout()
    
    def test_ChangePassword(self):
        self.client.loginAdmin()
        username, password = self.client.createUser()
        self.client.logout()
        self.client.login(username=username, password=password)
        jsonMe = self.client.checkMe( username)
        ugid = jsonMe['userid']
        # now logged in as the user, change the password
        self.client.post( "/system/userManager/user/%s.changePassword.html" % ugid, 
                         {
                          'WrongParams' : password,
                          'WrongParams' : "testPwd2",
                          'WrongParams' : "PasswordsDontMatch",
                          }, 400)
        self.client.post( "/system/userManager/user/%s.changePassword.html" % ugid, 
                         {
                          'oldPwd' : password,
                          'newPwd' : "testPwd2",
                          'newPwdConfirm' : "PasswordsDontMatch",
                          }, 400)
        
        self.client.post( "/system/userManager/user/%s.changePassword.html" % ugid, 
                         {
                          'oldPwd' : "wrong Fist Password",
                          'newPwd' : "testPwd2",
                          'newPwdConfirm' : "testPwd2",
                          }, 403)
        
        
        self.client.post( "/system/userManager/user/%s.changePassword.html" % ugid, 
                         {
                          'oldPwd' : password,
                          'newPwd' : "testPwd2",
                          'newPwdConfirm' : "testPwd2",
                          })
        self.client.logout()
        # login with new password
        self.client.login(username=username, password="testPwd2")
        self.client.checkMe( username)
        self.client.logout()
        
    def test_userExists(self):
        self.client.loginAdmin()
        username, password = self.client.createUser() #@UnusedVariable
        self.client.logout()
        self.client.get("/system/userManager/user.exists.html?userid=%s" %  username, 204)
        self.client.get("/system/userManager/user.exists.html?userid=%s" % "this-missing-link",404)
        
    def test_userManagerUpdate(self):
        self.client.loginAdmin()
        username, password = self.client.createUser()
        self.client.logout()
        self.client.login(username=username, password=password)
        jsonMe = self.client.checkMe( username)
        ugid = jsonMe['userid']
        self.client.post( "/system/userManager/user/%s-doesnotexist.update.html" % ugid, 
                         {
                          'prop1' : "prop1Value",
                          'prop2@Delete' : "deleted",
                          'prop3[]' : "arrayValue",
                          'prop4' : "implicitArray1",
                          'prop4' : "implicitArray2",
                          'prop5@Integer' : 5,
                          'prop6@Double' : 5.5555,
                          'prop7@Boolean' : True,   
                          },403) # 403 is correct, we dont want to indicate a user doesnt exist or does exist.
        self.client.post( "/system/userManager/user/%s.update.html" % ugid, 
                         {
                          'prop1' : "prop1Value",
                          'prop2@Delete' : "deleted",
                          'prop3[]' : "arrayValue",
                          'prop4' : "implicitArray1",
                          'prop4' : "implicitArray2",
                          'prop5@Integer' : 5,
                          'prop6@Double' : 5.5555,
                          'prop7@Boolean' : True,   
                          })
        self.client.logout()
        
        
    def test_privateUserStore(self):
        self.client.loginAdmin()
        username, password = self.client.createUser()
        self.client.logout()
        self.client.login(username=username, password=password)
        jsonMe = self.client.checkMe( username)
        ugid = jsonMe['userid']
        self.client.post( "/~the-missing-link/private/bla/bla/bla", 
                         {
                          'prop1' : "prop1Value",
                          'prop2@Delete' : "deleted",
                          'prop3[]' : "arrayValue",
                          'prop4' : "implicitArray1",
                          'prop4' : "implicitArray2",
                          'prop5@Integer' : 5,
                          'prop6@Double' : 5.5555,
                          'prop7@Boolean' : True,   
                          },404) # 403 is correct, we dont want to indicate a user doesnt exist or does exist.
        
        self.client.post( "/~%s/private/create/this/location" % ugid, 
                         {
                          'prop1' : "prop1Value",
                          'prop2@Delete' : "deleted",
                          'prop3[]' : "arrayValue",
                          'prop4' : "implicitArray1",
                          'prop4' : "implicitArray2",
                          'prop5@Integer' : 5,
                          'prop6@Double' : 5.5555,
                          'prop7@Boolean' : True,   
                          },200) # 403 is correct, we dont want to indicate a user doesnt exist or does exist.

        self.client.get(  "/~%s/private/create/this/location.json" % ugid)
        self.client.get(  "/~%s/private/create.json" % ugid)
        self.client.logout()
        self.client.get(  "/~%s/private/create/this/location.json" % ugid, 404)
        self.client.get(  "/~%s/private/create.json" % ugid, 404)


    def test_publicUserStore(self):
        self.client.loginAdmin()
        username, password = self.client.createUser()
        self.client.logout()
        self.client.login(username=username, password=password)
        jsonMe = self.client.checkMe( username)
        ugid = jsonMe['userid']
        self.client.post( "/~the-missing-link/public/bla/bla/bla", 
                         {
                          'prop1' : "prop1Value",
                          'prop2@Delete' : "deleted",
                          'prop3[]' : "arrayValue",
                          'prop4' : "implicitArray1",
                          'prop4' : "implicitArray2",
                          'prop5@Integer' : 5,
                          'prop6@Double' : 5.5555,
                          'prop7@Boolean' : True,   
                          },403) # 403 is correct, we dont want to indicate a user doesnt exist or does exist.
        
        self.client.post( "/~%s/public/create/this/location" % ugid, 
                         {
                          'prop1' : "prop1Value",
                          'prop2@Delete' : "deleted",
                          'prop3[]' : "arrayValue",
                          'prop4' : "implicitArray1",
                          'prop4' : "implicitArray2",
                          'prop5@Integer' : 5,
                          'prop6@Double' : 5.5555,
                          'prop7@Boolean' : True,   
                          },200) # 403 is correct, we dont want to indicate a user doesnt exist or does exist.

        self.client.get(  "/~%s/public/create/this/location.json" % ugid)
        self.client.get(  "/~%s/public/create.json" % ugid)
        self.client.logout()
        self.client.get(  "/~%s/public/create/this/location.json" % ugid)
        self.client.get(  "/~%s/public/create.json" % ugid)
    
    
        