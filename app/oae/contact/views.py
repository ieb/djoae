'''
Created on Mar 20, 2012

@author: ieb
'''
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import models
from django.http import HttpResponse, HttpResponseNotAllowed, \
    HttpResponseBadRequest
from django.utils.datastructures import MultiValueDictKeyError
from oae.common.util import Search, JSON_CT, MultiEncoder
from oae.contact.contact_manager import ContactManager
from oae.contact.encoder import ConnectionEncoder
from oae.contact.models import Connection
import django.utils.simplejson as json
import logging
import traceback
from oae.tag.models import Tag
from oae.user.models import Profile
from django.views.decorators.cache import never_cache

JSON_INDENT, ALLOW_QUERY_BY_USERNAME= ( settings.JSON_INDENT, settings.ALLOW_QUERY_BY_USERNAME)

def _getUser(targetUserId):
    try:
        if ALLOW_QUERY_BY_USERNAME:
            return User.objects.get(models.Q(profile__ugid=targetUserId)|
                                          models.Q(username=targetUserId))
        else:
            return User.objects.get(models.Q(profile__ugid=targetUserId))
    except User.DoesNotExist:
        logging.error("User Does not exist  %s " % targetUserId)
        raise User.DoesNotExist

@login_required
@never_cache  # Cache at a lower level with invalidation.
def doFindState(request):
    '''
    Finds all connections for the current user with the requested state.
    output is in the normal Search response format eg {'total': 0, 'items' : 0, 'results' : []}
    Request parameters:
        state : the state to find, one of the keys in ContactManager.STATE_TYPES
        
        
    Where results is in the standard Connection object format eg 
    {
    "items": 1,
    "total": 1,
    "results": [
        {
            "profile": {
                "hash": "EEGVIKlEEeGscgAjMsoUZA",
                "rep:userId": "EEGVIKlEEeGscgAjMsoUZA",
                "aboutme": {
                    "privilege": "granted",
                    "init": "True",
                    "principalId": "g-contacts-EEGVIKlEEeGscgAjMsoUZA"
                },
                "userid": "EEGVIKlEEeGscgAjMsoUZA",
                "sakai:tags": [],
                "_lastModifiedBy": "EEGVIKlEEeGscgAjMsoUZA",
                "publications": {
                    "privilege": "granted",
                    "init": "True",
                    "principalId": "g-contacts-EEGVIKlEEeGscgAjMsoUZA"
                },
                "basic": {
                    "access": "everybody",
                    "init": "True",
                    "elements": {
                        "sakai:tags": {
                            "value": []
                        },
                        "lastName": {
                            "value": "Boston"
                        },
                        "email": {
                            "value": "susie@example.org"
                        },
                        "firstName": {
                            "value": "Susie"
                        }
                    }
                },
                "userprogress": {
                    "madeContactRequest": true
                },
                "counts": {
                    "membershipsCount": 0,
                    "contactsCount": 1,
                    "contentCount": 1
                },
                "email": "susie@example.org"
            },
            "target": "EEGVIKlEEeGscgAjMsoUZA",
            "details": {
                "_created": 1338555949000,
                "reference": "/~EEGVIKlEEeGscgAjMsoUZA/public/authprofile",
                "firstName": "Susie",
                "sakai:contactstorepath": "/~9swgBqlDEeGscgAjMsoUZA/contacts",
                "lastName": "Boston",
                "sakai:types": [
                    "__MSG__CLASSMATE__",
                    "__MSG__SUPERVISOR__",
                    "__MSG__LECTURER__"
                ],
                "sakai:state": "ACCEPTED",
                "_path": "/~9swgBqlDEeGscgAjMsoUZA/contacts/EEGVIKlEEeGscgAjMsoUZA"
            }
        }
      ]
    } 
    @param request:
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    try:
        state = ContactManager.STATE_TYPES[request.GET["state"]]
        # What we want here is out invites so thats
        q = Connection.objects.all().filter(owner=request.user,state=state).order_by("id")
        results = Search().dbSearch(request, Connection, q) 
        
        return HttpResponse(json.dumps(results,cls=MultiEncoder, encoder_classes=ConnectionEncoder.depend() ,indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)
    except KeyError:
        return HttpResponseBadRequest()
    
@login_required
@never_cache  # Cache at a lower level with invalidation.
def doFindContact(request):
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    try:
        q = Connection.objects.filter(
                        models.Q(owner=request.user))
        state = None
        if "state" in request.GET:
            state = ContactManager.STATE_TYPES[request.GET["state"]]
            q = q.filter(state=state)
        if 'q' in request.GET and request.GET['q'] != '*':
                sq = request.GET['q']
                q = q.filter(models.Q(connection__first_name__icontains=sq)|
                             models.Q(connection__last_name__icontains=sq))
        search = Search()   
        results = search.dbSearch(request, Connection, q.order_by(search.getSortOn(request)))
        
        return HttpResponse(json.dumps(results,cls=MultiEncoder, encoder_classes=ConnectionEncoder.depend(),indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)
    except KeyError:
        return HttpResponseBadRequest()
    
@login_required
@never_cache  # Cache at a lower level with invalidation.
def doRelatedContact(request):
    '''
    People with an overlapping set of tags that I have not contacted.
    @param request:
    '''
    if "GET" != request.method:
        return HttpResponseNotAllowed("GET")
    # My tags on profile objects
    tags = Tag.objects.filter(profile_tags__user=request.user)
    # profile objects with matching tags
    q1 = Profile.objects.filter(tags__in=tags).exclude(user__contacts__owner=request.user)
    # selected to avoid duplicates
    q = Profile.objects.filter(id__in=q1.values("id"))
    search = Search()
    results = search.dbSearch(request, Profile, q.order_by(search.getSortOn(request)).prefetch_related("group__usergroupdata"))
    return HttpResponse(json.dumps(results,cls=MultiEncoder, encoder_classes=ConnectionEncoder.depend(),indent=JSON_INDENT,ensure_ascii=False), content_type=JSON_CT)
    


@login_required
def doContactsInvite(request, ugid):
    '''
    fromRelationships:__MSG__CLASSMATE__
fromRelationships:__MSG__SHARES_INTERESTS__
fromRelationships:__MSG__CLASSMATE__
toRelationships:__MSG__SHARES_INTERESTS__
targetUserId:8jJzA8FtCHSFvrwHXe-RbN6L-sU
_charset_:utf-8
    @param request:
    @param ugid:
    '''
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    try:
        sourceUser = request.user
        targetUser = _getUser(request.POST['targetUserId'])
        toRelationships = ",".join(request.POST.getlist('toRelationships'))
        fromRelationships = ",".join(request.POST.getlist('fromRelationships'))
        contactManager = ContactManager(sourceUser)
        contactManager.load(targetUser, sourceState="P", targetState="I", fromRelationships=fromRelationships, toRelationships=toRelationships)
        return contactManager._processStates("PI")
    except MultiValueDictKeyError:
        return HttpResponseBadRequest()
    except:
        logging.error(traceback.format_exc())
        return HttpResponse(status=409) 


@login_required
def doContactsAccept(request, ugid):
    '''
    targetUserId:8jJzA8FtCHSFvrwHXe-RbN6L-sU
    @param request:
    @param ugid:
    '''
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    try:
        sourceUser = request.user
        targetUser = _getUser(request.POST['targetUserId'])
        contactManager = ContactManager(sourceUser)
        contactManager.load(targetUser)
        return contactManager._processStates("AA")
    except MultiValueDictKeyError:
        return HttpResponseBadRequest()
    except:
        logging.error(traceback.format_exc())
        return HttpResponse(status=409) 
    
    
def doContactsIgnore(request, ugid):
    '''
    targetUserId:8jJzA8FtCHSFvrwHXe-RbN6L-sU
    @param request:
    @param ugid:
    '''
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    try:
        sourceUser = request.user
        targetUser = _getUser(request.POST['targetUserId'])
        contactManager = ContactManager(sourceUser)
        contactManager.load(targetUser)
        return contactManager._processStates("G_")
    except MultiValueDictKeyError:
        return HttpResponseBadRequest()
    except:
        logging.error(traceback.format_exc())
        return HttpResponse(status=409) 


def doContactsRemove(request, ugid):
    '''
    targetUserId:8jJzA8FtCHSFvrwHXe-RbN6L-sU
    @param request:
    @param ugid:
    '''
    if "POST" != request.method:
        return HttpResponseNotAllowed("POST")
    try:
        sourceUser = request.user
        targetUser = _getUser(request.POST['targetUserId'])
        contactManager = ContactManager(sourceUser)
        contactManager.load(targetUser)
        return contactManager._processStates("!_")
    except MultiValueDictKeyError:
        return HttpResponseBadRequest()
    except:
        logging.error(traceback.format_exc())
        return HttpResponse(status=409) 
