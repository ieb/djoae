'''
Created on Mar 20, 2012

@author: ieb
'''
from oae.contact.models import Connection
import time
from oae.user.encoder import ProfileEncoder
import logging
import django.utils.simplejson as json

class ConnectionEncoder(json.JSONEncoder):
    '''
    
            "target": "johnfking2",
            "profile": {
                "hash": "johnfking2",
                "basic": {
                    "access": "everybody",
                    "elements": {
                        "lastName": {
                            "value": "King"
                        },
                        "email": {
                            "value": "johnk@media.berkeley.edu"
                        },
                        "firstName": {
                            "value": "John"
                        }
                    }
                },
                "rep:userId": "johnfking2",
                "userid": "johnfking2",
                "counts": {
                    "contactsCount": 23,
                    "membershipsCount": 1,
                    "contentCount": 4,
                    "countLastUpdate": 1332227608358
                },
                "sakai:excludeSearch": false,
                "homePath": "/~johnfking2"
            },

    '''
    STATE_TO_UI = {
        "A" : "ACCEPTED",
        "P" : "PENDING",
        "G" : "IGNORED",
        "I" : "INVITED",
        "B" : "BLOCKED",
        "F" : "FOLLOWING"}
    
    @staticmethod
    def depend():
        s = ProfileEncoder.depend()
        s.update([ConnectionEncoder])
        return s
    
    def getState(self, state):
        try:
            return ConnectionEncoder.STATE_TO_UI[state]
        except:
            logging.error("Unknown state of connection found %s " % state)
            return "UNKNOWN"
            
    def default(self, obj):
        if isinstance(obj, Connection):
            connection = obj.connection
            profile = obj.connection.profile
            connectionUgid = profile.ugid
            ownerUgid = obj.owner.profile.ugid
            return {
                'target' : connectionUgid,
                'profile' : profile,
                'details' : {                            
                    "lastName": connection.last_name,
                    "firstName": connection.first_name,
                    "sakai:types":  obj.relation.split(","),
                    "sakai:contactstorepath": "/~%s/contacts" % ownerUgid,
                    "reference": "/~%s/public/authprofile" % connectionUgid,
                    "_path": "/~%s/contacts/%s" % (ownerUgid,connectionUgid),
                    "sakai:state": self.getState(obj.state),
                    '_created' : int(time.mktime(obj.created.timetuple())*1000),
                    }
                }
        return super(ConnectionEncoder,self).default(obj)

