'''
Created on Mar 20, 2012

@author: ieb
'''
from django.test.testcases import TestCase
from django.utils import simplejson as json
from oae.common.tests import OAETestClient

class ContactsTest(TestCase):
    
    def setUp(self):
        self.client = OAETestClient(self)
        self.client.createAdmin()
        self.client.loginAdmin()
        self.un1, self.pw1 = self.client.createUser()
        self.un2, self.pw2 = self.client.createUser()
        self.client.logout()

        
    def testConnect(self):
        self.client.login(self.un1, self.pw1)
        meJson1 = self.client.checkMe(self.un1)
        userid1 = meJson1['userid']
        self.client.login(self.un2, self.pw2)
        meJson2 = self.client.checkMe(self.un2)
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=ACCEPTED").content)['items'],"Accepted should have been zero")
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=INVITED").content)['items'],"Invited should dhave been zero")
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=PENDING").content)['items'],"Pending shoudl have been zeor")
        userid2 = meJson2['userid']
        
        self.client.post("/~%s/contacts.invite.html" % userid2, 
                         {
                           "fromRelationships" : ["__MSG__CLASSMATE__","__MSG__SHARES_INTERESTS__","__MSG__CLASSMATE__" ],
                           "toRelationships" : "__MSG__SHARES_INTERESTS__",
                           "targetUserId": userid1
                          })
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=ACCEPTED").content)['items'],"Accepted should have been zero")
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=INVITED").content)['items'],"Invited should have been zero")
        self.assertEqual(1,json.loads(self.client.get("/var/contacts/findstate.json?state=PENDING").content)['items'],"Pending should have been pending")
        self.client.login(self.un1, self.pw1)
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=ACCEPTED").content)['items'],"Accepted should have been zero")
        self.assertEqual(1,json.loads(self.client.get("/var/contacts/findstate.json?state=INVITED").content)['items'],"Should have been invited")
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=PENDING").content)['items'],"Pending should have been zero")
        self.client.post("/~%s/contacts.accept.html" % userid1, 
                         {
                           "targetUserId": userid2
                          })
        self.assertEqual(1,json.loads(self.client.get("/var/contacts/findstate.json?state=ACCEPTED").content)['items'],"Not accepeted")
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=INVITED").content)['items'],"Invited should dhave been zero")
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=PENDING").content)['items'],"Pending shoudl have been zeor")
        self.client.login(self.un2, self.pw2)
        self.assertEqual(1,json.loads(self.client.get("/var/contacts/findstate.json?state=ACCEPTED").content)['items'],"Not accepeted")
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=INVITED").content)['items'],"Invited should dhave been zero")
        self.assertEqual(0,json.loads(self.client.get("/var/contacts/findstate.json?state=PENDING").content)['items'],"Pending shoudl have been zeor")
        