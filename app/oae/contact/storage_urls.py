from django.conf.urls.defaults import patterns, url

from oae.contact.views import doContactsInvite, doContactsAccept,\
    doContactsRemove, doContactsIgnore

'''
/~userid/contacts. or /~userid/contacts/ 
'''
urlpatterns = patterns('',
    url(r'^invite\.html$', doContactsInvite ),
    url(r'^remove\.html$', doContactsRemove ),
    url(r'^accept\.html$', doContactsAccept ),
    url(r'^ignore\.html$', doContactsIgnore ),
)