'''
Created on Mar 20, 2012

NB: Not change the schema without generating a migration.

@author: ieb
'''
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete
from django.db.models.aggregates import Count
from oae.user.models import Profile
import logging



class Connection(models.Model):
    '''
    For the user that makes the connection state goes through the following transitions
    pending->(accepted|rejected)
    accepted->blocked
    
    For the user receiving the connection
    invited->(accepted|rejected|ignored|blocked)
    accepted->blocked
    
    For any user
    accepted->following
    following->accepted
    
    '''
    class Meta:
        unique_together = (("owner", "connection"),)
    
    STATE_CHOICES = (
      ('I', 'Invited'),
      ('P', 'Pending'),
      ('A', 'Accepted'),
      ('R', 'Rejected'),
      ('G', 'Ignored'),
      ('B', 'Blocked'),
      ('F', 'Following'),
             )
    owner = models.ForeignKey(User,related_name="othersContacts",verbose_name="From User", help_text="This identifies the user that owns this connection")
    connection = models.ForeignKey(User,related_name="contacts", verbose_name="To User", help_text="This identifies the user that this connection connects to")
    state = models.CharField(
                max_length=1, 
                db_index=True, 
                choices=STATE_CHOICES,
                verbose_name="State of the connection", 
                help_text="The state of this end of the connection")
    relation = models.CharField(max_length=512)
    created = models.DateTimeField(
                auto_now_add=True,
                verbose_name="Time the connection was created",
                help_text="Time the connection item was created")
    modified = models.DateTimeField(
                auto_now_add=True,
                verbose_name="Time the connection was updated",
                help_text="Time the connection item was updated")
    def getStateName(self):
        for c,name in Connection.STATE_CHOICES:
            if c == self.state:
                return name
        return "Undefined"
    
    def __str__(self):
        return unicode(self)
            
    def __unicode__(self):
            return "%s->%s %s" % (self.owner.username, self.connection.username, self.getStateName())


class ConnectionStats(object):
    @staticmethod
    def syncFromConnection(sender, instance=None, created=False, **kwargs):
        if instance is not None:
            vals = Connection.objects.filter(owner=instance.owner).values("state").annotate(Count("state"))
            logging.debug("Got contact state for %s as %s " % (instance.owner.username, vals))
            profile = Profile.objects.get(user=instance.owner)
            m = {'I':0,'P':0,'A':0,'B':0,'R':0,'G':0,'F':0}
            for v in vals:
                m[v['state']] = v['state__count']
            logging.debug("States %s " % m)
            profile.contact_sent = m['I']
            profile.contact_recieved = m['P']
            profile.contact_accepted = m['A']
            profile.contact_blocked = m['B']
            profile.contact_rejected = m['R']
            profile.contact_ignored = m['G']
            profile.contact_following = m['F']
                            
            profile.save()
        else:
            logging.debug("Instance was none")
                
        
post_save.connect(ConnectionStats.syncFromConnection, sender=Connection)
post_delete.connect(ConnectionStats.syncFromConnection, sender=Connection)
