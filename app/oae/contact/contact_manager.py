'''
Created on May 7, 2012

@author: ieb
'''
import logging
from oae.common.util import Response
from django.http import HttpResponse, HttpResponseBadRequest
from oae.contact.models import Connection
from oae.common.xact import xact
class ContactManager(object):
    INVITED = "INVITED"
    PENDING = "PENDING"
    ACCEPTED = "ACCEPTED"
    REJECTED = "REJECTED"
    IGNORED = "IGNORED"
    BLOCKED = "BLOCKED"
    FOLLOWING = "FOLLOWING"
    DELETE = "DELETE"
    ANY = "ANY"
    
    STATE_TYPES = {
        INVITED : "I",
        PENDING : "P",
        ACCEPTED : "A",
        REJECTED : "R",
        IGNORED : "G",
        BLOCKED : "B",
        FOLLOWING : "F"
        }
    ADD_STATE_TYPES = {
        INVITED : "I",
        PENDING : "P",
        ACCEPTED : "A",
        REJECTED : "R",
        IGNORED : "G",
        BLOCKED : "B",
        FOLLOWING : "F",
        DELETE : "!",
        ANY : "_"
        }
    
    
    
    
    CONNECTION_STATE_MACHINE = (
        ("PI", "P",  "I",  None, None, 200, "Ok"),
        ("PI", None, "B",  None, None, 403,"Blocked by target"),
        ("PI", "I",  "P",  "A", "A", 200, "Accepted"),
        ("PI", None, "A",  "A", "A", 200, "Accepted, target already accepted"),
        ("PI", None, "F",  "A", "A", 200, "Accepted, target already following"),
        ("PI", None, "R",  "P", None, 200, "Target rejected mark as pending but dont retry"),
        ("PI", None, "G",  "P", None, 200, "Target ignored mark as pending but dont retry"),
        ("PI", "A", None,  None, None, 409, "Already Connected"),
        ("PI", "F", None,  None, None, 409, "Already Connected"),
        ("PI", None, None, "P", "I", 200, "Default Try again"),
        ("AA", "A", "A",  None, None, 200, "Already Connected"),
        ("AA", "F", "A",  None, None, 200, "Already Connected"),
        ("AA", "A", "F",  None, None, 200, "Already Connected"),
        ("AA", "I", "P",  "A", "A", 200, "Connected"),
        ("AA", "I", "P",  "A", "A", 200, "Connected"),
        ("AA", None, "B",  None, None, 403, "Blocked by target"),
        ("AA", None, "R",  None, None, 200, "Rejected by target"),
        ("AA", None, None, "A", None, 200, "Default Connect"),
        ("F_", "A", None, "F", None, 200, "Following"),
        ("G_", None, None, "G", None, 200, "Ignored"),
        ("B_", None, None, "B", None, 200, "Ignored"),
        ("R_", "I", "P", "R", "R", 200, "Reject"),
        ("!_", "I", "P", "!", "!", 200, "Removed"),
        ("!_", "P", "I", "!", "!", 200, "Removed"),
        ("!_", None, None, "!", None, 200, "Removed"),
    )

    def __init__(self, sourceUser):
        self.sourceUser = sourceUser
        
    
    @xact
    def processStates(self, sourceDesired, targetDesired):
        desired = ContactManager.ALL_STATE_TYPES[sourceDesired]+ContactManager.ALL_STATE_TYPES[targetDesired]
        return self._processStates(desired)
    
    def _processStates(self, desired):
        for (c,cs, ct, ss,ts,resp, cause) in ContactManager.CONNECTION_STATE_MACHINE:        
            if desired == c:
                logging.debug("Testing %s %s %s %s %s %s %s against %s %s  " % (c,cs, ct, ss,ts,resp, cause, self.source, self.target))
                if ((cs is None  or (self.source is not None and cs == self.source.state)) 
                        and (ct is None or (self.target is not None and ct == self.target.state))):
                    saveSource = self.source is not None
                    saveTarget = self.target is not None
                    if ss == "!":
                        if saveSource:
                            self.source.delete()
                            saveSource = False
                    elif ss is not None:
                        self.source.state = ss
                    
                    if ts == "!":
                        if saveTarget:
                            self.target.delete()
                            saveTarget = False
                    if ts is not None:
                        self.target.state = ts
                        
                    if resp == 200:
                        if saveSource:
                            self.source.save()
                        if saveTarget:
                            self.target.save()
                        return Response.Ok()
                    return HttpResponse(cause, status=resp)
        logging.error("Unable to process %s %s %s " % ( desired, self.source, self.target))
        return HttpResponseBadRequest()
    
    def load(self, targetUser, sourceState=None, targetState=None, fromRelationships=None, toRelationships=None):
        try:
            self.source = Connection.objects.get(owner=self.sourceUser,connection=targetUser)
        except Connection.DoesNotExist:
            if sourceState is None:
                self.source = None
            else:
                self.source = Connection(
                                    owner=self.sourceUser,
                                    connection=targetUser,
                                    state=sourceState,
                                    relation=fromRelationships)
        try:
            self.target = Connection.objects.get(owner=targetUser, connection=self.sourceUser)
        except Connection.DoesNotExist:
            if sourceState is None:
                self.target = None
            else:
                self.target = Connection(
                                owner=targetUser,
                                connection=self.sourceUser,
                                state="I",
                                relation=toRelationships)
        return True
