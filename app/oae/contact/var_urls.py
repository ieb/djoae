'''
Created on Mar 20, 2012

@author: ieb
'''
from django.conf.urls.defaults import patterns, url
from oae.contact.views import doFindState, doFindContact, doRelatedContact


'''
 base of /var/contacts
'''
urlpatterns = patterns('',
    url(r'^findstate\.infinity\.json$', doFindState),
    url(r'^findstate\.json$', doFindState),
    url(r'^find\.json$', doFindContact),
    url(r'^find-all\.json$', doFindContact),
    url(r'^related-contacts\.json$', doRelatedContact),
)